package com.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderAdvSearchElement {
	
	//Order Number Search Text Field
	@FindBy(xpath = "//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[1]/input")
	private WebElement OrderNumber;
	
	//Source ID Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[2]/input")
	private WebElement SourceID;
	
	//Vendor Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[3]/input")
	private WebElement Vendor;
	
	//DAC Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[4]/input")
	public WebElement DAC;
	
	//Project Number Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[6]/input")
	private WebElement ProjectNumber;
	
	//Contact Details Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[7]/input")
	private WebElement ContactDetails;
	
	//Item ID Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[2]/input")
	private WebElement ItemID;
		
	//Item Description Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[3]/input")
	public WebElement ItemDesc;
		
	//Cost Center Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[4]/input") 
	public WebElement CostCenter;
		
	//Priority Code Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[11]/input")
	public WebElement PriorityCode;
	                
	
	//Mrp Controller Search Text Field
	@FindBy(id = "mrpcontroller")
	public WebElement MrpController;
			
	//Adv Search Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/div[1]/div/button")
	public WebElement AdvSearch;
	
	//Clear Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/h4/span/button")
	private WebElement Clear;
	
    //View Order Details
	@FindBy(xpath = "//a[contains(@href, 'orderNo=Y')]")
	public WebElement OrderDetail;
	
	//Order List
	@FindBy(xpath = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[2]/a")
	public WebElement OrderList;
	
	//Order Line
	@FindBy(xpath = ".//*[@id='orderlines']/tbody/tr[2]/td[2]/a")
	public WebElement OrderLine;
	
	//ShowMore/ShowLess Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[12]/a")
	private WebElement Show;
	
	//Search Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[11]/button")
	private WebElement Search;
	
	//Maximum Record
		@FindBy(id = "maxrecords")
		public WebElement MaxRecord;
		
		//From DatePicker
		@FindBy(xpath = ".//*[@id='FromReqShipDate']")
		public WebElement FromReqShipDate;
	
		//To DatePicker
		@FindBy(xpath = ".//*[@id='ToReqShipDate']")
		public WebElement ToReqShipDate;
			
	// Initializing the Objects
	public OrderAdvSearchElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	//Send values to Order number Text 
	public void OrderSearch(String ON) {
		OrderNumber.sendKeys(ON);
	}
	
	//Send values to SourceID Text 
	public void SourceSearch(String SID) {
		SourceID.sendKeys(SID);
	}
	
	//Send values to Vendor Text 
	public void VendorSearch(String VID) {
		Vendor.sendKeys(VID);
	}
		
	//Send values to DAC Text 
	public void DACSearch(String Dac) {
		DAC.sendKeys(Dac);
	}
	
	//Send values to Project number Text 
	public void ProjectSearch(String PN) {
		ProjectNumber.sendKeys(PN);
	}
		
	//Send values to Contact Details Text 
	public void ContactSearch(String CD) {
		ContactDetails.sendKeys(CD);
	}
		
	//Send values to Item ID Text 
	public void ItemSearch(String IID) {
		ItemID.sendKeys(IID);
	}
			
	//Send values to Item Description Text 
	public void ItemDescSearch(String ID) {
		ItemDesc.sendKeys(ID);
	}
	
	//Send values to Cost Center Text 
	public void CostCenterSearch(String CC) {
		CostCenter.sendKeys(CC);
	}
			
	//Send values to Priority Code Text 
	public void PriorityCodeSearch(String PC) {
		PriorityCode.sendKeys(PC);
	}
				
	//Send values to Mrp Controller Text 
	public void MrpControllerSearch(String MRP) {
		MrpController.sendKeys(MRP);
	}
	
    // Click on Adv Search button
	public void clickAdvSearchButton(){
		AdvSearch.click();
	}
	
	// Click on Clear button
	public void clickClearButton(){
		Clear.click();
	}
	
	// Click on Order Details
	public void clickOrderDetails(){
		OrderDetail.click();
	}
	
	// Click on Order List
		public void clickOrderList(){
			OrderList.click();
		}
	
	// Click on Order List
		public void clickOrderLine(){
			OrderLine.click();
		}
		
	// Click on Show button
	public void clickShowButton() {
		Show.click();
	}
		
	// Click on Search button
	public void clickSearchButton() {
		Search.click();
	}
	
	// Click on MaxRecord text
		public void MaxRecordSearch(String MX) {
			MaxRecord.clear();
			MaxRecord.sendKeys(MX);
		}
		
	// Click on Date Picker
	public void FromReqShipDate() {
		FromReqShipDate.click();
	}
	
	// Click on Date Picker
		public void ToReqShipDate(String Date) {
			ToReqShipDate.sendKeys(Date);
		}
}