package com.automation;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
 

public class MrpController  {

            public WebDriver driver;
            
            @BeforeClass
        	public void beforeMethod() throws InterruptedException {

        		// initialize the FireFox driver
        		System.setProperty("webdriver.gecko.driver",
        				"C:/geckodriver/geckodriver.exe");
        		driver = new FirefoxDriver();
        		driver.get("http://146.89.240.252:9080/hubble-sp/login.jsp");
        		driver.manage().window().maximize();
        		Thread.sleep(2000);
        	}

            @Parameters({ "username", "password", "pagetitle" })
        	@Test
        	
        	public void loginSP(String userName, String password, String pageTitle) throws InterruptedException
            {
        		System.out.println("USERNAME:" + userName);
        		System.out.println("USERNAME1:" + password);
        		System.out.println("USERNAME2:" + pageTitle);
        		LoginPage login = new LoginPage(driver);
        		login.Login(userName, password);
        		Thread.sleep(500);
        		  // Submitting the data by clicking on login button
        		LoginPage clckLoginBtn = new LoginPage(driver);
        		clckLoginBtn.clickLoginBtn();
        		Thread.sleep(10000);
        		System.out.println("Order header");
        		WebElement ordericon=driver.findElement(By.xpath("//a[contains(@href, '#!/orderlist')]"));
        		ordericon.click();
        		System.out.println("Order clicked:::" + ordericon);	
        		System.out.println("Order header is clicked");
        		Thread.sleep(3000);
            }  
        	
              		
        		
        		@Test(dataProvider = "MrpController")
        		public void verifyMrpController(String MC, String pageTitle) throws InterruptedException {
        			String Actualtext = "";
        			OrderAdvSearchElement mc = new OrderAdvSearchElement(driver);
        			if(!mc.MrpController.isDisplayed()){
        				OrderAdvSearchElement clickShowButton = new OrderAdvSearchElement(driver);
            			clickShowButton.clickShowButton();
            			System.out.println("Clicked show more button");
            			Thread.sleep(5000);
        			}        			
        			OrderAdvSearchElement MrpController = new OrderAdvSearchElement(driver);
        			MrpController.MrpControllerSearch(MC);
        			System.out.println("Inside the method to verify order search");
        			//search button clicked
        			OrderAdvSearchElement clickSearchButton = new OrderAdvSearchElement(driver);
        			clickSearchButton.clickSearchButton();
        			System.out.println("Clicked search button");
        			Thread.sleep(5000);
        			OrderAdvSearchElement clickOrderDetails = new OrderAdvSearchElement(driver);
        			clickOrderDetails.clickOrderDetails();
        			//driver.findElement(By.xpath("//a[contains(@href, 'orderNo=Y')]")).click();
        			Thread.sleep(10000);
        			//driver.findElement(By.xpath(".//*[@id='orderlines']/tbody/tr[2]/td[2]/a")).click();
        			OrderAdvSearchElement clickOrderLine = new OrderAdvSearchElement(driver);
        			clickOrderLine.clickOrderLine();
        		    Thread.sleep(3000);
        			Actualtext = driver.findElement(By.xpath(".//*[@id='orderDetailsLink']/div/div/div/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div/span")).getAttribute("innerText");
        			System.out.println(Actualtext);
        			System.out.println(pageTitle);
        			
        			if (Actualtext.equals(pageTitle)){
        		          System.out.println("Test for MRP Controller Search Passed!");
        		      } else {
        		          System.out.println("Test for MRP Controller Search Failed");
        		      }
        			//Assert.assertEquals(Actualtext, CC);
        			Thread.sleep(2000);
        			//navigate to Order List
        			OrderAdvSearchElement clickOrderList = new OrderAdvSearchElement(driver);
        			clickOrderList.clickOrderList();
        			System.out.println("Clicked Order List button");
        			Thread.sleep(5000);
        			//navigate to ADV Search
        			OrderAdvSearchElement clickAdvSearchButton = new OrderAdvSearchElement(driver);
        			clickAdvSearchButton.clickAdvSearchButton();
        			System.out.println("Clicked Adv search button");
        			Thread.sleep(5000);
        			//clear button clicked
        			OrderAdvSearchElement clickClearButton = new OrderAdvSearchElement(driver);
        			clickClearButton.clickClearButton();
        			System.out.println("Clicked clear button");
        			Thread.sleep(5000);
        			      		      			        			
        		}
        		
        		@DataProvider(name = "MrpController")
        		public String[][] getExcelData1() throws IOException, Exception {
        			String xl = "C:/Users/Karthik/Desktop/MrpController.xlsx";
        			String sheet = "Sheet1";
        			int rowCount = ExcelUtil.getRowCount(xl, sheet);
        			Sheet sheetDetail = ExcelUtil.getSheet(xl, sheet);
        			String MrpControllerData[][] = null;
        			int colCount = sheetDetail.getRow(0).getLastCellNum();
        			MrpControllerData = new String[rowCount][colCount];
        			int count = 0;
        			for (int i = 1; i <= rowCount; i++) {
        				System.out.println("CountOfRows:" + rowCount);

        				System.out.println("CountOfColumns:" + rowCount);

        				for (int j = 0; j < colCount; j++) {
        					MrpControllerData[count][j] = ExcelUtil.getCellValue(xl, sheet, i, j);
        				}
        				count++;
        			}
        			System.out.println(Arrays.deepToString(MrpControllerData));
        			return MrpControllerData;
        		}
        		        		
        }
