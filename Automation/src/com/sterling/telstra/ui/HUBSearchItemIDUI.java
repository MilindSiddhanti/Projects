package com.sterling.telstra.ui;

import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sterling.telstra.util.HUBExcelUtil;
import com.sterling.telstra.util.HUBOrderUtil;

import org.testng.annotations.BeforeClass;


public class HUBSearchItemIDUI {
	public WebDriver driver;

	@BeforeClass
	public void beforeMethod() throws InterruptedException {

		// initialize the Firefox driver
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Milind.Siddhanti\\Downloads\\geckodriver-v0.11.1-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://146.89.240.252:9080/hubble-sp/login.jsp");
		driver.manage().window().maximize();
		Thread.sleep(2000);
	}

	@Parameters({ "username", "password", "pagetitle" })
	@Test
	
	public void loginSP(String userName, String password, String pageTitle) throws InterruptedException {
		System.out.println("Username:" + userName);
		System.out.println("Password:" + password);
		System.out.println("Header:" + pageTitle);
		LoginPage login = new LoginPage(driver);
		login.Login(userName, password);
		Thread.sleep(500);
		// Submitting the data by clicking on login button
		LoginPage clckLoginBtn = new LoginPage(driver);
		clckLoginBtn.clickLoginBtn();
		Thread.sleep(4000);
		System.out.println("Order header");
		WebElement ordericon=driver.findElement(By.xpath("//a[contains(@href, '#!/orderlist')]"));
		ordericon.click();
		System.out.println("Order clicked:::" + ordericon);	
		System.out.println("Order header is clicked");
		Thread.sleep(3000);
	     
	}
	

	@Test(dataProvider = "ItemID")
	public void verifyOrderSearchUsingItemId(String itemid, String pageTitle) throws InterruptedException {
		Thread.sleep(5000);
		String Actualtext = "";
		Actualtext = driver.findElement(By.xpath(".//*[@id='app']/div[2]/div[2]/div[1]/h5")).getText();
		Assert.assertEquals(Actualtext, pageTitle);
		HUBOrderUtil Item = new HUBOrderUtil(driver);
		if(!Item.ItemID.isDisplayed()){
			//Click on show more button
			HUBOrderUtil clickShowButton = new HUBOrderUtil(driver);
			clickShowButton.clickShowButton();
			Thread.sleep(5000);
		}		
		//Entering the item id
		HUBOrderUtil ItemId = new HUBOrderUtil(driver);
		ItemId.ItemSearch(itemid);
		Thread.sleep(5000);
		//scroll down to click the search button
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scroll(0, 250);");
		Thread.sleep(2000);
		// Submitting the data by clicking on search button
		HUBOrderUtil clickSearchButton = new HUBOrderUtil(driver);
		clickSearchButton.clickSearchButton();
		System.out.println("Search button clicked");
		Thread.sleep(5000);
		HUBOrderUtil NDF = new HUBOrderUtil(driver);
		if(NDF.NoDataFound.isDisplayed()){
			System.out.println("No records found::::" + NDF);
			//Click Advanced search button
			HUBOrderUtil clckAdvSearchBtn = new HUBOrderUtil(driver);
			clckAdvSearchBtn.clickAdvSearchButton();
			System.out.println("Button clicked");
			Thread.sleep(5000);
			//Click Clear Button
			HUBOrderUtil clckclearBtn = new HUBOrderUtil(driver);
			clckclearBtn.clickClearButton();
			System.out.println("Entry cleared");
			Thread.sleep(5000);
		} else {
			HUBOrderUtil clickOrderLink = new HUBOrderUtil(driver);
			clickOrderLink.clickOrderLink();
			System.out.println("Order Number hyperlink is clicked");
			Thread.sleep(5000);
//			String ItemIdText = driver.findElement(By.xpath(".//*[@id='orderlines']/tbody/tr[2]/td[3]")).getText();
//			System.out.println("ItemIdText:"+ItemIdText);
//			Assert.assertEquals(Actualtext, itemid);
			Thread.sleep(5000);
			//use bread crumbs to come back to list page
			driver.navigate().back();
//			HUBOrderUtils clckOrderListBreadCrumbs = new HUBOrderUtils(driver);
//			clckOrderListBreadCrumbs.clickOrdersBreadCrumbs();
			System.out.println("Navigated to the order list page from the order details page");
			Thread.sleep(5000);
			//ADV Search clicked
			HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
			clickAdvSearchButton.clickAdvSearchButton();
			System.out.println("Clicked Adv search button");
			Thread.sleep(5000);
			//clear button clicked
			HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
			clickClearButton.clickClearButton();
			System.out.println("Clicked clear button");
			Thread.sleep(5000);	
			}
		}
	
	@DataProvider(name = "ItemID")
	public String[][] getExcelData() throws IOException, Exception {
		String xl = "C://Users//Milind.Siddhanti//Desktop//Telstra//orderNumberSearch.xlsx";
		String sheet = "ItemId";
		int rowCount = HUBExcelUtil.getRowCount(xl, sheet);
		Sheet sheetDetail = HUBExcelUtil.getSheet(xl, sheet);
		String itemIDTestData[][] = null;
		int colCount = sheetDetail.getRow(0).getLastCellNum();
		itemIDTestData = new String[rowCount][colCount];
		int count = 0;
		for (int i = 1; i <= rowCount; i++) {
			System.out.println("CountOfRows:" + rowCount);

			System.out.println("CountOfColumns:" + rowCount);
			
			for (int j = 0; j < colCount; j++) {
				itemIDTestData[count][j] = HUBExcelUtil.getCellValue(xl, sheet, i,
						j);
			}
			count++;
		}
		System.out.println(Arrays.deepToString(itemIDTestData));
		return itemIDTestData;
	}
}
