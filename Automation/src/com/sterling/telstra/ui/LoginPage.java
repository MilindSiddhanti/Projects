package com.sterling.telstra.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	// Locating Login button
	@FindBy(id = "LoginID")
	private WebElement userName;

	// Locating password Text box
	@FindBy(id = "Password")
	private WebElement password;

	// Locating Login button
	@FindBy(id = "login-submit")
	private WebElement loginBtn;

	// Initializing the Objects
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void Login(String UN, String PWD) {
		userName.sendKeys(UN);
		password.sendKeys(PWD);
	}

	// Clicking on Login button
	public void clickLoginBtn() {
		loginBtn.click();
	}
}
