package com.sterling.telstra.ui;

import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sterling.telstra.util.HUBExcelUtil;
import com.sterling.telstra.util.HUBOrderUtil;

import org.testng.annotations.BeforeClass;

public class HUBSearchOrderDateUI {
	public WebDriver driver;
	    
	@BeforeClass
	public void beforeMethod() throws Exception {
		
		// initialize the Firefox driver
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Milind.Siddhanti\\Downloads\\geckodriver-v0.11.1-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://146.89.240.252:9080/hubble-sp/login.jsp");
		driver.manage().window().maximize();
		Thread.sleep(2000);			
		
	}
	
	@Parameters({ "username", "password", "pagetitle" })
	@Test
	
	public void loginSP(String userName, String password, String pageTitle) throws InterruptedException
    {
		System.out.println("Username:" + userName);
		System.out.println("Password:" + password);
		System.out.println("Header:" + pageTitle);
		LoginPage login = new LoginPage(driver);
		login.Login(userName, password);
		Thread.sleep(500);
		// Submitting the data by clicking on login button
		LoginPage clckLoginBtn = new LoginPage(driver);
		clckLoginBtn.clickLoginBtn();
		Thread.sleep(4000);
		System.out.println("Order header");
		WebElement ordericon=driver.findElement(By.xpath("//a[contains(@href, '#!/orderlist')]"));
		ordericon.click();
		System.out.println("Order clicked:::" + ordericon);	
		System.out.println("Order header is clicked");
		Thread.sleep(3000);
    }  
	
	  @Test(dataProvider = "OrderDate")
		public void verifyOrderSearch(String FromOrderDate, String ToOrderDate, String pageTitle) throws InterruptedException {
			String Actualtext = "";
			Actualtext = driver.findElement(By.xpath(".//*[@id='app']/div[2]/div[2]/div[1]/h5")).getText();
			Assert.assertEquals(Actualtext, pageTitle);
			System.out.println("Actual Header::" + Actualtext);
			System.out.println("Header fetched from the excel--" + pageTitle);
			System.out.println("From order date==" + FromOrderDate);
			System.out.println("To order date==" + ToOrderDate);
			Thread.sleep(1000);
			HUBOrderUtil Date = new HUBOrderUtil(driver);
			if(!Date.FromOrderDate.isDisplayed()){
				HUBOrderUtil clickShowButton = new HUBOrderUtil(driver);
				clickShowButton.clickShowButton();
				System.out.println("Clicked show more button");
				Thread.sleep(5000);
	        }
			HUBOrderUtil fromdate = new HUBOrderUtil(driver);
			fromdate.clickFromOrderDate(FromOrderDate);
			Thread.sleep(5000);
			HUBOrderUtil todate = new HUBOrderUtil(driver);
			todate.clickToOrderDate(ToOrderDate);
			Thread.sleep(5000);
			//handle error condition for from and to date
			HUBOrderUtil errorFromDate = new HUBOrderUtil(driver);
			HUBOrderUtil errorToDate = new HUBOrderUtil(driver);
			if(errorFromDate.ErrorFromDate.isDisplayed()){
				fromdate.FromOrderDate.clear();
				todate.ToOrderDate.clear();
				//clear button clicked
				HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
				clickClearButton.clickClearButton();
				System.out.println("Clicked clear button");
				Thread.sleep(5000);
			} else if(errorToDate.ErrorToDate.isDisplayed()){
				fromdate.FromOrderDate.clear();
				todate.ToOrderDate.clear();
				//clear button clicked
				HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
				clickClearButton.clickClearButton();
				System.out.println("Clicked clear button");
				Thread.sleep(5000);
			} else {
				System.out.println("Inside the method to verify receiving node search");
				//scroll down to click the search button
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("scroll(0, 250);");
				Thread.sleep(5000);
				//search button clicked
				HUBOrderUtil clickSearchButton = new HUBOrderUtil(driver);
				clickSearchButton.clickSearchButton();
				System.out.println("Clicked search button");
				Thread.sleep(5000);
				//Navigate to Order Details
				HUBOrderUtil NDF = new HUBOrderUtil(driver);
				if(NDF.NoDataFound.isDisplayed()){
					//ADV Search clicked
					HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
					clickAdvSearchButton.clickAdvSearchButton();
					System.out.println("Clicked Adv search button");
					Thread.sleep(5000);
					//clear button clicked
					HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
					clickClearButton.clickClearButton();
					System.out.println("Clicked clear button");
					Thread.sleep(5000);	
				} else {
					HUBOrderUtil clickOrderLink = new HUBOrderUtil(driver);
					clickOrderLink.clickOrderLink();
					System.out.println("Clicked order link");
					Thread.sleep(5000);
					//Validate Order Details
					System.out.println("Order Details");			
					String OrderDateText;
					OrderDateText = driver.findElement(By.xpath(".//*[@id='app']/div[2]/div[3]/div[2]/div/div/div[2]/div/ul/div[4]/li[1]/span")).getText();
					Assert.assertEquals(OrderDateText, FromOrderDate);
					System.out.println("OrderDateText::" + OrderDateText);
					System.out.println("From Order Date fetched from the excel--" + FromOrderDate);
					Thread.sleep(5000);
					//Navigate back to Order List			
					driver.navigate().back();
					System.out.println("Navigate back to Order List");
					//ADV Search clicked
					HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
					clickAdvSearchButton.clickAdvSearchButton();
					System.out.println("Clicked Adv search button");
					Thread.sleep(5000);
					//clear button clicked
					HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
					clickClearButton.clickClearButton();
					System.out.println("Clicked clear button");
					Thread.sleep(5000);
				}	
			}						
		}
 
		@DataProvider(name = "OrderDate")
		public String[][] getExcelData1() throws IOException, Exception {
			String xl = "C://Users//Milind.Siddhanti//Desktop//Telstra//orderNumberSearch.xlsx";
			String sheet = "OrderDate";
			int rowCount = HUBExcelUtil.getRowCount(xl, sheet);
			Sheet sheetDetail = HUBExcelUtil.getSheet(xl, sheet);
			String orderDateTestData[][] = null;
			int colCount = sheetDetail.getRow(0).getLastCellNum();
			orderDateTestData = new String[rowCount][colCount];
			int count = 0;
			for (int i = 1; i <= rowCount; i++) {
				System.out.println("CountOfRows:" + rowCount);

				System.out.println("CountOfColumns:" + rowCount);

				for (int j = 0; j < colCount; j++) {
					orderDateTestData[count][j] = HUBExcelUtil.getCellValue(xl, sheet, i, j);
				}
				count++;
			}
			System.out.println(Arrays.deepToString(orderDateTestData));
			return orderDateTestData;
		}
}
