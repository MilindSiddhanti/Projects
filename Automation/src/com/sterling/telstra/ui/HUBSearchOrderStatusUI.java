package com.sterling.telstra.ui;

import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sterling.telstra.util.HUBExcelUtil;
import com.sterling.telstra.util.HUBOrderUtil;

import org.testng.annotations.BeforeClass;

public class HUBSearchOrderStatusUI {
	public WebDriver driver;
	    
	@BeforeClass
	public void beforeMethod() throws Exception {
		
		// initialize the Firefox driver
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Milind.Siddhanti\\Downloads\\geckodriver-v0.11.1-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://146.89.240.252:9080/hubble-sp/login.jsp");
		driver.manage().window().maximize();
		Thread.sleep(2000);			
		
	}
	
	@Parameters({ "username", "password", "pagetitle" })
	@Test
	
	public void loginSP(String userName, String password, String pageTitle) throws InterruptedException
    {
		System.out.println("Username:" + userName);
		System.out.println("Password:" + password);
		System.out.println("Header:" + pageTitle);
		LoginPage login = new LoginPage(driver);
		login.Login(userName, password);
		Thread.sleep(500);
		// Submitting the data by clicking on login button
		LoginPage clckLoginBtn = new LoginPage(driver);
		clckLoginBtn.clickLoginBtn();
		Thread.sleep(4000);
		System.out.println("Order header");
		WebElement ordericon=driver.findElement(By.xpath("//a[contains(@href, '#!/orderlist')]"));
		ordericon.click();
		System.out.println("Order clicked:::" + ordericon);	
		System.out.println("Order header is clicked");
		Thread.sleep(3000);
    }  
	
	  @Test(dataProvider = "OrderStatus")
		public void verifyOrderSearch(String StatusFrom, String StatusTo, String pageTitle) throws InterruptedException {
			String Actualtext = "";
			Actualtext = driver.findElement(By.xpath(".//*[@id='app']/div[2]/div[2]/div[1]/h5")).getText();
			Assert.assertEquals(Actualtext, pageTitle);
			System.out.println("Actual Header::" + Actualtext);
			System.out.println("Header fetched from the excel--" + pageTitle);
			Thread.sleep(1000);
			HUBOrderUtil Status = new HUBOrderUtil(driver);
			if(!Status.StatusFrom.isDisplayed()){
				HUBOrderUtil clickShowButton = new HUBOrderUtil(driver);
				clickShowButton.clickShowButton();
				System.out.println("Clicked show more button");
				Thread.sleep(5000);
	        }
			HUBOrderUtil clickStatusFrom = new HUBOrderUtil(driver);
			clickStatusFrom.clickStatusFrom();
			System.out.println("Clicked Status From Select Box");
			Thread.sleep(5000);
			HUBOrderUtil statusFrom = new HUBOrderUtil(driver);
			statusFrom.StatusFromSearch(StatusFrom);
			Thread.sleep(5000);
			HUBOrderUtil clickStatusTo = new HUBOrderUtil(driver);
			clickStatusTo.clickStatusTo();
			System.out.println("Clicked Status To Select Box");
			Thread.sleep(5000);
			HUBOrderUtil statusTo = new HUBOrderUtil(driver);
			statusTo.StatusToSearch(StatusTo);
			Thread.sleep(5000);
			System.out.println("Inside the method to verify order status search");
			//scroll down to click the search button
			HUBOrderUtil search = new HUBOrderUtil(driver);
			if(!search.Search.isDisplayed()){
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("scroll(0, 250);");
				Thread.sleep(5000);
			}			
			//search button clicked
			HUBOrderUtil clickSearchButton = new HUBOrderUtil(driver);
			clickSearchButton.clickSearchButton();
			System.out.println("Clicked search button");
			Thread.sleep(5000);
			//Navigate to Order Details
			HUBOrderUtil NDF = new HUBOrderUtil(driver);
			if(NDF.NoDataFound.isDisplayed()){
				//ADV Search clicked
				HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
				clickAdvSearchButton.clickAdvSearchButton();
				System.out.println("Clicked Adv search button");
				Thread.sleep(5000);
				//clear button clicked
				HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
				clickClearButton.clickClearButton();
				System.out.println("Clicked search button");
				Thread.sleep(5000);
			} else {
				HUBOrderUtil clickOrderLink = new HUBOrderUtil(driver);
				clickOrderLink.clickOrderLink();
				System.out.println("Clicked order link");
				Thread.sleep(5000);
				//Validate Order Details
				System.out.println("Order Details");			
//				String OrderStreamText;
//				OrderStreamText = driver.findElement(By.xpath(".//*[@id='app']/div[2]/div[3]/div[2]/div/div/div[2]/div/ul/div[2]/li[2]/span")).getText();
//				Assert.assertEquals(OrderStreamText, OrderStream);
//				System.out.println("OrderStreamText::" + OrderStreamText);
//				System.out.println("Order Stream fetched from the excel--" + OrderStream);
				Thread.sleep(5000);
				//Navigate back to Order List			
				driver.navigate().back();
				System.out.println("Navigate back to Order List");
				//ADV Search clicked
				HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
				clickAdvSearchButton.clickAdvSearchButton();
				System.out.println("Clicked Adv search button");
				Thread.sleep(5000);
				//clear button clicked
				HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
				clickClearButton.clickClearButton();
				System.out.println("Clicked clear button");
				Thread.sleep(5000);	
			}
		}

		@DataProvider(name = "OrderStatus")
		public String[][] getExcelData1() throws IOException, Exception {
			String xl = "C://Users//Milind.Siddhanti//Desktop//Telstra//orderNumberSearch.xlsx";
			String sheet = "OrderStatus";
			int rowCount = HUBExcelUtil.getRowCount(xl, sheet);
			Sheet sheetDetail = HUBExcelUtil.getSheet(xl, sheet);
			String orderStatusTestData[][] = null;
			int colCount = sheetDetail.getRow(0).getLastCellNum();
			orderStatusTestData = new String[rowCount][colCount];
			int count = 0;
			for (int i = 1; i <= rowCount; i++) {
				System.out.println("CountOfRows:" + rowCount);

				System.out.println("CountOfColumns:" + rowCount);

				for (int j = 0; j < colCount; j++) {
					orderStatusTestData[count][j] = HUBExcelUtil.getCellValue(xl, sheet, i, j);
				}
				count++;
			}
			System.out.println(Arrays.deepToString(orderStatusTestData));
			return orderStatusTestData;
		}
}
