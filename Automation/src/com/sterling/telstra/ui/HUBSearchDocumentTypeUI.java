package com.sterling.telstra.ui;

import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sterling.telstra.util.HUBExcelUtil;
import com.sterling.telstra.util.HUBOrderUtil;

import org.testng.annotations.BeforeClass;

public class HUBSearchDocumentTypeUI {
	public WebDriver driver;

	@BeforeClass
	public void beforeMethod() throws InterruptedException {

		// initialize the Firefox driver
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Milind.Siddhanti\\Downloads\\geckodriver-v0.11.1-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://146.89.240.252:9080/hubble-sp/login.jsp");
		driver.manage().window().maximize();
		Thread.sleep(2000);
	}

	@Parameters({ "username", "password", "pagetitle" })
	@Test
	
	public void loginSP(String userName, String password, String pageTitle) throws InterruptedException {
		System.out.println("Username:" + userName);
		System.out.println("Password:" + password);
		System.out.println("Header:" + pageTitle);
		LoginPage login = new LoginPage(driver);
		login.Login(userName, password);
		Thread.sleep(500);
		// Submitting the data by clicking on login button
		LoginPage clckLoginBtn = new LoginPage(driver);
		clckLoginBtn.clickLoginBtn();
		Thread.sleep(4000);
		System.out.println("Order header");
		WebElement ordericon=driver.findElement(By.xpath("//a[contains(@href, '#!/orderlist')]"));
		ordericon.click();
		System.out.println("Order clicked:::" + ordericon);	
		System.out.println("Order header is clicked");
		Thread.sleep(3000);
	}
	@Test(dataProvider = "DocumentType")
	public void verifyOrderSearchUsingDocType(String DocType, String pageTitle) throws InterruptedException {
		Thread.sleep(5000);
		String Actualtext = "";
		//Click on show more button
		HUBOrderUtil clickShowButton = new HUBOrderUtil(driver);
		clickShowButton.clickShowButton();
		Thread.sleep(5000);
		//Scroll down
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scroll(0, 250);");
		Thread.sleep(5000);
		//Click on the doc type drop down
		HUBOrderUtil clickDocumentType = new HUBOrderUtil(driver);
		clickDocumentType.clickDocumentType();
		Thread.sleep(5000);
		//Select values
		HUBOrderUtil DocumentTypeSearch = new HUBOrderUtil(driver);
		DocumentTypeSearch.DocumentTypeSearch(DocType);
		Thread.sleep(5000);
		//Click on search
		HUBOrderUtil clickSearchButton = new HUBOrderUtil(driver);
		clickSearchButton.clickSearchButton();
		System.out.println("Search button clicked");
		Thread.sleep(5000);
		//Navigated to the order details page
		HUBOrderUtil NDF = new HUBOrderUtil(driver);
		if(NDF.NoDataFound.isDisplayed()){
			//ADV Search clicked
			HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
			clickAdvSearchButton.clickAdvSearchButton();
			System.out.println("Clicked Adv search button");
			Thread.sleep(5000);
			//clear button clicked
			HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
			clickClearButton.clickClearButton();
			System.out.println("Clicked search button");
			Thread.sleep(5000);
		} else {
			//Click on the order number hyperlink
			HUBOrderUtil clickOrderLink = new HUBOrderUtil(driver);
			clickOrderLink.clickOrderLink();
			System.out.println("Order Number hyperlink is clicked");
			Thread.sleep(5000);
			Actualtext = driver.findElement(By.xpath("//*[@id='app']/div[2]/div[3]/div[2]/div/div/div[2]/div/ul/div[2]/li[1]/span")).getText();
			System.out.println("Actualtext:"+Actualtext);
			Assert.assertEquals(Actualtext, pageTitle);
			Thread.sleep(5000);
			//Scroll up
			JavascriptExecutor js1 = (JavascriptExecutor)driver;
			js1.executeScript("scroll(250, 0);");
			Thread.sleep(5000);
			//use bread crumbs to come back to list page
			driver.navigate().back();
			System.out.println("Navigated to the order list page from the order details page");
			Thread.sleep(10000);
			//Click Advanced search button
			HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
			clickAdvSearchButton.clickAdvSearchButton();
			System.out.println("Button clicked");
			Thread.sleep(5000);
			//Click Clear Button
			HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
			clickClearButton.clickClearButton();
			System.out.println("Entry cleared");
			Thread.sleep(10000);
		}
}	
	@DataProvider(name = "DocumentType")
	public String[][] getExcelData() throws IOException, Exception {
		String xl = "C://Users//Milind.Siddhanti//Desktop//Telstra//orderNumberSearch.xlsx";
		String sheet = "DocumentType";
		int rowCount = HUBExcelUtil.getRowCount(xl, sheet);
		Sheet sheetDetail = HUBExcelUtil.getSheet(xl, sheet);
		String documentTypeTestData[][] = null;
		int colCount = sheetDetail.getRow(0).getLastCellNum();
		documentTypeTestData = new String[rowCount][colCount];
		int count = 0;
		for (int i = 1; i <= rowCount; i++) {
			System.out.println("CountOfRows:" + rowCount);

			System.out.println("CountOfColumns:" + rowCount);
			
			for (int j = 0; j < colCount; j++) {
				documentTypeTestData[count][j] = HUBExcelUtil.getCellValue(xl, sheet, i,
						j);
			}
			count++;
		}
		System.out.println(Arrays.deepToString(documentTypeTestData));
		return documentTypeTestData;
	}
}