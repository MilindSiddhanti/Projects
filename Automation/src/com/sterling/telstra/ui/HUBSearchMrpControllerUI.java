package com.sterling.telstra.ui;

import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.sterling.telstra.util.HUBExcelUtil;
import com.sterling.telstra.util.HUBOrderUtil;

import org.testng.annotations.BeforeClass;
 

public class HUBSearchMrpControllerUI  {

            public WebDriver driver;
            
            @BeforeClass
        	public void beforeMethod() throws InterruptedException {

        		// initialize the FireFox driver
        		System.setProperty("webdriver.gecko.driver","C:\\Users\\Milind.Siddhanti\\Downloads\\geckodriver-v0.11.1-win64\\geckodriver.exe");
        		driver = new FirefoxDriver();
        		driver.get("http://146.89.240.252:9080/hubble-sp/login.jsp");
        		driver.manage().window().maximize();
        		Thread.sleep(2000);
        	}

            @Parameters({ "username", "password", "pagetitle" })
        	@Test
        	
        	public void loginSP(String userName, String password, String pageTitle) throws InterruptedException
            {
        		System.out.println("USERNAME:" + userName);
        		System.out.println("USERNAME1:" + password);
        		System.out.println("USERNAME2:" + pageTitle);
        		LoginPage login = new LoginPage(driver);
        		login.Login(userName, password);
        		Thread.sleep(500);
        		 // Submitting the data by clicking on login button
        		LoginPage clckLoginBtn = new LoginPage(driver);
        		clckLoginBtn.clickLoginBtn();
        		Thread.sleep(10000);
        		System.out.println("Order header");
        		WebElement ordericon=driver.findElement(By.xpath("//a[contains(@href, '#!/orderlist')]"));
        		ordericon.click();
        		System.out.println("Order clicked:::" + ordericon);	
        		System.out.println("Order header is clicked");
        		Thread.sleep(3000);
            }  
        		
        		@Test(dataProvider = "MrpController")
        		public void verifyMrpController(String MC, String pageHeader, String pageTitle) throws InterruptedException {
        			String Actualtext = driver.findElement(By.xpath(".//*[@id='app']/div[2]/div[2]/div[1]/h5")).getText();
        			Assert.assertEquals(Actualtext, pageTitle);
        			Thread.sleep(1000);
        			HUBOrderUtil mc = new HUBOrderUtil(driver);
        			if(!mc.MrpController.isDisplayed()){
        				HUBOrderUtil clickShowButton = new HUBOrderUtil(driver);
            			clickShowButton.clickShowButton();
            			System.out.println("Clicked show more button");
            			Thread.sleep(5000);
        			}        			
        			HUBOrderUtil MrpController = new HUBOrderUtil(driver);
        			MrpController.MrpControllerSearch(MC);
        			System.out.println("Inside the method to verify order search");
        			//search button clicked
        			HUBOrderUtil clickSearchButton = new HUBOrderUtil(driver);
        			clickSearchButton.clickSearchButton();
        			System.out.println("Clicked search button");
        			Thread.sleep(5000);
        			HUBOrderUtil clickOrderLink = new HUBOrderUtil(driver);
    				clickOrderLink.clickOrderLink();
    				System.out.println("Clicked order link");
    				Thread.sleep(5000);
    				HUBOrderUtil clickOrderLineLink = new HUBOrderUtil(driver);
    				clickOrderLineLink.clickOrderLineLink();
    				System.out.println("Clicked order line link");
        		    Thread.sleep(3000);
        			String MrpControllertext = driver.findElement(By.xpath(".//*[@id='orderDetailsLink']/div/div/div/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div/span")).getAttribute("innerText");
        			System.out.println(MrpControllertext);
        			System.out.println(pageHeader);        			
        			if (MrpControllertext.equals(pageHeader)){
        		          System.out.println("Test for MRP Controller Search Passed!");
        		      } else {
        		          System.out.println("Test for MRP Controller Search Failed");
        		      }
        			Assert.assertEquals(MrpControllertext, pageHeader);
        			Thread.sleep(2000);
        			HUBOrderUtil clickCloseOrderLineButton = new HUBOrderUtil(driver);
    				clickCloseOrderLineButton.clickCloseOrderLineButton();
    				System.out.println("Clicked Close button of Order line");
    				Thread.sleep(5000);
        			//navigate to Order List
        			driver.navigate().back();
        			System.out.println("Clicked Order List button");
        			Thread.sleep(5000);
        			//navigate to ADV Search
        			HUBOrderUtil clickAdvSearchButton = new HUBOrderUtil(driver);
        			clickAdvSearchButton.clickAdvSearchButton();
        			System.out.println("Clicked Adv search button");
        			Thread.sleep(5000);
        			//clear button clicked
        			HUBOrderUtil clickClearButton = new HUBOrderUtil(driver);
        			clickClearButton.clickClearButton();
        			System.out.println("Clicked clear button");
        			Thread.sleep(5000);
        			      		      			        			
        		}
        		
        		@DataProvider(name = "MrpController")
        		public String[][] getExcelData1() throws IOException, Exception {
        			String xl = "C://Users//Milind.Siddhanti//Desktop//Telstra//orderNumberSearch.xlsx";
        			String sheet = "MrpController";
        			int rowCount = HUBExcelUtil.getRowCount(xl, sheet);
        			Sheet sheetDetail = HUBExcelUtil.getSheet(xl, sheet);
        			String MrpControllerData[][] = null;
        			int colCount = sheetDetail.getRow(0).getLastCellNum();
        			MrpControllerData = new String[rowCount][colCount];
        			int count = 0;
        			for (int i = 1; i <= rowCount; i++) {
        				System.out.println("CountOfRows:" + rowCount);

        				System.out.println("CountOfColumns:" + rowCount);

        				for (int j = 0; j < colCount; j++) {
        					MrpControllerData[count][j] = HUBExcelUtil.getCellValue(xl, sheet, i, j);
        				}
        				count++;
        			}
        			System.out.println(Arrays.deepToString(MrpControllerData));
        			return MrpControllerData;
        		}
        		        		
        }
