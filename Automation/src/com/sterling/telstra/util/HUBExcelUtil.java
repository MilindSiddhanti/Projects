package com.sterling.telstra.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.NumberToTextConverter;

public class HUBExcelUtil {
	@SuppressWarnings("deprecation")
	public static String getCellValue(String xl, String Sheet, int r, int c) {
		try {
			DataFormatter df = new DataFormatter();
			FileInputStream fis = new FileInputStream(xl);
			Workbook wb = WorkbookFactory.create(fis);
			Cell cell = wb.getSheet(Sheet).getRow(r).getCell(c);
			//return cell.getStringCellValue();
			String str="";
			if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			       str = NumberToTextConverter.toText(cell.getNumericCellValue());
			} else if (cell.getCellType()==Cell.CELL_TYPE_STRING) {
			    str=cell.getStringCellValue();
			} else{
			    str=df.getDefaultFormat(cell).toString();
		   }
			   return str;
		} catch (Exception e) {
			return "";
		}
	}

	public static int getRowCount(String xl, String Sheet) {
		try {
			FileInputStream fis = new FileInputStream(xl);
			Workbook wb = WorkbookFactory.create(fis);
			return wb.getSheet(Sheet).getLastRowNum();
		} catch (Exception e) {
			return 0;
		}
	}

	public static Sheet getSheet(String xl, String sheet) throws Exception, IOException {
		try {
			FileInputStream fis = new FileInputStream(xl);
			Workbook wb = WorkbookFactory.create(fis);
			return wb.getSheet(sheet);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
}

	
}