package com.sterling.telstra.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HUBOrderUtil {
	
	//Order Number Search Text Field
	@FindBy(xpath = "//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[1]/input")
	public WebElement OrderNumber;
	
	//Send values to Order number Text 
	public void OrderSearch(String ON) {
		OrderNumber.sendKeys(ON);
	}
	
	//Source ID Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[2]/input")
	public WebElement SourceID;
	
	//Send values to SourceID Text 
	public void SourceSearch(String SID) {
		SourceID.sendKeys(SID);
	}
	
	//Vendor Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[3]/input")
	public WebElement Vendor;
	
	//Send values to Vendor Text 
	public void VendorSearch(String VID) {
		Vendor.sendKeys(VID);
	}
	
	//DAC Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[4]/input")
	public WebElement DAC;
	
	//Send values to DAC Text 
	public void DACSearch(String Dac) {
		DAC.sendKeys(Dac);
	}
	
	//Project Number Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[6]/input")
	public WebElement ProjectNumber;
	
	//Send values to Project number Text 
	public void ProjectSearch(String PN) {
		ProjectNumber.sendKeys(PN);
	}
	
	//Contact Details Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[7]/input")
	public WebElement ContactDetails;
	
	//Send values to Contact Details Text 
	public void ContactSearch(String CD) {
		ContactDetails.sendKeys(CD);
	}
	
	//Item ID Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[2]/input")
	public WebElement ItemID;
	
	//Send values to Item ID Text 
	public void ItemSearch(String IID) {
		ItemID.sendKeys(IID);
	}
		
	//Item Description Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[3]/input")
	public WebElement ItemDesc;
	
	//Send values to Item Description Text 
	public void ItemDescSearch(String ID) {
		ItemDesc.sendKeys(ID);
	}
		
	//Cost Center Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[4]/input")
	public WebElement CostCenter;
	
	//Send values to Cost Center Text 
	public void CostCenterSearch(String CC) {
		CostCenter.sendKeys(CC);
	}
		
	//Priority Code Search Text Field
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[11]/input")
	public WebElement PriorityCode;
	
	//Send values to Priority Code Text 
	public void PriorityCodeSearch(String PC) {
		PriorityCode.sendKeys(PC);
	}
	
	//Mrp Controller Search Text Field
	@FindBy(id = "mrpcontroller")
	public WebElement MrpController;
	
	//Send values to Mrp Controller Text 
	public void MrpControllerSearch(String MRP) {
		MrpController.sendKeys(MRP);
	}
	
	//Max records Search Text Field
	@FindBy(xpath = ".//*[@id='maxrecords']")
	public WebElement MaxRecord;
		
	//Send values to Max records Text 
	public void MaxRecordSearch(String MX) {
		MaxRecord.sendKeys(MX);
	}
			
	//Adv Search Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/div[1]/div/button")
	public WebElement AdvSearch;
	
	// Click on Adv Search button
	public void clickAdvSearchButton(){
		AdvSearch.click();
	}
	
	//Clear Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/h4/span/button")
	public WebElement Clear;
	
	// Click on Clear button
	public void clickClearButton(){
		Clear.click();
	}
	
	//ShowMore/ShowLess Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[12]/a")
	public WebElement Show;
	
	// Click on Show button
	public void clickShowButton() {
		Show.click();
	}
	
	//Search Button
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[11]/button")
	public WebElement Search;
	
	// Click on Search button
	public void clickSearchButton() {
		Search.click();
	}
		
	//Order Link
	@FindBy(partialLinkText = "Y1000")
	public WebElement OrderLink;
	
	// Click on Order Link
	public void clickOrderLink() {
		OrderLink.click();
	}
	
	//Status From Select Box
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[5]/div/div/select")
	public WebElement StatusFrom;
			
	// Click on Status from Select Box
	public void clickStatusFrom() {
		StatusFrom.click();
	}
	//Send Values to Status From
	public void StatusFromSearch(String SF) {
		Select dropdown= new Select(StatusFrom);
		dropdown.selectByVisibleText(SF);
	}
	
	//Status To Select Box
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[5]/div/div[2]/select")
	public WebElement StatusTo;
	
	// Click on Status to Select Box
	public void clickStatusTo() {
		StatusTo.click();
	}
	//Send Values to Status To
	public void StatusToSearch(String ST) {
		Select dropdown= new Select(StatusTo);
		dropdown.selectByVisibleText(ST);
	}
	
	//Receiving Node Select Box
	@FindBy(id = "ReceivingNode")
	public WebElement ReceivingNode;
		
	// Click on Receiving Node Select Box
	public void clickReceivingNode() {
		ReceivingNode.click();
	}
	//Send Values to Receiving Node
	public void ReceivingNodeSearch(String RN) {
		Select dropdown= new Select(ReceivingNode);
		dropdown.selectByValue(RN);
	}
	
	//Order Stream Select Box
	@FindBy(id = "OrderStream")
	public WebElement OrderStream;
			
	// Click on Order Stream Select Box
	public void clickOrderStream() {
		OrderStream.click();
	}
	//Send Values to Order Stream
	public void OrderStreamSearch(String OS) {
		Select dropdown= new Select(OrderStream);
		dropdown.selectByValue(OS);
	}
	
	//Source system drop down
	@FindBy(xpath = ".//*[@id='entryType']")
	public WebElement SourceSystem;
	
	// Click on Source system Select Box
	public void clickSourceSystem() {
		SourceSystem.click();
	}
	//Send Values to Source system
	public void SourceSystemSearch(String SS) {
		Select dropdown= new Select(SourceSystem);
		dropdown.selectByValue(SS);
	}
	
	//Order History Orders Box
	@FindBy(id = "includeHistory")
	public WebElement IncludeHistory;
				
	// Click on History Orders Select Box
	public void clickIncludeHistory() {
		IncludeHistory.click();
	}
	//Send Values to History Orders
	public void IncludeHistorySearch(String IH) {
		Select dropdown= new Select(IncludeHistory);
		dropdown.selectByValue(IH);
	}
	
	//Document type drop down
	@FindBy(xpath = "//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[14]/select")
	public WebElement DocumentType;
	
	// Click on Order Stream Select Box
	public void clickDocumentType() {
		DocumentType.click();
	}
	//Send Values to Order Stream
	public void DocumentTypeSearch(String DT) {
		Select dropdown= new Select(DocumentType);
		dropdown.selectByVisibleText(DT);
	}
	
	//Find Location drop down
	@FindBy(xpath = ".//*[@id='From Location']")
	public WebElement FromLocation;
		
	// Click on Order Stream Select Box
	public void clickFromLocation() {
		FromLocation.click();
	}
	//Send Values to Order Stream
	public void FromLocationSearch(String FL) {
		Select dropdown= new Select(FromLocation);
		dropdown.selectByValue(FL);
	}
	
	//Order Type drop down
	@FindBy(xpath = ".//*[@id='orderType']")
	public WebElement OrderType;
			
	// Click on Order Stream Select Box
	public void clickOrderType() {
		OrderType.click();
	}
	//Send Values to Order Stream
	public void OrderTypeSearch(String OT) {
		Select dropdown= new Select(OrderType);
		dropdown.selectByValue(OT);
	}
	
	//From Order Date
	@FindBy(xpath=".//*[@id='FromOrderDate']")
	public WebElement FromOrderDate;
	
	// Click on From Order Date Picker
	public void clickFromOrderDate(String Date) {		
		//FromOrderDate.click();
		FromOrderDate.sendKeys(Date);
	}
	
	//To Order Date
	@FindBy(xpath=".//*[@id='ToOrderDate']")
	public WebElement ToOrderDate;
	
	// Click on To Order Date Picker
	public void clickToOrderDate(String Date) {
		//ToOrderDate.click();
		ToOrderDate.sendKeys(Date);
	}
	
	//From DatePicker ReqShip Date
	@FindBy(xpath = ".//*[@id='FromReqShipDate']")
	public WebElement FromReqShipDate;
	
	// Click on From Date Picker ReqShip Date
	public void clickFromReqShipDate(String Date) {
		FromReqShipDate.sendKeys(Date);
	}
	
	//To DatePicker ReqShip Date
	@FindBy(xpath = ".//*[@id='ToReqShipDate']")
	public WebElement ToReqShipDate;
	
	// Click on To Date Picker ReqShip Date
	public void clickToReqShipDate(String Date) {
		ToReqShipDate.sendKeys(Date);
	}
	
	//From DatePicker ReqDelivery Date
	@FindBy(xpath = ".//*[@id='FromReqDeliveryDate']")
	public WebElement FromReqDeliveryDate;
		
	// Click on From Date Picker ReqDelivery Date
	public void clickFromReqDeliveryDate(String Date) {
		FromReqDeliveryDate.sendKeys(Date);
	}
		
	//To DatePicker ReqDelivery Date
	@FindBy(xpath = ".//*[@id='ToReqDeliveryDate']")
	public WebElement ToReqDeliveryDate;
	
	// Click on To Date Picker ReReqDeliveryqShip Date
	public void clickToReqDeliveryDate(String Date) {
		ToReqDeliveryDate.sendKeys(Date);
	}
	
	//OrderLine Link 
	@FindBy(xpath = ".//*[@id='orderlines']/tbody/tr[2]/td[2]/a")
	public WebElement OrderLineLink;
	
	// Click on OrderLine Link
	public void clickOrderLineLink() {
		OrderLineLink.click();
	}
	
	//OrderLine Link Close button
	@FindBy(xpath = ".//*[@id='orderDetailsLink']/div/div/div/div/div[3]/button")
	public WebElement CloseOrderLine;
		
	// Click on OrderLine Link Close button
	public void clickCloseOrderLineButton() {
		CloseOrderLine.click();
	}
	
	//Capture No data found element
	@FindBy(xpath = ".//*[@class='noRecordCtrl']")
	public WebElement NoDataFound;
	
	//Error on wrong entry of from date
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[5]/div/div[1]/p")
	public WebElement ErrorFromDate;
	
	//Error on wrong entry of to date
	@FindBy(xpath = ".//*[@id='app']/div[2]/div[2]/div[2]/form/div/div/div[9]/div[5]/div/div[2]/p")
	public WebElement ErrorToDate;
	
	// Initializing the Objects
	public HUBOrderUtil(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
}