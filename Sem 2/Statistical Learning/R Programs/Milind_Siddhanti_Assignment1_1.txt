# read the data inside the csv file
csv <- "austin_house_price.csv"
data <- read.csv(csv, header = TRUE, na.strings = "?")
summary(data)

# Question 2
# plot the values in different graphical format
# 1) scatter plot
plot(data$FullBath, data$SalePrice)

# 2) histogram plot
hist(data$HalfBath)

# 3) box plot
boxplot(data$OverallQual)

# 4) scatterplot matrix for all the data variables
pairs(data, main = "Scatter plot of all variables in data set")

# 5) scatter plot matrix of selected data variables
attach(data)
pairs(~OverallQual+OverallCond+FullBath+HalfBath+SalePrice, data = data, main = "Scatter plot of selected variables from data set")

# correaltion matrix
corr_mat <- cor(data)

# multiple linear regression using 4 variables
mlr_1 <- lm(SalePrice ~ OverallQual + OverallCond + FullBath + HalfBath, data = data)
summary_mlr_1 <- summary(mlr_1)

# multiple linear regression using all the variables
mlr_all <- lm(SalePrice ~ ., data = data)
summary_mlr_all <- summary(mlr_all)

# to find the relation of the features on the response, we will have to come up with a null hypothesis which states there is no relationship between the response and the features
# and an alternate hypothesis which states there is a relationship between the response and the features.
# this could be determined by the F-statistic value from the summary of the model which has a p-value < 2.2e-16. Considering this small value of p, we can reject the null hypothesis.
# the coefficient for the age variable suggests that for increase in age by 1 year, the saleprice response decreases by a quantity of 243.83 against the scale considered.

# multiple linear regression with changes in variable
# removing the values of age which is 0 and 1 to use them under log function
#age <- subset(data, Age > 1)
#new_age <- log(age[, 13])
new_overallcond <- log(OverallCond) 
new_fireplaces <- Fireplaces * Fireplaces
new_garagecars <- sqrt(GarageCars)
mlr_new <- lm(SalePrice ~ new_overallcond + new_fireplaces + new_garagecars, data = data)
summary_mlr_new <- summary(mlr_new)

# we can analyze from the t-values of the individual features that log function for the chosen feature follows the null hypothesis and the other transformations such as square root
# and square for the chosen features reject the null hypothesis by a large margin. 
# Due to this we have lower p-value for the F-statistic which rejects the null hypothesis for the new mlr model.

# Question 3
# random normalized data
x <- rnorm(100)

# random normalized data with variance as 0.25
eps <- rnorm(100, mean = 0, sd = sqrt(0.25))
# > eps <- rnorm(100, mean = 0, sd = sqrt(0.25))
# > var(eps)
# [1] 0.2454037

# y = -1 + (0.5)x + eps
y <- eps - 1 + 0.5 * x
length(y) # length of y
# length of y is 100
# B0 = -1 and B1 = 0.5 since the model is in the format of y = B0 + B1*X1 + eps

# scatterplot
plot(x,y) 
# y varies almost linearlly with x

# least squares linear model
set.seed(100)
least_sq_linear <- lm(y ~ x)
summary_least_sq_linear <- summary(least_sq_linear)
# The values are B0 = -1.04015 and B1 = 0.50809 for the above model. The B0 and B1 values for the equation and the model are almost same which suggests that the predicted value is 
# close to that of the true function. The F-statistic has a p-value < 2.2e-16 which is low, hence we can reject the null hypothesis which states that there is no relation between x 
# and y

# polynomial regression model
set.seed(200)
poly_reg <- lm(y ~ x + x^2)
# there was no consideration of the polynomial expression in the equation
# found that there is a function I() which defines the object as is

set.seed(201)
poly_reg <- lm(y ~ x + I(x^2))
summary_poly_reg <- summary(poly_reg)
# p-value for the variable x^2 is 0.139 which is more and proves that the significance of the polynomial function is small on the prediction model. Since the predicton model has a
# low p-value and high F-statistic, we cannot reject the alternate hypothesis.

# to reduce the noise bringing down the var of the function to 0.1 from 0.25 for the eps variable
set.seed(300)
eps_noise <- rnorm(100, mean = 0, sd = sqrt(0.1))
x_noise <- rnorm(100)

y_noise <- -1 + 0.5 * x_noise + eps_noise

plot(x_noise, y_noise)
# the plot now consists low count of outliers which results in the reduction of noise.

least_sq_linear_noise <- lm(y_noise ~ x_noise)
# the B0 and B1 values now appear to be more closer to the actual values of (B0=0.5 and B1=-1) with reduction in noise. The values are B0=-0.94286 and B1=0.53458.

poly_reg_noise <- lm(y_noise ~ x_noise + I(x_noise^2))
# with the reduction of noise the p-value for the polynomial term is increased to 0.161 thus becoming less significant to the model as a variable.

