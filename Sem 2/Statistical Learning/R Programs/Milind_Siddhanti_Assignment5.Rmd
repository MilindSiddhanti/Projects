---
title: "Assignment5"
author: "Milind Siddhanti"
date: "April 20, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
# Question 1
# In this exercise, we will use carseat dataset and seek to predict Sales using regression trees and related approaches, #treating the response as a quantitative variable.
```{r}
csv <- "carseat.csv"
data <- read.csv(csv, header = TRUE, na.strings = "?")
summary(data)
```
# (a) Split the data set into a training set and a test set.
```{r}
sample <- sample.int(n = nrow(data), size = floor(0.7*nrow(data)), replace = F)
train <- data[sample, ]
test  <- data[-sample, ]
```
# (b) Fit a regression tree to the training set. Plot the tree, and interpret the results. What test MSE do you obtain?
```{r}
library(rpart)
# grow tree 
fit_a <- rpart(Sales~., method="anova", data=train)
printcp(fit_a) # display the results 
summary(fit_a) # detailed summary of splits
``` 
```{r}
plotcp(fit_a) # visualize cross-validation results
```

```{r}
# plot tree 
plot(fit_a, uniform=TRUE, main="Regression Tree for sales ")
text(fit_a, use.n=TRUE, all=TRUE, cex=.8)
```
```{r}
a_pred = predict(fit_a, test)
mean((a_pred - test$Sales)^2)
```
# test mse for the regression tree model with anova as the method is 4.5727.
```{r}
# grow tree 
fit_p <- rpart(Sales~., method="poisson", data=train)
printcp(fit_p) # display the results 
summary(fit_p) # detailed summary of splits
```
```{r}
plotcp(fit_p) # visualize cross-validation results
```

```{r}
# plot tree 
plot(fit_p, uniform=TRUE, main="Regression Tree for sales ")
text(fit_p, use.n=TRUE, all=TRUE, cex=.8)
```
```{r}
p_pred = predict(fit_p, test)
mean((p_pred - test$Sales)^2)
```
# test mse for the regression tree model with poisson as the method is 5.439716.

# (c) Use cross-validation in order to determine the optimal level of tree complexity. Does pruning the tree improve the test MSE?
```{r}
# find the minimum cp value t oreduce the cross validation error
min_cp_a <- fit_a$cptable[which.min(fit_a$cptable[,'xerror']),'CP']
min_cp_a
```
# the best cp value choosen for tree pruning is 0.01 based on the cross-validation error obtained from the model.
```{r}
# prune the tree
apfit<- prune(fit_a, cp=min_cp_a) # from cptable 
summary(apfit)
```

```{r}
# plot the pruned tree 
plot(apfit, uniform=TRUE, main="Pruned Regression Tree for Sales")
text(apfit, use.n=TRUE, all=TRUE, cex=.8)
```

```{r}
# find the minimum cp value t oreduce the cross validation error
min_cp_p <- fit_p$cptable[which.min(fit_p$cptable[,'xerror']),'CP']
min_cp_p
```
# the best cp value choosen for tree pruning is 0.01 based on the cross-validation error obtained from the model.
```{r}
# prune the tree 
ppfit<- prune(fit_p, cp=min_cp_p) # from cptable 
summary(ppfit)
```

```{r}
# plot the pruned tree 
plot(ppfit, uniform=TRUE, main="Pruned Regression Tree for Sales")
text(ppfit, use.n=TRUE, all=TRUE, cex=.8)
```
# (d) Use the bagging approach in order to analyze this data. What test MSE do you obtain?
```{r}
# install.packages("ipred")
library(ipred)
a_bag <- bagging(Sales~ ., data = train, coob=TRUE)
print(a_bag)

pred_bag <- predict(a_bag, test, type="class")
mean ((pred_bag - test$Sales)^2)
```
# test mse for the bagging model is 3.650569, which is less than the regression tree model's test mse. Bagging helps in reduction of the test mse.
# (e) Use random forests to analyze this data. What test MSE do you obtain?
```{r}
# install.packages("randomForest")
library(randomForest)
a_rf = randomForest(Sales~., data = train, mtry = 3, importance = TRUE)
pred_rf = predict (a_rf , test)
mean ((pred_rf - test$Sales)^2)
```
# test mse for the random forest model is 3.465834, which is less than the regression tree and bagging model's test mse. Random forest helps in reduction of the test mse better comapred to Bagging as m is sqaured root of p.
#(f) Also, report the important features in your random forest. One can do this by using importance() function in R or #feature_importance_ on a fitted model in sklearn in python.
# Describe the effect of m, the number of variables considered at each split, on the error rate obtained.
```{r}
importance(a_rf)
```

```{r}
varImpPlot(a_rf)
```

# Question 2
#In this problem, you will use support vector approaches in order to predict whether a given red wine is high-quality or #low-quality based on the red-wine data set. You definitely should have some wine after completing the assignment (Please be #mindful of the age-limit).

```{r}
csv <- "redwine.csv"
redwine <- read.csv(csv, header = TRUE, na.strings = "?")
summary(redwine)
```
# (a) Create a binary variable that takes on a 1 with quality above the mean wine quality, and 0 for quality below the mean.
```{r}
final_quality_mean <- mean(redwine$quality)
redwine$final_quality <- ifelse(redwine$quality > mean(final_quality_mean), 1, 0)
```
# (b) Fit a support vector classifier to the data with various values of penalty (cost in R, C in python), in order to predict whether a given red wine is high-quality or low-quality. Report the cross-validation errors associated with different values of this parameter. Comment on your findings.
```{r}
# install.packages("e1071")
library("e1071")
rows <- sample(x = nrow(redwine), size = 0.20 * nrow(redwine))
wine_test <- redwine[rows, ]
wine_train <- redwine[-rows, ]

svm_model <- svm(wine_train$final_quality ~ ., data = wine_train, cost = 20)
svm_model
```

```{r}
svm_model_c <- sapply(1:100, function(i) svm(wine_train$final_quality ~ ., data = wine_train, cost = i)$tot.nSV)
plot(svm_model_c)
```
# the number of support vectors change with the value of cost. higher the cost lower is the support vector count.
# (c) Now repeat (b), this time using SVMs with radial and polynomial basis kernels, with three different values of gamma and degree and penalty each (check cost in R, C in python for penalty). What’s happening and what might be causing the change in cross validation error. Summary and comment on your choices of parameters and the corresponding cross validation errors in a table.
```{r}
for(i in 1:3){
  for(j in 1:3){
    for(k in 1:3){
      radial_svm <- svm(wine_train$final_quality~. , data = wine_train, kernel = "radial", cost = i, gamma = j, degree = k)
      pred_rad <- predict(radial_svm, wine_test)
      print(mean ((pred_rad - wine_test$final_quality)^2))
    }
  }
}
```

```{r}
for(i in 1:3){
  for(j in 1:3){
    for(k in 1:3){
      poly_svm <- svm(wine_train$final_quality~. , data = wine_train, kernel = "polynomial", cost = i, gamma = j, degree = k)
      pred_poly <- predict(poly_svm, wine_test)
      print(mean ((pred_poly - wine_test$final_quality)^2))
    }
  }
}
```
# with increase in the cost, gamma and degree values the support vector machine has the best test mse values.