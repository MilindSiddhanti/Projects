install.packages("swirl")
library(swirl)

# find the average of the following numbers
x <- c(2.23, 3.45, 1.87, 2.11, 7.33, 18.34, 19.23)
y <- mean(x)
y

# find the sum of first 25 numbers
for(i in 1:25){
  y[i] <- i * i
  sum <- sum(y)
}
print(sum)

# find the class of "cars"
print(class(cars))

# find number of rows in cars
print(nrow(cars))

# name of the second column in cars
print(colnames(cars[2]))

# average distance travelled
distance <- cars[,2]
print(distance)
average_distance_travelled <- mean(distance)
print(average_distance_travelled)

# what row of cars has a distance of 85
row_with_85 <- which(cars$dist == 85)
print(row_with_85)
