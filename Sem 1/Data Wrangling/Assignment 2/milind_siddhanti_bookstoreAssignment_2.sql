-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2017 at 12:28 PM
-- Server version: 10.0.32-MariaDB-1~xenial
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `milind_siddhanti_bookstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `street_part` text NOT NULL,
  `zip` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `street_part`, `zip`) VALUES
(301, '45th/Bernet', '78761'),
(302, '7th/Blue Park', '78701'),
(303, '21st/Warden', '78783'),
(304, '33rd/Foster', '76766'),
(305, '18th/Rondon', '78792');

-- --------------------------------------------------------

--
-- Table structure for table `address_records`
--

CREATE TABLE `address_records` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `valid_from` text NOT NULL,
  `valid_to` text NOT NULL,
  `person_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address_records`
--

INSERT INTO `address_records` (`id`, `type`, `valid_from`, `valid_to`, `person_id`, `address_id`) VALUES
(401, 'Warehouse – 1', 'Jan 2010', 'Jan 2020', 12, 301),
(402, 'Warehouse – 2', 'Feb 2013', 'Feb 2023', 14, 302),
(403, 'Home - Current', 'Mar 2015', 'Mar 2030', 15, 303),
(404, 'Office', 'April 2012', 'April 2021', 16, 304),
(405, 'Home - Summer', 'May 2017', 'Aug 2017', 15, 305);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `no_in_stock` int(11) NOT NULL,
  `year_published` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `no_in_stock`, `year_published`) VALUES
(201, 'Inferno', 20, '2013'),
(202, 'Origin', 30, '2017'),
(203, 'Famous Five - Part 1', 49, '1942'),
(204, 'Harry Potter - Part 4', 7, '2000');

-- --------------------------------------------------------

--
-- Table structure for table `book_purchases`
--

CREATE TABLE `book_purchases` (
  `book_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_purchases`
--

INSERT INTO `book_purchases` (`book_id`, `purchase_id`) VALUES
(203, 501),
(201, 502),
(204, 503);

-- --------------------------------------------------------

--
-- Table structure for table `book_roles`
--

CREATE TABLE `book_roles` (
  `id` int(11) NOT NULL,
  `role` text NOT NULL,
  `person_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_roles`
--

INSERT INTO `book_roles` (`id`, `role`, `person_id`, `book_id`) VALUES
(101, 'Author', 11, 201),
(102, 'Supplier', 12, 203),
(103, 'Author', 11, 202),
(104, 'Author', 13, 203),
(105, 'Supplier', 14, 201),
(106, 'Editor', 16, 204);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `name`, `phone_no`) VALUES
(11, 'Dan Brown', 12345),
(12, 'David', 67890),
(13, 'Enid Blyton', 45656),
(14, 'John', 56789),
(15, 'Roger', 10987),
(16, 'Emily', 34568);

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL,
  `occured_at_month` text NOT NULL,
  `occured_at_year` text NOT NULL,
  `person_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `occured_at_month`, `occured_at_year`, `person_id`) VALUES
(501, 'May', '2016', 15),
(502, 'October', '2014', 16),
(503, 'February', '2017', 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `address_records`
--
ALTER TABLE `address_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_roles`
--
ALTER TABLE `book_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
