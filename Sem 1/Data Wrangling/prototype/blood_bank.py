import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="milind_siddhanti",  # mysql user
            passwd="nokiayu7k",  # mysql passd
            db="milind_siddhanti_project",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    sql_select_state = "SELECT id from states WHERE name = %(state)s"

    sql_insert_blood_bank = """INSERT INTO blood_banks(state_id,name,website,contact)
                    VALUE (%(state_id)s, %(name)s, %(website)s, %(contact)s)"""

    with open('blood_bank.csv') as csvfile:
        # tell python about the specific csv format
        myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

        for row in myCSVReader:
            cursor.execute(sql_select_state, row)
            results = cursor.fetchone()
            state_id = results['id']
            print(state_id)

            # we have what we need.
            param_dict = {'state_id': state_id,
                          'name': row['name'],
                          'website': row['website'],
                          'contact': row['contact']}

            cursor.execute(sql_insert_blood_bank, param_dict)
