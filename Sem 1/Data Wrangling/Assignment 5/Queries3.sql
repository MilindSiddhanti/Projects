"1) What is the name of the person who spent the most (and how much did they spend)?"

--query the tickets table to fetch the price list
SELECT *
FROM tickets
--107673 records of tickets sold

--join purchases table to determine the purchases of tickets
SELECT *
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id

--join people table to fetch person details
SELECT *
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id
    JOIN people
      ON people.id = purchases.person_id

--order by purchase_id
SELECT *
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id
    JOIN people
      ON people.id = purchases.person_id
ORDER BY tickets.purchase_id
--results of names of people who made those purchases are obtained

--group by purchase_id
SELECT tickets.purchase_id
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id
    JOIN people
      ON people.id = purchases.person_id
GROUP BY tickets.purchase_id
--grouped by the purchases made by everybody

--calculate the sum of ticket prices per person_id
SELECT tickets.purchase_id, SUM(tickets.price) AS purchase_price, people.name
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id
    JOIN people
      ON people.id = purchases.person_id
GROUP BY tickets.purchase_id
--sum of the purchases made by each of them is in results

--order by to sort the data by highest to lowest of purchase price
SELECT tickets.purchase_id, SUM(tickets.price) AS purchase_price, people.name
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id
    JOIN people
      ON people.id = purchases.person_id
GROUP BY tickets.purchase_id
ORDER BY purchase_price DESC
--sorted data from highest and lowest purchase total

--fetch the highest purchase made by a person
SELECT tickets.purchase_id, SUM(tickets.price) AS purchase_price, people.name
FROM tickets
    JOIN purchases
      ON purchases.id = tickets.purchase_id
    JOIN people
      ON people.id = purchases.person_id
GROUP BY tickets.purchase_id
ORDER BY purchase_price DESC
LIMIT 1
--Result of the highest ticket purchase made by a person
"Person name: Vebil Miesys, Amount spent on purchases: 765"

"2) Which performance had the highest revenue? (ticket prices are incoming income, thus revenue)"

--query the table tickets to fetch prices of tickets
SELECT *
FROM tickets
--107673 records of tickets sold

--group by performance_id
SELECT tickets.performance_id
FROM tickets
GROUP BY tickets.performance_id
--132 records grouped by performances

--use of aggregate function, sum, calcuate the sum of prices for the performances
SELECT tickets.performance_id, SUM(tickets.price) AS REVENUE
FROM tickets
GROUP BY tickets.performance_id
-- 132 records grouped by performances and revenue of the performances

--order by the revenue of the performances to attain the highest revenue performance
SELECT tickets.performance_id, SUM(tickets.price) AS REVENUE
FROM tickets
GROUP BY tickets.performance_id
ORDER BY REVENUE DESC
-- records sorted out from highest to the lowest revenue of performance

--retrieve performance with the highest revenue by Limiting the record to 1
SELECT tickets.performance_id, SUM(tickets.price) AS REVENUE
FROM tickets
GROUP BY tickets.performance_id
ORDER BY REVENUE DESC
LIMIT 1
"highest revenue recorded from tickets purchases is for the performance_id of 41"

"3) Which performance was the most profitable? (profit is revenue - costs.  bands.fee is the cost)"

--query the table tickets to fetch prices of tickets
SELECT *
FROM tickets
--107673 records of tickets sold

--join on purchases
SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id
--purchases of the tickets made

--join on the bands
SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
--band details are retrived

--order by performance_id
SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
ORDER BY tickets.performance_id
--ordered by performance_id

--group by performance_id
SELECT tickets.performance_id
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY tickets.performance_id
--grouped by performances

--use of aggregate function, sum, calcuate the sum of prices for the performances and subtract them from band fee
SELECT tickets.performance_id, SUM(tickets.price) - bands.fee AS PROFIT
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY tickets.performance_id
-- profit of the bands are available

--sort it to get the highest profitable performance
SELECT tickets.performance_id, SUM(tickets.price) - bands.fee AS PROFIT
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY tickets.performance_id
ORDER BY PROFIT DESC
LIMIT 1
"highest profitable performance has a profit of 53305 for the performance_id of 41"

"4) Which band was the least profitable for the festival?"
--query tickets to find the revenue
SELECT *
FROM tickets
--107673 records are found

--join performances
SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
--107673 records joined from performances

--join bands to integrate the band fee and revenue
SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
--bands joined with tickets and performances

--order by bands to sort
SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
ORDER BY performances.band_id
--ordered by bands are the performances and tickets

--group by the bands
SELECT performances.band_id
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY performances.band_id
--125 records are grouped by the bands

--use having to select distinct venue ids
SELECT performances.band_id, bands.name, SUM(tickets.price) - bands.fee AS PROFIT
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY performances.band_id
HAVING COUNT(DISTINCT performances.venue_id)
--the results are distinguished by the venues

--find the least profitable band
SELECT performances.band_id, bands.name, SUM(tickets.price) - bands.fee AS PROFIT
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY performances.band_id
HAVING COUNT(DISTINCT performances.venue_id)
ORDER BY PROFIT ASC
LIMIT 1
"least profitable band is WAX and has a profit of 2279"

"5) Which venues were oversold (and what were their capacities)?"
--query venues to find the venues
SELECT *
FROM venues

--join performances
SELECT *
FROM venues
  JOIN performances
    ON performances.venue_id = venues.id
--10 records with the venues and performances

--join tickets to find the tickets sold for the venues
SELECT *
FROM venues
  JOIN performances
    ON performances.venue_id = venues.id
  JOIN tickets
    ON tickets.performance_id = performances.id
--10 records with the tickets details

--order by the venue to sort the tickets by venue
SELECT *
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
ORDER BY performances.venue_id
--ordered by the venues

--order by the performances
SELECT *
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
ORDER BY tickets.performance_id\
--ordered by performances

--group by performances and venues
SELECT tickets.performance_id, performances.venue_id
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
GROUP BY tickets.performance_id, performances.venue_id
--132 records grouped by performances first and then by venues

--calculating the oversold tickets for the performances at the venues
SELECT tickets.performance_id, performances.venue_id, venues.name, venues.capacity, (COUNT(tickets.ticketnum) - venues.capacity) AS OVERSOLD
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
GROUP BY tickets.performance_id, performances.venue_id
HAVING COUNT(tickets.ticketnum) - venues.capacity > 0
"venue AMD was oversold for 14 performances"

"6) What was the total revenue from ticket sales each month?"
--query purchases table
SELECT *
FROM purchases
--10768 records found

--join on tickets
SELECT *
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
--10768 records joined on tickets

--order by month of the purchase dates
SELECT *
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
ORDER BY MONTH(purchases.date)
--ordered by the month of purchases made

--group by month of the purchase dates
SELECT MONTH(purchases.date)
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
GROUP BY MONTH(purchases.date)
--grouped by the months

--calculate the revenue by summing up the prices of tickets
SELECT MONTH(purchases.date) as MONTH, SUM(tickets.price) AS REVENUE
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
GROUP BY MONTH(purchases.date)
"
MONTH - REVENUE
1 - 264510
2 - 226199
3 - 247469
4 - 254352
5 - 281448
6 - 258480
7 - 260668
8 - 263922
9 - 127317"

"7) What was the average purchase total each month?"
--query purchases table
SELECT *
FROM purchases
--10768 records found

--join on tickets
SELECT *
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
--10768 records joined on tickets

--order by month of the purchase dates
SELECT *
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
ORDER BY MONTH(purchases.date)
--ordered by the month of purchases made

--group by month of the purchase dates
SELECT MONTH(purchases.date)
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
GROUP BY MONTH(purchases.date)
--grouped by the months

--calculate the average purchases by summing up the ticket prices and dividing by the distinct purchases made
SELECT Month(purchases.date) AS MONTH, SUM(tickets.price)/COUNT(DISTINCT tickets.purchase_id) AS "AVERAGE PURCHASE TOTAL"
  FROM purchases
    JOIN tickets
      ON tickets.purchase_id = purchases.id
GROUP BY Month(purchases.date)
"MONTH - AVERAGE PURCHASE TOTAL
1 - 201.7620
2 - 203.2336
3 - 204.6890
4 - 202.3484
5 - 207.2518
6 - 203.2075
7 - 196.4341
8 - 205.2271
9 - 200.4992"
