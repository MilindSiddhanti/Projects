import pprint
import csv

print("---------------Q2_1----------------")
with open('Q1_queries3q5.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    for row in myCSVReader:
     # pprint.pprint(row)
     print("The venue {venues} with capacity {capacity} was oversold by {oversold} seats.".format(venues = row["name"], capacity = row["capacity"], oversold = row["OVERSOLD"]))
print("---------------Q2_1----------------")

print("---------------Q2_2----------------")
with open('Q1_queries3q6.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    for row in myCSVReader:
     # pprint.pprint(row)
     print("The total revenue of ticket sales in {month} is {revenue}.".format(month = row["MONTHNAME"], revenue = row["REVENUE"]))
print("---------------Q2_2----------------")

print("---------------Q2_3----------------")
with open('Q1_queries3q7.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    for row in myCSVReader:
     # pprint.pprint(row)
     print("The average total purchase in {month} is {revenue}.".format(month = row["NAME"], revenue = row["AVERAGE PURCHASE TOTAL"]))
print("---------------Q2_3----------------")
