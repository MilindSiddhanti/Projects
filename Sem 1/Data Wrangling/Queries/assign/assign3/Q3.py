import pprint
import csv

with open('foundation.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    print("{0:<22} {1:<22} {2:<22}".format("Title", "Author", "Year"))

    for row in myCSVReader:
      print("{0:<22} {1:<22} {2:<22}".format(row["Title"], row["Author"], row["Year"]))

      length_key = [len(x) for x in row.values()]
      max_length = max(length_key)
      print(max_length)
      print(length_key)

      # column_names = row[0].values
      # print(column_names)
