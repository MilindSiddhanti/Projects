# SELECT MONTH(purchases.date) as MONTH, SUM(tickets.price) AS REVENUE
# FROM purchases
#   JOIN tickets
#     ON tickets.purchase_id = purchases.id
# GROUP BY MONTH(purchases.date)

import pymysql.cursors
import pprint
import csv

connection = pymysql.connect(
            host="localhost",
            user="testuser",
            passwd="rSs8xvnAQHFS5Men",
            db="class_music_festival",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    sql = """SELECT MONTH(purchases.date) as month_number, SUM(tickets.price) AS revenue FROM purchases
             JOIN tickets ON tickets.purchase_id = purchases.id GROUP BY MONTH(purchases.date) """
    cursor.execute(sql)
    results = cursor.fetchall()
    print(sql)
    pprint.pprint(results)

    column_names = results[0].keys()
    print(column_names)

    with open('q3.csv', 'w') as csvfile:

        myCsvWriter = csv.DictWriter(csvfile,
                                     delimiter=',',
                                     quotechar='"',
                                     fieldnames = column_names)

        myCsvWriter.writeheader()

        for row in results:
            myCsvWriter.writerow(row)

print("Done writing csv.")
