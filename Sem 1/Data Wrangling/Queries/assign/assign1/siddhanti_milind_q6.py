print("There once was a man named {name} \n"
"He had whiskers on his chinnegan, \n"
"The wind came up and blew them in ag'in, \n"
"Poor old {name} (begin ag'in)".format(name = "Milind Siddhanti"))

#this is the basic approach of printing the value inside the function by representing the index value
#print("There once was a man named {0} \n"
#"He had whiskers on his chinnegan, \n"
#"The wind came up and blew them in ag'in, \n"
#"Poor old {0} (begin ag'in)".format("Milind Siddhanti"))
