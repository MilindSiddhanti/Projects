number = 4
guess = input("Guess the number ")

while(guess != number):
    if(guess > number):
        print("Guess is incorrect and higher than the actual. Please enter a number between 0-9.")
    else:
        print("Guess is incorrect and lower than the actual. Please enter a number between 0-9.")
    guess = input("Guess the number again ")

print("Hurray!!! Your guess is correct")

""" Here the variables number is integer and guess is string. When any value is entered for the guess, an error message is shown as 'TypeError: unorderable types: str() > int()' """

""" Observed that comparison of strings and integers is not allowed if we use '>' or '<' and is allowed when '==' or '!=' is used. In the below code we end up in an infinte loop
    because string is being compared to an integer and the computer is not able to analyze the situation even if the guess is correct. """

# number = 4
# guess = input("Guess the number ")
#
# while(guess != number):
#     guess = input("Guess the number again ")
# print("Hurray!!! Your guess is correct")

""" converting string into an integer and comparing integers """
# number = 4
# guess = input("Guess the number ")
#
# while(int(guess) != number):
#     if(int(guess) > number):
#         print("Guess is incorrect and higher than the actual. Please enter a number between 0-9.")
#     else:
#         print("Guess is incorrect and lower than the actual. Please enter a number between 0-9.")
#     guess = input("Guess the number again ")
#
# print("Hurray!!! Your guess is correct")
