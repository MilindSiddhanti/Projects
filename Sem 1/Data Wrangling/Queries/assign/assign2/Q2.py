number = "4"
guess = input("Guess the number ")

if(guess == number):
    print("The guess is right. Hurray!!")
elif(guess > number):
    print("The guess is wrong and higher than the actual. Please enter a number between 0-9.")
elif(guess < number):
    print("The guess is wrong and lower than the actual. Please enter a number between 0-9.")
else:
    print("The guess is wrong. Enter a number between 0-9. Better luck Next Time!!!")


""" Here the variables number and guess are strings. When the guess is entered as '10', the message shown to the user is
    'The guess is wrong and lower than the actual. Please enter a number between 0-9.'. This is because strings are compared 'lexicographically', character by character
    until they are not equal or there aren't any characters left to compare. The first character of '10' is less than the first character of '4' and hence the above print message
    is displayed. If the guess is '50', then the message 'The guess is wrong and higher than the actual. Please enter a number between 0-9.' is displayed as the first character
    of '50' is greater the first character of '4'. """

""" When values such as 'a', 'ab', 'abc' are entered as a guess, the message is displayed as 'The guess is wrong and higher than the actual. Please enter a number between 0-9.',
    as it translates a to it's ASCII character and then compares it with the number. """
