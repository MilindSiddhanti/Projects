-- Name the object that is clicky
SELECT * FROM noises;
-- 2 rows

SELECT * FROM noises WHERE noises.name = "Clicky";
-- 1 row

SELECT * FROM noises, objects WHERE noises.name = "Clicky";
-- 3 rows

SELECT * FROM noises, objects WHERE noises.name = "Clicky" and  objects.noise_id = noises.id;
-- 1 row

SELECT objects.name FROM noises, objects WHERE noises.name = "Clicky" and  objects.noise_id = noises.id;
-- pen

SELECT * FROM noises JOIN objects ON noises.id = objects.noise_id;
-- 3 rows

SELECT * FROM noises JOIN objects ON noises.id = objects.noise_id WHERE noises.name = "Clicky";
-- 1 row

SELECT objects.name FROM noises JOIN objects ON noises.id = objects.noise_id WHERE noises.name = "Clicky";
-- pen
