INSERT INTO books(id, title) VALUE ('1', 'HARRY POTTER');

INSERT INTO people(id, name) VALUE ('101', 'J K ROWLING');

INSERT INTO book_roles(id, role, book_id, person_id) VALUE ('201', 'AUTHOR', '1', '101');

INSERT INTO books(title) VALUES ('FAMOUS FIVE'), ('TITANIC');

INSERT INTO people(name) VALUES ('ENID BLYTON'), ('JAMES CAMEROON');

INSERT INTO book_roles(role, book_id, person_id) VALUES ('AUTHOR', '2', '102'), ('AUTHOR', '3', '103');

SELECT *
FROM books
WHERE books.title = 'HARRY POTTER';

SELECT id
FROM books
WHERE books.title = 'HARRY POTTER';

SELECT *
FROM books
WHERE books.id = 1;

DELETE
FROM books
WHERE books.id = 1;
