-- P1: What is the name of the person who spent the most (and how much did they spend)? (Vebil Miesys,765)
SELECT *
FROM tickets


SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id

SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id
  JOIN people
    ON purchases.person_id = people.id

SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id
  JOIN people
    ON purchases.person_id = people.id
ORDER BY person_id

SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id
  JOIN people
    ON purchases.person_id = people.id
GROUP BY person_id

SELECT person_id, name, SUM(price)
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id
  JOIN people
    ON purchases.person_id = people.id
GROUP BY person_id

SELECT person_id, name, SUM(price) as sum_price
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id
  JOIN people
    ON purchases.person_id = people.id
GROUP BY person_id
ORDER BY sum_price DESC
LIMIT 1

-- P2: Which performance had the highest revenue? (ticket prices are incoming income, thus revenue) (ID: 41)

SELECT *
FROM tickets

SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id

SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
ORDER BY performance_id

SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
GROUP BY performance_id

SELECT performance_id, SUM(price)
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
GROUP BY performance_id

SELECT performance_id, SUM(price) as revenue
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
GROUP BY performance_id
ORDER BY revenue DESC
LIMIT 1

-- P3: Which performance was the most profitable? (profit is revenue - costs.  bands.fee is the cost) (ID: 41)

SELECT *
FROM tickets

SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id

SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
ORDER BY performance_id

SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY performance_id

SELECT performance_id, SUM(price) - fee as profit
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY performance_id

SELECT performance_id, SUM(price) - fee as profit
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY performance_id
ORDER BY profit DESC
LIMIT 1

-- P4: Which band was the least profitable for the festival? (note: bands are paid their fee for each performance, hint: dealing with that requires a COUNT(DISTINCT ...) within a group. Look closely at the rows in the group by using ORDER BY, which column might tell you the number of shows?) (If you have trouble with this one, just do as though they are only paid the fee once). (WAX)

SELECT *
FROM tickets

SELECT *
FROM tickets
  JOIN purchases
    ON tickets.purchase_id = purchases.id

SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
ORDER BY band_id

SELECT *
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
ORDER BY band_id
HAVING COUNT(DISTINCT venue_id)

SELECT name, SUM(price) - fee as profit
FROM tickets
  JOIN performances
    ON tickets.performance_id = performances.id
  JOIN bands
    ON performances.band_id = bands.id
GROUP BY band_id
HAVING COUNT(DISTINCT venue_id)
ORDER BY profit ASC
LIMIT 1

--P5: Which venues were oversold (and what were their capacities)? (hint: this should use a HAVING clause) (oversold requires math around capacity   COUNT(DISTINCT purchase_id)  number of purchases- capacity) (AMD)

SELECT *
FROM venues

SELECT *
FROM venues
  JOIN performances
    ON performances.venue_id = venues.id
  JOIN tickets
    ON tickets.performance_id = performances.id

SELECT *
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
ORDER BY venue_id

SELECT *
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
ORDER BY performance_id

SELECT *
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
GROUP BY performance_id, venue_id

SELECT performance_id, venue_id, name, COUNT(ticketnum), capacity, COUNT(ticketnum) - capacity as oversold
  FROM venues
    JOIN performances
      ON performances.venue_id = venues.id
    JOIN tickets
      ON tickets.performance_id = performances.id
    JOIN purchases
      ON tickets.purchase_id = purchases.id
GROUP BY performance_id, venue_id
HAVING COUNT(ticketnum) - capacity > 0

-- P6: What was the total revenue from ticket sales each month? (hint: needs MONTH(date) in the SELECT clause, MONTH() extracts the number of the month from the date) (Month 7: 260668. Note: Please indicate the total revenue for each month, we have just provided one month for checking your work.)

SELECT *
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id

SELECT MONTH(date), SUM(price)
FROM purchases
  JOIN tickets
    ON tickets.purchase_id = purchases.id
GROUP BY MONTH(date)

--P7: What was the average purchase total each month? (note: purchase total is the sum of the price of tickets bought together in the same purchase, Q1 in in-class exercises.) (two ways to get this done, one that uses AVG and that uses divide using the slash character /). (Month 7:196.4341)

SELECT Month(date), SUM(price)/COUNT(DISTINCT purchase_id) as "average_purchase_total"
  FROM purchases
    JOIN tickets
      ON tickets.purchase_id = purchases.id
GROUP BY Month(date)
