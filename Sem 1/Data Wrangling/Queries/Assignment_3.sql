"Assignment 3 Queries - Milind Siddhanti"


-- What fee does Cut Copy charge?
select * from bands;

select * from bands where bands.name = "Cut Copy";

select bands.fee from bands where bands.name = "Cut Copy";
"754"

-- What was the price of ticket number TA2080128?
select * from tickets;

select * from tickets where tickets.ticketnum = "TA2080128";

select tickets.price from tickets where tickets.ticketnum = "TA2080128";
"48"

-- Which venues have capacities greater than 2000?
select * from venues;

select * from venues where venues.capacity > 2000;
"id = 3, name = Austin Kiddie Limits presented by H-E-B, capacity = 2300"
select venues.name where venues.capacity > 2000;
"Austin Kiddie Limits presented by H-E-B"

-- When did Domhog Kiwter purchase his tickets?
select * from people;

select * from people where people.name = "Domhog Kiwter";
"id = 71, name = Domhog Kiwter"
-----------------------------------------select people.id from people where people.name = "Domhog Kiwter";

select * from purchases;

select * from purchases where purchases.person_id = 71;

select purchases.date from purchases where purchases.person_id = 71;
"2011-04-28 00:05:06"

select purchases.date from people join purchases where people.id = purchases.person_id and people.name = "Domhog Kiwter";

-- Which bands performed at the venue with more than 2000 capacity?
select * from venues;
-- num of rows: 10
select * from venues where venues.capacity > 2000;
"id = 3, name = Austin Kiddie Limits presented by H-E-B, capacity = 2300"
-- num of rows: 1

select * from performances;
-- num of rows: 132
select * from performances where performances.venue_id = 3;
-- 16 results
select performances.band_id from performances where performances.venue_id = 3;
-- ids are ('11', '16', '22', '23', '30', '48', '55', '100', '108')

select * from bands;
-- num of rows: 134
select * from bands where id in ('11', '16', '22', '23', '30', '48', '55', '100', '108');

select bands.name from bands where id in ('11', '16', '22', '23', '30', '48', '55', '100', '108');
"Heidi Swedberg"
"Brady Rymer"
"Sara Hickman"
"Quinn Sullivan"
"Recess Monkey"
"The School of Rock"
"Mariana Iranzi"
"The Q Brothers"
"Peter DiStefano & Tor"
