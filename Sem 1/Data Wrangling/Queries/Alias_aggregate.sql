SELECT SUM(tickets.price) AS "Sum of Tickets"
FROM tickets

SELECT SUM(tickets.price) AS Sum_of_Tickets
FROM tickets

what is the average city_mpg for each manufacturer name?
SELECT *
FROM cars

SELECT cars.manufacturer, cars.city_mpg
FROM cars
ORDER BY cars.manufacturer

SELECT cars.manufacturer, AVG(cars.city_mpg)
FROM cars
GROUP BY cars.manufacturer

how many different models does each manufacturer make?
SELECT *
FROM cars

SELECT cars.manufacturer, cars.model
FROM cars
ORDER BY cars.manufacturer

SELECT cars.manufacturer, COUNT(DISTINCT cars.model)
FROM cars
GROUP BY cars.manufacturer
