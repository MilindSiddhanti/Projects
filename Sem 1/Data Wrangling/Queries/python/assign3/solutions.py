# filename = "foundation-input.csv"
filename = "foundation.csv"

import csv
import pprint

with open(filename) as csvfile:
    myCSVReader = csv.DictReader(csvfile,
                                 delimiter=",",
                                 quotechar='"')

    # for each column, detect largest value
    # set up some variables to hold the needed column width
    longest_field_in_column = {} # keys will come as we go through the rows.

    # need to set initial width value to header length for each column
    # DictReader provides access to the headers (had to google that)
    for header in myCSVReader.fieldnames:
        longest_field_in_column[header] = len(header)

    pprint.pprint(longest_field_in_column)

    # Now need to detect the largest item in each column.
    # Going to check each field and test if it is longer
    # than the longest seen. First one will be longer than 0.
    # move row by row through the file

    for row in myCSVReader:
    # Now move field by field through.
        # pprint.pprint(row)
        for header in row: # go key by key through the row dict
            curr_length = len(row[header])
            if (curr_length > longest_field_in_column[header]):
                longest_field_in_column[header] = curr_length
        # pprint.pprint(longest_field_in_column)
    # At end of this we'll have looked at each field and stored
    # the longest in each column
    pprint.pprint(longest_field_in_column)

    # close the file and re-open it.
with open(filename) as csvfile:
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    #-------------------------------------
    # Now we know, in advance, the size of the column
    # It's longest_field_in_column[header] + 2
    # Now we can output the file. Have to go through again, though.

    header_row = ""
    for header in myCSVReader.fieldnames:
        curr_field_length = len(header)
        # Now how many spaces will we need?
        num_spaces = longest_field_in_column[header] - curr_field_length
        # Add column padding
        num_spaces += 2

        header_row = header_row + header + " " * num_spaces
    print(header_row)

    # Dividing row, copied the code above.
    dividing_row = ""
    # retain consistent order
    for header in myCSVReader.fieldnames:
        curr_field_length = longest_field_in_column[header]
        # We need longest_field --- and then 2 spaces.
        dividing_row = dividing_row + "-" * curr_field_length + "  "
    print(dividing_row)

    for row in myCSVReader:
        row_to_output = "" # build up a row to output
        # Need to output things in a consistent order
        for header in myCSVReader.fieldnames:
            # get length of current field
            curr_field_length = len(row[header])
            # Now how many spaces will we need?
            num_spaces = longest_field_in_column[header] - curr_field_length
            # Add column padding
            num_spaces += 2

            field_with_padding  =  row[header] + " " * num_spaces

            # add to row
            row_to_output = row_to_output + field_with_padding

        print(row_to_output)
