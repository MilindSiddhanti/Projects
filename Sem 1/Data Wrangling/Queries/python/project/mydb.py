import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="milind_siddhanti",  # mysql user
            passwd="nokiayu7k",  # mysql passd
            db="milind_siddhanti_music_festival",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    with open('venues-header.csv') as csvfile:
        myCSVReader = csv.DictReader(csvfile)

        sql = """INSERT INTO venues(name,capacity)
                    VALUE (%(name)s,%(capacity)s)"""

        for row in myCSVReader:
            # use row directly when csv headers match column names.
            cursor.execute(sql, row)

    with open('venues-diff-header.csv') as csvfile:
        myCSVReader = csv.DictReader(csvfile)

        sql = """INSERT INTO venues(name,capacity)
                    VALUE (%(some_name_in_csv)s,%(capacity_in_csv)s)"""

        for row in myCSVReader:
            # use row directly when csv headers match column names.
            cursor.execute(sql, row)
