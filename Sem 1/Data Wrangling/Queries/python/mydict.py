namedict = {
            "name1" : "abc",
            "name2" : "xyz"
            }
#print(', '.join(namedict.values()) + ", " + ', '.join(namedict.keys()))

ex = { "day": "Wednesday",
	     "count": 45,
         }

ex1 = { "day": "Tuesday",
	     "count": 34,
         }

#print(ex)

list = [ex, ex1]
print(list)

list4 = [{ "day": "Wednesday", "count": 45}, { "day": "Tuesday", "count": 34}]
print(list4)

list1 = ["wednesday", 45]
list2 = ["tuesday", 34]
list3 = [list1, list2]
print(list3)
