# SELECT bands.name, performances.start, venues.name
# FROM performances
#     JOIN bands
#       ON performances.band_id = bands.id
#     JOIN venues
#       ON performances.venue_id = venues.id

import pymysql.cursors
import pprint
import csv

connection = pymysql.connect(
            host="localhost",
            user="testuser",
            passwd="rSs8xvnAQHFS5Men",
            db="class_music_festival",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    sql = """SELECT bands.name as band_name, performances.start as performance_start_time, venues.name as venue_name FROM performances
             JOIN bands ON performances.band_id = bands.id JOIN venues ON performances.venue_id = venues.id """
    cursor.execute(sql)
    results = cursor.fetchall()
    #print(sql)
    #pprint.pprint(results)

    column_names = results[0].keys()
    print(column_names)

    for row in results:

        filename = "{}.csv".format(row['venue_name'])
        print(filename.rsplit('.', 1)[0] + '  ' + row['venue_name'])

        if(row['venue_name'] == filename.rsplit('.', 1)[0]):
            print("inside the if loop")
            with open(filename,'w') as csvfile:
            # with open('q5_0.csv', 'w') as csvfile:

                myCsvWriter = csv.DictWriter(csvfile,
                                             delimiter=',',
                                             quotechar='"',
                                             fieldnames = column_names)

                myCsvWriter.writeheader()
                for row in results:
                    if(row['venue_name'] == filename.rsplit('.', 1)[0]):
                        myCsvWriter.writerow(row)
