import csv
import os.path as pth

# A list of dicts.
venues = [{'id': 1, 'capacity': 700, 'name': 'AMD'},
{'capacity': 2000, 'id': 2, 'name': 'Honda'},
{'capacity': 2300, 'id': 3, 'name': 'Austin Kiddie Limits'},
{'capacity': 2000, 'id': 4, 'name': 'Austin Ventures'}]

#column_names = ['id','name','capacity']
column_names = venues[0].keys()

filename = "exported."
filenum = 0
while (pth.exists(pth.abspath(filename+str(filenum)+".csv"))):
    print("inside the while loop")
    filenum = filenum + 1
    #name = "{}{}.csv".format(filename, filenum)
    #print("name is "+ name)

#with open(name,'w') as csvfile:
with open("{}{}.csv".format(filename, filenum),'w') as csvfile:
    print("inside the with block")
# with open('exported.csv', 'w') as csvfile:

    myCsvWriter = csv.DictWriter(csvfile,
                                 delimiter=',',
                                 quotechar='"',
                                 fieldnames = column_names)

    myCsvWriter.writeheader() # uses contents of column_names

    for row in venues:
        print("writing the values")
        # every row must have all keys from column_names
        myCsvWriter.writerow(row)

print("Done writing csv.")
