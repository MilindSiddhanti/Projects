# import csv
#
# # A list of dicts.
# venues = [{'capacity': 700, 'id': 1, 'name': 'AMD'},
# {'capacity': 2000, 'id': 2, 'name': 'Honda'},
# {'capacity': 2300, 'id': 3, 'name': 'Austin Kiddie Limits'},
# {'capacity': 2000, 'id': 4, 'name': 'Austin Ventures'}]
#
# column_names = ['id','name','capacity']
# # column_names = venues[0].keys()
#
# with open('exported_2.csv', 'w') as csvfile:
#
#     myCsvWriter = csv.DictWriter(csvfile,
#                                  delimiter=',',
#                                  quotechar='"',
#                                  fieldnames = column_names)
#
#     myCsvWriter.writeheader() # uses contents of column_names
#
#     for row in venues:
#         if(int(row["capacity"]) >= 2000):
#             # every row must have all keys from column_names
#             myCsvWriter.writerow(row)
#             print("file has capacity more than 2000 venues")
#         else:
#             print("not writing anything in the file")
#
# print("Done writing csv.")


import csv

# A list of dicts.
venues = [{'capacity': 700, 'id': 1, 'name': 'AMD'},
{'capacity': 2000, 'id': 2, 'name': 'Honda'},
{'capacity': 2300, 'id': 3, 'name': 'Austin Kiddie Limits'},
{'capacity': 2000, 'id': 4, 'name': 'Austin Ventures'}]

column_names = ['id','name','capacity']
column_names.append('venue_type')
print(column_names)
# column_names = venues[0].keys()

with open('exported_3.csv', 'w') as csvfile:

    myCsvWriter = csv.DictWriter(csvfile,
                                 delimiter=',',
                                 quotechar='"',
                                 fieldnames = column_names)

    myCsvWriter.writeheader() # uses contents of column_names

    for row in venues:
        row['venue_type'] = 'outdoor'
        # every row must have all keys from column_names
        myCsvWriter.writerow(row)


print("Done writing csv.")
