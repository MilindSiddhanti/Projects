import pprint
import csv

with open('venues.csv') as csvfile:
    # tell python about the specific csv format
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    # move row by row through the file as though it was a list.
    for row in myCSVReader:
     #Each row arrives in Python as a Dict
     pprint.pprint(row)
     print("{venues} has {capacity} seats".format(venues = row["name"], capacity = row["capacity"]))
