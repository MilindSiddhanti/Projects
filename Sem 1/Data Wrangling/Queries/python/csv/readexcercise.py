import pprint
import csv

with open('excercise.csv') as csvfile:
    # tell python about the specific csv format
    myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

    # move row by row through the file as though it was a list.
    count = 0
    for row in myCSVReader:
        count = count + 1
        #Each row arrives in Python as a Dict
        #pprint.pprint(row)
        lowscore = 50
        if(int(row["Score"]) < lowscore):
            print("{venues} has {capacity} points".format(venues = row["Restaurant Name"], capacity = int(row["Score"])))
