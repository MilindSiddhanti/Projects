from bs4 import BeautifulSoup
import requests

import pandas
import numpy
import config
import getpass
from pymongo import MongoClient

url = "http://transcripts.wikia.com/wiki/Harry_Potter_and_the_Philosopher%27s_Stone"
request_url  = requests.get(url)
data = request_url.text
#soup = BeautifulSoup(data)
soup = BeautifulSoup(data, "lxml")

paragraph = str(soup.find_all(['p', '/p', 'i', '/i', '\n']))
#paragraph = paragraph.split('<p>')    #split all <p> tags
#paragraph = paragraph.split(['<p>', '</p>', '<i>', '</i>', '\n'])

scene_sequence = 0  #initialize scene_sequence and sequence variables
sequence = 0
scene = ''#reset variables to null for every occurence in the loop
character = ''
description = ''
text = ''
user = config.DATABASE_CONFIG['user']
pwd = config.DATABASE_CONFIG['password']
userName = ''
password = ''

#if(pass == true):
 #   userAuthentication()
#else:
 #   print("Authentication failed!!. Please retry again.")
  #  getUserInput()
#def getUserInput(pass):
def main():
    uName, upass = getUserInput()
    userDetails(uName, upass)
    return true

def getUserInput():
    userName = input("Please provide the user name for the database: ")
    password = getpass.getpass("Please provide the password for the database: ")
    return userName, password
#getUserInput(userName = input("Please provide the user name for the database: "), password = getpass.getpass("Please provide the password for the database: "))

def userDetails(userName, password):
    if((userName != user) | (password != pwd)):
        print("Authentication failed! Please retry.")
        getUserInput()
    else:
        return userName, password

main()
print(userName)
print(password)
while true:
    mongoConn = MongoClient('mongodb://'+userName+':'+password+'@'+config.DATABASE_CONFIG['host']+':'+str(config.DATABASE_CONFIG['port'])+'/'+config.DATABASE_CONFIG['dbname'])
    print("Connection successful? ------" +mongoConn)

    #def userAuthentication():
    database = mongoConn[config.DATABASE_CONFIG['dbname']]
    collectionName = (config.DATABASE_CONFIG['collectionname'])
    title = input("Please provide the name of the Harry Potter movie: ")
    seriesNumber = input("Please provide the series number of the Harry Potter movie: ")
    #for row in paragraph1:
    #for count in range(1,20):
    for count in range(1,len(paragraph)):
        if 'Scene:' in paragraph[count]: #if "Scene:" is found, store that paragraph
            scene = paragraph[count].split(':')[1]
            scene = scene.split('<')[0] # remove html \n and </p> tags from description
            scene_sequence = scene_sequence + 1
            sequence = 0

        elif '<b>' in paragraph[count]:
            scene = paragraph[count]
            scene_sequence = scene_sequence + 1
            sequence = 0

        elif ':' in paragraph[count]:
            text = paragraph[count].split(':')[1]
            text = text.split('\\')[0] # remove html \n and </p> tags from lines
            character = paragraph[count].split(':')[0]
            character = character.split('(')[0] # remove descriptions from character name
            sequence = sequence + 1

    	#Code to split lines
    	#        if '{' in text and '}' in text:
    	#            buffer_text = text
    	#            buffer_description = buffer_text.split('{')[0]
    	#            print('************')
    	#            print(buffer_description)
    	#            buffer_description = buffer_text.split('{')[1]
    	#            buffer_description2 = buffer_description.split('}')[0]
    	#            text = buffer_description.split('}')[1]
    	#            print('************')
    	#            print(buffer_description2)

        elif '{' in paragraph[count] and '}' in paragraph[count]:
            description = paragraph[count]
            sequence = sequence + 1

        elif '(' in paragraph[count] and ')' in paragraph[count]:
            buffer_desc = paragraph[count].split('(')[1] #removing the "(" and ")"
            description = buffer_desc.split(')')[0]
            if '<i>' in description:
                buffer_desc = buffer_desc.split('<i>')[1]
                description = buffer_desc.split('</i>')[0]
                sequence = sequence + 1

    	#    db.createCollection("nimish")
    	#    db.nimish.insert ({
    	#            "Title":"Harry Potter test data",
    	#            "Series":1,
    	#            "Sequence":1,
    	#            "Contents": [{
    	 #               "Sequence":sequence,
    	#                    "Description":description,
    	#                    "Lines":text,
    	#                    "Character":character
    	#                    }]
    	#            })

    	# argumentParser = argparse.ArgumentParser(description='Please provide the name of the Harry Potter movie')
    	# argumentParser.add_argument('-f', '--filename', required=True, dest='filename', type=str,
    				   # help='The name of the file from which to read the images')



    	# insert mongoDB code here

    	#mongoConn = MongoClient((config.DATABASE_CONFIG['host'] + ":" + str(config.DATABASE_CONFIG['port'])))
    	#mongoConn = MongoClient('mongodb://HarryTranscripts:TranscriptsPotter!@saab.ischool.utexas.edu:22/HarryPotterTranscripts')
    	#db = mongoConn.HarryPotterTranscripts
    	#mongoConn.database.authenticate(config.DATABASE_CONFIG['user'],config.DATABASE_CONFIG['password'])
    	#print('username:' + userName + 'password:' + password + 'databaseName:' + database)

    	#for record in collectionName.find():
        print('*****************************')

        print('---------' + 'title' + '---------')
        print(title)

        print('---------' + 'seriesNumber' + '---------')
        print(seriesNumber)
        print('---------' + 'scene' + '---------')
        print(scene)

        print('---------' + 'description' + '---------')
        print(description)

        print('---------' + 'text' + '---------')
        print(text)

        print('---------' + 'character' + '---------')
        print(character)

        print('---------' + 'scene sequence' + '---------')
        print(scene_sequence)

        print('---------' + 'sequence' + '---------')
        print(sequence)

        print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        #print(row)
        new_posts = [{
        'movie' :{
                'title':title,
                'seriesNumber':seriesNumber
                },
        'sequence':scene_sequence,
        'scene':scene,
        'contents' :[
                    {
                    'sequence':sequence,
                    'character':character,
                    'text':text
                    },
                    {
                    'sequence':sequence,
                    'description':description
                    }
                    ]
        }]
        print('!@#$%^&*()(*&^%$#@#$%^&*(*&^%$#@#$%^&*')
        print(new_posts)
        database.collectionName.insert_many(new_posts)
