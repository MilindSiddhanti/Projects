import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="testuser",  # mysql user
            passwd="rSs8xvnAQHFS5Men",  # mysql passd
            db="class_music_festival",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    min_capacity = input("What is the minimum capacity needed? ")

    param_dict = { "min_cap": min_capacity }

    # The placeholder format is %(keyname)s where keyname is the
    # appropriate key in the param_dict. This differs from .format
    # where the placeholder would have been {keyname}
    sql_placeholder = "SELECT * FROM venues WHERE capacity >= %(min_cap)s"

    # Then we pass the param_dict along with the query
    # string to cursor.execute
    cursor.execute(sql_placeholder, param_dict)
    # print(cursor.execute)
    print(sql_placeholder, param_dict)

    results = cursor.fetchall()

    if(results != ()):
        print("i am here")
        pprint.pprint(results)
    else:
        print("Results not available")
