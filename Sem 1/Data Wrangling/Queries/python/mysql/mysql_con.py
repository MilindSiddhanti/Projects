import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="testuser",  # mysql user
            passwd="rSs8xvnAQHFS5Men",  # mysql passd
            db="class_music_festival",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    # SQL queries are just a string.
    sql = "SELECT * FROM venues"
    cursor.execute(sql)
    results = cursor.fetchall()  # list of dicts

    # examine query and results
    print(sql)
    pprint.pprint(results) # see, a list of dicts

    column_names = results[0].keys()
    print(column_names)

    with open('results_0.csv', 'w') as csvfile:

        myCsvWriter = csv.DictWriter(csvfile,
                                     delimiter=',',
                                     quotechar='"',
                                     fieldnames = column_names)

        myCsvWriter.writeheader() # uses contents of column_names

        for row in results:
            # every row must have all keys from column_names
            myCsvWriter.writerow(row)


print("Done writing csv.")
