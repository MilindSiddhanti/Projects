"""A short script to demonstrate branching."""
# No branching

destination = "home"

print("Done with work, I'm off {}".format(destination))

##############
# One branch
#############

day = input("What is today?") #"Friday"

# Is it Friday yet?
if (day == "Friday"):
    destination = "bar"
else:
    destination = "gym"

print("Done with work, I'm off {}".format(destination))

##########
# Two branches
##########

day = input("What day is today? ")

# Is it Friday yet?
if (day == "Friday"):
    destination = "bar"
else:
    if (day == "Wednesday"):
        destination = "Park"
    else:
        destination = "gym"

print("Done with work, I'm off {}".format(destination))
##########################

# 3 conditions

day = input("What day is today? ")

if(day == "Friday"):
    destination = "bar"
else:
    if(day == "Saturday"):
        destination = "park"
    else:
        if(day == "Sunday"):
            destination = "Party"
        else:
            if(day == "Monday"):
                destination = "Home"
            else:
                if(day == "Tuesday"):
                    destination = "dog walk"
                else:
                    if(day == "Wednesday"):
                        destination = "school"
                    else:
                        destination = "gym"
print("Done with work, off to {}".format(destination))
