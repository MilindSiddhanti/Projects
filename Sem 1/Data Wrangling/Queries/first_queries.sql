select * from cars;

select * from cars where cars.cylinders = 4;

select * from cars where cars.cylinders = 6;

select * from cars where cars.cylinders = 4 and cars.year < 2000;

select cars.manufacturer, cars.model from cars where cars.cylinders = 4 and cars.year < 2000;

select cars.manufacturer, cars.model from cars where cars.cylinders = 6 and cars.year < 2000;

select * from cars where cars.manufacturer = "Audi";

select * from cars where cars.manufacturer = "Audi" OR cars.manufacturer = "Ford"

select * from cars where cars.year = 2008 and cars.manufacturer = "Audi" OR cars.manufacturer = "Ford"

select * from cars where (cars.manufacturer = "Audi" OR cars.manufacturer = "Ford") and cars.year = 2008

select * from cars where cars.year = 2008 and cars.manufacturer in ("Ford", "Audi")


select purchases.date from purchases where person_id in (select people.id from people where people.name = "Domhog Kiwter");
"2011-04-28 00:05:06"

select distinct bands.name from venues, performances, bands where venues.capacity > 2000
and venues.id = performances.venue_id and performances.band_id = bands.id;
"Heidi Swedberg"
"Brady Rymer"
"Sara Hickman"
"Quinn Sullivan"
"Recess Monkey"
"The School of Rock"
"Mariana Iranzi"
"The Q Brothers"
"Peter DiStefano & Tor"
