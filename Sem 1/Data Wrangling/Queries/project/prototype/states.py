import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="milind_siddhanti",  # mysql user
            passwd="nokiayu7k",  # mysql passd
            db="milind_siddhanti_project",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:

    with open('states.csv') as csvfile:
        myCSVReader = csv.DictReader(csvfile)

# change names in placeholder to match names in csv file.
        sql = """INSERT INTO states(id,name,population,life_expectancy,abbreviation)
        VALUE (%(id)s,%(name)s,%(population)s,%(life_expectancy)s,%(abbreviation)s)"""

        for row in myCSVReader:
    # use row directly
            cursor.execute(sql, row)
