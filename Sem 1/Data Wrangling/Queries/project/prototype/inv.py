import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="milind_siddhanti",  # mysql user
            passwd="nokiayu7k",  # mysql passd
            db="milind_siddhanti_project",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    sql_select_blood_bank = "SELECT id from blood_banks WHERE website = %(website)s"

    sql_insert_inventories = """INSERT INTO inventories(quantity_cc,blood_type,blood_bank_id)
                    VALUE (%(quantity_cc)s, %(blood_type)s, %(blood_bank_id)s)"""

    with open('inventories.csv') as csvfile:
        # tell python about the specific csv format
        myCSVReader = csv.DictReader(csvfile, delimiter=",", quotechar='"')

        for row in myCSVReader:
            cursor.execute(sql_select_blood_bank, row)
            results = cursor.fetchone()
            if(results!=()):
                blood_bank_id = results['id']

            # we have what we need.
            param_dict = {'blood_bank_id': blood_bank_id,
                          'quantity_cc': row['quantity_cc'],
                          'blood_type': row['blood_type']}
            cursor.execute(sql_insert_inventories, param_dict)
