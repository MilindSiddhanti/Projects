"1) What are the names of the bands that perform in the BMI venue?"

-- Start off with searching for the venue in venue table
SELECT *
FROM venues
-- 10 venues for bands to perform

-- Fetching the details of the venue by name as mentioned in the question
SELECT *
FROM venues
WHERE venues.name = "BMI"
-- 1 row with id as 5, capacity of 2000 and name of the venue is BMI.

-- Moving along tables to fetch the performance record
SELECT *
FROM venues
  JOIN performances
    ON venues.id = performances.venue_id
WHERE venues.name = "BMI"
-- 15 rows with venue as BMI and performances start and end time along with band id

-- Moving on to bands table to get the details of the bands performed in the given venue
SELECT *
FROM venues
  JOIN performances
    ON venues.id = performances.venue_id
  JOIN bands
    ON bands.id = performances.band_id
WHERE venues.name = "BMI"
--15 rows with venue and performances in cartesian with the bands table.

-- Determining the name of the bands that has performed in the given venue
SELECT bands.name
FROM venues
  JOIN performances
    ON venues.id = performances.venue_id
  JOIN bands
    ON bands.id = performances.band_id
WHERE venues.name = "BMI"
-- 15 rows with band names as below:
"Beardyman,
Bobby Long,
Chancellor Warhol,
Courtney Jaye,
Gary Clark Jr.,
Ha Ha Tonka,
Hudson Moore,
Jon Pardi,
MilkDrive,
Patrice Pike,
Pernikoff Brothers,
Seth Walker,
The Kingston Springs,
Tyler Bryant,
We Are Augustines"

"2) Who purchased ticket number TA9875424?"

-- Find whether data is a valid column or not by querying the ticket table
SELECT *
FROM tickets
-- 107673  rows

-- Filtering the table with the data that has been provided in the question
SELECT *
FROM tickets
WHERE tickets.ticketnum = "TA9875424"
-- 1 row with the details of the ticket number, performance and purchases.

-- Validating the purchase details of the ticket
SELECT *
FROM tickets
  JOIN purchases
    ON purchases.id = tickets.purchase_id
WHERE tickets.ticketnum = "TA9875424"
-- 1 row with the information of ticket number and purchase details.

-- Finding the person who has purchased the ticket by searching the people table
SELECT *
FROM tickets
  JOIN purchases
    ON purchases.id = tickets.purchase_id
  JOIN people
    ON people.id = purchases.person_id
WHERE tickets.ticketnum = "TA9875424"
-- 1 row with the details of the person who has purchased the ticket "TA9875424"

-- Pointing out the person id and name against the ticket purchase
SELECT people.name
FROM tickets
  JOIN purchases
    ON purchases.id = tickets.purchase_id
  JOIN people
    ON people.id = purchases.person_id
WHERE tickets.ticketnum = "TA9875424"
"Miplktip was the one who purchased the ticket with ticket number: TA9875424"


"3) When was the first ticket to a performance at the venue BMI purchased?"

-- Validate whether the data given is a part of venue table
SELECT *
FROM venue
-- 10 venues for bands to perform

-- Fetch the details of the venue given in the question
SELECT *
FROM venues
WHERE venues.name = "BMI"
-- 1 row, details of the venue with id and capacity of the venue

-- Moving towards the performance table to get a match in data
SELECT *
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
WHERE venues.name = "BMI"
-- 15 rows that match the venue with the performances in that venue

--Moving on to the tickets table to Validate the sales of the tickets for the performance
SELECT *
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN tickets
     ON tickets.performance_id = performances.id
WHERE venues.name = "BMI"
-- 12130 tickets were sold for the performances in the BMI venue

-- including purchases table to figure out the purchase dates
SELECT *
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN tickets
     ON tickets.performance_id = performances.id
   JOIN purchases
     ON tickets.purchase_id = purchases.id
WHERE venues.name = "BMI"
-- 12130 tickets were purchased for the performance of the bands

-- Fetching only the purchase dates
SELECT purchases.date
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN tickets
     ON tickets.performance_id = performances.id
   JOIN purchases
     ON tickets.purchase_id = purchases.id
WHERE venues.name = "BMI"
-- 12130 records of purchase dates (not sorted)

-- Determining the period of purchase dates
SELECT purchases.date
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN tickets
     ON tickets.performance_id = performances.id
   JOIN purchases
     ON tickets.purchase_id = purchases.id
WHERE venues.name = "BMI"
ORDER BY purchases.date ASC
-- 12130 tickets were purchased between 2011-01-01 07:13:51 - 2011-09-16 10:52:51 for the venue BMI

--Limiting the query to fetch the first ticket as mentioned in the question
SELECT purchases.date
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN tickets
     ON tickets.performance_id = performances.id
   JOIN purchases
     ON tickets.purchase_id = purchases.id
WHERE venues.name = "BMI"
ORDER BY purchases.date ASC
LIMIT 1
"The first ticket was purchased on 2011-01-01 07:13:51 for the performance at the BMI venue"


"4) When is the first show (band name, venue name and start time) that Domhog Kiwter has a ticket to?"

-- Search the people table for data
SELECT *
FROM people
--10768 rows of data retrieved

-- Search the table people for data provided in the question
SELECT *
FROM people
WHERE people.name = "Domhog Kiwter"
-- 1 record was found with the name given above in the people table

-- Move on to find whether the person has made any purchases
SELECT *
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
WHERE people.name = "Domhog Kiwter"
-- 1 record of purchase done by the person

-- search the tickets table to find the ticket number purchased by the person
SELECT *
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
   JOIN tickets
     ON tickets.purchase_id = purchases.id
WHERE people.name = "Domhog Kiwter"
-- 17 tickets were purchased by the person

-- Query the performances table to find out for which performances did the person purchase the tickets for
SELECT *
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
   JOIN tickets
     ON tickets.purchase_id = purchases.id
   JOIN performances
     ON performances.id = tickets.performance_id
WHERE people.name = "Domhog Kiwter"
-- 17 tickets were purchased for various performances

-- Finding out the band names for the performances and tickets purchased
SELECT *
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
   JOIN tickets
     ON tickets.purchase_id = purchases.id
   JOIN performances
     ON performances.id = tickets.performance_id
   JOIN bands
     ON bands.id = performances.band_id
WHERE people.name = "Domhog Kiwter"
--  Different band names for which the tickets were purchased for the performance by the person given in the question

-- Find the venues where the bands performed
SELECT *
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
   JOIN tickets
     ON tickets.purchase_id = purchases.id
   JOIN performances
     ON performances.id = tickets.performance_id
   JOIN bands
     ON bands.id = performances.band_id
   JOIN venues
     ON venues.id = performances.venue_id
WHERE people.name = "Domhog Kiwter"
-- All the details of the tables tickets, performances, bands, venues, purchases are in cartesian with the person, Domhog Kiwter

-- Fetching the specfics (band name, venue name and start time) from the list
SELECT bands.name, venues.name, performances.start
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
   JOIN tickets
     ON tickets.purchase_id = purchases.id
   JOIN performances
     ON performances.id = tickets.performance_id
   JOIN bands
     ON bands.id = performances.band_id
   JOIN venues
     ON venues.id = performances.venue_id
WHERE people.name = "Domhog Kiwter"
-- 17 rows of data with the specifics is derived

-- Sort the shows and find the first show details
SELECT bands.name, venues.name, performances.start
FROM people
   JOIN purchases
     ON purchases.person_id = people.id
   JOIN tickets
     ON tickets.purchase_id = purchases.id
   JOIN performances
     ON performances.id = tickets.performance_id
   JOIN bands
     ON bands.id = performances.band_id
   JOIN venues
     ON venues.id = performances.venue_id
WHERE people.name = "Domhog Kiwter"
ORDER BY performances.start
LIMIT 1
"The first show that Domhog Kiwter had a ticket to was band name: Cults, venue name: Honda, start time: 2011-09-16 11:45:00"


"5) Which band that performed at the AMD venue had the highest fee?"

-- Query the table venues to find the venue name given in the question
SELECT *
FROM venues
-- 10 entries of venues were found

-- Find the venue mentioned in the question
SELECT *
FROM venues
WHERE venues.name = "AMD"
-- 1 entry with the id and capacity of the venue is returned

-- Moving through the performances table to determine the fee charged by bands
SELECT *
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
WHERE venues.name = "AMD"
-- 14 performances were given at the venue AMD

-- Search for the band names using the band_id coulumn in performances table with id of bands table
SELECT *
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN bands
     ON bands.id = performances.band_id
WHERE venues.name = "AMD"
-- Merged the details of the bands table to the existing 14 records

-- Trim the data for the band name and band name column
SELECT bands.name, bands.fee
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN bands
     ON bands.id = performances.band_id
WHERE venues.name = "AMD"
-- Data trimmed down to 2 columns

-- Sort the data from highest to lowest fee
SELECT bands.name, bands.fee
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN bands
     ON bands.id = performances.band_id
WHERE venues.name = "AMD"
ORDER BY bands.fee DESC
-- Data sorted by highest to lowest with respect to the fee column in bands table

-- Find the band with highest fee by applying the LIMIT funcion
SELECT bands.name, bands.fee
FROM venues
   JOIN performances
     ON performances.venue_id = venues.id
   JOIN bands
     ON bands.id = performances.band_id
WHERE venues.name = "AMD"
ORDER BY bands.fee DESC
LIMIT 1
"Ryan Bingham & The Dead Horses is the band that offers the highest fee of 2585 to perform at the AMD venue"
