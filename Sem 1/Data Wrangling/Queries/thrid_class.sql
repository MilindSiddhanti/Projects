select * from cars where cars.cylinders = 4 order by cars.city_mpg desc limit 5;
-- limits the sort results to top 5

--names of people that made purchases prior to "2011-03-17 12:00:00"
select * from purchases;
--10768 rows

select * from purchases where purchases.date < "2011-03-17 12:00:00";
--3057 rows

select * from purchases
  join people
   on people.id = purchases.person_id
 where purchases.date < "2011-03-17 12:00:00";
--3057 rows

select people.name from purchases
  join people
   on people.id = purchases.person_id
 where purchases.date < "2011-03-17 12:00:00";
--3057 rows

--who made the purchase immediately prior to "2011-03-17 12:00:00"
select people.name, purchases.date from purchases
  join people
   on people.id = purchases.person_id
 where purchases.date < "2011-03-17 12:00:00"
order by purchases.date desc
limit 1;
-- 1 row
"Mik Reeh on 2011-03-17 11:46:06 "


--What color are the clicky objects?
select * from noises;
-- 2 rows
select * from noises where noises.name = "Clicky";
-- 1 row

select * from noises
   join objects
    on objects.noise_id = noises.id
  where noises.name = "clicky";
-- 1 row with clicky pen

select * from noises
   join objects
    on objects.noise_id = noises.id
   join colors
    on objects.color_id = colors.id
  where noises.name = "clicky";
-- 1 row

select colors.name from noises
   join objects
    on objects.noise_id = noises.id
   join colors
    on objects.color_id = colors.id
  where noises.name = "clicky";
-- red

--ticket number purchased by "Domhog Kiwter"

select * from people;
--10768 rows

select * from people
where people.name = "Domhog Kiwter";
-- 1 row

select * from people
    join purchases
     on purchases.person_id = people.id
where people.name = "Domhog Kiwter";
-- 1 row

select * from people
    join purchases
     on purchases.person_id = people.id
    join tickets
     on tickets.purchase_id = purchases.id
where people.name = "Domhog Kiwter";
-- 17 rows

select tickets.ticketnum, people.name, purchases.date from people
    join purchases
     on purchases.person_id = people.id
    join tickets
     on tickets.purchase_id = purchases.id
where people.name = "Domhog Kiwter";
-- 17 rows

--the name of the band for the last show that "Domhog Kiwter" will attend?

select tickets.ticketnum, people.name, purchases.date from people
    join purchases
     on purchases.person_id = people.id
    join tickets
     on tickets.purchase_id = purchases.id
where people.name = "Domhog Kiwter";

--17 rows

select tickets.ticketnum, people.name, purchases.date, performances.id from people
    join purchases
     on purchases.person_id = people.id
    join tickets
     on tickets.purchase_id = purchases.id
    join performances
     on performances.id = tickets.performance_id
where people.name = "Domhog Kiwter";
-- 17 rows

select tickets.ticketnum, people.name, purchases.date, bands.id from people
    join purchases
     on purchases.person_id = people.id
    join tickets
     on tickets.purchase_id = purchases.id
    join performances
     on performances.id = tickets.performance_id
    join bands
     on bands.id = performances.band_id
where people.name = "Domhog Kiwter";
-- 17 rows

select tickets.ticketnum, people.name, purchases.date, bands.name, performances.start from people
  join purchases
    on purchases.person_id = people.id
  join tickets
    on tickets.purchase_id = purchases.id
  join performances
    on performances.id = tickets.performance_id
  join bands
    on bands.id = performances.band_id
where people.name = "Domhog Kiwter"
order by performances.start desc
limit 1;
"Arcade Fire"
