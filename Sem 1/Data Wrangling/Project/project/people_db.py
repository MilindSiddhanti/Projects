import pymysql.cursors
import pprint
import csv
from datetime import datetime

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="milind_siddhanti",  # mysql user
            passwd="nokiayu7k",  # mysql passd
            db="milind_siddhanti_project",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    sql_select_state = "SELECT id from states WHERE abbreviation = %(state)s"

    sql_insert_people = """INSERT INTO people(first_name,last_name,state_id,phone,email,blood_type,dob)
            VALUE (%(first_name)s,%(last_name)s,%(state_id)s,%(phone)s,%(email)s,%(blood_type)s,%(dob)s)"""

    with open('people.csv') as csvfile:
        myCSVReader = csv.DictReader(csvfile)

        for row in myCSVReader:
            start_str = row['dob']
            template = "%m/%d/%Y"
            date_of_birth = datetime.strptime(start_str, template)

            cursor.execute(sql_select_state, row)
            results = cursor.fetchone()
            if(results != ()):
                state_id = results['id']

            param_dict = {'state_id': state_id,
                        'first_name': row['first_name'],
                        'last_name': row['last_name'],
                        'phone': row['phone'],
                        'email': row['email'],
                        'blood_type': row['blood_type'],
                        'dob' : date_of_birth
                         }
            cursor.execute(sql_insert_people, param_dict)
