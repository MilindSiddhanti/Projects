import pymysql.cursors
import pprint
import csv

# First set up the connection to the server
connection = pymysql.connect(
            host="localhost",
            user="milind_siddhanti",  # mysql user
            passwd="nokiayu7k",  # mysql passd
            db="milind_siddhanti_project",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    with open('inventories.csv') as csvfile:
        myCSVReader = csv.DictReader(csvfile)

        sql = """INSERT INTO inventories(quantity_cc,blood_type)
                    VALUE (%(quantity_cc)s,%(blood_type)s)"""

        for row in myCSVReader:
            # use row directly when csv headers match column names.
            cursor.execute(sql, row)
