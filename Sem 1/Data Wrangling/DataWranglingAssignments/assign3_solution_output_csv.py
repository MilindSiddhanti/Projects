"""Read csvs and output formatted text."""

import csv
import pprint

print("---------------Q2.1----------------")

# SELECT DISTINCT oversolds.venue_name, oversolds.capacity
# FROM (
# 	SELECT performances.id,
# 	       venues.name as venue_name,
# 	       COUNT(*) as tickets_sold,
# 		   venues.capacity
# 	FROM venues, performances, tickets
# 	WHERE venues.id = performances.venue_id
# 		AND tickets.performance_id = performances.id
# 	GROUP BY performances.id
# 	HAVING tickets_sold > venues.capacity
# ) as oversolds


out = "The venue {venue_name} with capacity {capacity} was oversold"

with open('query5.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile)

    for row in myCSVReader:
        print(out.format(**row))

print("---------------Q2.2----------------")

# SELECT MONTH(purchases.date) as month_of_purchase,
#        SUM(tickets.price) as total_revenue_per_month
# FROM tickets, purchases
# WHERE tickets.purchase_id = purchases.id
# GROUP BY MONTH(purchases.date)

out = "Tickets sold in month {month_of_purchase} brought {total_revenue_per_month} revenue."

with open('query6.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile)

    for row in myCSVReader:
        print(out.format(**row))

print("---------------Q2.3----------------")
# SELECT MONTH(purchases.date) as month_of_purchase,
#        SUM(tickets.price) / COUNT(DISTINCT tickets.purchase_id) as
#        avg_purchase_total
# FROM tickets, purchases
# WHERE tickets.purchase_id = purchases.id
# GROUP BY MONTH(purchases.date)

out = "Purchases made in month {month_of_purchase} averaged {avg_purchase_total}"

with open('query7.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile)

    for row in myCSVReader:
        # pprint.pprint(row)
        print(out.format(**row))

print("---------------Q2.3 formatted----------------")
# or can do some cool formatting using .format
# note that this doesn't work if the columns are read as strings which
# depends on the csv exporting. Here the csv export didn't carry over
# the type info from phpmyadmin (but running the query directly would)
out = "Purchases made in month {month_of_purchase} averaged ${avg_purchase_total:.2f}"

with open('query7.csv') as csvfile:
    myCSVReader = csv.DictReader(csvfile)

    for row in myCSVReader:
        # pprint.pprint(row)
        print(out.format(month_of_purchase=row["month_of_purchase"],
                         avg_purchase_total=float(row["avg_purchase_total"])))

# Note that structuring long strings in python source code is a little
# tricky. The Python style guide says don't have lines longer than 80 chars.
# but often you want longer lines when outputting sentences etc.

# Here are some options:

# too long
long_string = "Purchases made in month {month_of_purchase} averaged ${avg_purchase_total}"

# using += appends strings together.
long_string = "Purchases made in month {month_of_purchase} averaged"
long_string += "${avg_purchase_total}"

# take advantage of quirk of python compiler that merges adjacent string
# declarations
merge_strings = "This is joined with"        "that"  # even without a +
# use \ for line continuation
long_string = "Purchases made in month {month_of_purchase} averaged" \
               "${avg_purchase_total}"

# put parens around two adjacent string declarations (don't need \)
long_string = ("Purchases made in month {month_of_purchase} averaged"
               "${avg_purchase_total}")

# triple quoted string options
long_string = """Purchases made in month {month_of_purchase} averaged
               ${avg_purchase_total}"""

# ok, but adds a newline. You can use a \ inside a triple quoted string
# to avoid it being added.
long_string = """Purchases made in month {month_of_purchase} averaged \
               ${avg_purchase_total}"""

# A strong possibility (because it allows copying and pasting) uses
# the triple quoted string and removes newlines,
# and white space at front and back.
long_string = """
Purchases made in month {month_of_purchase} averaged ${avg_purchase_total}
and some really long run on stuff
""".replace("\n", "").strip()

print(long_string)
