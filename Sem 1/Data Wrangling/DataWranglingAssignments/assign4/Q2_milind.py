# SELECT people.name, tickets.ticketnum, purchases.date
# FROM tickets
#     JOIN purchases
#       ON tickets.purchase_id = purchases.id
#     JOIN people
#       ON people.id = purchases.person_id
# ORDER BY purchases.date asc
# LIMIT 15

import pymysql.cursors
import pprint
import csv

connection = pymysql.connect(
            host="localhost",
            user="testuser",
            passwd="rSs8xvnAQHFS5Men",
            db="class_music_festival",
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor
            )

with connection.cursor() as cursor:
    sql = """SELECT people.name, tickets.ticketnum, purchases.date FROM tickets JOIN purchases ON tickets.purchase_id = purchases.id
             JOIN people ON people.id = purchases.person_id ORDER BY purchases.date ASC LIMIT 15 """
    cursor.execute(sql)
    results = cursor.fetchall()
    print(sql)
    pprint.pprint(results)

    column_names = results[0].keys()
    print(column_names)

    with open('q2.csv', 'w') as csvfile:

        myCsvWriter = csv.DictWriter(csvfile,
                                     delimiter=',',
                                     quotechar='"',
                                     fieldnames = column_names)

        myCsvWriter.writeheader()

        for row in results:
            myCsvWriter.writerow(row)

print("Done writing csv.")
