define(['common/ctrljs/app'], function (myApp) {


myApp.config(function($stateProvider, $urlRouterProvider, routeStateLoaderProvider) {
	
	var route = routeStateLoaderProvider.route;
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
    .state('Receive',{
	    url: "/Receive:action?CSRFToken",
	    views: {
	    	'': {
	    		templateUrl: 'Receive/Config/ReceiveFrame.html',
	    		controller: 'receiveController',
	    		resolve   :  route.resolve('Receive/Controller/receiveController')
	    		},
	    	'receiveNavigator@Receive': {
	    		templateUrl: 'Receive/View/receivingNavigator.html',
	    		controller: 'receiveNavigatorController',
	    		resolve   :  route.resolve('Receive/Controller/receiveNavigatorController')
	    	},
	    	'criteria@Receive': {
    	    		templateUrl: 'Receive/View/frmReceiveDocTypes.html',
    	    		controller: 'receiveControllerDocTypes',
    	    		resolve   :  route.resolve('Receive/Controller/receiveControllerDocTypes')
	    	},
	    },
     })
		
		  .state('Receive.po',{
        	url: '/po',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveFindPO.html',
                	controller: 'receiveControllerFPO',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerFPO')
        		},
        	},
        	
        })
         
         .state('Receive.blindreceipt',{
        	url: '/blindreceipt',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveBlindReceivePOcreate.html',
                	controller: 'receiveControllerBlindReceiveFPO',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerBlindReceiveFPO')
        		},
        	},
        	
        })

        .state('Receive.pallets',{
        	url: '/pallets',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceivePallets.html',
                	controller: 'receiveControllerPallets',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerPallets')
        		},
        	},
        	
        })
         .state('Receive.itemExtraWork',{
        	url: '/itemExtraWork',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveExtraWork.html',
                	controller: 'receiveExtraWorkController',
                	resolve   :  route.resolve('Receive/Controller/receiveExtraWorkController')
        		},
        	},
        	
        })
        .state('Receive.itemScan',{
        	url: '/itemScan',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveItemScan.html',
                	controller: 'receiveControllerItemScan',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerItemScan')
        		},
        		
        	},
        	
        })
        
 .state('Receive.itemTag',{
        	url: '/itemTag',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveItemTag.html',
                	controller: 'receiveControllerItemTag',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerItemTag')
        		},
        		
        	},
        	
        })
        
         .state('Receive.itemTimeSensitive',{
        	url: '/itemTimeSensitive',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveItemTimeSensitive.html',
                	controller: 'receiveControllerItemTimeSensitive',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerItemTimeSensitive')
        		},
        		
        	},
        	
        })
        
        .state('Receive.itemSerial',{
        	url: '/itemSerial',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveItemSerial.html',
                	controller: 'receiveControllerItemSerial',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerItemSerial')
        		},
        		
        	},
        	
        })
/*
               .state('Receive.itemDim',{
        	url: '/itemDim',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveCaseItemDimension.html',
                	controller: 'receiveControllerCaseItemDimension',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerCaseItemDimension')
        		},
        		
        	},
        	
        })
   */     
           .state('Receive.itemDetails',{
        	url: '/itemDetails',
        	views: {
        		'criteria@Receive': {
        			templateUrl: 'Receive/View/frmReceiveItemDetails.html',
                	controller: 'receiveControllerItemDetails',
                	resolve   :  route.resolve('Receive/Controller/receiveControllerItemDetails')
        		},
        		
        	},
        	
        });
        
});
});
