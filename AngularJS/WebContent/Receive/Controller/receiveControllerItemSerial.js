/*******************************************************************************
 * Name: receiveControllerItemSerial
 * Usage: Controller for receive module to render frmReceiveItemSerial.html view 
 * Purpose: This Controller is loaded if the item scanned is a serial tracked 
 * item to capture the serial details.
 ******************************************************************************/


define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerItemSerial', function($scope, $rootScope,$timeout,$location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){
		$rootScope.receive_moduleFlow = 'Item Serial';
		$rootScope.receive_NavActive = "Item Serial";
		console.log("Serial controller");

		var formName = 'frmReceiveitemserial';
		console.log("serial  page");
		var itemId = $rootScope.ItemID;

		$rootScope.ScannedQty=0;
		var serArray= [[]];
		//var secSerArray=[];
		var OverallFlag = false;
		var secserial;
		var serial_no;
		
		/*****************getSerialList Api Xml formation*****************************************/

		function getSerialListApi(serial_no){
			var postObject = new Object();
			postObject.CommandName = "getSerialList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Serial";
			postObject.InputXml.SerialNo = serial_no;

			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName= "InventoryItem";
			postObject.InputXml.childNodes[0].ItemID= itemId;

			postObject.Template = new Object();
			postObject.Template.tagName = "SerialList";
			postObject.Template.TotalNumberOfRecords="";

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object SerialList "+JSON.stringify(postObject));
			return postObject;
		};
		/*****************initial textbox not shown*****************************************/
		$scope.showsecserial = false;
		$scope.showSecSerial1 = false;
		$scope.showSecSerial2 = false;
		$scope.showSecSerial3 = false;
		$scope.showSecSerial4 = false;
		$scope.showSecSerial5 = false;
		$scope.showSecSerial6 = false;
		$scope.showSecSerial7 = false;
		$scope.showSecSerial8 = false;
		$scope.showSecSerial9 = false;
		
		
		var ItemNodeContext= $rootScope.ItemNodeContext; 
		console.log("ItemNodeContext");
		console.log(ItemNodeContext);
		console.log("ItemNodeContext.Item.PrimaryInformation.NumSecondarySerials:" + ItemNodeContext.Item.PrimaryInformation.NumSecondarySerials);
		secserial= ItemNodeContext.Item.PrimaryInformation.NumSecondarySerials;
		console.log("secserial:" + secserial);

		if(secserial > 0){
			
			// check for no of secondary serials
			$scope.showsecserial = true;


			if(secserial == 1){
				console.log("1st");		          			
				$scope.showSecSerial1 = true;
			}

			if(secserial == 2){
				console.log("2nd");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;
			}

			if(secserial == 3){
				console.log("3rd");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
			}

			if(secserial == 4){
				console.log("4th");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
				$scope.showSecSerial4 = true;
			}

			if(secserial == 5){
				console.log("5th");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
				$scope.showSecSerial4 = true;
				$scope.showSecSerial5 = true;
			}
			if(secserial == 6){
				console.log("6th");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
				$scope.showSecSerial4 = true;
				$scope.showSecSerial5 = true;
				$scope.showSecSerial6 = true;
			}

			if(secserial == 7){
				console.log("7th");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
				$scope.showSecSerial4 = true;
				$scope.showSecSerial5 = true;
				$scope.showSecSerial6 = true;
				$scope.showSecSerial7 = true;
			}
			if(secserial == 8){
				console.log("8th");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
				$scope.showSecSerial4 = true;
				$scope.showSecSerial5 = true;
				$scope.showSecSerial6 = true;
				$scope.showSecSerial7 = true;
				$scope.showSecSerial8 = true;
			}

			if(secserial == 9){
				console.log("9th");
				$scope.showsecserial = true;
				$scope.showSecSerial1 = true;
				$scope.showSecSerial2 = true;	
				$scope.showSecSerial3 = true;
				$scope.showSecSerial4 = true;
				$scope.showSecSerial5 = true;
				$scope.showSecSerial6 = true;
				$scope.showSecSerial7 = true;
				$scope.showSecSerial8 = true;
				$scope.showSecSerial9 = true;
			}

		}// end if sec serial exists

		
		processContoller.InputController = function(scannedVariable){
			$scope.ErrorMessage = "";

			var ScannedData = $scope.$apply(formName+'_'+scannedVariable);
			if(scannedVariable == "serialno"){
				serial_no= ScannedData;

				if(serial_no){

					var seriallistObject=getSerialListApi(serial_no);
					invokeApi.async(seriallistObject).then(function(seriallistResponse) {
						$scope.postResponse = seriallistResponse;
						console.log("item detail Output" +seriallistResponse );

						var tnr= $scope.postResponse.SerialList.TotalNumberOfRecords;


						if(tnr == 1){
							$scope.ErrorMessage= "Serial No already exists";
						}  

						else {

							
							if(secserial == 0){
								
								console.log("without sec serial");
								console.log("Serial Array length : "+serArray.length);
								var serialFlag = false;
								if(serArray.length >1){
									for(var i=0; i < serArray.length; i++){
										console.log("inside for loop")
										if(serial_no == serArray[i].serialno){
											console.log("Serial no exists in the array");
											$scope.ErrorMessage= "Duplicate serial number";
											$scope.frmReceiveitemserial_serialno="";
											serialFlag = true;
											break;
											
										}
									}
									if(!serialFlag)
									{
											console.log("Serial no doesnt exists in the array")
											serArray.push({serialno:$scope.frmReceiveitemserial_serialno});
											//serArray[serArray.length].serialno == serial_no;
											$rootScope.ScannedQty++;
											$scope.frmReceiveitemserial_serialno="";
											$scope.ErrorMessage= "";
									}
								}
								
								else {
										console.log("Serial array len is 0")
										serArray.push({serialno:$scope.frmReceiveitemserial_serialno});
										//serArray[serArray.length].serialno == serial_no;
										$rootScope.ScannedQty++;
										$scope.frmReceiveitemserial_serialno="";
										$scope.ErrorMessage= "";
							   }
								/*
								if(serArray.indexOf($scope.frmReceiveitemserial_serialno)==-1){
									console.log("into the without sec serial loop");
									serArray.push({serialno:$scope.frmReceiveitemserial_serialno});
									$rootScope.ScannedQty++;
									$scope.frmReceiveitemserial_serialno="";
									$scope.ErrorMessage= "";
								}else{
									$scope.ErrorMessage= "Duplicate serial number";
								}*/
								
								console.log("out of the without sec serial loop");

							}// end of no secondory serial
						}// if no duplicate serial number in system		
					}); //	end of getSerialListApi invoke

				}
			}

		};// end of process controller


			$scope.serialsave = function(){
				
				
				var SecondSerial1Flag = false;
				var SecondSerial2Flag = false;
				var SecondSerial3Flag = false;
				var SecondSerial4Flag = false;
				var SecondSerial5Flag = false;
				var SecondSerial6Flag = false;
				var SecondSerial7Flag = false;
				var SecondSerial8Flag = false;
				var SecondSerial9Flag = false;
				secSerArray=[];
								
				console.log($scope.frmReceiveitemserial_serialno + " $scope.frmReceiveitemserial_serialno");
				console.log(SecondSerial1Flag + " SecondSerial1Flag");
				console.log(SecondSerial2Flag + " SecondSerial2Flag");
				console.log(SecondSerial3Flag + " SecondSerial3Flag");
				console.log(SecondSerial4Flag + " SecondSerial4Flag");
				console.log(SecondSerial5Flag + " SecondSerial5Flag");
				console.log(SecondSerial6Flag + " SecondSerial6Flag");
				console.log(SecondSerial7Flag + " SecondSerial7Flag");
				console.log(SecondSerial8Flag + " SecondSerial8Flag");
				console.log(SecondSerial9Flag + " SecondSerial9Flag");
				
				if(angular.isDefined($scope.frmReceiveitemserial_serialno) && $scope.frmReceiveitemserial_serialno.length>0){
																			
					if($scope.showSecSerial1 || $scope.showSecSerial2 || $scope.showSecSerial3 || $scope.showSecSerial4 || $scope.showSecSerial5 || 
							$scope.showSecSerial6 || $scope.showSecSerial7 || $scope.showSecSerial8 || $scope.showSecSerial9 ){

						if($scope.showSecSerial1){
							if(angular.isDefined($scope.frmReceiveitemserial_secs1) && $scope.frmReceiveitemserial_secs1.length>0){
								
								SecondSerial1Flag = true;
								secSerArray.push({key1:$scope.frmReceiveitemserial_secs1});
								//serArray.push({ serialno:$scope.frmReceiveitemserial_serialno, secserials:[{key1:$scope.frmReceiveitemserial_secs1}]});
							}else{
								//$scope.ErrorMessage= "";
								$scope.ErrorMessage= "Enter Secondary Serial1";
						    }
						}else{
							console.log("true in else loop");
							SecondSerial1Flag = true;
						}
						
						if($scope.showSecSerial2){
							if(angular.isDefined($scope.frmReceiveitemserial_secs2) && $scope.frmReceiveitemserial_secs2.length>0){
								
								SecondSerial2Flag = true;
								
								secSerArray.push({key2:$scope.frmReceiveitemserial_secs2});
							}else{
								//$scope.ErrorMessage= "";
								$scope.ErrorMessage= "Enter Secondary Serial2";
						    }
						}
						else{
							SecondSerial2Flag = true;
						}
						if($scope.showSecSerial3){
							if(angular.isDefined($scope.frmReceiveitemserial_secs3) && $scope.frmReceiveitemserial_secs3.length>0){
								
								SecondSerial3Flag = true;
								
								secSerArray.push({key3:$scope.frmReceiveitemserial_secs3});
							}else{
								//$scope.ErrorMessage= "";
								$scope.ErrorMessage= "Enter Secondary Serial3";
						    }
						}else{
							SecondSerial3Flag = true;
						}
						
						if($scope.showSecSerial4){
							if(angular.isDefined($scope.frmReceiveitemserial_secs4) && $scope.frmReceiveitemserial_secs4.length>0){
							
							SecondSerial4Flag = true;
							
							secSerArray.push({key4:$scope.frmReceiveitemserial_secs4});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Secondary Serial4";
							}
						}
						else{
							SecondSerial4Flag = true;
						}
						
						if($scope.showSecSerial5){
						if(angular.isDefined($scope.frmReceiveitemserial_secs5) && $scope.frmReceiveitemserial_secs5.length>0){
							
							SecondSerial5Flag = true;
							
							secSerArray.push({key5:$scope.frmReceiveitemserial_secs5});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Secondary Serial5";
					    }
						}
						else{
							SecondSerial5Flag = true;
						}
						
						if($scope.showSecSerial6){
						if(angular.isDefined($scope.frmReceiveitemserial_secs6) && $scope.frmReceiveitemserial_secs6.length>0){
							
							SecondSerial6Flag = true;
							
							secSerArray.push({key6:$scope.frmReceiveitemserial_secs6});
							}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Secondary Serial6";
					    }
					}else{
						SecondSerial6Flag = true;
					}
						
						if($scope.showSecSerial7){
							if(angular.isDefined($scope.frmReceiveitemserial_secs7) && $scope.frmReceiveitemserial_secs7.length>0){
								
								SecondSerial7Flag = true;
								
								secSerArray.push({key7:$scope.frmReceiveitemserial_secs7});
							}else{
								//$scope.ErrorMessage= "";
								$scope.ErrorMessage= "Enter Secondary Serial7";
						    }
							
								
						}else{
							SecondSerial7Flag = true;
						}
						
						if($scope.showSecSerial8){
							if(angular.isDefined($scope.frmReceiveitemserial_secs8) && $scope.frmReceiveitemserial_secs8.length>0){
								
								SecondSerial8Flag = true;
								
								secSerArray.push({key8:$scope.frmReceiveitemserial_secs8});
							}else{
								//$scope.ErrorMessage= "";
								$scope.ErrorMessage= "Enter Secondary Serial8";
						    }
						}else{
							SecondSerial8Flag = true;
						}
						
						
						
						if($scope.showSecSerial9){
							if(angular.isDefined($scope.frmReceiveitemserial_secs9) && $scope.frmReceiveitemserial_secs9.length>0){
								
								SecondSerial9Flag = true;
								
								secSerArray.push({key9:$scope.frmReceiveitemserial_secs9});
							}else{
								//$scope.ErrorMessage= "";
								$scope.ErrorMessage= "Enter Secondary Serial9";
						    }
						}else{
							SecondSerial9Flag = true;
						}
					
						
                        console.log("check====> "+SecondSerial1Flag && SecondSerial2Flag && SecondSerial3Flag && SecondSerial4Flag && SecondSerial5Flag && 
								SecondSerial6Flag && SecondSerial7Flag && SecondSerial8Flag && SecondSerial9Flag);
                        
						if(SecondSerial1Flag && SecondSerial2Flag && SecondSerial3Flag && SecondSerial4Flag && SecondSerial5Flag && 
								SecondSerial6Flag && SecondSerial7Flag && SecondSerial8Flag && SecondSerial9Flag ){
							OverallFlag = true;
			
								console.log("serArray "+serArray);
								console.log("serArray stringify "+JSON.stringify(serArray));
								var serialFlag=false;
							    console.log(serArray.length);
							    if(serArray.length>1){
									for(i=0; i < serArray.length; i++){
										console.log("inside for loop");
										console.log(serial_no+" serial_no");
										console.log(serArray[i].serialno+" serArray[i].serialno");
										if(serial_no == serArray[i].serialno){
											console.log("serial no matches in the array");
											$scope.ErrorMessage= "Duplicate serial number";
											$scope.frmReceiveitemserial_serialno="";
											$scope.frmReceiveitemserial_secs1="";
											$scope.frmReceiveitemserial_secs2="";
											$scope.frmReceiveitemserial_secs3="";
											$scope.frmReceiveitemserial_secs4=""; 
											$scope.frmReceiveitemserial_secs5="";
											$scope.frmReceiveitemserial_secs6="";
											$scope.frmReceiveitemserial_secs7="";
											$scope.frmReceiveitemserial_secs8="";
											$scope.frmReceiveitemserial_secs9="";
											serialFlag = true;
											break;
										}
									}
									if(!serialFlag){
										console.log("Serial no doesnt exists in the array");
										serArray.push([{serialno:$scope.frmReceiveitemserial_serialno}, secSerArray]);
											//serArray[serArray.length].serialno == serial_no;
										$rootScope.ScannedQty++;
										$scope.ErrorMessage= "";
										$scope.frmReceiveitemserial_serialno="";
										$scope.frmReceiveitemserial_secs1="";
										$scope.frmReceiveitemserial_secs2="";
										$scope.frmReceiveitemserial_secs3="";
										$scope.frmReceiveitemserial_secs4=""; 
										$scope.frmReceiveitemserial_secs5="";
										$scope.frmReceiveitemserial_secs6="";
										$scope.frmReceiveitemserial_secs7="";
										$scope.frmReceiveitemserial_secs8="";
										$scope.frmReceiveitemserial_secs9="";
											
									}
										
								}    
							    
							    
								else {
											console.log("here at the else line 199")
											serArray.push([{serialno:$scope.frmReceiveitemserial_serialno}, secSerArray]);
											//serArray[serArray.length].serialno == serial_no;
											$rootScope.ScannedQty++;
											$scope.frmReceiveitemserial_serialno="";
											$scope.frmReceiveitemserial_secs1="";
											$scope.frmReceiveitemserial_secs2="";
											$scope.frmReceiveitemserial_secs3="";
											$scope.frmReceiveitemserial_secs4=""; 
											$scope.frmReceiveitemserial_secs5="";
											$scope.frmReceiveitemserial_secs6="";
											$scope.frmReceiveitemserial_secs7="";
											$scope.frmReceiveitemserial_secs8="";
											$scope.frmReceiveitemserial_secs9="";
											$scope.ErrorMessage= "";
								   }
								
							/*
								if(serArray.indexOf($scope.frmReceiveitemserial_serialno)==-1){
									serArray.push($scope.frmReceiveitemserial_serialno);
									$rootScope.ScannedQty++;
									$scope.frmReceiveitemserial_serialno="";
									$scope.frmReceiveitemserial_secs1="";
									$scope.frmReceiveitemserial_secs2="";
									$scope.frmReceiveitemserial_secs3="";
									$scope.frmReceiveitemserial_secs4=""; 
									$scope.frmReceiveitemserial_secs5="";
									$scope.frmReceiveitemserial_secs6="";
									$scope.frmReceiveitemserial_secs7="";
									$scope.frmReceiveitemserial_secs8="";
									$scope.frmReceiveitemserial_secs9="";
									$scope.ErrorMessage= "";
							}else{
								$scope.ErrorMessage= "Duplicate serial number";
								}*/
								
						}	
				
					}
			  }
				
			 else {
    				console.log("2nd else");
					$scope.ErrorMessage= "Enter Serial No";
			  }
				
			$rootScope.serArray = serArray;
			//$rootScope.sersecArray = sersecArray;
			
			};
					
				$scope.serialdone = function(){	
					
					
					if(secserial > 0){
						
						if(OverallFlag){
							console.log("GLOBAL ITEM ID");
							console.log($rootScope.ItemID);
							console.log("Call the item detail screen");
							console.log("ARRAY" + JSON.stringify(serArray));
							
							$location.path('/Receive/itemDetails');
			                				
						}
						else{
							$scope.ErrorMessage="Enter Mandatory Fields";
						}
						
		                				
					}						
					else {
									
						//if(angular.isDefined($scope.frmReceiveitemserial_serialno) && $scope.frmReceiveitemserial_serialno.length>0){
						if($rootScope.ScannedQty > 0){
	      	                console.log(OverallFlag); 
							console.log("GLOBAL ITEM ID");
							$rootScope.serArray = serArray;
							console.log($rootScope.ItemID);
							console.log("Call the item detail screen ");
							console.log("ARRAY" + JSON.stringify(serArray));
							$location.path('/Receive/itemDetails');
			                
						}else {
				    		console.log("done else");
							$scope.ErrorMessage= "Enter Serial No";
						}
					}
								
			};
				
				
	});


});