define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerFPO', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){
		var formName = 'frmReceivepo';
		var recevingNode = $cookieStore.get('globals').OrganizationCode;
		$rootScope.recevingNode=recevingNode;
		$rootScope.receive_moduleFlow = 'Find Shipment';
		$rootScope.receive_NavActive = "Shipments";
		console.log("FPO controller");
		
		
		
		$scope.OrderBasedFlag= false;
		$rootScope.ReceiptFlag = false;
		var Logger = $log.getInstance($state.$current);
		

		Logger.info("Log from ReveiveController FPO"); 
		
		/*****************getOrganizationList Api Xml formation*****************************************/
		function getOrganizationListApi(recevingNode){
			var postObject = new Object();
			postObject.CommandName = "getOrganizationList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Organization";
			postObject.InputXml.OrganizationCode = recevingNode;
			postObject.Template = new Object();
			postObject.Template.tagName = "OrganizationList";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Organization";
			postObject.Template.childNodes[0].PrimaryEnterpriseKey="";		
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object org hir "+JSON.stringify(postObject));
			return postObject;
		}
		
	
		
		/*****************getOrderList Api Xml formation*****************************************/
		function getOrderListApi(orderNo){
			var postObject = new Object();
			postObject.CommandName = "getOrderList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Order";
			postObject.InputXml.DocumentType = "0005";
			//postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.OrderNo = orderNo;
			
			postObject.Template = new Object();
			postObject.Template.tagName = "OrderList";
			postObject.Template.TotalOrderList="";
			
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Order";
			postObject.Template.childNodes[0].OrderHeaderKey="";
			postObject.Template.childNodes[0].BillToID="";
			postObject.Template.childNodes[0].EnterpriseCode="";
			postObject.Template.childNodes[0].BuyerOrganizationCode="";
			postObject.Template.childNodes[0].SellerOrganizationCode="";
			postObject.Template.childNodes[0].OrderNo="";
		
			
			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="OrderLines";
			postObject.Template.childNodes[0].childNodes[0].TotalNumberOfRecords="";
			
			postObject.Template.childNodes[0].childNodes[0].childNodes = [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]= new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName = "OrderLine";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].OrderLineKey="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].OrderedQty="";
			
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes = [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0]= new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].tagName="ItemDetails";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].ItemID="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].UnitOfMeasure="";
			
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes = [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0]= new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].tagName="PrimaryInformation";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].Description="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].Status="";
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			
			console.log("postObject object order list  "+JSON.stringify(postObject));
			return postObject;
		};
		/*****************getShipmentList Api Xml formation*****************************************/
		function getShipmentListApi( recevingNode, shipmentKey, orderno){
			var postObject = new Object();
			postObject.CommandName = "getShipmentList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Shipment";
			postObject.InputXml.DocumentType = "0005";
			//postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.ReceivingNode = recevingNode;
			if($rootScope.OrderBasedFlag){
				postObject.InputXml.ShipmentKey = shipmentKey;
			}
			else{
				postObject.InputXml.ShipmentNo= shipmentKey;
			}
			postObject.InputXml.OrderNo=orderno;
			postObject.Template = new Object();
			postObject.Template.tagName = "Shipments";
			
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Shipment";
			postObject.Template.childNodes[0].DoNotVerifyCaseContent="";
			postObject.Template.childNodes[0].DoNotVerifyPalletContent="";	
			postObject.Template.childNodes[0].DocumentType="";	
			postObject.Template.childNodes[0].EnterpriseCode="";	
			postObject.Template.childNodes[0].ReceivingNode="";	
			postObject.Template.childNodes[0].SellerOrganizationCode="";	
			postObject.Template.childNodes[0].ShipmentNo="";	
			postObject.Template.childNodes[0].Status="" ;
			postObject.Template.childNodes[0].ShipmentKey="";	
			
			
			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="Containers";
			
			postObject.Template.childNodes[0].childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="Container";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerScm="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerNo="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerType="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ShipmentContainerKey="";
		
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			return postObject;
		};
		/*****************getReceiptList Api Xml formation*****************************************/
		function getReceiptListApi(EntCode, recevingNode, shipmentKey,orderno){
			var postObject = new Object();
			postObject.CommandName = "getReceiptList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Receipt";
			postObject.InputXml.DocumentType = "0005";
			postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.ReceivingNode = recevingNode;
			postObject.InputXml.ShipmentKey = shipmentKey;
			
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="Shipment";
			postObject.InputXml.childNodes[0].OrderNo=orderno;
			
			
			
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			return postObject;
		}
		
		
		processContoller.InputController = function(scannedVariable){
			$scope.ErrorMessage="";
		
			var ScannedData = $scope.$apply(formName+'_'+scannedVariable);
			
			if(scannedVariable == "shipment"){
				
				// check if it is valid order
				
				var orderPostObject = getOrderListApi(ScannedData);
				invokeApi.async(orderPostObject).then(function(orderResponse){
				console.log(orderResponse);
					    		 
				if(orderResponse.OrderList.TotalOrderList){
					$rootScope.orderContext = orderResponse;
					console.log("Order exists for the passed inputs");
					console.log(orderResponse);
					//console.log("Calling pallet screen as order is there but no shipment");
					$rootScope.OrderBasedFlag= true;
					
					// ADD the funda of checking if receipt exists. so as to populate the location field.
					var shpkey= "";
					var entNo=orderResponse.OrderList.EnterpriseCode;
					var postReciptlstObject = getReceiptListApi(entNo, recevingNode, shpkey,ScannedData);
			     	console.log(postReciptlstObject);
					invokeApi.async(postReciptlstObject).then(function(receiptResponse) {
			    		  console.log(receiptResponse);
			    		  $scope.postReceiptResponse = receiptResponse;
			    		  if($scope.postReceiptResponse.ReceiptList.TotalNumberOfRecords){
					    	  $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList; 
					    	  var receiptContext = $rootScope.receiptContext ;
					    	  console.log("Receipt Details");
							  console.log(receiptContext);
							  
							  var recpListarrayflag = angular.isArray(receiptContext.Receipt);
								
								if (recpListarrayflag){
									
									for (var i=0; i < receiptContext.Receipt.length; i++){
										
										if(receiptContext.Receipt[i].OpenReceiptFlag=="Y"){
											$rootScope.ReceiptFlag = true;
											locid= receiptContext.Receipt[i].ReceivingDock;
											var shpKey= receiptContext.Receipt[i].ShipmentKey;
											console.log("Open receipt is there for the order passed in loc " +locid );
											$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt[i]; 
									    	var receiptContext = $rootScope.receiptContext ;
									    	console.log("Receipt Details");
											console.log(receiptContext);
											  
											  //get the shipment Context:
											  
											 var postShipmentObject = getShipmentListApi(recevingNode, shpKey, ScannedData);
										     console.log(postShipmentObject);
											 invokeApi.async(postShipmentObject).then(function(shipResponse) {
													if(shipResponse.Shipments.Shipment){
												    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
												    	  console.log("Shipment Details");
														  console.log($rootScope.shipmentContext);
													
													}  
													
											});
								  
											break;							
										}
									
									}
								}
								else{
									
									if(receiptContext.Receipt.OpenReceiptFlag == "Y"){
										$rootScope.ReceiptFlag = true;
										locid= receiptContext.Receipt.ReceivingDock;
										var shpKey = receiptContext.Receipt.ShipmentKey;
										console.log("Open receipt is there for teh order passed in loc "+ locid);
										 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt; 
								    	  var receiptContext = $rootScope.receiptContext ;
								    	  console.log("Receipt Details");
										  console.log(receiptContext);
										  
										  //get the shipment Context:
										  
										  var postShipmentObject = getShipmentListApi(recevingNode, shpKey, ScannedData);
									     	console.log(postShipmentObject);
											invokeApi.async(postShipmentObject).then(function(shipResponse) {
												console.log("SHIPEMNT DETAILS ++"+shipResponse);
												if(shipResponse.Shipments.Shipment){
											    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
											    	  console.log("Shipment Details");
													  console.log($rootScope.shipmentContext);
												}  
												
											});
									}
								}
			    		  }
			    		  
			    		  console.log("Order based receipt flag:"+ $rootScope.ReceiptFlag);
						  if($rootScope.ReceiptFlag){
							  
							  $location.path('/Receive/pallets');
							
						  }
						  else{
							  $scope.ErrorMessage="Add the order to start the receipt";
						  }
			    	});
					
					
					
					
				}
				
				//check if valid shipment
				else {
							  
					var orderno="";
					var postObject = getShipmentListApi(recevingNode, ScannedData,orderno);
					console.log(postObject);
					invokeApi.async(postObject).then(function(response) {
					$scope.postResponse = response;
					console.log( $scope.postResponse);
							    
					if($scope.postResponse.Shipments.Shipment){
						$rootScope.ShipmentBasedFlag= true;
						console.log($scope.postResponse);
					    $rootScope.shipmentContext = $scope.postResponse.Shipments.Shipment;
						var EntCode=$scope.postResponse.Shipments.Shipment.EnterpriseCode; 	    	  
					    // var shipmentContext = $rootScope.shipmentContext || {};	
					    var postReciptlstObject = getReceiptListApi(EntCode, recevingNode, $rootScope.shipmentContext.ShipmentKey,orderno);
					    console.log(postReciptlstObject);
										
					    invokeApi.async(postReciptlstObject).then(function(receiptResponse) {
					    	console.log(receiptResponse);
					    	$scope.postReceiptResponse = receiptResponse;
					    	if($scope.postReceiptResponse.ReceiptList.TotalNumberOfRecords){
					    							  
					    		$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList; 
					    		var receiptContext = $rootScope.receiptContext ;
					    		console.log("Receipt Details");
					    		console.log(receiptContext);
					    							  
					    		var recpListarrayflag = angular.isArray(receiptContext.Receipt);
					    		console.log("recpListarrayflag" +recpListarrayflag);
					    								
					    		if (recpListarrayflag){
					    									
					    			for (var i=0; i < receiptContext.Receipt.length; i++){
					    										
					    				if(receiptContext.Receipt[i].OpenReceiptFlag=="Y"){
					    					$rootScope.ReceiptFlag = true;
					    					locid= receiptContext.Receipt[i].ReceivingDock;
					    					var shpKey= receiptContext.Receipt[i].ShipmentKey;
					    					console.log("Open receipt is there for the shipment passed in loc " +locid );
					    					$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt[i]; 
					    					var receiptContext = $rootScope.receiptContext ;
					    					console.log("Receipt Details");
					    					console.log(receiptContext);
					    											  
					    					//get the shipment Context:
					    											  
					    					var postShipmentObject = getShipmentListApi(recevingNode, shpKey, orderno);
					    					console.log(postShipmentObject);
					    					invokeApi.async(postShipmentObject).then(function(shipResponse) {
					    						if(shipResponse.Shipments.Shipment){
					    							$rootScope.shipmentContext = shipResponse.Shipments.Shipment;
					    							console.log("Shipment Details");
					    							console.log($rootScope.shipmentContext);
					    													
					    						}  
					    													
					    					});
					    					
					    					console.log("Valid Receipt");
		    								$rootScope.OrderBasedFlag= false;
		    								$location.path('/Receive/pallets');
					    					break;							
					    					}
					    									
					    				}
					    				if(!$rootScope.ReceiptFlag){
					    					console.log("Receipt doesn't Exsist for the Shipment");
						    				$scope.ErrorMessage="Receipt does not exsist for shipment. Add to receipt";
					    				}
					    			}
					    			else{
					    									
					    				if(receiptContext.Receipt.OpenReceiptFlag == "Y"){
					    					$rootScope.ReceiptFlag = true;
					    					locid= receiptContext.Receipt.ReceivingDock;
					    					var shpKey = receiptContext.Receipt.ShipmentKey;
					    					console.log("Open receipt is there for teh shipment passed in loc "+ locid);
					    					$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt; 
					    					var receiptContext = $rootScope.receiptContext ;
					    					console.log("Receipt Details");
					    					console.log(receiptContext);
					    										  
					    					//get the shipment Context:
					    										  
					    					var postShipmentObject = getShipmentListApi( recevingNode, shpKey, orderno);
					    					console.log(postShipmentObject);
					    					invokeApi.async(postShipmentObject).then(function(shipResponse) {
					    						if(shipResponse.Shipments.Shipment){
					    							$rootScope.shipmentContext = shipResponse.Shipments.Shipment;
					    							console.log("Shipment Details");
					    							console.log($rootScope.shipmentContext);
					    						}  
					    												
					    					});
					    					console.log("Valid Receipt");
		    								$rootScope.OrderBasedFlag= false;
		    								$location.path('/Receive/pallets');
					    				}
					    				else{
						    				console.log("Receipt doesn't Exsist for the Shipment");
						    				$scope.ErrorMessage="Receipt does not exsist for shipment. Add to receipt";
						    			}
						    			}
						    		}
					    						  
					    			
							    		  
					    			});// end of receipt api call
							   	
					    		}
							else{
								$scope.ErrorMessage = "Invalid Shipment";
								//$location.path('/receive/pallets');
							}
					      
						});// end of shipmentlist call api.
					
						}
					});//end of order api call
				}
			
			console.log("$rootScope.ReceiptFlag "+ $rootScope.ReceiptFlag);

		}; //end of scan variable of shipment/order
		
		
		
		
		$scope.startReceipt=  function(){
			
		console.log("to check if receipt exists");
		console.log($scope.frmReceivepo_shipment);
		if($scope.frmReceivepo_shipment!=undefined){
			$scope.ReceiptFlag="N";
			console.log("$rootScope.ReceiptFlag in startreceipt" +$scope.ReceiptFlag);
			$location.path('/Receive/pallets');
		}
		else{
			$scope.ErrorMessage="Enter Shipment# / Order# to add to receipt";
		}
		};


	});


});




