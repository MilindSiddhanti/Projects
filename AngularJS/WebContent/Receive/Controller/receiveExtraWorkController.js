define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveExtraWorkController', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){

		var formName = 'frmReceiveExtraWork';
		var recevingNode = $cookieStore.get('globals').OrganizationCode;
		$rootScope.recevingNode=recevingNode;

		
		$rootScope.receive_moduleFlow = 'Extra Work';
		$rootScope.receive_NavActive = "Extra Work";
		console.log("Item Extra Work Controller");
		
		var shipmentContext = $rootScope.shipmentContext || {};
		var containerContext = $rootScope.containerContext || {};
		var receiptContext = $rootScope.receiptContext || {};
		var orderContext =$rootScope.orderContext || {};
		var itemNodeContext = $rootScope.ItemNodeContext || {};
		console.log("item node context: " +JSON.stringify(itemNodeContext));

		//var itemDesc = itemNodeContext.PrimaryInformation.DisplayItemDescription;
		//console.log("-------------item Desc------------" +itemDesc);
		//$scope.itemDescription = itemDesc;
		//console.log("-------------item Desc------------" +$scope.itemDescription);
		//var itemid = itemNodeContext.ItemID;
		//$scope.itemID = itemid;
	
		function invokeCustomApi(node, extraWork, extraWorkReason){

			var postObject = new Object();
			postObject.CommandName = "invokeCustomApi";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "ExtraWork";

			postObject.InputXml.ApiName = "manageExtnUserExtraWork";
			postObject.InputXml.Node = node;
			postObject.InputXml.Reference1 = "RECEIVE_EXTRA_WORK";
			postObject.InputXml.Reference2 = "TOTAL_MINUTES";
			postObject.InputXml.ActivityGroup = "RECEIVE";
			postObject.InputXml.TableName = "YFS_RECEIPT_HEADER";
			//postObject.InputXml.TableKey = sReceiptHeaderKey;
			//postObject.InputXml.UserId = sUserId;
			postObject.InputXml.ExtraWork = extraWork;
			postObject.InputXml.ReasonCode = "EXTRA_WORK";
			postObject.InputXml.ReasonText = extraWorkReason;
		
			postObject.Template = new Object();

			postObject.IsService = "Y";
			postObject.Authenticate=AuthenticationService.getAuth();

			console.log("postObject object Extra Work "+JSON.stringify(postObject));
			return postObject;
		};
		
		
		$scope.saveExtraWork = function(){	
			console.log("---------saveExtraWork()---------------");
			$scope.ErrorMessage = "";
			scanned_extraWork = $scope.frmBlindReceiveextraw_ExtraWork;
			scanned_extraWorkReason = $scope.frmBlindReceiveextraw_ExtraWorkDesc;
			//var postObject = invokeCustomApi(recevingNode, activityUOM, sReceiptHeaderKey, extraWork, reasonCode, extraWorkReason);
			var postObject = invokeCustomApi(recevingNode, scanned_extraWork, scanned_extraWorkReason);
			console.log("Abhilash post object: " +postObject);

			invokeApi.async(postObject).then(function(response)
					{
				$scope.postResponse = response;
				console.log("Abhilash Manage Item Extra Work: " +JSON.stringify($scope.postResponse));
					});
		
		}
	});
});