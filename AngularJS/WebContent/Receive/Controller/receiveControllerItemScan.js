define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerItemScan', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){

		var formName = 'frmReceiveitemscan';
		var recevingNode = $cookieStore.get('globals').OrganizationCode;
		$rootScope.recevingNode=recevingNode;

		if($rootScope.itemScan_done_button){
			$scope.frmReceiveitemscan_done_button=true;
		}
		if($rootScope.closeCase_button){
			$scope.frmReceiveitemscan_closeCase_button=true;
		}

		$rootScope.receive_moduleFlow = 'Item Scan';
		$rootScope.receive_NavActive = "Item Scan";
		console.log("Item Scan controller");


		var shipmentContext = $rootScope.shipmentContext || {};
		var containerContext = $rootScope.containerContext || {};
		var receiptContext = $rootScope.receiptContext || {};
		var orderContext =$rootScope.orderContext || {};
		var case_value="";
		$rootScope.CASE="";
		
		var Logger = $log.getInstance($state.$current.parent);//parent

		//Logger.enableLogging(true);
		console.log("Shipment Details:");
		console.log(shipmentContext);
		console.log("receipt Details:");
		console.log(receiptContext);


		Logger.info("Log from ReveiveController ItemDetail"); 


		/*****************getShipmentLineList Api Xml formation*****************************************/

		function getShipmentLineListApi(shipmentKey){
			var postObject = new Object();
			postObject.CommandName = "getShipmentLineList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "ShipmentLine";
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="Shipment";
			postObject.InputXml.childNodes[0].ShipmentKey = shipmentKey;

			postObject.Template = new Object();
			postObject.Template.tagName = "ShipmentLines";

			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="ShipmentLine";
			postObject.Template.childNodes[0].ItemID = "";
			postObject.Template.childNodes[0].Quantity = "";
			postObject.Template.childNodes[0].ItemDesc = "";
			postObject.Template.childNodes[0].UnitOfMeasure="";
			postObject.Template.childNodes[0].ReceivedQuantity="";

			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0] = new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="ContainerDetails";

			postObject.Template.childNodes[0].childNodes[0].childNodes = [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]= new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName= "ContainerDetail";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ItemID="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].Quantity="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ShipmentContainerKey="";

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object ShipLine "+JSON.stringify(postObject));
			return postObject;
		};




		/*****************translateBarCode Api Xml formation*****************************************/

		function translateBarCodeApi(itemid){

			var entNo= shipmentContext.EnterpriseCode;

			console.log("Ent NO:" +entNo );
			var postObject = new Object();
			postObject.CommandName = "translateBarCode";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "BarCode";
			postObject.InputXml.BarCodeData = itemid;
			postObject.InputXml.BarCodeType = "ItemOrShippingContainer";

			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="ContextualInfo";
			postObject.InputXml.childNodes[0].OrganizationCode=recevingNode;
			postObject.InputXml.childNodes[0].EnterpriseCode=entNo;

			postObject.Template = new Object();
			postObject.Template.tagName="BarCode";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Translations";
			postObject.Template.childNodes[0].BarCodeTranslationSource="";

			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="Translation";

			postObject.Template.childNodes[0].childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object;
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="ContainerContextualInfo";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].CaseId="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].PalletId="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].IsNewContainer="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[1]=new Object;
			postObject.Template.childNodes[0].childNodes[0].childNodes[1].tagName="ItemContextualInfo";
			postObject.Template.childNodes[0].childNodes[0].childNodes[1].ItemID="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[1].InventoryUOM="";

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();

			console.log("postObject object Barcode "+JSON.stringify(postObject));

			return postObject;

		};

		/*****************getNodeItemDetails Api Xml formation*****************************************/

		function getNodeItemDetailsApi(itemid, uom){
			var entNo= shipmentContext.EnterpriseCode;
			var postObject = new Object();
			postObject.CommandName = "getNodeItemDetails";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Item";

			postObject.InputXml.ItemID = itemid;
			postObject.InputXml.Node = recevingNode;
			postObject.InputXml.OrganizationCode= entNo;
			postObject.InputXml.UnitOfMeasure= uom;

			//$scope.postResponse.Item.AlternateUOMList.AlternateUOM.Height;

			postObject.Template = new Object();
			postObject.Template.tagName = "Item";
			postObject.Template.ItemKey = "";
			postObject.Template.UnitOfMeasure = "";
			postObject.Template.OrganizationCode = "";
			postObject.Template.ItemID = "";

			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0] = new Object();
			postObject.Template.childNodes[0].tagName = "PrimaryInformation";
			postObject.Template.childNodes[0].UnitHeight = "";
			postObject.Template.childNodes[0].UnitHeightUOM = "";
			postObject.Template.childNodes[0].UnitLength = "";
			postObject.Template.childNodes[0].UnitLengthUOM = "";
			postObject.Template.childNodes[0].UnitWeight = "";
			postObject.Template.childNodes[0].UnitWeightUOM = "";
			postObject.Template.childNodes[0].UnitWidth = "";
			postObject.Template.childNodes[0].UnitWidthUOM = "";
			postObject.Template.childNodes[0].DisplayItemDescription = "";
			postObject.Template.childNodes[0].NumSecondarySerials="";

			postObject.Template.childNodes[1]=new Object();
			postObject.Template.childNodes[1].tagName="InventoryParameters";
			postObject.Template.childNodes[1].IsSerialTracked = "";
			postObject.Template.childNodes[1].TagControlFlag = "";
			postObject.Template.childNodes[1].TimeSensitive = "";

			postObject.Template.childNodes[2]=new Object();
			postObject.Template.childNodes[2].tagName="InventoryTagAttributes";
			postObject.Template.childNodes[2].LotNumber = "";
			postObject.Template.childNodes[2].BatchNo = "";
			postObject.Template.childNodes[2].LotAttribute1 = "";
			postObject.Template.childNodes[2].LotAttribute2 = "";
			postObject.Template.childNodes[2].LotAttribute3 = "";
			postObject.Template.childNodes[2].LotKeyReference = "";
			postObject.Template.childNodes[2].ManufacturingDate = "";
			postObject.Template.childNodes[2].RevisionNo = "";
			


			postObject.Template.childNodes[3] = new Object();
			postObject.Template.childNodes[3].tagName = "AlternateUOMList";

			postObject.Template.childNodes[3].childNodes = [];
			postObject.Template.childNodes[3].childNodes[0] = new Object();
			postObject.Template.childNodes[3].childNodes[0].tagName = "AlternateUOM";
			postObject.Template.childNodes[3].childNodes[0].Height = "";
			postObject.Template.childNodes[3].childNodes[0].HeightUOM = "";
			postObject.Template.childNodes[3].childNodes[0].Length = "";
			postObject.Template.childNodes[3].childNodes[0].LengthUOM = "";
			postObject.Template.childNodes[3].childNodes[0].Quantity = "";
			postObject.Template.childNodes[3].childNodes[0].UnitOfMeasure = "";
			postObject.Template.childNodes[3].childNodes[0].Weight = "";
			postObject.Template.childNodes[3].childNodes[0].WeightUOM = "";
			postObject.Template.childNodes[3].childNodes[0].Width = "";
			postObject.Template.childNodes[3].childNodes[0].WidthUOM = "";


			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object item detail "+JSON.stringify(postObject));
			return postObject;
		};


		/*****************manageItemApi Xml formation*****************************************/

		function manageItemApi(itemid, orgcode, uom, itemWidth, itemWeight, itemLength, itemHeight, alterUOMHeight, alterUOMLength, alterUOMQuantity, alterUOM, alterUOMWeight, alterUOMWidth){

			var postObject = new Object();
			postObject.CommandName = "manageItem";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "ItemList";

			postObject.InputXml.childNodes = [];
			postObject.InputXml.childNodes[0] = new Object();
			postObject.InputXml.childNodes[0].tagName = "Item";
			postObject.InputXml.childNodes[0].Action = "Modify";
			postObject.InputXml.childNodes[0].ItemID = itemid;
			postObject.InputXml.childNodes[0].OrganizationCode = orgcode;
			postObject.InputXml.childNodes[0].UnitOfMeasure = uom;

			postObject.InputXml.childNodes[1] = new Object();
			postObject.InputXml.childNodes[1].tagName = "PrimaryInformation";
			postObject.InputXml.childNodes[1].UnitWidth = itemWidth;
			postObject.InputXml.childNodes[1].UnitWeight = itemWeight;
			postObject.InputXml.childNodes[1].UnitLength = itemLength;
			postObject.InputXml.childNodes[1].UnitHeight = itemHeight;


			postObject.InputXml.childNodes[2] = new Object();
			postObject.InputXml.childNodes[2].tagName = "AlternateUOMList";

			postObject.InputXml.childNodes[2].childNodes = [];
			postObject.InputXml.childNodes[2].childNodes[0] = new Object();
			postObject.InputXml.childNodes[2].childNodes[0].tagName = "AlternateUOM";
			postObject.InputXml.childNodes[2].childNodes[0].Height = alterUOMHeight;
			postObject.InputXml.childNodes[2].childNodes[0].Length = alterUOMLength;
			postObject.InputXml.childNodes[2].childNodes[0].Quantity = alterUOMQuantity;
			postObject.InputXml.childNodes[2].childNodes[0].UnitOfMeasure = alterUOM;
			postObject.InputXml.childNodes[2].childNodes[0].Weight = alterUOMWeight;
			postObject.InputXml.childNodes[2].childNodes[0].Width = alterUOMWidth;


			postObject.Template = new Object();

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object item detail "+JSON.stringify(postObject));
			return postObject;
		};

		
		/*********************ITEM dimension FUNCTION**************************************/
	/*
		var ItemDimensionChangeFlag = false;
		
		function itemDimension(itemid, uom){
			
				var itemNodeContext= $rootScope.ItemNodeContext;
				console.log("---------Check for ItemNodeContext:----------- " +itemNodeContext);
				console.log("check code after invoking getNodeItemDetailsApi...");

				var orgcode = itemNodeContext.Item.OrganizationCode;
				var uom = itemNodeContext.Item.UnitOfMeasure;
				var itemWidth = itemNodeContext.Item.PrimaryInformation.UnitWidth;
				var itemWeight = itemNodeContext.Item.PrimaryInformation.UnitWeight;
				var itemLength = itemNodeContext.Item.PrimaryInformation.UnitLength;
				var itemHeight =itemNodeContext.Item.PrimaryInformation.UnitHeight;

				console.log("itemWidth: " +itemWidth);
				console.log("itemWeight: " +itemWeight);
				console.log("itemLength: " +itemLength);
				console.log("itemHeight: " +itemHeight);
				
				if(itemWidth == 0 || itemWeight == 0 || itemLength == 0 || itemHeight == 0)
                {
                $location.path('/Receive/itemDim');
                }
				
				else{


				if(angular.isArray(itemNodeContext.Item.AlternateUOMList.AlternateUOM))
				{
					console.log("---------Found Multiple Alternate UOM value for this item-----------");
					var i;
					for(i=0;i<itemNodeContext.Item.AlternateUOMList.AlternateUOM.length; i++)
					{

						console.log("------------------Condition loop for PALLET -----------");
						var alterUOMHeight = itemNodeContext.Item.AlternateUOMList.AlternateUOM[i].Height;
						var alterUOMLength =itemNodeContext.Item.AlternateUOMList.AlternateUOM[i].Length;
						var alterUOMQuantity = itemNodeContext.Item.AlternateUOMList.AlternateUOM[i].Quantity;
						var alterUOM = itemNodeContext.Item.AlternateUOMList.AlternateUOM[i].UnitOfMeasure;
						var alterUOMWeight = itemNodeContext.Item.AlternateUOMList.AlternateUOM[i].Weight;
						var alterUOMWidth = itemNodeContext.Item.AlternateUOMList.AlternateUOM[i].Width;


						console.log("alterUOMHeight: " +alterUOMHeight);
						console.log("alterUOMLength: " +alterUOMLength);
						console.log("alterUOMQuantity: " +alterUOMQuantity);
						console.log("alterUOMWeight: " +alterUOMWeight);
						console.log("alterUOMWidth: " +alterUOMWidth);

						if(itemWidth == 0 || itemWeight == 0 || itemLength == 0 || itemHeight == 0 || alterUOMHeight == 0 || alterUOMLength == 0 || alterUOMQuantity == 0 || alterUOMWeight == 0 || alterUOMWidth == 0)
						{
							ItemDimensionChangeFlag=true;
							$location.path('/Receive/itemDim');

						}
						else{
							ItemDimensionChangeFlag=false;
						}
					}
				}
				//single Alt UOM
				else{
					if(itemNodeContext.Item.AlternateUOMList.AlternateUOM)
					{
						console.log("---------Found Single Alternate UOM value for this item-----------");
			
						var orgcode = itemNodeContext.Item.OrganizationCode;
						var uom = itemNodeContext.Item.UnitOfMeasure;
						var itemWidth = itemNodeContext.Item.PrimaryInformation.UnitWidth;
						var itemWeight = itemNodeContext.Item.PrimaryInformation.UnitWeight;
						var itemLength = itemNodeContext.Item.PrimaryInformation.UnitLength;
						var itemHeight = itemNodeContext.Item.PrimaryInformation.UnitHeight;
						var alterUOMHeight = itemNodeContext.Item.AlternateUOMList.AlternateUOM.Height;
						var alterUOMLength = itemNodeContext.Item.AlternateUOMList.AlternateUOM.Length;
						var alterUOMQuantity = itemNodeContext.Item.AlternateUOMList.AlternateUOM.Quantity;
						var alterUOM = itemNodeContext.Item.AlternateUOMList.AlternateUOM.UnitOfMeasure;
						var alterUOMWeight = itemNodeContext.Item.AlternateUOMList.AlternateUOM.Weight;
						var alterUOMWidth = itemNodeContext.Item.AlternateUOMList.AlternateUOM.Width;

						console.log("itemWidth: " +itemWidth);
						console.log("itemWeight: " +itemWeight);
						console.log("itemLength: " +itemLength);
						console.log("itemHeight: " +itemHeight);
						console.log("alterUOMHeight: " +alterUOMHeight);
						console.log("alterUOMLength: " +alterUOMLength);
						console.log("alterUOMQuantity: " +alterUOMQuantity);
						console.log("alterUOMWeight: " +alterUOMWeight);
						console.log("alterUOMWidth: " +alterUOMWidth);

						if(itemWidth == 0 || itemWeight == 0 || itemLength == 0 || itemHeight == 0 || alterUOMHeight == 0 || alterUOMLength == 0 || alterUOMQuantity == 0 || alterUOMWeight == 0 || alterUOMWidth == 0)
						{

							$location.path('/Receive/itemDim');
							ItemDimensionChangeFlag = true;

						}
						else{
							ItemDimensionChangeFlag=false;
						}
					}
				}
		}
			
			return ItemDimensionChangeFlag;
			
		};
	*/	
		
		
		
		/*********************inventoryAttributeCheck FUNCTION**************************************/
		
		function inventoryAttributeCheck(itemid){
		
			var itemNodeContext= $rootScope.ItemNodeContext;
				console.log("Tag Flag: "+itemNodeContext.Item.InventoryParameters.TagControlFlag);
				if((itemNodeContext.Item.InventoryParameters.TagControlFlag == "Y")){
					
					console.log("Tag Flag: "+itemNodeContext.Item.InventoryParameters.TagControlFlag);
					$rootScope.ItemID= itemid;
					console.log("GLOBAL ITEM ID");
					console.log($rootScope.ItemID);
					console.log("Call the tag screen");
					$location.path('/Receive/itemTag');
					
					
				}else if(itemNodeContext.Item.InventoryParameters.TimeSensitive=="Y"){
					console.log("TimeSensitive Flag: "+itemNodeContext.Item.InventoryParameters.TimeSensitive);
					$rootScope.ItemID= itemid;
					console.log("GLOBAL ITEM ID");
					console.log($rootScope.ItemID);
					console.log("Call the Time Sensitive screen");
					$location.path('/Receive/itemTimeSensitive');
					
				}else if(itemNodeContext.Item.InventoryParameters.IsSerialTracked=="Y"){
					console.log("Seraial Flag: "+itemNodeContext.Item.InventoryParameters.IsSerialTracked);
					$rootScope.ItemID= itemid;
					console.log("GLOBAL ITEM ID");
					console.log($rootScope.ItemID);
					console.log("Call the serial screen");
					$location.path('/Receive/itemSerial');
					
				}else if((itemNodeContext.Item.InventoryParameters.IsSerialTracked!="Y") &&(itemNodeContext.Item.InventoryParameters.TagControlFlag != "Y")&&(itemNodeContext.Item.InventoryParameters.TimeSensitive != "Y")){
					
					console.log("out of the object");
					
					$rootScope.ItemID= itemid;
					console.log("GLOBAL ITEM ID");
					console.log($rootScope.ItemID);
					console.log("Call the item detail screen");
					$location.path('/Receive/itemDetails');
					
				}
				
			
			
		};



		processContoller.InputController = function(scannedVariable){
			console.log("------------Input process controller ----------------");
			$scope.ErrorMessage="";
			var ScannedData = $scope.$apply(formName+'_'+scannedVariable);
			if(scannedVariable == "item"){
				var itemid= ScannedData;

				if(itemid){

					var reference_value;
					var reference_field;
					var uom = "";
					var validItemFlag = false;

					var barCodeObject=translateBarCodeApi(itemid);
					invokeApi.async(barCodeObject).then(function(barCodeResponse) {
						$scope.postResponse = barCodeResponse;
						console.log("Barcode Output");
						console.log($scope.postResponse);

						if($scope.postResponse.BarCode.Translations.TotalNumberOfRecords){
							$rootScope.barCodeContext = $scope.postResponse.BarCode.Translations;
							if($rootScope.barCodeContext.BarCodeTranslationSource == "ItemID"){
								console.log("Bar Code Source is:" +$rootScope.barCodeContext.BarCodeTranslationSource );
								//console.log($rootScope.barCodeContext.BarCodeTranslationSource);
								reference_field = "ItemID";	
								$rootScope.barCodeTranslationContext = $rootScope.barCodeContext.Translation.ItemContextualInfo;
								reference_value=$rootScope.barCodeTranslationContext.ItemID;
								uom=$rootScope.barCodeTranslationContext.InventoryUOM;
								console.log("Ref Value:");
								console.log(reference_value);
								
								
								// calling the getNodeItemDetail  api to have the itemContext:
								var itemDetailObject=getNodeItemDetailsApi(itemid,uom);
								invokeApi.async(itemDetailObject).then(function(itemDetailResponse) {
									console.log(itemDetailResponse);
									$rootScope.ItemNodeContext = itemDetailResponse;
									console.log("item detail Output " +$rootScope.ItemNodeContext);
									//console.log("postObject object item detail " + itemDetailResponse);
									if(!itemDetailResponse.ErrorDescription){
															
									
										
										console.log("ROOT ITEM CONTEXT" +JSON.stringify($rootScope.ItemNodeContext)  );
										var itemNodeContext = $rootScope.ItemNodeContext;
								
								// check if the item belongs to the order / shipment first:
								
								if($rootScope.OrderBasedFlag){
									
									var flag= false;
									console.log("Order context:");
									console.log(orderContext);
									var orderLineContext = orderContext.OrderList.Order.OrderLines;
									console.log("orderLineContext:");
									console.log(orderLineContext);

									if (orderLineContext.TotalNumberOfRecords){
										var ordLinearrayflag = angular.isArray(orderLineContext.OrderLine);
										console.log("Order line context:");
										console.log(orderLineContext);
										console.log("Order line flag:" + ordLinearrayflag);
										if (ordLinearrayflag){

											for (var i=0; i < orderLineContext.OrderLine.length; i++){

												if(orderLineContext.OrderLine[i].ItemDetails != undefined){
													if(orderLineContext.OrderLine[i].ItemDetails.ItemID == reference_value){
														validItemFlag = true;
														$rootScope.ItemIDDesc = orderLineContext.OrderLine[i].ItemDetails.PrimaryInformation.Description;
														console.log("item matched to order line in array");
														$rootScope.orderLineContext = orderLineContext.OrderLine[i];
														break;

													}
												}

											}
										}
										else{

											if(orderLineContext.OrderLine.ItemDetails.ItemID == reference_value){
												console.log(orderLineContext.OrderLine.ItemDetails.ItemID);
												$rootScope.ItemIDDesc = orderLineContext.OrderLine.ItemDetails.PrimaryInformation.Description;
												console.log("item matched to order line");
												$rootScope.orderLineContext = orderLineContext.OrderLine;
												validItemFlag = true;

											}
										}

										if(validItemFlag){
											console.log("Scanned item is valid item of the order.")
											// calling the item dimesion function;
											
										//	var itemChange = itemDimension(reference_value,uom);
											
										//	if(!itemChange){
											
											// calling the tag controlled item function;
											inventoryAttributeCheck(reference_value);
											//console.log("invAttCapture" +invAttCapture);
										//	}
											
											
										}
										
										else{
											$scope.ErrorMessage = "Item Scanned doesnt belong to the Order";
											$scope.frmReceiveitemscan_item="";
										}

									}
									
									
								}
								
								if($rootScope.ShipmentBasedFlag){
									
									var shipmentKey  = shipmentContext.ShipmentKey;
									var shpLineObject=getShipmentLineListApi(shipmentKey);
									invokeApi.async(shpLineObject).then(function(ShpLineResponse) {
										var postResponse = ShpLineResponse;
										console.log("Ship Line Output");
										console.log(postResponse);

										if(postResponse.ShipmentLines){

											var shipmentLineContext = postResponse.ShipmentLines;
											$rootScope.shipmentLineContext = shipmentLineContext;
											console.log("Ship Line Context");
											console.log(shipmentLineContext);
									
											var shipmentlineArrayFlag = angular.isArray(shipmentLineContext.ShipmentLine);
											console.log("shipmentlineArrayFlag" +shipmentlineArrayFlag);
											if (shipmentlineArrayFlag){
												for(var i=0; i < shipmentLineContext.ShipmentLine.length; i++){
													console.log("ref val in for loop");
													console.log(reference_value);
													console.log("item Val");
													console.log(shipmentLineContext.ShipmentLine[i]);
													if(shipmentLineContext.ShipmentLine[i].ItemID == reference_value ){

														$rootScope.shipmentLineContext =shipmentLineContext.ShipmentLine[i];
														console.log("shipemnt lien");
														console.log($rootScope.shipmentLineContext);
														validItemFlag = true;
														$rootScope.ItemIDDesc = shipmentLineContext.ShipmentLine[i].ItemDesc;
														break;
													}
												}// end of for loop
											}
											else{
												if(shipmentLineContext.ShipmentLine.ItemID == reference_value){
													console.log(shipmentLineContext.ShipmentLine.ItemID);

													$rootScope.shipmentLineContext =shipmentLineContext.ShipmentLine;
													console.log("shipemnt lien");
													console.log($rootScope.shipmentLineContext);
														$rootScope.ItemIDDesc = shipmentLineContext.ShipmentLine.ItemDesc;
												validItemFlag = true;

												}

											}

											if(validItemFlag){
										
												console.log("Valid Item Match found in the shipment based receving");
												// calling the item dimesion function;
										
											//	var itemChange = itemDimension(reference_value,uom);
										
											//	if(!itemChange){
										
													// calling the tag controlled item function;
													inventoryAttributeCheck(reference_value);
											
												
											//	}
										
										}
										else{
											$scope.ErrorMessage="Item Scanned doesnt belong to the shipment";
											$scope.frmReceiveitemscan_item="";
										}
								
										}//end of shipmeline check
									});//end of shipline api call
									
								}
								
								if($rootScope.BlindReceiptBasedFlag){
									console.log("Blind receipt item can be anything which is validated. ");
									validItemFlag = true;
									// calling the item dimesion function;
									
								//	var itemChange = itemDimension(reference_value,uom);
									
								//	if(!itemChange){
									
										// calling the tag controlled item function;
										inventoryAttributeCheck(reference_value);
									
								//		}
										
									
								}
								
									}
								});// end of nodeitemdetail call
								
								
							}// end of check when an item id is scanned
							
							//if case is scanned then 
							if($rootScope.barCodeContext.BarCodeTranslationSource != "ItemID"){
								
								var barCodeContext= $scope.postResponse.BarCode.Translations;
								$rootScope.barCodeContext = barCodeContext;
								if(barCodeContext.Translation.ContainerContextualInfo){
									var barCodeTranslationContext=barCodeContext.Translation.ContainerContextualInfo;
									console.log("BArcode ContainerContextualInfo translation");
									console.log(barCodeTranslationContext);
									
									if(barCodeTranslationContext.IsNewContainer != undefined){
										//it is new case that can be used for receiving
										case_value = barCodeTranslationContext.CaseId;
										if(case_value != undefined){
											$rootScope.CASE=case_value;
											console.log("the case scanned is " +$rootScope.CASE );
											
										}
										else{
											$scope.ErrorMessage="Invalid CaseId scan ";
										}
									}
									else{
										//exixting pallet throw error for scanning new pallet
										$scope.ErrorMessage="Invalid CaseID scan ";
									}
								}
								
							}
						}
					});// end of barcode translation
				}


				// bar code translation output check
				else{

					$scope.ErrorMessage="Invalid ItemId scan";
					$scope.frmReceiveitemscan_item="";

				}
			}// end of translaion api callcheck
			else{
				$scope.ErrorMessage="Invalid ItemId scan";
				$scope.frmReceiveitemscan_item="";
			}
			//after one scan
			$scope.frmReceiveitemscan_item="";

		};// end of controller

		
		$scope.itemdone=  function(){
				$scope.frmReceiveitemscan_done_button=false;

				$rootScope.caseId="";
				$rootScope.palletId="";
				$rootScope.CASE="";
				$rootScope.ITEMS=[];
				$rootScope.itemScan_done_button=false;
				
				//console.log("Clearing all the var" +$rootScope.ITEMS);
				$location.path('/Receive/pallets');

			
		};
		//close case function
		$scope.closeCase = function(){
			// call receive Order with the Close Case flag Y
			/*
			 <Receipt CaseId="" CloseCase="" ClosePallet="Y" DocumentType="0005"
    DriverName="" IgnoreOrdering="Y" LocationId="D1-010101"
    PalletId="00100001000010000100"
    ReceiptHeaderKey="20150909214028194629" ReceivingDock="D1-010101"
    ReceivingNode="DC1" ShipmentKey="20150909213816194594">
    <Shipment BolNo="" DocumentType="0005" EnterpriseCode="XYZ-CORP"
        OrderNo="" SCAC="" ShipmentNo=""/>
</Receipt>
			 */
			$rootScope.closeCase_button=false;
		};
		



	});
});
