define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerItemTag', function($scope, $rootScope, $timeout, $location, $log, $cookieStore, $state){
		$rootScope.receive_moduleFlow = 'Item Tag';
		$rootScope.receive_NavActive = "Item Tag";
		
		var ItemNodeContext= $rootScope.ItemNodeContext;
		console.log("Tag controller "+JSON.stringify(ItemNodeContext));
		
		console.log("Time Sensitive Flag: "+ItemNodeContext.Item.InventoryParameters.TimeSensitive);
		
		var showtagidn = false;
		var showtagatt = false;		
		
		if(ItemNodeContext.Item.InventoryTagAttributes){
			
			var LotNo, BatchNo, RevisionNo, LotAttribute1, LotAttribute2, LotAttribute3, LotKeyReference, ManufacturingDate;

			LotNo = ItemNodeContext.Item.InventoryTagAttributes.LotNumber;
			BatchNo = ItemNodeContext.Item.InventoryTagAttributes.BatchNo;
			RevisionNo = ItemNodeContext.Item.InventoryTagAttributes.RevisionNo;
			LotAttribute1 = ItemNodeContext.Item.InventoryTagAttributes.LotAttribute1;
			LotAttribute2 = ItemNodeContext.Item.InventoryTagAttributes.LotAttribute2;
			LotAttribute3 = ItemNodeContext.Item.InventoryTagAttributes.LotAttribute3;
			LotKeyReference = ItemNodeContext.Item.InventoryTagAttributes.LotKeyReference;
			ManufacturingDate = ItemNodeContext.Item.InventoryTagAttributes.ManufacturingDate;

			if(LotNo == "02"){
				$rootScope.LN = "TI";
			}else if(LotNo == "01"){
				$rootScope.LN = "TD";
			}else{
				$rootScope.LN = "N";
			}

			if(BatchNo == "02"){
				$rootScope.BN = "TI";
			}else if(BatchNo == "01"){
				$rootScope.BN = "TD";
			}else{
				$rootScope.BN = "N";
			}

			if(RevisionNo == "02"){
				$rootScope.RN = "TI";
			}else if(RevisionNo == "01"){
				$rootScope.RN = "TD";
			}else{
				$rootScope.RN = "N";
			}

			if(LotAttribute1 == "01"){
				$rootScope.LotAttribute1 = "TD";
			}else{
				$rootScope.LotAttribute1 = "N";
			}

			if(LotAttribute2 == "01"){
				$rootScope.LotAttribute2 = "TD";
			}else{
				$rootScope.LotAttribute2 = "N";
			}

			if(LotAttribute3 == "01"){
				$rootScope.LotAttribute3 = "TD";
			}else{
				$rootScope.LotAttribute3 = "N";
			}

			if(LotKeyReference == "01"){
				$rootScope.LotKeyReference = "TD";
			}else{
				$rootScope.LotKeyReference = "N";
			}

			if(ManufacturingDate == "01"){
				$rootScope.ManufacturingDate = "TD";
			}else{
				$rootScope.ManufacturingDate = "N";
			}

		}


		// for tag identifiers
		console.log('$rootScope.LN :: '+ $rootScope.LN);
		console.log('$rootScope.BN :: '+ $rootScope.BN);
		console.log('$rootScope.RN :: '+ $rootScope.RN);
       
		if($rootScope.LN == "TI"){
			$scope.showtagidn = true;
			$scope.showLotNumber = true;
		}else{
			$scope.showLotNumber = false;
		}

		if($rootScope.BN == "TI"){
			$scope.showtagidn = true;
			$scope.showBatchNumber = true;
		}else{
			$scope.showBatchNumber = false;
		}

		if($rootScope.RN == "TI"){
			$scope.showtagidn = true;
			$scope.showRevisionNumber = true;
		}else{
			$scope.showRevisionNumber = false;
		}
		// for tag descriptors
		console.log('$rootScope.LotAttribute1 :: '+ $rootScope.LotAttribute1);
		console.log('$rootScope.LotAttribute2 :: '+ $rootScope.LotAttribute2);
		console.log('$rootScope.LotAttribute3 :: '+ $rootScope.LotAttribute3);
		console.log('$rootScope.LotKeyReference :: '+ $rootScope.LotKeyReference);
		console.log('$rootScope.ManufacturingDate :: '+ $rootScope.ManufacturingDate); 
		console.log('$rootScope.BN :: '+ $rootScope.BN);
		console.log('$rootScope.RN :: '+ $rootScope.RN);

		if($rootScope.LN == "TD"){
			$scope.showtagatt = true;
			$scope.showLotNumberDesc = true;
		}else{
			$scope.showLotNumberDesc = false;
		}

		if($rootScope.BN == "TD"){
			$scope.showtagatt = true;
			$scope.showBatchNumberDesc = true;
		}else{
			$scope.showBatchNumberDesc = false;
		}

		if($rootScope.RN == "TD"){
			$scope.showtagatt = true;
			$scope.showRevisionNumberDesc = true;
		}else{
			$scope.showRevisionNumberDesc = false;
		}

		if($rootScope.LotAttribute1 == "TD"){
			$scope.showtagatt = true;
			$scope.showLotAttribute1 = true;
		}else{
			$scope.showLotAttribute1 = false;
		}

		if($rootScope.LotAttribute2 == "TD"){
			$scope.showtagatt = true;
			$scope.showLotAttribute2 = true;
		}else{
			$scope.showLotAttribute2 = false;
		}

		if($rootScope.LotAttribute3 == "TD"){
			$scope.showtagatt = true;
			$scope.showLotAttribute3 = true;
		}else{
			$scope.showLotAttribute3 = false;
		}

		if($rootScope.LotKeyReference == "TD"){
			$scope.showtagatt = true;
			$scope.showLotKeyReference = true;
		}else{
			$scope.showLotKeyReference = false;
		}

		if($rootScope.ManufacturingDate == "TD"){
			$scope.showtagatt = true;
			$scope.showManufacturingDate = true;
		}else{
			$scope.showManufacturingDate = false;
		}

		  $scope.tagdetail = function(){
			  
			var recAttFields = [];
			var OverallFlag = false;
			var LotNumberFlag = false;
			var BatchNumberFlag = false;
			var RevisionNumberFlag = false;
			var LotNumberDescFlag = false;
			var BatchNumberDescFlag = false;
			var RevisionNumberDescFlag = false;
			var LotAttribute1Flag = false;
			var LotAttribute2Flag = false;
			var LotAttribute3Flag = false;
			var LotKeyReferenceFlag = false;
			var ManufacturingDateFlag = false;
						

			if($scope.showLotNumber || $scope.showBatchNumber || $scope.showRevisionNumber){
				
				if($scope.showLotNumber){
					if(angular.isDefined($scope.frmReceiveitemtag_lotno) && $scope.frmReceiveitemtag_lotno.length>0){
						
						LotNumberFlag = true;
						recAttFields.push({ id:"LotNumber", value: $scope.frmReceiveitemtag_lotno});
					}else{
						//$scope.ErrorMessage= "";
						$scope.ErrorMessage= "Enter Lot No";
				    }
				}else{
					
					LotNumberFlag = true;
				}

				if($scope.showBatchNumber){
					if(angular.isDefined($scope.frmReceiveitemtag_batchno) && $scope.frmReceiveitemtag_batchno.length>0){
						
						BatchNumberFlag = true;
						recAttFields.push({ id:"BatchNumber", value: $scope.frmReceiveitemtag_batchno});
					}else{
						//$scope.ErrorMessage= "";
						$scope.ErrorMessage= "Enter Batch No";
				    }
				}else{
					
					BatchNumberFlag = true;
				}

				if($scope.showRevisionNumber){
					if(angular.isDefined($scope.frmReceiveitemtag_revno) && $scope.frmReceiveitemtag_revno.length>0){
						
						RevisionNumberFlag = true;
						recAttFields.push({ id:"RevisionNumber", value: $scope.frmReceiveitemtag_revno});
					}else{
						//$scope.ErrorMessage= "";
						$scope.ErrorMessage= "Enter Revision No";
				    }
				}else{
					
					RevisionNumberFlag = true;
				}

				if($scope.showLotNumberDesc || $scope.showBatchNumberDesc || $scope.showRevisionNumberDesc || $scope.showLotAttribute1 || $scope.showLotAttribute2 ||
						$scope.showLotAttribute3 || $scope.showLotKeyReference || $scope.showManufacturingDate ){

					if($scope.showLotNumberDesc){
						if(angular.isDefined($scope.frmReceiveitemtag_lotnodesc) && $scope.frmReceiveitemtag_lotnodesc.length>0){
							
							LotNumberDescFlag = true;
							recAttFields.push({ id:"LotAttNo", value: $scope.frmReceiveitemtag_lotnodesc});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Lot Att No";
					    }
					}else{
						
						LotNumberDescFlag = true;
					}

					if($scope.showBatchNumberDesc){
						if(angular.isDefined($scope.frmReceiveitemtag_batchnodesc) && $scope.frmReceiveitemtag_batchnodesc.length>0){
							
							BatchNumberDescFlag = true;
							recAttFields.push({ id:"BatchAttNo", value: $scope.frmReceiveitemtag_batchnodesc});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Batch Att No";
					    }
					}else{
						
						BatchNumberDescFlag = true;
					}

					if($scope.showRevisionNumberDesc){
						if(angular.isDefined($scope.frmReceiveitemtag_revnodesc) && $scope.frmReceiveitemtag_revnodesc.length>0){
							
							RevisionNumberDescFlag = true;
							recAttFields.push({ id:"RevisionAttNo", value: $scope.frmReceiveitemtag_revnodesc});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Revision Att No";
					    }
					}else{
						
						RevisionNumberDescFlag = true;
					}

					if($scope.showLotAttribute1){
						if(angular.isDefined($scope.frmReceiveitemtag_lotatt1) && $scope.frmReceiveitemtag_lotatt1.length>0){
							
							LotAttribute1Flag = true;
							recAttFields.push({ id:"LotAttribute1", value: $scope.frmReceiveitemtag_lotatt1});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Lot Attribute1";
					    }
					}else{
						
						LotAttribute1Flag = true;
					}

					if($scope.showLotAttribute2){
						if(angular.isDefined($scope.frmReceiveitemtag_lotatt2) && $scope.frmReceiveitemtag_lotatt2.length>0){
							
							LotAttribute2Flag = true;
							recAttFields.push({ id:"LotAttribute2", value: $scope.frmReceiveitemtag_lotatt2});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Lot Attribute2";
					    }
					}else{
						
						LotAttribute2Flag = true;
					}

					if($scope.showLotAttribute3){
						if(angular.isDefined($scope.frmReceiveitemtag_lotatt3) && $scope.frmReceiveitemtag_lotatt3.length>0){
							
							LotAttribute3Flag = true;
							recAttFields.push({ id:"LotAttribute3", value: $scope.frmReceiveitemtag_lotatt3});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Lot Attribute3";
					    }
					}else{
						
						LotAttribute3Flag = true;
					}

					if($scope.showLotKeyReference){
						if(angular.isDefined($scope.frmReceiveitemtag_lotref) && $scope.frmReceiveitemtag_lotref.length>0){
							
							LotKeyReferenceFlag = true;
							recAttFields.push({ id:"LotReference", value: $scope.frmReceiveitemtag_lotref});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Lot Reference";
					    }
					}else{
						
						LotKeyReferenceFlag = true;
					}

					if($scope.showManufacturingDate){
						if(angular.isDefined($scope.frmReceiveitemtag_mnfdate) && $scope.frmReceiveitemtag_mnfdate.length>0){
							
							ManufacturingDateFlag = true;
							recAttFields.push({ id:"ManufacturingDate", value: $scope.frmReceiveitemtag_mnfdate});
						}else{
							//$scope.ErrorMessage= "";
							$scope.ErrorMessage= "Enter Manufacturing Date";
					    }
					}else{
						
						ManufacturingDateFlag = true;
					}
				}
				
				else{
					OverallFlag = true;
				}
				console.log(" $scope.showLotNumber "+$scope.frmReceiveitemtag_lotno + LotNumberFlag);
				console.log(" $scope.showBatchNumber "+$scope.frmReceiveitemtag_batchno + BatchNumberFlag);
				console.log(" $scope.showRevisionNumber "+$scope.frmReceiveitemtag_revno + RevisionNumberFlag);
				console.log(" $scope.showLotNumberDesc "+$scope.frmReceiveitemtag_lotnodesc + LotNumberDescFlag);
				console.log(" $scope.showBatchNumberDesc "+$scope.frmReceiveitemtag_batchnodesc + BatchNumberDescFlag);
				console.log(" $scope.showRevisionNumberDesc "+$scope.frmReceiveitemtag_revnodesc + RevisionNumberDescFlag);
				console.log(" $scope.showLotAttribute1 "+$scope.frmReceiveitemtag_lotatt1 + LotAttribute1Flag);
				console.log(" $scope.showLotAttribute2 "+$scope.frmReceiveitemtag_lotatt2 + LotAttribute2Flag);
				console.log(" $scope.showLotAttribute3 "+$scope.frmReceiveitemtag_lotatt3 + LotAttribute3Flag);
				console.log(" $scope.showLotKeyReference "+$scope.frmReceiveitemtag_lotref + LotKeyReferenceFlag);
				console.log(" $scope.showManufacturingDate "+$scope.frmReceiveitemtag_mnfdate + ManufacturingDateFlag);
				
				console.log("OverallFlag before:" + OverallFlag);
			}
			$rootScope.recAttFields = recAttFields;
			
			if(LotNumberFlag && BatchNumberFlag && RevisionNumberFlag && LotNumberDescFlag && BatchNumberDescFlag && RevisionNumberDescFlag &&LotAttribute1Flag
					&& LotAttribute2Flag && LotAttribute3Flag && LotKeyReferenceFlag && ManufacturingDateFlag ){
				OverallFlag = true;
				console.log("OverallFlag after:" + OverallFlag);
			}
			console.log("OverallFlag after if:" + OverallFlag);
			if(ItemNodeContext.Item.InventoryParameters.TimeSensitive=="Y" && OverallFlag){
				console.log("Time Sensitive Flag: "+ItemNodeContext.Item.InventoryParameters.TimeSensitive);

				console.log("GLOBAL ITEM ID");
				console.log($rootScope.ItemID);
				console.log("Call the Time Sensitive screen");
				$location.path('/Receive/itemTimeSensitive');

			}
			else if(ItemNodeContext.Item.InventoryParameters.IsSerialTracked=="Y" && OverallFlag){
				console.log("Seraial Flag: "+ItemNodeContext.Item.InventoryParameters.IsSerialTracked);

				console.log("GLOBAL ITEM ID");
				console.log($rootScope.ItemID);
				console.log("ItemNodeContext.PrimaryInformation.NumSecondarySerials:" + ItemNodeContext.Item.PrimaryInformation.NumSecondarySerials);
				console.log("Call the serial screen");
				$location.path('/Receive/itemSerial');

			}

			else if((ItemNodeContext.Item.InventoryParameters.IsSerialTracked=="N")&&(ItemNodeContext.Item.InventoryParameters.TimeSensitive=="N" && OverallFlag)){
				console.log("go to item details screen");
				console.log("Seraial Flag: "+ItemNodeContext.Item.InventoryParameters.IsSerialTracked);
				console.log("Time Sensitive Flag: "+ItemNodeContext.Item.InventoryParameters.TimeSensitive);
				console.log("GLOBAL ITEM ID");
				console.log($rootScope.ItemID);
				console.log("Call the item detail screen");
				$location.path('/Receive/itemDetails');

			}

		}

	});

});