define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerItemTimeSensitive', function($scope, $rootScope,$timeout,$location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){
		$rootScope.receive_moduleFlow = 'Item Time Sensitive';
		$rootScope.receive_NavActive = "Item Time Sensitive";
	console.log("Time Sensitive controller");
	
	var formName = 'frmReceiveitemtimesensitive';
	var itemId = $rootScope.ItemID;
	var ItemNodeContext=$rootScope.ItemNodeContext;
	
	$scope.timedetail = function(){
	 
		if(angular.isDefined($scope.frmReceiveitemtimesensitive_expdate)){
			if(ItemNodeContext.Item.InventoryParameters.IsSerialTracked=="Y"){
				console.log("Seraial Flag: "+ItemNodeContext.Item.InventoryParameters.IsSerialTracked);
				
				console.log("GLOBAL ITEM ID");
				console.log($rootScope.ItemID);
				console.log("Call the serial screen");
				$location.path('/Receive/itemSerial');
				$rootScope.datatime= $scope.frmReceiveitemtimesensitive_expdate;
			}
			
			else if(ItemNodeContext.Item.InventoryParameters.IsSerialTracked=="N"){	
				console.log("Seraial Flag: "+ItemNodeContext.Item.InventoryParameters.IsSerialTracked);
			    console.log("go to item details screen");
			    console.log("Time Sensitive Flag: "+ItemNodeContext.Item.InventoryParameters.TimeSensitive);
			    console.log("Expiration Date" + $rootScope.datatime);
			    console.log("GLOBAL ITEM ID");
			    console.log($rootScope.ItemID);
			    console.log("Call the item detail screen");
			    $location.path('/Receive/itemDetails');
			}
	}
		else{
			$scope.ErrorMessage= "Enter Expiration Date";
		}
	}
	
	});
    
});