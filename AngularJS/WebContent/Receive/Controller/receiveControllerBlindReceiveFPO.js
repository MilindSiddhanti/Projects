define(['common/ctrljs/app'], function (myApp) {


	myApp.register.controller('receiveControllerBlindReceiveFPO', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){

		var formName = 'frmReceiveBlindReceivePOcreate';
		var node = $cookieStore.get('globals').OrganizationCode;
		$rootScope.recevingNode=node;
		$rootScope.ReceiptFlag = false;
		$rootScope.BlindReceiptBasedFlag= false;
		
		
		$rootScope.receive_moduleFlow = 'Find Shipment';
		$rootScope.receive_NavActive = "POs";
		console.log($rootScope.seller);
		console.log("FPO controller in Blind Receipt");


		$scope.OrderBasedFlag= false;
		var Logger = $log.getInstance($state.$current);
		Logger.info("Log from BlindReveiveController FPO"); 


		/*****************getOrganizationList Api Xml formation*****************************************/

		function getOrganizationListApi(seller){
			var postObject = new Object();
			postObject.CommandName = "getOrganizationList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Organization";
			postObject.InputXml.OrganizationCode = seller;
			postObject.Template = new Object();
			postObject.Template.tagName = "OrganizationList";
			postObject.Template.childNodes = [];
			postObject.Template.childNodes[0]  = new Object();
			postObject.Template.childNodes[0].tagName = "Organization";
			postObject.Template.childNodes[0].PrimaryEnterpriseKey="";

			postObject.IsService = "N";
			postObject.Authenticate = AuthenticationService.getAuth();
			console.log("postObject object org hir" + postObject);
			return postObject;

		}
		/*****************getShipmentList Api Xml formation*****************************************/
		function getShipmentListApi( recevingNode, shipmentkey){
			var postObject = new Object();
			postObject.CommandName = "getShipmentList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Shipment";
			postObject.InputXml.DocumentType = "0010";
			//postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.ReceivingNode = recevingNode;
			postObject.InputXml.ShipmentKey = shipmentkey;
			//postObject.InputXml.OrderNo=orderno;
			postObject.Template = new Object();
			/*postObject.Template.tagName = "Shipments";
			
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Shipment";
			postObject.Template.childNodes[0].DoNotVerifyCaseContent="";
			postObject.Template.childNodes[0].DoNotVerifyPalletContent="";	
			postObject.Template.childNodes[0].DocumentType="";	
			postObject.Template.childNodes[0].EnterpriseCode="";	
			postObject.Template.childNodes[0].ReceivingNode="";	
			postObject.Template.childNodes[0].SellerOrganizationCode="";	
			postObject.Template.childNodes[0].ShipmentNo="";	
			postObject.Template.childNodes[0].Status="" ;
			postObject.Template.childNodes[0].ShipmentKey="";	
			
			
			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="Containers";
			
			postObject.Template.childNodes[0].childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="Container";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerScm="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerNo="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerType="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ShipmentContainerKey="";
		
			*/
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			return postObject;
		};
		/********************createShipment API***********************/

		function createShipmentApi(enterprisecode, seller, receivingnode){
			var postObject = new Object();
			postObject.CommandName = "createShipment";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Shipment";
			postObject.InputXml.DocumentType = "0010";
			postObject.InputXml.EnterpriseCode = enterprisecode;
			postObject.InputXml.SellerOrganizationCode = seller;
			postObject.InputXml.ReceivingNode = receivingnode;
			postObject.InputXml.ConfirmShip = "Y";
			postObject.InputXml.ManuallyEntered="Y";
			postObject.Template = new Object();
			postObject.Template.tagName = "Shipment";
			postObject.Template.SellerOrganizationCode = "";
			postObject.Template.ShipmentKey = "";
			postObject.Template.ShipmentNo = "";
			postObject.Template.childNodes= [];

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("Create Shipment Api obj" + JSON.stringify(postObject));
			return postObject;

		}

		/*****************getReceiptList Api Xml formation*****************************************/
		function getReceiptListApi(EntCode, SellerCode, recevingNode){
			var postObject = new Object();
			postObject.CommandName = "getReceiptList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Receipt";
			postObject.InputXml.DocumentType = "0010";
			postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.ReceivingNode = recevingNode;
			postObject.InputXml.childNodes = [];
			postObject.InputXml.childNodes[0] = new Object();
			postObject.InputXml.childNodes[0].tagName = "Shipment";
			postObject.InputXml.childNodes[0].SellerOrganizationCode=SellerCode;
			
			
			postObject.Template = new Object();
			/*postObject.Template.tagName="ReceiptList";
			postObject.Template.TotalNumberOfRecords="";

			postObject.Template.childNodes = [];
			postObject.Template.childNodes[0] = new Object();
			postObject.Template.childNodes[0].tagName = "Receipt";
			postObject.Template.childNodes[0].Status = "";
			postObject.Template.childNodes[0].childNodes = [];
			postObject.Template.childNodes[0].childNodes[0] = new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName = "Shipment";

			postObject.Template.childNodes[0].childNodes[0].EnterpriseCode = "";
			postObject.Template.childNodes[0].childNodes[0].SellerOrganizationCode = "";
			postObject.Template.childNodes[0].childNodes[0].ShipmentKey = "";
			postObject.Template.childNodes[0].childNodes[0].ShipmentNo = "";*/

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			return postObject;
		}



		var enterprise="";
		var seller="";
		processContoller.InputController = function(scannedVariable){
			$scope.ErrorMessage="";
			
			var shipmentKey="";
			seller=$scope.frmBlindReceivepo_seller;
			console.log("Seller: " +seller);
			enterprise = $scope.frmBlindReceivepo_principal;
			console.log("Principal: " + $scope.frmBlindReceivepo_principal);

			//invoking getOrganizationList
			if(seller != undefined && enterprise != undefined){
				var postObject = getOrganizationListApi(seller);
				console.log("Abhilash post object: " +postObject);


				invokeApi.async(postObject).then(function(response){
				$scope.postResponse = response;
				console.log("Abhilash Next check: " +JSON.stringify($scope.postResponse));

				if($scope.postResponse.OrganizationList.Organization)
				{
					console.log("EnterPrise: " +$scope.postResponse.OrganizationList.Organization);
					var ent = $scope.postResponse.OrganizationList.Organization.PrimaryEnterpriseKey;
					console.log("EnterPrise: " +$scope.postResponse.OrganizationList.Organization.PrimaryEnterpriseKey);
					if (enterprise == ent){

						var postReciptlstObject = getReceiptListApi(enterprise, seller, node);
						console.log(postReciptlstObject);

						invokeApi.async(postReciptlstObject).then(function(receiptResponse){
							console.log(receiptResponse);
							$scope.postReceiptResponse = receiptResponse;

							console.log("Receipt Check: " +JSON.stringify($scope.postReceiptResponse));

							
							if($scope.postReceiptResponse.ReceiptList.TotalNumberOfRecords != 0){
								
								$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList; 
						    	  var receiptContext = $rootScope.receiptContext ;
								if(angular.isArray($scope.postReceiptResponse.ReceiptList.Receipt)){
									var i;
									for(i=0;i<$scope.postReceiptResponse.ReceiptList.Receipt.length; i++)
									{
										var receiptStatus = $scope.postReceiptResponse.ReceiptList.Receipt[i].Status;
										var receiptSeller = $scope.postReceiptResponse.ReceiptList.Receipt[i].Shipment.SellerOrganizationCode;
										var receiptEnt = $scope.postReceiptResponse.ReceiptList.Receipt[i].Shipment.EnterpriseCode;

										console.log("Receipt Status: " +$scope.postReceiptResponse.ReceiptList.Receipt[i].Status);
										console.log("Receipt Seller: " +receiptSeller);
										console.log("Receipt Enterprise: " +receiptEnt);
										if(receiptStatus != "1300" && receiptStatus != "1200"){
											console.log("Condition for multi receipt:---- receiptStatus != 1300 && receiptStatus != 1200");
											$scope.ErrorMessage = "Click on button to create a new shipment";
											
										}
										else{
											$rootScope.ReceiptFlag=true;
											$rootScope.BlindReceiptBasedFlag= true;	
											$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt[i];
											
											var shpKey = $scope.postReceiptResponse.ReceiptList.Receipt[i].ShipmentKey;
											
											//get the shipmentcontext:
											
											var postShipmentObject = getShipmentListApi(node, shpKey);
									     	console.log(postShipmentObject);
											invokeApi.async(postShipmentObject).then(function(shipResponse) {
												console.log("Shipment List O.p" +shipResponse );
												if(shipResponse.Shipments.Shipment){
											    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
											    	  console.log("Shipment Details");
													  console.log($rootScope.shipmentContext);
													  console.log("Proceed with existing open receipt");
													  $location.path('/Receive/pallets');
												}  
											
											
											
											});
										}
									}// end of for loop
								}
								else{
									
									var receiptStatus = $scope.postReceiptResponse.ReceiptList.Receipt.Status;
									if($scope.postReceiptResponse.ReceiptList.Receipt){
											if(receiptStatus != "1300" && receiptStatus != "1200"){
												console.log("Condition for single receipt:---- receiptStatus != 1300 && receiptStatus != 1200");
												$scope.ErrorMessage = "Click on button to create a new shipment";
												
											}
											else{
												$rootScope.ReceiptFlag=true;
												$rootScope.BlindReceiptBasedFlag= true;	
												$rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt;
												var shpKey = $scope.postReceiptResponse.ReceiptList.Receipt.ShipmentKey;
												
												//get the shipmentcontext:
												
												var postShipmentObject = getShipmentListApi(node, shpKey);
										     	console.log(postShipmentObject);
										     	invokeApi.async(postShipmentObject).then(function(shipResponse) {
										     		//$scope.postResponse = shipResponse;
										     		console.log("Shipment List O.p" +$scope.postResponse );
										     		if(shipResponse.Shipments.Shipment){
										     			$rootScope.shipmentContext = shipResponse.Shipments.Shipment;
										     			console.log("Shipment Details");
										     			console.log($rootScope.shipmentContext);
										     			console.log("Proceed with existing open receipt");
										     			$location.path('/Receive/pallets');
										     		}
										     	});
											}
									}
								}
								}
							else
								{
								console.log("Condition for new receipt:---- ");
								$rootScope.ReceiptFlag=false;
								$scope.ErrorMessage = "Click on button to create a new shipment";
								
								
								}
							
						});// end of getreceiptlist
					}
					else{
						$scope.ErrorMessage = "Invalid enterprise for seller";
					}
					
				}
				else{
					$scope.ErrorMessage = "Invalid seller";
				}
				
			});// end of OrganizationList
			}
			else{
				$scope.ErrorMessage = "Enter seller / enterprise";
			}
			
		};
		
		$scope.createPO = function()
		{
			var postObject = createShipmentApi(enterprise, seller, node);
			invokeApi.async(postObject).then(function(response){
				$scope.postResponse = response;
				console.log($scope.postResponse);
				var shpKey= $scope.postResponse.Shipment.ShipmentKey;
				console.log("SHIPMNT KEY" +shpKey );
				console.log("Abhilash Create Shipment API check: " +JSON.stringify($scope.postResponse));
				
				console.log("SHIPMNT KEY" +shpKey );
				//get the shipment context:
				var postShipmentObject = getShipmentListApi(node, shpKey);
		     	console.log(postShipmentObject);
				invokeApi.async(postShipmentObject).then(function(shipResponse) {
					if(shipResponse.Shipments.Shipment){
				    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
				    	  console.log("Shipment Details");
						  console.log($rootScope.shipmentContext);
					}  
					
				
				$rootScope.BlindReceiptBasedFlag= true;
				$rootScope.ReceiptFlag=false;
				$location.path('/Receive/pallets');
				});
			});
		};// end of function createPO
		
	});

});

