define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerItemDetails', function($scope, $rootScope,$timeout,$location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService)
			{

		var formName = 'frmReceiveitemdetails';
		var recevingNode = $cookieStore.get('globals').OrganizationCode;
		$rootScope.recevingNode=recevingNode;
		
		
		
		// for last scanned item storage
		if(!$rootScope.ITEMS){
			$rootScope.ITEMS=[];
		}
		/*var caseid =  $rootScope.caseId;
		var palletid = $rootScope.palletId;
		var tagFlag="";
		
		console.log("Case / pallet");
		console.log(caseid+palletid);*/
		
		$rootScope.receive_moduleFlow = 'Item Details';
		$rootScope.receive_NavActive = "Item Details";
		console.log("Item Detail controller");

		var orderLineContext = $rootScope.orderLineContext || {};
		var orderContext= $rootScope.orderContext || {};
		var itemNodeContext= $rootScope.ItemNodeContext;
		console.log("itemNodeContext:");
		console.log(itemNodeContext);
		var shipmentContext = $rootScope.shipmentContext || {};
		console.log("shipmentContext:");
		console.log(shipmentContext);
		var shipmentLineContext =$rootScope.shipmentLineContext || {};
		var receiptContext = $rootScope.receiptContext || {};
		console.log("Recept context deta:");
		console.log(receiptContext);
		
		var nodePrefResponseContext= $rootScope.nodePrefResponseContext || {};
		console.log("nodePrefResponseContext");
		console.log(nodePrefResponseContext);
		
		var dispCode = nodePrefResponseContext.NodeReceivingPreferences.NodeReceivingPreference.DefaultReceivingDispositionCode;
		console.log("Disp Code:::" +dispCode);
		if(dispCode != undefined){
			$scope.frmReceiveitemdetails_dispositionCode=dispCode;
		}
		
		var itemId = $rootScope.ItemID;
		
		//serial tracked item
		var serArray = $rootScope.serArray;
		console.log("serArray in item details screen " + serArray);
		var serialQty = $rootScope.ScannedQty;
		$scope.showqty=true;
		if(serArray){
			$scope.showqty=false;
		}
		
		/*if($rootScope.OrderBasedFlag){
			var Qty =orderLineContext.OrderedQty;
			$rootScope.Qty=Qty;
		}
		else{
			var shpQty = shipmentLineContext.Quantity;
			var recvQty =shipmentLineContext.ReceivedQuantity;
			$rootScope.Qty=shpQty-recvQty;
			var Qty = $rootScope.Qty;
		}*/
		
		
		
		/*$scope.lotno_display= false;
		$scope.batchno_display= false;*/
		
		  var entNo="";
		  var docType="";
		  if($rootScope.OrderBasedFlag){
			  entNo= orderContext.OrderList.Order.EnterpriseCode; 
			  console.log("ENT nO in Order based" +entNo );
			  doctype="0005";
			  console.log("doctype in Order based" +doctype );
		  }
		  
		  if( $rootScope.ShipmentBasedFlag){
			  entNo= shipmentContext.EnterpriseCode;
			  console.log("ENT nO in SHIP" +entNo );
			  doctype="0005";
			  console.log("doctype in Blind based" +doctype );
		  }
		 
			 
			 
		  if($rootScope.BlindReceiptBasedFlag){
			  entNo= shipmentContext.EnterpriseCode;
			  console.log("ENT nO in BLIND" +entNo );
			  doctype="0010";
			  console.log("doctype in Blind based" +doctype );
			  }
			 
		
		/*****************receiveOrder Api Xml formation*****************************************/
		function receiveOrderApi(itemId, recHdrKey, RecDock, recevingNode, qty, dispositionCode){
			console.log("$rootScope.PALLET:" + $rootScope.PALLET);
			console.log("$rootScope.CASE:" +$rootScope.CASE);
			var postObject = new Object();
			postObject.CommandName = "receiveOrder";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Receipt";
			postObject.InputXml.DocumentType = doctype;
			postObject.InputXml.ReceiptHeaderKey = recHdrKey;
			postObject.InputXml.ReceivingDock = RecDock;
			postObject.InputXml.ReceivingNode = recevingNode;
			postObject.InputXml.IgnoreOrdering="Y";
			
			if($rootScope.CASE== "" && $rootScope.PALLET != "" ){
				postObject.InputXml.PalletId = $rootScope.PALLET;
			}
			else{
				postObject.InputXml.PalletId = "";
			}
			
			if(entNo!= undefined)
				postObject.InputXml.OrganizationCode=entNo;
			else
				postObject.InputXml.OrganizationCode="";
			
			var shipmentKey=  shipmentContext.ShipmentKey;
			if(shipmentKey != undefined)
				postObject.InputXml.ShipmentKey = shipmentKey;
			else
				postObject.InputXml.ShipmentKey = "";
			
			//write the logic for reading the array
			if($rootScope.CASE != ""){
				postObject.InputXml.CaseId = $rootScope.CASE;
				
			}
			else{
				postObject.InputXml.CaseId = "";;
			}
			
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="ReceiptLines";
			
			if($rootScope.PALLET != "" && $rootScope.CASE != "" ){
				
				postObject.InputXml.childNodes[1]=new Object();
				postObject.InputXml.childNodes[1].tagName="ParentLPN";
				postObject.InputXml.childNodes[1].PalletId=$rootScope.PALLET;
			}
			

			// check if serial item scanned or not:
			
			console.log("Serial Array" +JSON.stringify(serArray) );
			console.log("Serial Qty" +serialQty );
			var secserial= itemNodeContext.Item.PrimaryInformation.NumSecondarySerials;
			if(serArray != undefined){
				postObject.InputXml.childNodes[0].childNodes= [];
				
				for (var i =1; i<=serialQty;i++ ){
					
					postObject.InputXml.childNodes[0].childNodes[i]=new Object();
					postObject.InputXml.childNodes[0].childNodes[i].tagName="ReceiptLine";
					postObject.InputXml.childNodes[0].childNodes[i].ItemID=itemId;
					postObject.InputXml.childNodes[0].childNodes[i].Quantity="1";
					var dispCode = nodePrefResponseContext.NodeReceivingPreferences.NodeReceivingPreference.DefaultReceivingDispositionCode;
					console.log("Disp Code:::" +dispCode);
					if(dispCode) 
						postObject.InputXml.childNodes[0].childNodes[i].DispositionCode=dispCode;
					else{
						postObject.InputXml.childNodes[0].childNodes[i].DispositionCode="";
					}
					if($rootScope.BlindReceiptBasedFlag){
						var uom = itemNodeContext.Item.UnitOfMeasure;
						postObject.InputXml.childNodes[0].childNodes[i].UnitOfMeasure=uom;
						
					}
					postObject.InputXml.childNodes[0].childNodes[i].SerialNo=serArray[i][0].serialno;
					
					
					// add the sec serial data SerialDetail tag
					//<SerialDetail SecondarySerial1="" SecondarySerial2="" SecondarySerial3="" SecondarySerial4="" SecondarySerial5="" SecondarySerial6="" SecondarySerial7="" SecondarySerial8="" SecondarySerial9="" />
					if(secserial !=0){
						postObject.InputXml.childNodes[0].childNodes[i].childNodes = [];
						postObject.InputXml.childNodes[0].childNodes[i].childNodes[i] = new Object();
							postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].tagName="SerialDetail";
							var arrSecSerial = serArray[i][1];
							console.log("arrSecSerial======="+arrSecSerial);
							console.log("arrSecSerial val======="+arrSecSerial[0].key1);
							for(var j=0; j<secserial; j++){
								if(arrSecSerial[j].key1 != undefined){
									
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial1=arrSecSerial[j].key1;
								}	
								if(arrSecSerial[j].key2  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial2=arrSecSerial[j].key2;
								}	
								if(arrSecSerial[j].key3  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial3=arrSecSerial[j].key3;
								}	
								if(arrSecSerial[j].key4  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial4=arrSecSerial[j].key4;
								}	
								if(arrSecSerial[j].key5  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial5=arrSecSerial[j].key5;
								}	
								if(arrSecSerial[j].key6  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial6=arrSecSerial[j].key6;
								}	
								if(arrSecSerial[j].key7  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial7=arrSecSerial[j].key7;
								}	
								if(arrSecSerial[j].key8 != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial8=arrSecSerial[j].key8;
								}	
								if(arrSecSerial[j].key9  != undefined){
									postObject.InputXml.childNodes[0].childNodes[i].childNodes[i].SecondarySerial9=arrSecSerial[j].key9;
								}	
							}
					}
					
					//if time sensitive item
					if($rootScope.datatime){
						postObject.InputXml.childNodes[0].childNodes[0].ShipByDate=$rootScope.datatime;
					}
					
					
					//tag detail capturing
					var recAttFields= $rootScope.recAttFields;
					console.log(recAttFields);
					if(recAttFields != undefined){
						for(var j=0; i<recAttFields.length; j++) {
					    
					    
							if($rootScope.recAttFields[i].id =="LotNumber") {
								postObject.InputXml.childNodes[0].childNodes[0].LotNumber=recAttFields[j].value;
				           
							}
							if($rootScope.recAttFields[i].id =="BatchNumber") {
								postObject.InputXml.childNodes[0].childNodes[0].BatchNo=recAttFields[j].value;
				           
							}
							if($rootScope.recAttFields[i].id =="RevisionNumber") {
								postObject.InputXml.childNodes[0].childNodes[0].RevisionNo=recAttFields[j].value;
				           
					        }
							if($rootScope.recAttFields[i].id =="LotAttNo") {
									postObject.InputXml.childNodes[0].childNodes[0].LotNumber=recAttFields[j].value;
				           
							}
							if($rootScope.recAttFields[i].id =="BatchAttNo") {
								postObject.InputXml.childNodes[0].childNodes[0].BatchNo=recAttFields[j].value;
				           
							}
							if($rootScope.recAttFields[i].id =="RevisionAttNo") {
								postObject.InputXml.childNodes[0].childNodes[0].RevisionNo=recAttFields[j].value;
				           
							}
							if($rootScope.recAttFields[i].id =="LotAttribute1") {
								postObject.InputXml.childNodes[0].childNodes[0].LotAttribute1=recAttFields[j].value;
					           
					        }
							if($rootScope.recAttFields[i].id =="LotAttribute2") {
								postObject.InputXml.childNodes[0].childNodes[0].LotAttribute2=recAttFields[j].value;
					           
					        }
							if($rootScope.recAttFields[i].id =="LotAttribute3") {
								postObject.InputXml.childNodes[0].childNodes[0].LotAttribute3=recAttFields[j].value;
					           
					        }
							if($rootScope.recAttFields[i].id =="LotReference") {
								postObject.InputXml.childNodes[0].childNodes[0].LotKeyReference=recAttFields[j].value;
							
					        }
							if($rootScope.recAttFields[i].id =="ManufacturingDate") {
								postObject.InputXml.childNodes[0].childNodes[0].ManufacturingDate=recAttFields[j].value;
					           
					        }

						}//end of tag array
					}
				}// end of serial array
				
			}
			else{
				
				postObject.InputXml.childNodes[0].childNodes= [];
				postObject.InputXml.childNodes[0].childNodes[0]=new Object();
				postObject.InputXml.childNodes[0].childNodes[0].tagName="ReceiptLine";
				postObject.InputXml.childNodes[0].childNodes[0].ItemID=itemId;
				postObject.InputXml.childNodes[0].childNodes[0].Quantity=qty;
				
				if(dispCode)
					postObject.InputXml.childNodes[0].childNodes[0].DispositionCode=dispCode;
				else{
					postObject.InputXml.childNodes[0].childNodes[0].DispositionCode="";
				}
				
				if($rootScope.BlindReceiptBasedFlag){
					var uom = itemNodeContext.Item.UnitOfMeasure;
					postObject.InputXml.childNodes[0].childNodes[0].UnitOfMeasure=uom;
					
				}
				
				//if time sensitive item
				if($rootScope.datatime){
					postObject.InputXml.childNodes[0].childNodes[0].ShipByDate=$rootScope.datatime;
				}
				
				//if tag controlled item
				var recAttFields= $rootScope.recAttFields;
				//console.log(recAttFields +"Length:"+ recAttFields.length);
				if(recAttFields != undefined){
					for(var j=0; j<recAttFields.length; j++) {
				    
				    
						if($rootScope.recAttFields[j].id =="LotNumber") {
							postObject.InputXml.childNodes[0].childNodes[0].LotNumber=recAttFields[j].value;
			           
						}
						if($rootScope.recAttFields[j].id =="BatchNumber") {
							postObject.InputXml.childNodes[0].childNodes[0].BatchNo=recAttFields[j].value;
			           
						}
						if($rootScope.recAttFields[j].id =="RevisionNumber") {
							postObject.InputXml.childNodes[0].childNodes[0].RevisionNo=recAttFields[j].value;
			           
						}
						if($rootScope.recAttFields[j].id =="LotAttNo") {
							postObject.InputXml.childNodes[0].childNodes[0].LotNumber=recAttFields[j].value;
			           
						}
						if($rootScope.recAttFields[j].id =="BatchAttNo") {
							postObject.InputXml.childNodes[0].childNodes[0].BatchNo=recAttFields[j].value;
			           
						}
						if($rootScope.recAttFields[j].id =="RevisionAttNo") {
							postObject.InputXml.childNodes[0].childNodes[0].RevisionNo=recAttFields[j].value;
			           
						}
						if($rootScope.recAttFields[j].id =="LotAttribute1") {
							postObject.InputXml.childNodes[0].childNodes[0].LotAttribute1=recAttFields[j].value;
				           
				        }
						if($rootScope.recAttFields[j].id =="LotAttribute2") {
							postObject.InputXml.childNodes[0].childNodes[0].LotAttribute2=recAttFields[j].value;
				           
				        }
						if($rootScope.recAttFields[j].id =="LotAttribute3") {
							postObject.InputXml.childNodes[0].childNodes[0].LotAttribute3=recAttFields[j].value;
				           
				        }
					if($rootScope.recAttFields[j].id =="LotReference") {
						postObject.InputXml.childNodes[0].childNodes[0].LotKeyReference=recAttFields[j].value;
				           
				        }
						if($rootScope.recAttFields[j].id =="ManufacturingDate") {
							postObject.InputXml.childNodes[0].childNodes[0].ManufacturingDate=recAttFields[j].value;
				           
				        }

					}//end of tag array
				}
			}
				
		
			
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();

			console.log("postObject object receive Order "+JSON.stringify(postObject));
			return postObject;
		};

		


		$scope.receiveOrder = function(){
			
			$scope.ErrorMessage="";
			var qty = $scope.frmReceiveitemdetails_qty;
			var dispCode = $scope.frmReceiveitemdetails_dispositionCode;
			
			if (serArray){
				qty=serialQty;
			}
			
			if(qty != undefined && dispCode != undefined){
			
					var shpKey="";
					var revOrderPostObject = receiveOrderApi(itemId,receiptContext.ReceiptHeaderKey, receiptContext.ReceivingDock,recevingNode, qty,dispCode);
					console.log("Receive Order for receving");
					console.log(revOrderPostObject);
					invokeApi.async(revOrderPostObject).then(function(revOrderResponse) {
							$scope.postReciveOrderResponse = revOrderResponse;
							console.log($scope.postReciveOrderResponse);

							if($scope.postReciveOrderResponse.ErrorCode){
								$scope.ErrorMessage=" "+$scope.postReciveOrderResponse.ErrorCode+" "+$scope.postReciveOrderResponse.ErrorDescription;
								//$scope.frmReceivepallet_closeReceiveSubmit = false;
								console.log("Error in receive Order api ");
							}
							else{

								$scope.InfoMessage = " Received item " +itemId;
								console.log("received the item");
								$rootScope.itemScan_done_button = true;
								$rootScope.closeCase_button = true;
								$rootScope.ITEMS.push(itemId);
                                //$rootScope.ITEMS.reverse();
								
								 $timeout(function() {
									 $location.path('/Receive/itemScan');
								      },1000);
								 $rootScope.Qty="";
								 
								 // clearing the variables
								 
								 $rootScope.recAttFields="";
								 $rootScope.datatime="";
								 $rootScope.serArray="";
								 $rootScope.ScannedQty="";
								 $rootScope.ItemID="";
							}
					});
				}
			
				
			};
			
			
			

		});
});








