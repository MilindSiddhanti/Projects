define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('receiveControllerPallets', function($scope, $rootScope,$timeout, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){
		
	  var formName = 'frmReceivepallet';
	  var recevingNode = $cookieStore.get('globals').OrganizationCode;
	  $rootScope.recevingNode=recevingNode;
	  var loc_value="";//location id scan value
	  $rootScope.itemScan_done_button = false; // for item scan page
	  $rootScope.itemDetail_save_button= true; // for item detail screen
	  
	  console.log("Order BAsed flag:" + $rootScope.OrderBasedFlag);  
	  console.log("Shipment BAsed flag:" + $rootScope.ShipmentBasedFlag); 
	  console.log("Blind Receipt BAsed flag:" + $rootScope.BlindReceiptBasedFlag); 
	  console.log("Receipt Open flag: " + $scope.ReceiptFlag);
	  
	  var shipmentContext = $rootScope.shipmentContext || {};
	  var orderContext = $rootScope.orderContext || {};
	  var receiptContext = $rootScope.receiptContext || {};
	  var reciptOpenFlag= false;
	  var entNo="";
	  var ordNo="";
	  var doctype="";
	  
	  // for last scanned item storage
		if(!$rootScope.LPNS){
			$rootScope.LPNS=[];
		}
		
		$rootScope.PALLET="";
		
		
	 
	  
	  if($rootScope.OrderBasedFlag){
		  entNo= orderContext.OrderList.Order.EnterpriseCode; 
		  console.log("ENT nO in Order based" +entNo );
		  doctype="0005";
		  console.log("doctype in Order based" +doctype );
	  }
	  
	  if( $rootScope.ShipmentBasedFlag){
		  entNo= shipmentContext.EnterpriseCode;
		  console.log("ENT nO in SHIP" +entNo );
		  doctype="0005";
		  console.log("doctype in Blind based" +doctype );
	  }
	 
		 
		 
	  if($rootScope.BlindReceiptBasedFlag){
		  entNo= shipmentContext.EnterpriseCode;
		  console.log("ENT nO in BLIND" +entNo );
		  doctype="0010";
		  console.log("doctype in Blind based" +doctype );
		  }
		 
	  
	  
		if($scope.ReceiptFlag){
			//location should be populated in the text box
			var locid="";
			console.log("receipt context inside the pallet screen" +receiptContext );
				
	
			if(receiptContext.OpenReceiptFlag == "Y"){
				locid= receiptContext.ReceivingDock;
				console.log("Receiving dock" +locid);
			}
			
			$scope.frmReceivepallet_locId =locid;
		}

	  
	 
		
		
		
	  
	 /* if($rootScope.OrderBasedFlag){
		  var entNo= orderContext.OrderList.Order.EnterpriseCode; 
		  var ordNo= orderContext.OrderList.Order.OrderNo;
		  var locid="";
		  $rootScope.receive_moduleFlow = 'Location Scan';
		  $rootScope.receive_NavActive = "Location Scan";
	  	  console.log("Location controller");
	  	  $rootScope.pallet_display= false;
	  	  $rootScope.loc_display= true;
	  	  
	  	  //populate the location text box with location id:

		  var nodePrefObject=getNodeReceivingPreferenceListApi(entNo,recevingNode);
		  invokeApi.async(nodePrefObject).then(function(nodePrefResponse) {
			console.log("Node Pref list");
			console.log(nodePrefResponse);
			
			//if shipment entry is allowed createing a shipment
			if(nodePrefResponse.NodeReceivingPreferences.NodeReceivingPreference.AllowManualShipmentEntry){
			
				var postReciptlstObject = getReceiptListApi(entNo, recevingNode, ordNo,locid);
		     	console.log(postReciptlstObject);
				invokeApi.async(postReciptlstObject).then(function(receiptResponse) {
		    		  console.log(receiptResponse);
		    		  $scope.postReceiptResponse = receiptResponse;
		    		  if($scope.postReceiptResponse.ReceiptList.TotalNumberOfRecords){
				    	  $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList; 
				    	  var receiptContext = $rootScope.receiptContext ;
				    	  console.log("Receipt Details");
						  console.log(receiptContext);
						  
						  var recpListarrayflag = angular.isArray(receiptContext.Receipt);
							
							if (recpListarrayflag){
								
								for (var i=0; i < receiptContext.Receipt.length; i++){
									
									if(receiptContext.Receipt[i].OpenReceiptFlag=="Y"){
										reciptOpenFlag = true;
										locid= receiptContext.Receipt[i].ReceivingDock;
										var shpKey= receiptContext.Receipt[i].ShipmentKey;
										console.log("Open receipt is there for the order passed in loc " +locid );
										 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt[i]; 
								    	  var receiptContext = $rootScope.receiptContext ;
								    	  console.log("Receipt Details");
										  console.log(receiptContext);
										  
										  //get the shipment Context:
										  
										  var postShipmentObject = getShipmentListApi(entNo, recevingNode, shpKey, ordNo);
									     	console.log(postShipmentObject);
											invokeApi.async(postShipmentObject).then(function(shipResponse) {
												if(shipResponse.Shipments.Shipment){
											    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
											    	  console.log("Shipment Details");
													  console.log($rootScope.shipmentContext);
												
												}  
												
											});
							  
										break;							
									}
								
								}
							}
							else{
								
								if(receiptContext.Receipt.OpenReceiptFlag == "Y"){
									reciptOpenFlag = true;
									locid= receiptContext.Receipt.ReceivingDock;
									var shpKey = receiptContext.Receipt.ShipmentKey;
									console.log("Open receipt is there for teh order passed in loc "+ locid);
									 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt; 
							    	  var receiptContext = $rootScope.receiptContext ;
							    	  console.log("Receipt Details");
									  console.log(receiptContext);
									  
									  //get the shipment Context:
									  
									  var postShipmentObject = getShipmentListApi(entNo, recevingNode, shpKey, ordNo);
								     	console.log(postShipmentObject);
										invokeApi.async(postShipmentObject).then(function(shipResponse) {
											if(shipResponse.Shipments.Shipment){
										    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
										    	  console.log("Shipment Details");
												  console.log($rootScope.shipmentContext);
											}  
											
										});
								}
							}
		    		  }
		    		  
		    		  console.log("loc+flag"+locid + reciptOpenFlag);
					  if(reciptOpenFlag){
						  
						 $scope.frmReceivepallet_locId =locid;
						
					  }
		    	});
			}
			else{
				$scope.ErrorMessage("Manual entry for shipment not checked");
			}
			
			
		 });
		
		  
	  	  
	  }
	  else{
		  $rootScope.receive_moduleFlow = 'LPNs';
		  $rootScope.receive_NavActive = "LPNs";
	  	  console.log("Pallet controller");
	  	  $rootScope.loc_display= false;
	  	  $rootScope.pallet_display= true;
	  }*/
	  
	 
	  
	  var Logger = $log.getInstance($state.$current.parent);//parent

		//Logger.enableLogging(true);
	  console.log("Shipment Details:");
	  console.log(shipmentContext);
	  console.log("receipt Details:");
	  
	  console.log(receiptContext);
	  

	  Logger.info("Log from ReveiveController Pallets"); 
	

	  
	  /*****************getNodeReceivingPreferenceList Api Xml formation*****************************************/
	  
	  
	  
	  
	  function getNodeReceivingPreferenceListApi(EnterpriseCode,Node,doctype){
		  var postObject = new Object();
		  postObject.CommandName = "getNodeReceivingPreferenceList";
		  postObject.InputXml = new Object();
		  postObject.InputXml.tagName = "NodeReceivingPreference";
		  postObject.InputXml.DocumentType = doctype;
		  postObject.InputXml.EnterpriseCode = EnterpriseCode;
		  postObject.InputXml.Node =Node;
		  postObject.InputXml.IgnoreOrdering = "Y";
		  
		  postObject.Template = new Object();
		  postObject.Template.tagName="NodeReceivingPreferences";
		  postObject.Template.childNodes= [];
		  postObject.Template.childNodes[0]=new Object();
		  postObject.Template.childNodes[0].tagName="NodeReceivingPreference";
		  postObject.Template.childNodes[0].AllowManualShipmentEntry="";
		  postObject.Template.childNodes[0].PalletBuildPreference="";
		  postObject.Template.childNodes[0].CaseBuildPreference="";
		  postObject.Template.childNodes[0].CloseReceiptPreference="";
		  postObject.Template.childNodes[0].DefaultReceivingDispositionCode="";
		  postObject.Template.childNodes[0].DefaultInspectionDispositionCode=""
		  
		  postObject.IsService = "N";
		  postObject.Authenticate=AuthenticationService.getAuth();
		  console.log("postObject Node Rec Pref "+JSON.stringify(postObject));
		  return postObject;
		  
	  };
	  
	  
	  /*********************** startReceiptApi XML **************************/
	  
	  function startReceiptApi(entNo,recevingNode,loc_value,ordNo,shpno,doctype){
		
		 
		  var postObject = new Object();
		  postObject.CommandName = "startReceipt";
		  postObject.InputXml = new Object();
		  postObject.InputXml.tagName = "Receipt";
		  postObject.InputXml.DocumentType = doctype;
		  postObject.InputXml.IgnoreOrdering="Y";
		  postObject.InputXml.ReceivingDock = loc_value;
		  postObject.InputXml.ReceivingNode = recevingNode;
		  postObject.InputXml.ShipmentKey = "";
		  
		  postObject.InputXml.childNodes= [];
		  postObject.InputXml.childNodes[0]=new Object();
		  postObject.InputXml.childNodes[0].tagName="Shipment";
		  postObject.InputXml.childNodes[0].EnterpriseCode=entNo;
		  postObject.InputXml.childNodes[0].OrderNo=ordNo;
		  postObject.InputXml.childNodes[0].ShipmentNo=shpno;
		  
		  postObject.Template = new Object();
		  
		  postObject.IsService = "N";
		  postObject.Authenticate=AuthenticationService.getAuth();
		  
		  console.log("postObject start receipt "+JSON.stringify(postObject));
		  return postObject;
		  
	  };

	  /*****************receiveOrder Api Xml formation*****************************************/
	  function receiveOrderApi(containerScm, recHdrKey, RecDock, recevingNode, shipmentKey){
		 
		//var entNo =shipmentContext.EnterpriseCode;
		var postObject = new Object();
		postObject.CommandName = "receiveOrder";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Receipt";
		postObject.InputXml.DocumentType = doctype;
		postObject.InputXml.IgnoreOrdering="Y" ;
		
		postObject.InputXml.PalletId = containerScm;
			//postObject.InputXml.ClosePallet = "Y";
		
		postObject.InputXml.ReceiptHeaderKey = recHdrKey;
		postObject.InputXml.ReceivingDock = RecDock;
		postObject.InputXml.ReceivingNode = recevingNode;
		postObject.InputXml.ShipmentKey = shipmentKey;
		
		postObject.InputXml.childNodes= [];
		postObject.InputXml.childNodes[0]=new Object();
		postObject.InputXml.childNodes[0].tagName="Shipment";
		postObject.InputXml.childNodes[0].EnterpriseCode=entNo;
		
		postObject.Template = new Object();
		
		postObject.IsService = "N";
		postObject.Authenticate=AuthenticationService.getAuth();
		
		console.log("postObject object receiveorder "+JSON.stringify(postObject));
		return postObject;
	   };
	   
	   /*****************translateBarCode Api Xml formation*****************************************/
		
		function translateBarCodeApi(lpnNo){
			var postObject = new Object();
			//var entNo= shipmentContext.EnterpriseCode;
			console.log("Ent NO" +entNo );
			postObject.CommandName = "translateBarCode";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "BarCode";
			postObject.InputXml.BarCodeData = lpnNo;
			postObject.InputXml.BarCodeType = "ShippingOrInventoryContainer";
			
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="ContextualInfo";
			postObject.InputXml.childNodes[0].OrganizationCode=recevingNode;
			postObject.InputXml.childNodes[0].EnterpriseCode=entNo;
			
			postObject.Template = new Object();
			postObject.Template.tagName="BarCode";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Translations";
			postObject.Template.childNodes[0].BarCodeTranslationSource="";
			postObject.Template.childNodes[0].TotalNumberOfRecords="";
			
			
			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="Translation";
		
			postObject.Template.childNodes[0].childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object;
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="ContainerContextualInfo";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].CaseId="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].PalletId="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].IsNewContainer="";
			 
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			
			console.log("postObject object Barcode "+JSON.stringify(postObject));
			
			return postObject;
			
		};
		
		/*****************getReceiptList Api Xml formation*****************************************/
		function getReceiptListApi(entNo, recevingNode, ordNo,locid,shipno,doctype){
			
			//var recDock="";
			var postObject = new Object();
			postObject.CommandName = "getReceiptList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Receipt";
			postObject.InputXml.DocumentType = doctype;
			postObject.InputXml.ReceivingDock =locid ;
			
			
			postObject.InputXml.ReceivingNode = recevingNode;
			//postObject.InputXml.ShipmentKey = shipmentKey;
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="Shipment";
			postObject.InputXml.childNodes[0].OrderNo=ordNo;
			postObject.InputXml.childNodes[0].ShipmentNo=shipno;
			postObject.InputXml.childNodes[0].EnterpriseCode=entNo;
			
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object get Receipt list "+JSON.stringify(postObject));
			return postObject;
		}
		
		
		/*****************getShipmentList Api Xml formation*****************************************/
		function getShipmentListApi(EntCode, recevingNode, ShpKey, orderno){
			var postObject = new Object();
			postObject.CommandName = "getShipmentList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Shipment";
			postObject.InputXml.DocumentType = doctype;
			postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.ReceivingNode = recevingNode;
			postObject.InputXml.ShipmentKey = ShpKey;
			postObject.InputXml.OrderNo=orderno;
			postObject.Template = new Object();
			postObject.Template.tagName = "Shipments";
			
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Shipment";
			postObject.Template.childNodes[0].DoNotVerifyCaseContent="";
			postObject.Template.childNodes[0].DoNotVerifyPalletContent="";	
			postObject.Template.childNodes[0].DocumentType="";	
			postObject.Template.childNodes[0].EnterpriseCode="";	
			postObject.Template.childNodes[0].ReceivingNode="";	
			postObject.Template.childNodes[0].SellerOrganizationCode="";	
			postObject.Template.childNodes[0].ShipmentNo="";	
			postObject.Template.childNodes[0].ShipmentKey="";	
			
			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="Containers";
			
			postObject.Template.childNodes[0].childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="Container";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerScm="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerNo="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerType="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ShipmentContainerKey="";
		
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object shipment list "+JSON.stringify(postObject));
			return postObject;
		};
		
		  /*****************getLPNListApi Xml formation*****************************************/
		
		function getLPNListApi(reference_value,reference_field){
			var postObject = new Object();
			postObject.CommandName = "getLPNList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "LPN";
			if(reference_field == "Pallet")
				postObject.InputXml.PalletId = reference_value;
			else
				postObject.InputXml.PalletId = "";
			if(reference_field == "Case")
				postObject.InputXml.CaseId = reference_value;
			else
				postObject.InputXml.CaseId = "";
			
			postObject.Template = new Object();
			postObject.Template.tagName="LPNs";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="LPN";
			postObject.Template.childNodes[0].CaseId="";
			postObject.Template.childNodes[0].PalletId="";
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object LPN LIst  "+JSON.stringify(postObject));
			return postObject;
		};
		
		 /*****************createLPN api Xml formation*****************************************/
		function createLPNapi(palletid){
			
			console.log("Inside the create LPN obj ");
			console.log(receiptContext);
			var postObject = new Object();
			postObject.CommandName = "createLPN";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "CreateLPN";
			postObject.InputXml.EnterpriseCode = entNo;
			postObject.InputXml.Node = recevingNode;
			
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="LPN";
			postObject.InputXml.childNodes[0].PalletId=palletid;
			postObject.InputXml.childNodes[0].ReceiptHeaderKey=receiptContext.ReceiptHeaderKey;
			
			postObject.InputXml.childNodes[0].childNodes= [];
			postObject.InputXml.childNodes[0].childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].childNodes[0].tagName="LPNLocation";
			postObject.InputXml.childNodes[0].childNodes[0].LocationId= receiptContext.ReceivingDock;
			
			postObject.InputXml.childNodes[1]=new Object();
			postObject.InputXml.childNodes[1].tagName="Audit";
			postObject.InputXml.childNodes[1].AdjustmentType="RECEIPT";
			postObject.InputXml.childNodes[1].ReasonCode="RECEIPT";
			postObject.InputXml.childNodes[1].DocumentType=doctype;
			postObject.InputXml.childNodes[1].ReceiptHeaderKey=receiptContext.ReceiptHeaderKey;
			postObject.InputXml.childNodes[1].ShipmentNo=shipmentContext.ShipmentNo;
			postObject.InputXml.childNodes[1].TaskType="Receive";
			
			postObject.Template = new Object();
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();

			console.log("postObject object createLPNapi "+JSON.stringify(postObject));

			return postObject;
			
		};
		
		/*****************getShipmentContainerListApi Xml formation*****************************************/
	
		function getShipmentContainerListApi(palletid,shpkey){
			
			var postObject = new Object();
			postObject.CommandName = "getShipmentContainerList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Container";
			postObject.InputXml.ContainerScm = palletid;
			postObject.InputXml.ShipmentKey = shpkey;
			postObject.InputXml.IgnoreOrdering = "Y";
			
			postObject.Template = new Object();
			postObject.Template.tagName="Containers";
			postObject.Template.TotalNumberOfRecords="";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Container";
			postObject.Template.childNodes[0].ContainerType="";
			postObject.Template.childNodes[0].ContainerNo="";
			postObject.Template.childNodes[0].ContainerScm="";
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();

			console.log("postObject object getShipmentContainerListApi "+JSON.stringify(postObject));

			return postObject;
			
		};
		
	  /*****************translateBarCode loc Api Xml formation*****************************************/

		function translateBarCodeLocApi(locid,entNo){
			
			console.log("Ent NO" +entNo );
			var postObject = new Object();
			postObject.CommandName = "translateBarCode";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "BarCode";
			postObject.InputXml.BarCodeData = locid;
			postObject.InputXml.BarCodeType = "Location";

			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="ContextualInfo";
			postObject.InputXml.childNodes[0].OrganizationCode=recevingNode;
			postObject.InputXml.childNodes[0].EnterpriseCode=entNo;

			postObject.Template = new Object();
			postObject.Template.tagName="BarCode";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="Translations";
			postObject.Template.childNodes[0].BarCodeTranslationSource="";

			postObject.Template.childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0]=new Object();
			postObject.Template.childNodes[0].childNodes[0].tagName="Translation";

			postObject.Template.childNodes[0].childNodes[0].childNodes= [];
			postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object;
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="LocationContextualInfo";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].LocationId="";
			postObject.Template.childNodes[0].childNodes[0].childNodes[0].ZoneId="";
			

			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();

			console.log("postObject object loc Barcode "+JSON.stringify(postObject));

			return postObject;

		};
		
		
		 /*****************receipt_shipment_contextSetting*****************************************/
		function receipt_shipment_contextSetting(entNo, recevingNode, ordNo,locid,shipno,doctype){
			var postReciptlstObject = getReceiptListApi(entNo, recevingNode, ordNo,locid,shipno,doctype);
	     	console.log(postReciptlstObject);
			invokeApi.async(postReciptlstObject).then(function(receiptResponse) {
	    		  console.log(receiptResponse);
	    		  $scope.postReceiptResponse = receiptResponse;
	    		  $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList; 
		    	  receiptContext = $rootScope.receiptContext ;
		    	  console.log("Receipt Details receipt_shipment_contextSetting");
				  console.log(receiptContext);
				  
				  var recpListarrayflag = angular.isArray(receiptContext.Receipt);
					console.log("Recept flag:" + recpListarrayflag);
					if (recpListarrayflag){
						
						for (var i=0; i < receiptContext.Receipt.length; i++){
							
							if(receiptContext.Receipt[i].OpenReceiptFlag=="Y"){
								reciptOpenFlag = true;
								locid= receiptContext.Receipt[i].ReceivingDock;
								var shpKey= receiptContext.Receipt[i].ShipmentKey;
								console.log("Open receipt is there for the order passed in loc " +locid );
								 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt[i]; 
						    	  receiptContext = $rootScope.receiptContext ;
						    	  console.log("Receipt Details");
								  console.log(receiptContext);
								  
								  //get the shipment Context:
								  
								  var postShipmentObject = getShipmentListApi(entNo, recevingNode, shpKey, ordNo);
							     	console.log(postShipmentObject);
									invokeApi.async(postShipmentObject).then(function(shipResponse) {
										if(shipResponse.Shipments.Shipment){
									    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
									    	  console.log("Shipment Details");
											  console.log($rootScope.shipmentContext);
										
										}  
										
									});
					  
								break;							
							}
						
						}
					}
					else{
						
						if(receiptContext.Receipt.OpenReceiptFlag == "Y"){
							reciptOpenFlag = true;
							var shpKey = receiptContext.Receipt.ShipmentKey;
							console.log("Open receipt is there for teh order passed in asdasd loc "+ locid);
							 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt; 
					    	  receiptContext = $rootScope.receiptContext ;
					    	  console.log("Receipt Details receipt_shipment_contextSetting dfdfdsfdsf");
							  console.log(receiptContext);
							  
							  //get the shipment Context:
							  
							  var postShipmentObject = getShipmentListApi(entNo, recevingNode, shpKey, ordNo);
						     	console.log(postShipmentObject);
								invokeApi.async(postShipmentObject).then(function(shipResponse) {
									if(shipResponse.Shipments.Shipment){
								    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
								    	  console.log("Shipment Details");
										  console.log($rootScope.shipmentContext);
									}  
									
								});
						}
					}
	
				  
			});
		};
	   /*************************************************palletConfigCheck function *******************/
		function palletConfigCheck(){
			
			console.log("expectedContainerFlag" +expectedContainerFlag);
			console.log("contentPalletCheckFlag" +contentPalletCheckFlag);
			console.log("valid_pallet" + valid_pallet);
			
			
			if(valid_pallet){
				
				if((expectedContainerFlag)&& (contentPalletCheckFlag =="Y")){
					$rootScope.PALLET=pallet_value;
				console.log("the pallet scanned is " +$rootScope.PALLET );
				
				//receive order for pallet  without content check
				
				var postObject = receiveOrderApi(pallet_value,receiptContext.ReceiptHeaderKey, receiptContext.ReceivingDock,recevingNode, shipmentContext.ShipmentKey);
				console.log("receive order for pallet  without content check");
				console.log(postObject);
				invokeApi.async(postObject).then(function(response) {
					$scope.postReciveOrderResponse = response;
					console.log("Response from rec");
					console.log(response);
					if($scope.postReciveOrderResponse.ErrorCode){
						$scope.ErrorMessage=" "+$scope.postReciveOrderResponse.ErrorCode+" "+$scope.postReciveOrderResponse.ErrorDescription;
						$scope.frmReceivepallet_closeReceiveSubmit = false;
					}
					else{
						$scope.frmReceivepallet_closeReceiveSubmit = false;
						
						$timeout(function() {
							
						      },1000);
							
						$scope.InfoMessage="LPN No: " + pallet_value + "   received";
						$rootScope.LPNS.push(pallet_value);
                        //$rootScope.LPNS.reverse();
					}
						
					console.log($scope.postReciveOrderResponse);
					
					//$location.path('/receive/pallets');
					$scope.frmReceivepallet_palletId="";
					
					
				});
				}
				if((expectedContainerFlag) && (contentPalletCheckFlag =="N")){
					// do content check for the valid pallet
					$rootScope.PALLET=pallet_value;
					console.log("the pallet scanned is " +$rootScope.PALLET );
					console.log("Going to the item scan page :do content check for the valid pallet");
					$location.path('/Receive/itemScan');
				}
				if(!expectedContainerFlag){
					// receive order for pallet creation and then go to item scan page
					
					var postObject = receiveOrderApi(pallet_value,receiptContext.ReceiptHeaderKey, receiptContext.ReceivingDock,recevingNode, shipmentContext.ShipmentKey);
					console.log("receive order for pallet creation and then go to item scan page");
					console.log(postObject);
					invokeApi.async(postObject).then(function(response) {
						$scope.postReciveOrderResponse = response;
						console.log("Response from rec");
						console.log(response);
						if($scope.postReciveOrderResponse.ErrorCode){
							$scope.ErrorMessage=" "+$scope.postReciveOrderResponse.ErrorCode+" "+$scope.postReciveOrderResponse.ErrorDescription;
							$scope.frmReceivepallet_closeReceiveSubmit = false;
						}
						else{
							$scope.frmReceivepallet_closeReceiveSubmit = false;
							$rootScope.LPNS.push(pallet_value);
	                        //$rootScope.LPNS.reverse();
							
							$scope.frmReceivepallet_palletId="";
							console.log("Going to the item scan page");
							$location.path('/Receive/itemScan');
						}
							
						console.log($scope.postReciveOrderResponse);
						
						//$location.path('/receive/pallets');
						
					});
				}
			}
			
			expectedContainerFlag=false;
			contentPalletCheckFlag="N";
			valid_pallet=false;
			
		};

	 
	  var pallet_value;
	  var valid_pallet = false;
	  var expectedContainerFlag=false;
	  var contentPalletCheckFlag="N";
	  processContoller.InputController = function(scannedVariable){
		  $scope.ErrorMessage="";
		  $scope.InfoMessage="";
			var ScannedData = $scope.$apply(formName+'_'+scannedVariable);
			if(scannedVariable == "palletId"){
				 var lpnNo= $scope.frmReceivepallet_palletId;
				   
				   if(lpnNo){
						
					   var case_value;
					   //var pallet_value;
					   /* var reference_value;
					   var reference_field;*/
					   
					   var barCodeObject=translateBarCodeApi(lpnNo);
					   invokeApi.async(barCodeObject).then(function(barCodeResponse) {
							$scope.postResponse = barCodeResponse;
							console.log("Barcode Output");
							console.log(barCodeResponse);
							console.log(barCodeResponse.BarCode.Translations.TotalNumberOfRecords);
							
							var noOfRecs = barCodeResponse.BarCode.Translations.TotalNumberOfRecords;
							console.log("Barcode rec:" + noOfRecs);
							if(noOfRecs=="1"){
								
								var barCodeContext= $scope.postResponse.BarCode.Translations;
								$rootScope.barCodeContext = barCodeContext;
								var conContextInfo= barCodeContext.Translation.ContainerContextualInfo;
								var itemContextInfo=barCodeContext.Translation.ItemContextualInfo;
								var locContextInfo = barCodeContext.Translation.LocationContextualInfo;
								
								if((conContextInfo  != undefined) && (itemContextInfo == undefined) && (locContextInfo == undefined)){
									var barCodeTranslationContext=barCodeContext.Translation.ContainerContextualInfo;
									console.log("BArcode ContainerContextualInfo translation");
									console.log(barCodeTranslationContext);
									pallet_value = barCodeTranslationContext.PalletId;
									if(barCodeTranslationContext.IsNewContainer != undefined){
										//it is new pallet that can be used for receiving
										
										if(pallet_value != undefined){
											valid_pallet= true;
										}
											
											
									}
									else if(barCodeTranslationContext.PalletId != undefined){
											valid_pallet= true;
											if($rootScope.ShipmentBasedFlag){
												var shpkey = shipmentContext.ShipmentKey;
												var getShipmentContainerListObj =getShipmentContainerListApi(lpnNo,shpkey);
												invokeApi.async(getShipmentContainerListObj).then(function(getShipmentContainerListObjResponse) {
														$scope.postResponse = getShipmentContainerListObjResponse;
														
														
														if(getShipmentContainerListObjResponse.Containers.TotalNumberOfRecords){
															var noOfRecs = getShipmentContainerListObjResponse.Containers.TotalNumberOfRecords;
															if(noOfRecs == "1"){
																expectedContainerFlag = true;
															}
														
														
														var contentPalletCheck = shipmentContext.DoNotVerifyPalletContent;
														if(contentPalletCheck == "Y"){
															contentPalletCheckFlag="Y";
														}
														}
														
												});
											}
											else{
												$scope.ErrorMessage="Invalid PaletId scan ";
											}
										}
										else{
											
											$scope.ErrorMessage="Invalid PaletId scan ";
										}
								}
								
								else if((conContextInfo  != undefined) &&  (locContextInfo != undefined)){
									var barCodeTranslationContext=barCodeContext.Translation.ContainerContextualInfo;
									console.log("BArcode ContainerContextualInfo translation");
									console.log(barCodeTranslationContext);
									pallet_value = barCodeTranslationContext.PalletId;
									if(barCodeTranslationContext.IsNewContainer != undefined){
										//it is new pallet that can be used for receiving
										pallet_value = barCodeTranslationContext.PalletId;
										if(pallet_value != undefined){
											valid_pallet= true;
										}
											
											
									}
									else if(barCodeTranslationContext.PalletId != undefined){
											valid_pallet= true;
											if($rootScope.ShipmentBasedFlag){
												var shpkey = shipmentContext.ShipmentKey;
												var getShipmentContainerListObj =getShipmentContainerListApi(lpnNo,shpkey);
												invokeApi.async(getShipmentContainerListObj).then(function(getShipmentContainerListObjResponse) {
														$scope.postResponse = getShipmentContainerListObjResponse;
														
														
														if(getShipmentContainerListObjResponse.Conatiners.TotalNumberOfRecords != undefined){
															var noOfRecs = getShipmentContainerListObjResponse.Conatiners.TotalNumberOfRecords;
															if(noOfRecs == "1"){
																expectedContainerFlag = true;
															}
														
														
														var contentPalletCheck = shipmentContext.DoNotVerifyPalletContent;
														if(contentPalletCheck == "Y"){
															contentPalletCheckFlag="Y";
														}
														}
														
														
												});
											}
											else{
												$scope.ErrorMessage="Invalid PaletId scan ";
											}
									}
								}
							}				
							
							else{
								$scope.ErrorMessage="Invalid LPN Scan";
								$scope.frmReceivepallet_palletId="";
							}
						
							});// end of barcode translation api call
				   }
				}
			
				if(scannedVariable == "locId"){
					 var locid= $scope.frmReceivepallet_locId;
					
					 var shipno="";
					
					   if(locid){
							
						   
						  
						  
						  var barCodeObject=translateBarCodeLocApi(locid,entNo);
						  invokeApi.async(barCodeObject).then(function(barCodeResponse) {
								$scope.postResponse = barCodeResponse;
								console.log("Barcode Output");
								console.log(barCodeResponse);
								console.log(barCodeResponse.BarCode.Translations.TotalNumberOfRecords);
										
								if(barCodeResponse.BarCode.Translations.TotalNumberOfRecords){
									
									var barCodeContext= $scope.postResponse.BarCode.Translations;
									
									var barCodeTranslationContext=barCodeContext.Translation.LocationContextualInfo;
									console.log("BArcode translation");
									console.log(barCodeContext);
									
									loc_value=barCodeTranslationContext.LocationId;
								
									console.log("Location Id");
									console.log(loc_value);
									
									if($rootScope.OrderBasedFlag){
										
										ordNo= orderContext.OrderList.Order.OrderNo;
										if(!$scope.ReceiptFlag){
											shipno = "";
											var startReceiptObject=startReceiptApi(entNo,recevingNode,locid,ordNo,shipno,doctype);
											invokeApi.async(startReceiptObject).then(function(startReceiptResponse) {
												console.log("Start receipt for order based");
												console.log(startReceiptResponse);
												
												// set the receipt context for the scans
												receipt_shipment_contextSetting(entNo, recevingNode, ordNo,locid,shipno,doctype);
												
												
											});
										}
									}
									
									if($rootScope.ShipmentBasedFlag){
										shipno= shipmentContext.ShipmentNo;
										console.log("Shipment No" +shipno );
										ordNo="";
										//doctype="0005";
										if(!$scope.ReceiptFlag){
											var startReceiptObject=startReceiptApi(entNo,recevingNode,locid,ordNo,shipno,doctype);
											invokeApi.async(startReceiptObject).then(function(startReceiptResponse) {
												console.log("Start receipt for shipment");
												console.log(startReceiptResponse);

												// set the receipt context for the scans
												receipt_shipment_contextSetting(entNo, recevingNode, ordNo,locid,shipno,doctype);
												
											});
										}
									}
									
									if($rootScope.BlindReceiptBasedFlag){
										var shipno= shipmentContext.ShipmentNo;
										console.log("Shipment No" +shipno );
										 ordNo="";
										//doctype="0010";
										if(!$scope.ReceiptFlag){
											var startReceiptObject=startReceiptApi(entNo,recevingNode,locid,ordNo,shipno,doctype);
											invokeApi.async(startReceiptObject).then(function(startReceiptResponse) {
												console.log("Start receipt for blind receipt");
												console.log(startReceiptResponse);
												
												
												// set the receipt context for the scans
												receipt_shipment_contextSetting(entNo, recevingNode, ordNo,locid,shipno,doctype);
												
												
											});
										}
									}
								}
								else{
									console.log("Invalid Location");
									$scope.ErrorMessage="Invalid Location";
									
								}
						   });// end of Barcode tanslation for location
					   }
					
				
				}
			
			
	  	};
	  	
	  	
	  	$scope.palletBuild = function(){
	  		 var nodePrefObject=getNodeReceivingPreferenceListApi(entNo,recevingNode,doctype);
			  invokeApi.async(nodePrefObject).then(function(nodePrefResponse) {
				console.log("Node Pref list");
				console.log(nodePrefResponse);
				
				 $scope.nodePrefResponse = nodePrefResponse;
	    		 $rootScope.nodePrefResponseContext = $scope.nodePrefResponse; 
	    		 
	    		 var locid= $scope.frmReceivepallet_locId;
				 var lpnNo= $scope.frmReceivepallet_palletId;
				 
				 
				 console.log("nodePrefResponse.NodeReceivingPreferences.NodeReceivingPreference.PalletBuildPreference");
				 console.log(nodePrefResponse.NodeReceivingPreferences.NodeReceivingPreference.PalletBuildPreference + locid );
				// build pallet config check
				if(nodePrefResponse.NodeReceivingPreferences.NodeReceivingPreference.PalletBuildPreference == "A"){
					
					
					if( locid != undefined && lpnNo!= undefined){
						// write the funda for pallet build thing
						
						// call the function to check on the pallet configs
						palletConfigCheck();
						
					}
					else{
						$scope.ErrorMessage="Enter location id and LPN number";
					}
				}
				else{
					if( locid != undefined){
						if(lpnNo!= undefined){
							if (valid_pallet){
								$rootScope.PALLET=pallet_value;
								console.log("the pallet scanned is " +$rootScope.PALLET );
								// call the function to check on the pallet configs
								palletConfigCheck();

								
							}
						}
						else{
							console.log("Going to the item scan page");
							$location.path('/Receive/itemScan');
						}
					}
					else{
						$scope.ErrorMessage="Enter location id";
					}
				}
			  });
	  	};
	  	
	  	
	  	//Receipt Complete button 
	  	$scope.closeReceipt = function(){
	  		console.log("Going to main screen:");
	  		$rootScope.OrderBasedFlag = false;
			$rootScope.shipmentContext= {};
			$rootScope.containerContext= {};
			$rootScope.receiptContext={};
			$rootScope.orderContext={};
			$rootScope.ItemID="";
			$rootScope.ItemIDDesc="";
			$rootScope.ITEMS=[];
			$rootScope.LPNS=[];
			$rootScope.PALLET="";
			$location.path('/Receive');
	  	};
	   
	  	//receive Item button
	   $scope.receiveData = function(){
			  
		   //calling the item screen
		  var locid= $scope.frmReceivepallet_locId;
		  
		  if($rootScope.OrderBasedFlag){ 
			  var entNo= orderContext.OrderList.Order.EnterpriseCode; 
			  var ordNo= orderContext.OrderList.Order.OrderNo;
			  
				if(reciptOpenFlag){
							 
					console.log("Item screen to call");
					$location.path('/Receive/itemScan');
								  
				}
				else{
					var startReceiptObject=startReceiptApi(entNo,recevingNode,loc_value,ordNo);
					invokeApi.async(startReceiptObject).then(function(startReceiptResponse) {
						console.log("Start receipt");
						console.log(startReceiptResponse);
						console.log("Item screen to call. New receipt has been created.");
						
						// set the receipt context for the scans
						
						var postReciptlstObject = getReceiptListApi(entNo, recevingNode, ordNo,locid,shipno);
				     	console.log(postReciptlstObject);
						invokeApi.async(postReciptlstObject).then(function(receiptResponse) {
				    		  console.log(receiptResponse);
				    		  $scope.postReceiptResponse = receiptResponse;
				    		  $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList; 
					    	  var receiptContext = $rootScope.receiptContext ;
					    	  console.log("Receipt Details");
							  console.log(receiptContext);
							  
							  var recpListarrayflag = angular.isArray(receiptContext.Receipt);
								console.log("Recept flag:" + recpListarrayflag);
								if (recpListarrayflag){
									
									for (var i=0; i < receiptContext.Receipt.length; i++){
										
										if(receiptContext.Receipt[i].OpenReceiptFlag=="Y"){
											reciptOpenFlag = true;
											locid= receiptContext.Receipt[i].ReceivingDock;
											var shpKey= receiptContext.Receipt[i].ShipmentKey;
											console.log("Open receipt is there for the order passed in loc " +locid );
											 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt[i]; 
									    	  var receiptContext = $rootScope.receiptContext ;
									    	  console.log("Receipt Details");
											  console.log(receiptContext);
											  
											  //get the shipment Context:
											  
											  var postShipmentObject = getShipmentListApi(entNo, recevingNode, shpKey, ordNo);
										     	console.log(postShipmentObject);
												invokeApi.async(postShipmentObject).then(function(shipResponse) {
													if(shipResponse.Shipments.Shipment){
												    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
												    	  console.log("Shipment Details");
														  console.log($rootScope.shipmentContext);
													
													}  
													
												});
								  
											break;							
										}
									
									}
								}
								else{
									
									if(receiptContext.Receipt.OpenReceiptFlag == "Y"){
										reciptOpenFlag = true;
										var shpKey = receiptContext.Receipt.ShipmentKey;
										console.log("Open receipt is there for teh order passed in loc "+ locid);
										 $rootScope.receiptContext = $scope.postReceiptResponse.ReceiptList.Receipt; 
								    	  var receiptContext = $rootScope.receiptContext ;
								    	  console.log("Receipt Details");
										  console.log(receiptContext);
										  
										  //get the shipment Context:
										  
										  var postShipmentObject = getShipmentListApi(entNo, recevingNode, shpKey, ordNo);
									     	console.log(postShipmentObject);
											invokeApi.async(postShipmentObject).then(function(shipResponse) {
												if(shipResponse.Shipments.Shipment){
											    	  $rootScope.shipmentContext = shipResponse.Shipments.Shipment;
											    	  console.log("Shipment Details");
													  console.log($rootScope.shipmentContext);
												}  
												
											});
									}
								}
				
							  
						});
						$location.path('/Receive/itemScan');
				
					});
				}	
			 
		  }
		  else{
			  console.log("Receving SKU for shipment Item screen to call");
			  $rootScope.caseId="";
			  $rootScope.palletId="";
			  $rootScope.verifyFlag=false;
			  $location.path('/Receive/itemScan');
			
		  }
		  
	   };
	
	

    });



});


