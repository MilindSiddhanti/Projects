[
		{
			"key" : "_UserNameHeader_",
			"value" : "K�ytt�j�nimi",
			"description" : "Login page Username field label and text"
		},
		{
			"key" : "_PasswordHeader_",
			"value" : "Salasana",
			"description" : "Login page Password field label and text"
		},
		{
			"key" : "_UserRequired_",
			"value" : "käyttäjätunnuksesi vaaditaan",
			"description" : "Login Page user required"
		},
		{
			"key" : "_PassRequired_",
			"value" : "Salasana vaaditaan",
			"description" : "Login Page pass required"
		},
		{
			"key" : "_CallHelpDesk_",
			"value" : "Virheellinen käyttäjä / salasana - Yritä uudelleen tai soita helpdesk",
			"description" : "General error message in Login screen to indicate to call helpdesk with incorrect credentials"
		},
		{
			"key" : "_Home_",
			"value" : "Home",
			"description" : "Home Tab to go back to the Home Page"
		},
		{
			"key" : "_Receive_",
			"value" : "vastaanottaa",
			"description" : "Receive menu option to go to Receive module first page"
		},
		{
			"key" : "_Putaway_",
			"value" : "ahmia",
			"description" : "Putaway menu option to go to Putaway module first page"
		},
		{
			"key" : "_InventoryInq_",
			"value" : "inventaario tiedustelu",
			"description" : "Inventory Inquiry menu option to go to Inventory module first page"
		},
		{
			"key" : "_OutboundPick_",
			"value" : "Lähtevä Pick",
			"description" : "Outbound Pick option to go to Outbound Pick module first page"
		},
		{
			"key" : "_Pack_",
			"value" : "pakkaus",
			"description" : "Pack menu option to go to Pack module first page"
		},
		{
			"key" : "_AdhocMove_",
			"value" : "AdhocMove",
			"description" : "AdhocMove menu option to go to Adhoc Move module first page"
		},
		{
			"key" : "_Count_",
			"value" : "laskea",
			"description" : "Count menu option to go to Count module first page"
		},
		{
			"key" : "_Task_",
			"value" : "tehtävä",
			"description" : "Task menu option to go to Task module first page"
		},
		{
			"key" : "_PackAudit_",
			"value" : "PackAudit",
			"description" : "Pack Audit option to go to Pack Audit module first page"
		},
		{
			"key" : "_Manifest_",
			"value" : "ilmeinen",
			"description" : "Manifest option to go to Manifest module first page"
		},
		{
			"key" : "_Logout_",
			"value" : "Kirjaudu ulos",
			"description" : "Logout option to Logout and navigate to first page"
		},
		{
			"key" : "_Settings_",
			"value" : "asetukset",
			"description" : "Settings option Tab to Settings module first Page"
		},
		{
			"key" : "_ChangeNode_",
			"value" : "Vaihda solmu",
			"description" : "Change Node under Settings menu"
		},
		{
			"key" : "_ChangePassword_",
			"value" : "Vaihda salasana",
			"description" : "Change Password under Settings menu"
		},
		{
			"key" : "_TraceOptions_",
			"value" : "jäljittää vaihtoehtoja",
			"description" : "Trace Options under Settings menu"
		},
		{
			"key" : "_ExistingPassword_",
			"value" : "Olemassa olevat Salasana",
			"description" : "Existing Password placeholder under ChangePassword screen"
		},
		{
			"key" : "_NewPassword_",
			"value" : "uusi salasana",
			"description" : "New Password placeholder under ChangePassword screen"
		},
		{
			"key" : "_ConfirmPassword_",
			"value" : "Vahvista Salasana",
			"description" : "Confirm Password placeholder under ChangePassword screen"
		},
		{
			"key" : "_ReceivingDock_",
			"value" : "vastaanottaminen Dock",
			"description" : "Receiving Dock label under Receiving screen"
		},
		{
			"key" : "_QuantityToReceive_",
			"value" : "Määrä Vastaanota",
			"description" : "Quantity to Receive under Receiving screen"
		},
		{
			"key" : "_ReceivedItem_",
			"value" : "sai Tuote",
			"description" : "Successful received message of items under Receiving screen"
		},
		{
			"key" : "_ReceivedLPN_",
			"value" : "sai Tuote",
			"description" : "Successful received message of LPNs under Receiving screen"
		},
		{
			"key" : "_DepositCompletedsuccessfully_",
			"value" : "Määrä Vastaanota",
			"description" : "Successful Deposit message under Putaway screen"
		},
		{
			"key" : "_PutawayCompletedsuccessfully_",
			"value" : "Laita pois on suoritettu onnistuneesti",
			"description" : "Successful Putaway message under Putaway screen"
		},
		{
			"key" : "_LPNsPicked_",
			"value" : "rekisteritunnus poimittuja",
			"description" : "LPNs Picked label under Putaway screen"
		},
		{
			"key" : "_DestinationLPN_",
			"value" : "määränpää",
			"description" : "Destination LPN in Pick screen"
		},
		{
			"key" : "_ShipmentPicked_",
			"value" : "lähetys poimittuja",
			"description" : "Shipment Picked in Pick screen"
		},
		{
			"key" : "_SuggestedQuantity_",
			"value" : "ehdotti määrä",
			"description" : "Suggested Quantity in Pick screen"
		},
		{
			"key" : "_ActualQuantity_",
			"value" : "todellinen määrä",
			"description" : "Actual Quantity in Pick screen"
		},
		{
			"key" : "_PickingCompletedsuccessfully_",
			"value" : "Keräilyn onnistui",
			"description" : "Successful Picking message under Picking screen"
		},
		{
			"key" : "_Node_",
			"value" : "solmu",
			"description" : "Node label placeholders input text fields across modules"
		},
		{
			"key" : "_Shipment_",
			"value" : "lähetys",
			"description" : "Shipment label placeholders input text fields across modules"
		},
		{
			"key" : "_Receipt_",
			"value" : "kuitti",
			"description" : "Receipt label placeholders input text fields across modules"
		},
		{
			"key" : "_Wave_",
			"value" : "aalto",
			"description" : "Wave label placeholders input text fields across modules"
		},
		{
			"key" : "_Order_",
			"value" : "järjestys",
			"description" : "Order label placeholders input text fields across modules"
		},
		{
			"key" : "_Vendor_",
			"value" : "myyjä",
			"description" : "Vendor label placeholders input text fields across modules"
		},
		{
			"key" : "_EnterpriseCode_",
			"value" : "Laitos koodi",
			"description" : "Enterprise Code label placeholders input text fields across modules"
		},
		{
			"key" : "_Summary_",
			"value" : "yhteenveto",
			"description" : "Summary label placeholders input text fields across modules"
		},
		{
			"key" : "_ItemID_",
			"value" : "tunnus",
			"description" : "Item ID placeholders input text fields across modules"
		},
		{
			"key" : "_Items_",
			"value" : "erä",
			"description" : "Items placeholders input text fields across modules"
		},
		{
			"key" : "_ItemDesc_",
			"value" : "Kohta Kuvaus",
			"description" : "Item Description placeholders input text fields across modules"
		},
		{
			"key" : "_Quantity_",
			"value" : "Määrä",
			"description" : "Quantity placeholders input text fields across modules"
		},
		{
			"key" : "_LocationID_",
			"value" : "Sijainti tunnus",
			"description" : "Location ID placeholders input text fields across modules"
		},
		{
			"key" : "_LPN_",
			"value" : "Rekisterinumero",
			"description" : "LPN placeholders input text fields across modules"
		},
		{
			"key" : "_LastScannedItems_",
			"value" : "Viimeisin Skannatut kohteet",
			"description" : "Last Scanned Items placeholders input text fields across modules"
		},
		{
			"key" : "_LastScannedLPNs_",
			"value" : "Viimeisin Skannatut kohteet",
			"description" : "Last Scanned LPNs placeholders input text fields across modules"
		},
		{
			"key" : "_CaseIDPalletID_",
			"value" : "Case ID Pallet ID",
			"description" : "CaseID PalletID placeholders input text fields across modules"
		},
		{
			"key" : "_Lot_",
			"value" : "Eränumero",
			"description" : "Lot# placeholders input text fields across modules"
		},
		{
			"key" : "_Batch_",
			"value" : "Eränumero",
			"description" : "Batch# placeholders input text fields across modules"
		},
		{
			"key" : "_Revision_",
			"value" : "Eränumero",
			"description" : "Revision# placeholders input text fields across modules"
		},
		{
			"key" : "_ExpirationDate_",
			"value" : "Vanhenemispäivämäärä",
			"description" : "Expiration Date placeholders input text fields across modules"
		},
		{
			"key" : "_Serial_",
			"value" : "Eränumero",
			"description" : "Serial# placeholders input text fields across modules"
		},
		{
			"key" : "_Equipment_",
			"value" : "laitteet",
			"description" : "Equipment placeholders input text fields across modules"
		},
		{
			"key" : "_StartLocation_",
			"value" : "aloituspaikka",
			"description" : "Start Location label placeholders input text fields across modules"
		},
		{
			"key" : "_SourceLocation_",
			"value" : "Lähde Sijainti",
			"description" : "Source Location label placeholders input text fields across modules"
		},
		{
			"key" : "_SourceLPN_",
			"value" : "Lähde Rekisterinumero",
			"description" : "Source LPN label placeholders input text fields across modules"
		},
		{
			"key" : "_UOM_",
			"value" : "UOM",
			"description" : "UOM label placeholders input text fields across modules"
		},
		{
			"key" : "_ProductClass_",
			"value" : "tuote luokka",
			"description" : "Product Class label placeholders input text fields across modules"
		},
		{
			"key" : "_InventoryStatus_",
			"value" : "varaston tila",
			"description" : "Inventory Status label placeholders input text fields across modules"
		},
		{
			"key" : "_COO_",
			"value" : "varaston tila",
			"description" : "Country of Origin label placeholders input text fields across modules"
		},
		{
			"key" : "_Segment_",
			"value" : "segmentti",
			"description" : "Segment # label placeholders input text fields across modules"
		},
		{
			"key" : "_SegmentType_",
			"value" : "segmentin tyyppi",
			"description" : "Segment Type label placeholders input text fields across modules"
		},
		{
			"key" : "_SuggestedDepositLocation_",
			"value" : "ehdotti talletus sijainti",
			"description" : "Suggested Deposit Location label placeholders input text fields across modules"
		},
		{
			"key" : "_SuggestedLPN_",
			"value" : "ehdotti rekisteritunnus",
			"description" : "Suggested LPN label placeholders input text fields across modules"
		},
		{
			"key" : "_RemainingDeposits_",
			"value" : "ehdotti sijainti",
			"description" : "Remaining deposits label placeholders input text fields across modules"
		},
		{
			"key" : "_Cases_",
			"value" : "tapaukset",
			"description" : "Cases label placeholders input text fields across modules"
		},
		{
			"key" : "_Pallets_",
			"value" : "lavat",
			"description" : "Pallets label placeholders input text fields across modules"
		},
		{
			"key" : "_Each_",
			"value" : "kukin",
			"description" : "Each label placeholders input text fields across modules"
		},
		{
			"key" : "_ReasonCode_",
			"value" : "syykoodi",
			"description" : "Reason Code label placeholders input text fields in Inventory modules"
		},
		{
			"key" : "_ReasonText_",
			"value" : "perustelutekstin",
			"description" : "Reason Text label placeholders input text fields in Inventory modules"
		},
		{
			"key" : "_EditAttribute_",
			"value" : "muokkaa ominaisuus",
			"description" : "Edit Attribute label placeholders input text fields in Inventory modules"
		},
		{
			"key" : "_LoginButtonLabel_",
			"value" : "Kirjaudu",
			"description" : "Login Page login button text"
		},
		{
			"key" : "_SaveButtonLabel_",
			"value" : "Tallenna",
			"description" : "Save button text across screens"
		},
		{
			"key" : "_BackButtonLabel_",
			"value" : "takaisin",
			"description" : "Back button text across screens"
		},
		{
			"key" : "_ItemScanButtonLabel_",
			"value" : "tuote skannaus",
			"description" : "Item Scan button text in Receiving screen"
		},
		{
			"key" : "_MenuButtonLabel_",
			"value" : "valikko",
			"description" : "Menu button text in Receiving screen"
		},
		{
			"key" : "_DoneButtonLabel_",
			"value" : "sai Tuote",
			"description" : "Done button text in Receiving screen"
		},
		{
			"key" : "_FindButtonLabel_",
			"value" : "löytää",
			"description" : "Find button text across screens"
		},
		{
			"key" : "_PutawayButtonLabel_",
			"value" : "Laittaa pois",
			"description" : "Putaway button text in Putaway screen"
		},
		{
			"key" : "_PickButtonLabel_",
			"value" : "poimia",
			"description" : "Pick button text in Putaway screen"
		},
		{
			"key" : "_DepositButtonLabel_",
			"value" : "talletus",
			"description" : "Deposit button text in Putaway screen"
		},
		{
			"key" : "_MultiDepositButtonLabel_",
			"value" : "multi talletus",
			"description" : "Multi Deposit button text in Putaway screen"
		},
		{
			"key" : "_NextButtonLabel_",
			"value" : "seuraava",
			"description" : "Next button text in Pick screen"
		},
		{
			"key" : "_ConfirmButtonLabel_",
			"value" : "vahvistaa",
			"description" : "Confirm button text in Pick screen"
		},
		{
			"key" : "_OverrideButtonLabel_",
			"value" : "ohittaa",
			"description" : "Override button text in Pick screen"
		},
		{
			"key" : "_ExceptionButtonLabel_",
			"value" : "poikkeus",
			"description" : "Exception button text across Pick screens"
		},
		{
			"key" : "_EditButtonLabel_",
			"value" : "muokata",
			"description" : "Edit button text in Inventory Edit screens"
		},
		{
			"key" : "_PickedQuantity_",
			"value" : "poimittuja määrä",
			"description" : "Picked Quantity text across Pick screens"
		},
		{
			"key" : "_SerialPicked_",
			"value" : "Eränumero poimittuja",
			"description" : "Serial Picked text across Pick screens"
		},
		{
			"key" : "_DepositLocation_",
			"value" : "talletus sijainti",
			"description" : "Deposit Location label placeholders input text fields across modules"
		},
		{
			"key" : "_ReferenceInput_",
			"value" : "PO # , SSCC , ASN , Myyjä tai Itemid",
			"description" : "Home Tab to go back to the Home Page"
		},

		{
			"key" : "_CreatePOButtonLabel_",
			"value" : "Luo PO",
			"description" : "Create Purchase Order for Blind Receipt"
		},

		{
			"key" : "_PurchaseOrder_",
			"value" : "Ostotilaus",
			"description" : "Main menu selection for PO based orders"
		},

		{
			"key" : "_BlindReceipt_",
			"value" : "Blind Kuitti",
			"description" : "Main menu selection for blind receipt"
		},

		{
			"key" : "_OrderScan_",
			"value" : "Lähetyksen # / Tilaus #",
			"description" : "Scan Order for Shipment based or PO based"
		},

		{
			"key" : "_AddToReceiptButtonLabel_",
			"value" : "Lisää kuitti",
			"description" : "Add PO to receipt"
		},

		{
			"key" : "_DispositionCode_",
			"value" : "disposition koodi",
			"description" : "Associated with Item to indicate the status of the inventory item"
		},

		{
			"key" : "_ScannedQuantity_",
			"value" : "skannattu Määrä",
			"description" : "No. of scanned quantity for the item with unique serials"
		},

		{
			"key" : "_SecondarySerial_",
			"value" : "toissijainen Serial",
			"description" : "No. of scanned quantity for the item with unique serials"
		},

		{
			"key" : "_TagIdentifiers_",
			"value" : "Tag Tunnisteet",
			"description" : "Tag Identifiers for the item scanned"
		},

		{
			"key" : "_TagAttributes_",
			"value" : "koodimääritteiden",
			"description" : "Tag Attributes for the item scanned"
		},

		{
			"key" : "_LotAttribute_",
			"value" : "Erä Taito",
			"description" : "Lot Attribute for the item scanned"
		},

		{
			"key" : "_LotReference_",
			"value" : "Erän viitenumero",
			"description" : "Lot reference for the item scanned"
		},

		{
			"key" : "_ManufactureDate_",
			"value" : "valmistus Päivämäärä",
			"description" : "Manufacture Date for the item scanned"
		},
		{
			"key" : "_ProceedButtonLabel_",
			"value" : "edetä",
			"description" : "On click of Proceed Button"
		},
		{
			"key":"_SuggestedStagingLocation_",
			"value":"Ehdotetut Staging Sijainti",
			"description":"Suggested Staging Location label placeholders input text fields across modules"
		},
		{
			"key":"_SuggestedOrder_",
			"value":"Ehdotetut Tilaa",
			"description":"Suggested Order label placeholders input text fields across modules"
		},
		{
			"key":"_LPN/ShipmentNumber_",
			"value":"LPN / lähetys lukumäärä",
			"description":"LPN/ShipmentNumber Label placeholders text in Putaway screen"
		},
		{
			"key":"_Reference_",
			"value":"Kuitti # , PO # , ASN , tai Myyjä",
			"description":"Reference field placeholder in close receipt screen"
		},
		{
			"key":"_SellerOrganizationCode_",
			"value":"myyjä",
			"description":"Seller label in close receipt screen"
		},
		{
			"key":"_ExpectedDeliveryDate_",
			"value":"Odotettu toimituspäivä",
			"description":"Expected Delivery Date label in close receipt screen"
		},
		{
			"key":"_FindReceiptButtonLabel_",
			"value":"Etsi Recepit",
			"description":"Find Recepit Button in close receipt screen"
		},
		{
			"key":"_CancelButtonLabel_",
			"value":"peruuttaa",
			"description":"Cancel Button in close receipt screen"
		},
		{
			"key":"_CloseReceiptButtonLabel_",
			"value":"Sulje Kuitti",
			"description":"Close Receipt Button in close receipt screen"
		},
		{
			"key":"_ReceiptList_",
			"value":"kuitti lista",
			"description":"Receipt List Label in close receipt screen"	
		}

]
