[
		{
			"key" : "_UserNameHeader_",
			"value" : "UserName",
			"description" : "Login page Username field label and text"
		},
		{
			"key" : "_PasswordHeader_",
			"value" : "Password",
			"description" : "Login page Password field label and text"
		},
		{
			"key" : "_LoginButtonLabel_",
			"value" : "Login",
			"description" : "Login Page login button text"
		},
		{
			"key" : "_UserRequired_",
			"value" : "UserName Required",
			"description" : "Login Page user required"
		},
		{
			"key" : "_PassRequired_",
			"value" : "Password Required",
			"description" : "Login Page pass required"
		},
		{
			"key" : "_CallHelpDesk_",
			"value" : "Invalid user/password - Try again or call helpdesk",
			"description" : "General error message in Login screen to indicate to call helpdesk with incorrect credentials"
		},
		{
			"key" : "_Home_",
			"value" : "Home",
			"description" : "Home Tab to go back to the Home Page"
		},
		,
		{
			"key" : "_Receive_",
			"value" : "Receive",
			"description" : "Receive menu option to go to Receive module first page"
		},
		{
			"key" : "_Putaway_",
			"value" : "Putaway",
			"description" : "Putaway menu option to go to Putaway module first page"
		},
		{
			"key" : "_InventoryInq_",
			"value" : "Inventory Inquiry",
			"description" : "Inventory Inquiry menu option to go to Inventory module first page"
		},
		{
			"key" : "_OutboundPick_",
			"value" : "Outbound Pick",
			"description" : "Outbound Pick option to go to Outbound Pick module first page"
		},
		{
			"key" : "_Pack_",
			"value" : "Pack",
			"description" : "Pack menu option to go to Pack module first page"
		},
		{
			"key" : "_AdhocMove_",
			"value" : "Adhoc Move",
			"description" : "AdhocMove menu option to go to Adhoc Move module first page"
		},
		{
			"key" : "_Count_",
			"value" : "Count",
			"description" : "Count menu option to go to Count module first page"
		},
		{
			"key" : "_Task_",
			"value" : "Task",
			"description" : "Task menu option to go to Task module first page"
		},
		{
			"key" : "_PackAudit_",
			"value" : "Pack Audit",
			"description" : "Pack Audit option to go to Pack Audit module first page"
		},
		{
			"key" : "_Manifest_",
			"value" : "Manifest",
			"description" : "Manifest option to go to Manifest module first page"
		},
		{
			"key" : "_Logout_",
			"value" : "Logout",
			"description" : "Logout option to Logout and navigate to first page"
		},
		{
			"key" : "_Settings_",
			"value" : "Setting",
			"description" : "Settings option Tab to Settings module first Page"
		},
		{
			"key" : "_ChangeNode_",
			"value" : "Change Node",
			"description" : "Change Node under Settings menu"
		},
		{
			"key" : "_ChangePassword_",
			"value" : "Change Password",
			"description" : "Change Password under Settings menu"
		},
		{
			"key" : "_TraceOptions_",
			"value" : "Trace Options",
			"description" : "Trace Options under Settings menu"
		},
		{
			"key" : "_ExistingPassword_",
			"value" : "Existing Password",
			"description" : "Existing Password placeholder under ChangePassword screen"
		},
		{
			"key" : "_NewPassword_",
			"value" : "New Password",
			"description" : "New Password placeholder under ChangePassword screen"
		},
		{
			"key" : "_ConfirmPassword_",
			"value" : "Confirm Password",
			"description" : "Confirm Password placeholder under ChangePassword screen"
		},
		{
			"key" : "_ReceivingDock_",
			"value" : "Receiving Dock",
			"description" : "Receiving Dock label under Receiving screen"
		},
		{
			"key" : "_QuantityToReceive_",
			"value" : "Quantity to be Received",
			"description" : "Quantity to Receive under Receiving screen"
		},
		{
			"key" : "_ReceivedItem_",
			"value" : "Received Item",
			"description" : "Successful received message of items under Receiving screen"
		},
		{
			"key" : "_ReceivedLPN_",
			"value" : "Received LPN",
			"description" : "Successful received message of LPNs under Receiving screen"
		},
		{
			"key" : "_DepositCompletedsuccessfully_",
			"value" : "Deposit Completed Successfully",
			"description" : "Successful Deposit message under Putaway screen"
		},
		{
			"key" : "_PutawayCompletedsuccessfully_",
			"value" : "Putaway Completed Successfully",
			"description" : "Successful Putaway message under Putaway screen"
		},
		{
			"key" : "_LPNsPicked_",
			"value" : "LPNs Picked",
			"description" : "LPNs Picked label under Putaway screen"
		},
		{
			"key" : "_DestinationLPN_",
			"value" : "Destination LPN",
			"description" : "Destination LPN in Pick screen"
		},
		{
			"key" : "_ShipmentPicked_",
			"value" : "Shipments Picked",
			"description" : "Shipment Picked in Pick screen"
		},
		{
			"key" : "_SuggestedQuantity_",
			"value" : "Suggested Quantity",
			"description" : "Suggested Quantity in Pick screen"
		},
		{
			"key" : "_ActualQuantity_",
			"value" : "Actual Quantity",
			"description" : "Actual Quantity in Pick screen"
		},
		{
			"key" : "_PickingCompletedsuccessfully_",
			"value" : "Picking Completed Successfully",
			"description" : "Successful Picking message under Picking screen"
		},
		{
			"key" : "_Node_",
			"value" : "Node",
			"description" : "Node label placeholders input text fields across modules"
		},
		{
			"key" : "_Shipment_",
			"value" : "Shipment",
			"description" : "Shipment label placeholders input text fields across modules"
		},
		{
			"key" : "_Receipt__",
			"value" : "Receipt",
			"description" : "Receipt label placeholders input text fields across modules"
		},
		{
			"key" : "_Wave__",
			"value" : "Wave",
			"description" : "Wave label placeholders input text fields across modules"
		},
		{
			"key" : "_Order_",
			"value" : "Order",
			"description" : "Order label placeholders input text fields across modules"
		},
		{
			"key" : "_Vendor_",
			"value" : "Vendor",
			"description" : "Vendor label placeholders input text fields across modules"
		},
		{
			"key" : "_EnterpriseCode_",
			"value" : "Enterprise Code",
			"description" : "Enterprise Code label placeholders input text fields across modules"
		},
		{
			"key" : "_Summary_",
			"value" : "Summary",
			"description" : "Summary label placeholders input text fields across modules"
		},
		{
			"key" : "_ItemID_",
			"value" : "Item",
			"description" : "Item ID placeholders input text fields across modules"
		},
		{
			"key" : "_Items_",
			"value" : "Items",
			"description" : "Items placeholders input text fields across modules"
		},
		{
			"key" : "_ItemDesc_",
			"value" : "Item Description",
			"description" : "Item Description placeholders input text fields across modules"
		},
		{
			"key" : "_Quantity_",
			"value" : "Quantity",
			"description" : "Quantity placeholders input text fields across modules"
		},
		{
			"key" : "_LocationID_",
			"value" : "Location",
			"description" : "Location ID placeholders input text fields across modules"
		},
		{
			"key" : "_LPN_",
			"value" : "LPN",
			"description" : "LPN placeholders input text fields across modules"
		},
		{
			"key" : "_LastScannedItems_",
			"value" : "Last Scanned Items",
			"description" : "Last Scanned Items placeholders input text fields across modules"
		},
		{
			"key" : "_LastScannedLPNs_",
			"value" : "Last Scanned LPNs",
			"description" : "Last Scanned LPNs placeholders input text fields across modules"
		},
		{
			"key" : "_CaseIDPalletID_",
			"value" : "Case ID Pallet ID",
			"description" : "CaseID PalletID placeholders input text fields across modules"
		},
		{
			"key" : "_Lot_",
			"value" : "Lot",
			"description" : "Lot# placeholders input text fields across modules"
		},
		{
			"key" : "_Batch_",
			"value" : "Batch",
			"description" : "Batch# placeholders input text fields across modules"
		},
		{
			"key" : "_Revision_",
			"value" : "Revision",
			"description" : "Revision# placeholders input text fields across modules"
		},
		{
			"key" : "_ExpirationDate_",
			"value" : "Expiration Date",
			"description" : "Expiration Date placeholders input text fields across modules"
		},
		{
			"key" : "_Serial_",
			"value" : "Serial",
			"description" : "Serial# placeholders input text fields across modules"
		},
		{
			"key" : "_Equipment_",
			"value" : "Equipment",
			"description" : "Equipment placeholders input text fields across modules"
		},
		{
			"key" : "_StartLocation_",
			"value" : "Start Location",
			"description" : "Start Location label placeholders input text fields across modules"
		},
		{
			"key" : "_SourceLocation_",
			"value" : "Source Location",
			"description" : "Source Location label placeholders input text fields across modules"
		},
		{
			"key" : "_SourceLPN_",
			"value" : "Source LPN",
			"description" : "Source LPN label placeholders input text fields across modules"
		},
		{
			"key" : "_UOM_",
			"value" : "UOM",
			"description" : "UOM label placeholders input text fields across modules"
		},
		{
			"key" : "_ProductClass_",
			"value" : "Product Class",
			"description" : "Product Class label placeholders input text fields across modules"
		},
		{
			"key" : "_InventoryStatus_",
			"value" : "Inventory Status",
			"description" : "Inventory Status label placeholders input text fields across modules"
		},
		{
			"key" : "_COO_",
			"value" : "COO",
			"description" : "Country of Origin label placeholders input text fields across modules"
		},
		{
			"key" : "_Segment_",
			"value" : "Segment",
			"description" : "Segment # label placeholders input text fields across modules"
		},
		{
			"key" : "_SegmentType_",
			"value" : "Segment Type",
			"description" : "Segment Type label placeholders input text fields across modules"
		},
		{
			"key" : "_SuggestedDepositLocation_",
			"value" : "Suggested Deposit Location",
			"description" : "Suggested Deposit Location label placeholders input text fields across modules"
		},
		{
			"key" : "_SuggestedLPN_",
			"value" : "Suggested LPN",
			"description" : "Suggested LPN label placeholders input text fields across modules"
		},
		{
			"key" : "_RemainingDeposits_",
			"value" : "Remaining Deposits",
			"description" : "Remaining deposits label placeholders input text fields across modules"
		},
		{
			"key" : "_Cases_",
			"value" : "Cases",
			"description" : "Cases label placeholders input text fields across modules"
		},
		{
			"key" : "_Pallets_",
			"value" : "Pallets",
			"description" : "Pallets label placeholders input text fields across modules"
		},
		{
			"key" : "_Each_",
			"value" : "Each",
			"description" : "Each label placeholders input text fields across modules"
		},
		{
			"key" : "_ReasonCode_",
			"value" : "Reason Code",
			"description" : "Reason Code label placeholders input text fields in Inventory modules"
		},
		{
			"key" : "_ReasonText_",
			"value" : "Reason Text",
			"description" : "Reason Text label placeholders input text fields in Inventory modules"
		},
		{
			"key" : "_EditAttribute_",
			"value" : "Edit Attribute",
			"description" : "Edit Attribute label placeholders input text fields in Inventory modules"
		},
		{
			"key" : "_LoginButtonLabel_",
			"value" : "Login",
			"description" : "Login Page login button text"
		},
		{
			"key" : "_SaveButtonLabel_",
			"value" : "Save",
			"description" : "Save button text across screens"
		},
		{
			"key" : "_BackButtonLabel_",
			"value" : "Back",
			"description" : "Back button text across screens"
		},
		{
			"key" : "_ItemScanButtonLabel_",
			"value" : "Item Scan",
			"description" : "Item Scan button text in Receiving screen"
		},
		{
			"key" : "_MenuButtonLabel_",
			"value" : "Menu",
			"description" : "Menu button text in Receiving screen"
		},
		{
			"key" : "_DoneButtonLabel_",
			"value" : "Done",
			"description" : "Done button text in Receiving screen"
		},
		{
			"key" : "_FindButtonLabel_",
			"value" : "Find",
			"description" : "Find button text across screens"
		},
		{
			"key" : "_PutawayButtonLabel_",
			"value" : "Putaway",
			"description" : "Putaway button text in Putaway screen"
		},
		{
			"key" : "_PickButtonLabel_",
			"value" : "Pick",
			"description" : "Pick button text in Putaway screen"
		},
		{
			"key" : "_DepositButtonLabel_",
			"value" : "Deposit",
			"description" : "Deposit button text in Putaway screen"
		},
		{
			"key" : "_MultiDepositButtonLabel_",
			"value" : "Multi Deposit",
			"description" : "Multi Deposit button text in Putaway screen"
		},
		{
			"key" : "_NextButtonLabel_",
			"value" : "Next",
			"description" : "Next button text in Pick screen"
		},
		{
			"key" : "_ConfirmButtonLabel_",
			"value" : "Confirm",
			"description" : "Confirm button text in Pick screen"
		},
		{
			"key" : "_OverrideButtonLabel_",
			"value" : "Override",
			"description" : "Override button text in Pick screen"
		},
		{
			"key" : "_ExceptionButtonLabel_",
			"value" : "Exception",
			"description" : "Exception button text across Pick screens"
		},
		{
			"key" : "_EditButtonLabel_",
			"value" : "Edit",
			"description" : "Edit button text in Inventory Edit screens"
		},
		{
			"key" : "_PickedQuantity_",
			"value" : "Picked Quantity",
			"description" : "Picked Quantity text across Pick screens"
		},
		{
			"key" : "_SerialPicked_",
			"value" : "Serial Picked",
			"description" : "Serial Picked text across Pick screens"
		},
		{
			"key" : "_DepositLocation_",
			"value" : "Deposit Location",
			"description" : "Deposit Location label placeholders input text fields across modules"
		},
		{
			"key" : "_ReferenceInput_",
			"value" : "PO # , SSCC , ASN , Seller , or Itemid",
			"description" : "Home Tab to go back to the Home Page"
		},

		{
			"key" : "_CreatePOButtonLabel_",
			"value" : "Create PO",
			"description" : "Create Purchase Order for Blind Receipt"
		},

		{
			"key" : "_PurchaseOrder_",
			"value" : "Purchase Order",
			"description" : "Main menu selection for PO based orders"
		},

		{
			"key" : "_BlindReceipt_",
			"value" : "Blind Receipt",
			"description" : "Main menu selection for blind receipt"
		},

		{
			"key" : "_OrderScan_",
			"value" : "Shipment # / Order #",
			"description" : "Scan Order for Shipment based or PO based"
		},

		{
			"key" : "_AddToReceiptButtonLabel_",
			"value" : "Add To Receipt",
			"description" : "Add PO to receipt"
		},

		{
			"key" : "_DispositionCode_",
			"value" : "Disposition Code",
			"description" : "Associated with Item to indicate the status of the inventory item"
		},

		{
			"key" : "_ScannedQuantity_",
			"value" : "Scanned Quantity",
			"description" : "No. of scanned quantity for the item with unique serials"
		},

		{
			"key" : "_SecondarySerial_",
			"value" : "Secondary Serial",
			"description" : "No. of scanned quantity for the item with unique serials"
		},

		{
			"key" : "_TagIdentifiers_",
			"value" : "Tag Identifiers",
			"description" : "Tag Identifiers for the item scanned"
		},

		{
			"key" : "_TagAttributes_",
			"value" : "Tag Attributes",
			"description" : "Tag Attributes for the item scanned"
		},

		{
			"key" : "_LotAttribute_",
			"value" : "Lot Attribute",
			"description" : "Lot Attribute for the item scanned"
		},

		{
			"key" : "_LotReference_",
			"value" : "Lot Reference",
			"description" : "Lot reference for the item scanned"
		},

		{
			"key" : "_ManufactureDate_",
			"value" : "Manufacture Date",
			"description" : "Manufacture Date for the item scanned"
		},

		{
			"key" : "_ProceedButtonLabel_",
			"value" : "Proceed",
			"description" : "On click of Proceed Button"
		},
		{
			"key":"_SuggestedStagingLocation_",
			"value":"Suggested Staging Location",
			"description":"Suggested Staging Location label placeholders input text fields across modules"
		},
		{
			"key":"_SuggestedOrder_",
			"value":"Suggested Order",
			"description":"Suggested Order label placeholders input text fields across modules"
		},
		{
			"key":"_LPN/ShipmentNumber_",
			"value":"LPN / Shipment Number",
			"description":"LPN/ShipmentNumber Label placeholders text in Putaway screen"
		},
		{
			"key":"_Reference_",
			"value":"Receipt#, PO #, ASN , or Seller",
			"description":"Reference field placeholder in close receipt screen"
		},
		{
			"key":"_SellerOrganizationCode_",
			"value":"Seller",
			"description":"Seller label in close receipt screen"
		},
		{
			"key":"_ExpectedDeliveryDate_",
			"value":"Expected Delivery Date",
			"description":"Expected Delivery Date label in close receipt screen"
		},
		{
			"key":"_FindReceiptButtonLabel_",
			"value":"Find Recepit",
			"description":"Find Recepit Button in close receipt screen"
		},
		{
			"key":"_CancelButtonLabel_",
			"value":"Cancel",
			"description":"Cancel Button in close receipt screen"
		},
		{
			"key":"_CloseReceiptButtonLabel_",
			"value":"Close Receipt",
			"description":"Close Receipt Button in close receipt screen"
		},
		{
			"key":"_ReceiptList_",
			"value":"Receipt List",
			"description":"Receipt List Label in close receipt screen"		
		}
]
