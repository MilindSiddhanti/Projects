define([ 'common/ctrljs/app' ], function(myApp) {
	myApp.register.controller('settingsNavigatorController', function($scope,
			$rootScope, Navigator) {

		$rootScope.module = 'Settings';
		console.log("settingsNavigatorController");
		$scope.navigatorMenu = Navigator.getNavigation();
		console.log("$scope.navigatorMenu " +$scope.navigatorMenu);
		$scope.isActive = function(temp, temp1) {
			if (temp.state == temp1)
				return true;
			else
				return false;
		};

	});
});