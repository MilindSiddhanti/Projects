define([ 'common/ctrljs/app' ], function(myApp) {
	myApp.register.controller('settingsAppController', function($scope, $location, AuthenticationService) {

		$scope.go = function(path) {

			console.log(path);
			$location.path(path);

		};

		$scope.logout = function() {
			console.log("logOut Call");
			AuthenticationService.ClearCredentials();
			$location.url('/');
		};

	
	});
});