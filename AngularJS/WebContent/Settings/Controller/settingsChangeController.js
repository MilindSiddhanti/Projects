define([ 'common/ctrljs/app' ], function(myApp) {
	myApp.register.controller('settingsChangeController', function($scope,
			$rootScope, $location, $log, $cookieStore, $state,
			processContoller, invokeApi, AuthenticationService, loggerHirerachy, toaster) {
		$rootScope.settings_moduleFlow = 'Settings';
		$rootScope.settings_NavActive = "";
		//var formName = 'frmSettings';
		
		
		var recevingNode = $cookieStore.get('globals').OrganizationCode;
		var SuperAcessMode = $rootScope.SuperAcessMode;
		$scope.SuperAcessMode = SuperAcessMode;
		
		var app_states = $cookieStore.get('NonRestricted');
		app_states.push("All");
		$scope.app_states = app_states;
		
		$scope.loggedInUsers = [];
		$scope.LoggedUserList = [];
		$scope.loggerHirerachy = loggerHirerachy;
		
		var Logger = $log.getInstance($state.$current);
		console.log(Logger);
		Logger.info("Log from Settings Controller");
		
		$scope.csrf = $location.search().CSRFToken;

		/*****************getShipNodeListApi Api Xml formation*****************************************/
		function getShipNodeListApi() {
			console.log("getShipNodeListApi");
			var postObject = new Object();
			postObject.CommandName = "getShipNodeList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "ShipNode";
			//postObject.InputXml.InterfaceType="INTEGRATED_WMS";
			postObject.Template = new Object();
			postObject.Template.tagName = "ShipNodeList";
			postObject.Template.TotalNumberOfRecords="";
			postObject.Template.childNodes= [];
			postObject.Template.childNodes[0]=new Object();
			postObject.Template.childNodes[0].tagName="ShipNode";
			postObject.Template.childNodes[0].Description="";
			postObject.Template.childNodes[0].ShipNode="";
			postObject.IsService = "N";
			postObject.Authenticate = AuthenticationService.getAuth();
			return postObject;
		};
		
		/*****************modifyUserHierarchy Api Xml formation*****************************************/
		function modifyUserHierarchy(userId,newPassword,existingPassword) {
			console.log("modifyUserHierarchy");
			var postObject = new Object();
			postObject.CommandName = "modifyUserHierarchy";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "User";
			postObject.InputXml.Loginid=userId;
			postObject.InputXml.ExistingPassword=existingPassword;
			postObject.InputXml.Password=newPassword;
			postObject.InputXml.ExistingPasswordRequired="Y";
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.Authenticate = AuthenticationService.getAuth();
			return postObject;
		};
		
		
		/*****************modifyUserLogStatus Api Xml formation*****************************************/
		function modifyUserLogStatusApi(loggedInUsers, loggedUsers, states, app_states, OrgCode) {
			console.log("modifyUserLogStatus");
			var postObject = new Object();
			postObject.CommandName = "modifyUserLogStatus";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Users";
			postObject.InputXml.childNodes= [];
			for(var i = 0; i<loggedInUsers.length; i++){
				postObject.InputXml.childNodes[i]= new Object();
				postObject.InputXml.childNodes[i].tagName = "User";
				postObject.InputXml.childNodes[i].UserId = loggedInUsers[i];
				postObject.InputXml.childNodes[i].OrgCode = OrgCode;
				
				for(var k = 0; k<app_states.length; k++){//states
					var temp = app_states[k];
				
					if(((loggedUsers.indexOf(loggedInUsers[i])) == -1) || ((states.indexOf(temp)) == -1))
					    postObject.InputXml.childNodes[i][temp] = "N";
					else
						postObject.InputXml.childNodes[i][temp] = "Y";
				}
				
			}
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.LocalApi = "Y";
			postObject.Authenticate = AuthenticationService.getAuth();
			return postObject;
		};
       
		/*****************getUserList Api Xml formation*****************************************/
		function getUserListApi(EntCode, OrgCode) {
			console.log("getUserList");
			var postObject = new Object();
			postObject.CommandName = "getUserList";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "User";
			postObject.InputXml.EnterpriseCode = EntCode;
			postObject.InputXml.OrganizationKey = OrgCode;
			postObject.InputXml.OnlyLoggedInUsers = "Y";
			
			postObject.Template = new Object();
			postObject.Template.tagName = "UserList";
			postObject.Template.childNodes = [];
			postObject.Template.childNodes[0] = new Object();
			postObject.Template.childNodes[0].tagName = "User";
			postObject.Template.childNodes[0].Activateflag = "";
			postObject.Template.childNodes[0].Loginid = "";
			postObject.Template.childNodes[0].MenuId = "";
			postObject.Template.childNodes[0].OrganizationKey = "";
			postObject.Template.childNodes[0].UserKey = "";
			postObject.IsService = "N";
			postObject.Authenticate = AuthenticationService.getAuth();
			return postObject;
		};
		
		/*****************getUserLogStatus Api Xml formation*****************************************/
		function getUserLogStatusApi(userid, OrgCode) {
			console.log("getUserLogStatus");
			var postObject = new Object();
			postObject.CommandName = "getUserLogStatus";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Users";
			postObject.InputXml.childNodes= [];
			for(var i = 0; i<userid.length; i++){
				postObject.InputXml.childNodes[i]= new Object();
				postObject.InputXml.childNodes[i].tagName = "User";//userIds[i];
				postObject.InputXml.childNodes[i].UserId = userid[i];
				postObject.InputXml.childNodes[i].OrgCode = OrgCode;
			}
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.LocalApi = "Y";
			postObject.Authenticate = AuthenticationService.getAuth();
			return postObject;
		};
		
		function generateLoggerHirarchy(objArray, UserId, Statelogged, userlogged){
			
			var objlength = objArray.length;
			
			if(objlength>0){  
				
				userlogged.push(UserId.substring(5));
				
				for(var i = 0; i<objArray.length; i++){
				    
					if((Statelogged.indexOf(objArray[i])) == -1)
						Statelogged.push(objArray[i]);
						
				}
				
			}
			
			
		}
		
		/******************************************************/
		
		
		var postObject = getShipNodeListApi();
		invokeApi.async(postObject).then(function(response) {
			console.log("inside invoke ");
			console.log(JSON.stringify($cookieStore.get('globals')) );
			$scope.postResponse = response;
			$rootScope.shipNodeListContext = $scope.postResponse.ShipNodeList;
			$rootScope.shipNodeContext =$scope.postResponse.ShipNodeList.ShipNode;
			console.log("shipNodeContext "+JSON.stringify($rootScope.shipNodeContext));
			var shipNodeLength = $scope.postResponse.ShipNodeList.ShipNode.length;
			 if(angular.isDefined(shipNodeLength)){
				 $rootScope.shipNodeContext =$scope.postResponse.ShipNodeList.ShipNode;
			 }else{
				 $rootScope.shipNodeContext =$scope.postResponse.ShipNodeList;
			 }
			// console.log("Are you the one");
			//console.log(JSON.stringify($cookieStore.get('globals')) );
			 if(angular.isDefined($scope.frmSettingschangenode)){
				 $scope.frmSettingschangenode.set_nodeSelect=$cookieStore.get('globals').OrganizationCode; 
			 
			 }
		
			
		});
	
		 $scope.saveNodeChange = function(csrf) {
			
			 console.log("saveNodeChange ");
			 //console.log($scope.frmSettings.set_nodeSelect);
			 if(angular.isDefined($scope.frmSettingschangenode.set_nodeSelect)){
				// console.log($scope.frmSettings.set_nodeSelect);
				 var cookieUser = $cookieStore.get('globals');
				 $cookieStore.remove('globals');
				// console.log(JSON.stringify($cookieStore.get('globals')) );
				 cookieUser.OrganizationCode = $scope.frmSettingschangenode.set_nodeSelect;
				$cookieStore.put('globals', cookieUser);
				//console.log(JSON.stringify($cookieStore.get('globals')) );
				 $scope.InfoMessage1=$scope.frmSettingschangenode.set_nodeSelect+" node has been selected successfully";
				 $scope.ErrorMessage1="";
				$location.path('/MobileHome').search({"CSRFToken": csrf});
			 }else{
				 console.log("Select a node");
	    		  $scope.ErrorMessage1="Select a node";
			 }
			
		 };
		
		 $scope.savePassword = function() {
			 var existingPassword = $scope.frmSettingschangepass.set_existingPassword;
			 var newPassword = $scope.frmSettingschangepass.set_newPassword;
			 var confirmPassword = $scope.frmSettingschangepass.set_confirmPassword;
			 
			
			 var userId = $cookieStore.get('globals').currentUserId;
			 if(!angular.isDefined(existingPassword) || !angular.isDefined(newPassword) ||!angular.isDefined(confirmPassword)){
				 console.log("Please enter a valid password");
	    		  /*$scope.ErrorMessage="Please enter a valid password";*/
	    		  toaster.error("Please enter a valid password");
			 }
			 else{
				 if(newPassword!=existingPassword){
					 if(newPassword==confirmPassword){
						 var postUserObject = modifyUserHierarchy(userId,newPassword,existingPassword);
						 invokeApi.async(postUserObject).then(function(response) {
							 $scope.postModUserResponse = response;
							 console.log("modifyUserHierarchy "+JSON.stringify($scope.postModUserResponse));
							 if($scope.postModUserResponse.ErrorDescription){
								 $scope.ErrorMessage=$scope.postModUserResponse.ErrorDescription;
							 }
							 else{
								 /*$scope.InfoMessage1="Password has been updated successfully";*/
								toaster.success({title: "title", body:"Password has been updated successfully"});
								 $scope.ErrorMessage="";
							 }
						 });
						
					 }else{
						 console.log("New Password and Confirm Password do not match");
			    		  /*$scope.ErrorMessage="New Password and Confirm Password do not match";*/
			    		  toaster.error("title", "New Password and Confirm Password do not match");
					 }
					 }
					 else{
						 console.log("Existing Password and New Password are same. Please enter a new password");
			    		  /*$scope.ErrorMessage="Existing Password and New Password are same. Please enter a new password";*/
						 toaster.error("title", "Existing Password and New Password are same. Please enter a new password");
					 } 
			 }
			 
		       
		    };
		    
		    /*********************** Logger ************************/
		    
		   /* $scope.pushLog = function(){
				 
				 
				 var Selected_state = $scope.frmSettingsenabletrace.set_logSelect;
				 
				 if((loggerHirerachy.indexOf(Selected_state)) == -1){
					 loggerHirerachy.push(Selected_state);
				 }
				
				 console.log(loggerHirerachy);
				 console.log("Works Fine"+$scope.frmSettingsenabletrace.set_logSelect);
			 };
			 */
			 
			 $scope.SaveLog = function(){
					console.log($scope.loggedInUsers);
					console.log($scope.LoggedUserList);
					
					var OrgNode = $scope.frmSettingschangenode.set_nodeSelect;				
					var postUserLogStatusObject = modifyUserLogStatusApi($scope.loggedInUsers, $scope.LoggedUserList, $scope.loggerHirerachy, app_states, OrgNode);
					console.log("Before UserLog");
					invokeApi.async(postUserLogStatusObject).then(function(responseUserLogStatusObject) {
					   
						console.log(responseUserLogStatusObject);
						
					});
				
			  };
			  
			  
			  $scope.selectNode = function(){
					
					 console.log($scope.frmSettingschangenode.set_nodeSelect);
					 console.log(recevingNode);
					 console.log(SuperAcessMode);
					 console.log("Calling from Select Node");
					 if(SuperAcessMode){
						 
					   var logNode = $scope.frmSettingschangenode.set_nodeSelect;
					   var loggedInUsers = [];
					   var Statelogged =[];
					   var userlogged = [];
					   $scope.loggedInUsers = [];
		  		       $scope.LoggedUserList = [];
					   $scope.loggerHirerachy = [];
					   var postgetUserListObj = getUserListApi(recevingNode, logNode);
					   invokeApi.async(postgetUserListObj).then(function(responsegetUserListObj) {
							angular.forEach(responsegetUserListObj.UserList, function(value, key) {
								
								if(angular.isUndefined(value.length)){ //Have One Users
									
									loggedInUsers.push(value.Loginid);
						
									
								}else{
									
								//	console.log("Get Logged in Users For Multiple");
									
									for(var i = 0; i<value.length; i++){
										
										loggedInUsers.push(value[i].Loginid);
								
										
									}
									
								}
								
								var postgetUserLogStatusObject = getUserLogStatusApi(loggedInUsers, logNode);
								
								invokeApi.async(postgetUserLogStatusObject).then(function(responsegetUserLogStatusObject){
									
									angular.forEach(responsegetUserLogStatusObject.Users, function(value, key) {
										
										angular.forEach(value, function(oValue, oKey) {
											
											if(!angular.equals(oValue,"")){
												
												var objArray = Object.keys(oValue);
												
												generateLoggerHirarchy(objArray, oKey,Statelogged, userlogged);
												
											}
											
											
											 
											
											
										});
										
									});
									
								});
								
								$scope.loggedInUsers = loggedInUsers;
								$scope.LoggedUserList = userlogged;
								$scope.loggerHirerachy = Statelogged;
								
						
								
							});
						  });
					 }
				};
			 
			
			$scope.removeLog = function(index){
					$scope.loggerHirerachy.splice(index,1);
				 };
				 
			$scope.removeUserLog = function(index){
					 $scope.LoggedInUserList.splice(index,1);
				 };
		
			/*************************End Logger******************/
			 
			 
			 

	});
});
