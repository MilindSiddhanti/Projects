define(['common/ctrljs/app' ],function(myApp) {

			myApp.config(function($stateProvider, $urlRouterProvider,
							routeStateLoaderProvider) {

						var route = routeStateLoaderProvider.route;
						
						$urlRouterProvider.otherwise('/');
						
									
						
						$stateProvider

								.state(
										'Settings',
										{
											url : '/Settings:action?CSRFToken',
											views : {
												'' : {
													templateUrl : 'Settings/Config/SettingsFrame.html',
													controller : 'settingsAppController',
													resolve : route
															.resolve('Settings/Controller/settingsAppController')
												},
												'settingsNavigator@Settings' : {
													templateUrl : 'Settings/settingsNavigator.html',
													controller : 'settingsNavigatorController',
													resolve : route
															.resolve('Settings/Controller/settingsNavigatorController')
												},
												'criteria@Settings' : {
													templateUrl : 'Settings/View/frmSettingsmenu.html',
													controller : 'settingsChangeController',
													resolve : route
															.resolve('Settings/Controller/settingsChangeController')
												},
											},
										})
								.state(
										'Settings.changenode',
										{
											url : '/changenode',
											views : {
												'criteria@Settings' : {
													templateUrl : 'Settings/View/frmSettingschangenode.html',
													controller : 'settingsChangeController',
													resolve : route
															.resolve('Settings/Controller/settingsChangeController')
												},
											},
										})
								.state(
										'Settings.changepass',
										{
											url : '/changepass',
											views : {
												'criteria@Settings' : {
													templateUrl : 'Settings/View/frmSettingschangepass.html',
													controller : 'settingsChangeController',
													resolve : route
															.resolve('Settings/Controller/settingsChangeController')
												},
											},
										})
										
								.state(
										'Settings.trace',
										{
											url : '/trace',
											views : {
												'criteria@Settings' : {
													templateUrl : 'Settings/View/frmSettingsenabletrace.html',
													controller : 'settingsChangeController',
													resolve : route
															.resolve('Settings/Controller/settingsChangeController')
												},
											},
										})
										
								
					});

		});
