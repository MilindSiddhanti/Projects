define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller("ConsoleHomeCtrl", function($scope, $location, $window, $rootScope, $log, AuthenticationService) {

   $scope.csrf = $location.search().CSRFToken;
   $scope.go = function(path) {
		console.log("Menu Options Page");
		console.log(path);
		$location.path(path);
	};
	$scope.logout = function(){
		console.log("logOut Call-----------");
		AuthenticationService.ClearCredentials();
		$location.url('/');
	}; 
   
	});

/*
	//angular.module('super-awesome-demo',['shoppinpal.mobile-menu'])
    myApp.config(['$routeProvider',function($routeProvider){
        $routeProvider
            .when("/skinny", {
                templateUrl: "skinny.html"//,
                //controller: "SkinnyCtrl"
            })
            .when("/wide", {
                templateUrl: "wide.html"//,
                //controller: "WideCtrl"
            })
            .otherwise({
                redirectTo: "/skinny"
            });
    
}])
    .run(["$rootScope","$spMenu",function(a,b){a.$spMenu=b}])
    .provider("$spMenu",function(){
    	this.$get=[function(){
    		var a={};
    		return a.show=function(){
    			var a=angular.element(document.querySelector("#sp-nav"));
    			console.log(a),a.addClass("show")},
    			a.hide=function(){
    				var a=angular.element(document.querySelector("#sp-nav"));
    				a.removeClass("show")},
    			a.toggle=function(){
    					var a=angular.element(document.querySelector("#sp-nav"));
    					a.toggleClass("show")},a}
    ]});
    */
});