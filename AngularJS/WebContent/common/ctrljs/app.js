'use strict';

define([], function () {

//var myApp = angular.module('BridgeApp', ['ui.router','localization','ngCookies', 'ngTouch', 'routeStateLoaderServices','angularjs-dropdown-multiselect', 'ngAnimate', 'toaster','sterlingConfigReaderProvider','HotKeyOverriding']);
var myApp = angular.module('BridgeApp', ['ui.router','localization','ngCookies', 'ngTouch', 'routeStateLoaderServices','angularjs-dropdown-multiselect', 'ngAnimate', 'toaster','sterlingConfigReaderProvider']);
  
   myApp.config(function ($httpProvider) {
      $httpProvider.interceptors.push('HttpInterceptor');
    });
   
   myApp.value("loggerHirerachy",[]);

   myApp.run(function($rootScope) {
	  
	  
	
	  $rootScope.receive_moduleFlow = "Find POs";
	  $rootScope.receive_NavActive = "POs";
	

   });
   
   
   myApp.run(function($log, $filter, loggerHirerachy, BridgeUtils) {
	   
	   $log.enabledContexts = [];
	   $log.ContextEntries = [];
	   
	   $log.getloggedEntries = function(){
		   return $log.ContextEntries;  
	   },
	   
	   $log.ClearloggedEntries = function(){
		   	 $log.ContextEntries = [];  
		   },
		   
	   $log.getInstance = function(context) {
			  var cntxt = getParentState(context);
	
		return {
	         log   : enhanceLogging($log.log, 'log', context, cntxt.name),
	         info  : enhanceLogging($log.info, 'info', context, cntxt.name),
	         warn  : enhanceLogging($log.warn, 'warn', context, cntxt.name),
	         debug : enhanceLogging($log.debug, 'debug', context, cntxt.name),
	         error : enhanceLogging($log.error, 'error', context, cntxt.name),
	         contextDump : function(){
	        	 return $log.ContextEntries[cntxt];
	         },
	         clearDump : function(){
	        	$log.ContextEntries[cntxt] = null;
	         },
	         enableLogging: function(enable) {
	            $log.enabledContexts[cntxt] = enable;
	         }
	      };
	   };
	   
	   function initlogger(cntxt){
		   $log.enabledContexts[cntxt] = "true";
	   };
	   
       function getParentState(cntxt){
    	  if(!(cntxt.parent == ""))
    	    	cntxt = getParentState(cntxt.parent);
    	  return cntxt;
       };
       
       function isContextEnabled(cntxt){
    	   if(((loggerHirerachy.indexOf('All')) == -1) && (((loggerHirerachy.indexOf(cntxt)) == -1)))
    		   return 0;
    	   
    	   return 1;
       }
       
	   function enhanceLogging(loggingFunc, logmethod, context, cntxt) {
	      return function() {
	    	 
	   	     var contextEnabled = BridgeUtils.isLogContextEnabled(cntxt);
	        // var contextEnabled = isContextEnabled(cntxt); //$log.enabledContexts[cntxt];
	        // console.log(contextEnabled);
	         var entries = $log.ContextEntries[cntxt];
	         if (contextEnabled) {
	        	var timestamp = $filter('date')(new Date(), 'dd-MM-yyyy HH:mm:ss:sss');
	        	var modifiedArguments = [].slice.call(arguments);
	            if(entries == null)
	            	$log.ContextEntries[cntxt] = '\n' + timestamp +" ::: " + context + ' ::: '+ logmethod+' ::: '+modifiedArguments[0];
	            else
	            	$log.ContextEntries[cntxt] = entries +'\n'+ timestamp +" ::: " + context+ ' ::: ' + logmethod +' ::: '+modifiedArguments[0];
	            modifiedArguments[0] = timestamp +" ::: "+ context + ' ::: ' + modifiedArguments[0];
	            loggingFunc.apply(null, modifiedArguments);
	         }
	      };
	   }
	});
   
   myApp.config(function ($provide) {
	    $provide.decorator('$exceptionHandler', function($delegate, $injector, BridgeUtils){
	    	 return function (exception, cause) {
	    		 $delegate(exception, cause);
	    		 var $location = $injector.get("$location");
	    		 var winError = exception.message + " Caused by "+cause+" in the path "+ $location.path();
	    		 var $state = $injector.get("$state");
	    		 var StartPage = BridgeUtils.getBaseState($state.$current.name);
	    		 var contextEnabled = BridgeUtils.isLogContextEnabled(StartPage);
	    		 
	    		 if(contextEnabled)
                   BridgeUtils.callBridgeLogger(true, StartPage, winError);
     
	    		 
	    	 };
	    } );
	});

		
   myApp.run(function($window, $rootScope, AuthenticationService, BridgeUtils) {
	   
	   $rootScope.globals = BridgeUtils.getCookieGlobal();
	   
       if ($rootScope.globals.currentUserId) {
    		AuthenticationService.setAuth($rootScope.globals);
	    };
	    
	   $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
		    console.log('toState.name: '+toState.name);
	        console.log('fromState.name: '+fromState.name);
          
	        BridgeUtils.callBridgeLogger(false, '', '');
	

	         if(angular.equals(toState.name, 'login')){
	    	    AuthenticationService.ClearCredentials();
	    	    console.log("Clear Credential");
	          }
		  
		     var CommonPage = BridgeUtils.getCommonAccessState();
		     var NonRestrict = BridgeUtils.getAuthorizedState();
		     var loggedIn = false;
		     var RestrictedPage = true;
		  		  
		     if(NonRestrict.indexOf(BridgeUtils.getBaseState(toState.name)) != -1){
			     RestrictedPage = false;
		        }
		   
		     if ($rootScope.globals) {
			    console.log("Presrent");
			    loggedIn = $rootScope.globals.currentUserId ;
		       };
		    
		      console.log("Restrict "+RestrictedPage);
		      console.log(NonRestrict);
		      console.log(loggedIn);

		     if (RestrictedPage && loggedIn && (CommonPage.indexOf(toState.name) == -1)) {
			       $window.location='/'+BridgeUtils.getAppName();
		     }else if((CommonPage.indexOf(toState.name) == -1) && (!loggedIn)){
			       $window.location='/'+BridgeUtils.getAppName();  
		     }
	   });
	
    });

  
   myApp.config(function($stateProvider, $urlRouterProvider, $controllerProvider, routeStateLoaderProvider) {
	   
	   myApp.register = {
				  controller: $controllerProvider.register,
				  
		  };
	   
	   var route = routeStateLoaderProvider.route;
	   
	   
	   $urlRouterProvider.otherwise('/');
	
	   $stateProvider
			.state('login', {
							url: '/',
							templateUrl: 'Login/LoginForm.html',
							controller: 'LoginCtrl',
							resolve : route.resolve('Login/LoginCtrl'),
						}
					 )
	        .state('MobileHome', {
	        		        	url: '/MobileHome:action?CSRFToken',
	        		        	templateUrl: 'Menu/Menu.html',
	        		        	controller: 'MenuCtrl',
	        		        	resolve : route.resolve('Menu/MenuCtrl'),
	        				 }
			  )
			   .state('menuopt', {
	        		        	url: '/menuopt',
	        		        	templateUrl: 'MenuOptions/MenuOpt.html',
	        		        	controller: 'MenuOptCtrl',
	        		        	resolve : route.resolve('MenuOptions/MenuCtrlOpt'),
	        				 }
			  )
			  .state('ConsoleHome', {
	        		        	url: '/chome:action?CSRFToken',
				  				templateUrl: 'MenuOptions/ConsoleHome.html',
	        		        	controller: 'ConsoleHomeCtrl',
	        		        	resolve : route.resolve('MenuOptions/ConsoleHomeCtrl'),
	        				 }
			  )
		
	});

   
  //  myApp.factory('AuthenticationService',function($rootScope, $http, $cookieStore, loggerHirerachy, HKOverride){
   myApp.factory('AuthenticationService',function($rootScope, $http, $cookieStore, loggerHirerachy){
	    var service = {};
	    var authJson = {};
   	    service.SetCredentials = function(authObj, csrf){
		   
   	    	$rootScope.globals = {
				"tagName" : authObj.tagName, 
				currentUserId : authObj.currentUserId, 
				OrganizationCode : authObj.OrganizationCode,
				"UserToken" : authObj.UserToken
				};
   	    	
   	    	 this.setAuth(authObj);
		     $cookieStore.put('globals', $rootScope.globals);
		     $cookieStore.put('CSRFToken', csrf);
		   };
	
	    service.ClearCredentials = function(){
	    	 console.log("called clear");
	    	 for (var prop in $rootScope) {
	    	    if (prop.substring(0,1) !== '$') {
	    	        delete $rootScope[prop];
	    	     }
	    	 }
	    	 $cookieStore.remove('globals');
		     $cookieStore.remove('CSRFToken');
		     $cookieStore.remove('NonRestricted');
		     $http.defaults.headers.common['X-CSRFToken'] = '';
		     this.setAuth({});
		     loggerHirerachy.length = 0;
			 HKOverride.setOverride(false);
		 };
		 
		service.setAuth = function(temp){
			authJson = temp;
		 };
		
		service.getAuth = function(){
			   return authJson;
		 };
	
	    return service;
     });


    myApp.factory('invokeApi', function($http, BridgeUtils) {
	      var promise;
	      var invokeApi = {
	        async: function(postObject) {
	             var s=JSON.stringify(postObject);
	             promise = $http({
		                 url: '/'+BridgeUtils.getAppName()+'/proxy/service/Httpinvoke',
		                 method: 'POST',
		                 data: s,
		                 headers: {
		        	       "Accept" : "text/plain",
		                   "Content-Type": "application/json"
		                 }
		            }).then(function (response) {
	                   return response.data;
	              });
	      
	              return promise;
	          }
	    };
	    return invokeApi;
	  });

     myApp.factory('loggerService', function($http, BridgeUtils) {
	      var promise;
	      var loggerService = {
	        async: function(postObject) {
	        	
	        	   var logger = '';
	        	   
	        	   Object.keys(postObject).forEach(function (key) {
	        		  logger = logger + postObject[key] + '\n';
	        		});
	        	   
	        	   var postconent = new Object();
	        	   postconent.logger = logger;
	        	   
	        	   postconent.UserId = '';
	        	   
	        	   var cookieGlobals = BridgeUtils.getCookieGlobal();
	        	   
	   			   if(! angular.isUndefined(cookieGlobals)){
	   				postconent.UserId = cookieGlobals.currentUserId;
	   			   };
	        	   
	        	   
	        	  promise = $http({
		       	         url: '/'+BridgeUtils.getAppName()+'/proxy/service/AppLogger',
		                 method: 'POST',
		                 data: JSON.stringify(postconent) ,
		                 headers: {
		        	      "Content-Type": "text/plain"
		                 }
		            }).then(function (response) {
	                   return response.data;
	              });
	      
	             return promise;
	          }
	    };
	    return loggerService;
	  });
     
     
     
     myApp.factory('HttpInterceptor', function ($q, $location, $cookieStore, $rootScope) {
    	    return {
    	    	request: function(request){
    	    		console.log("Request Intercept Request Param test"+$location.search().CSRFToken);
    	    		request.headers['X-CSRFToken'] = $location.search().CSRFToken;

    	    		return request;
    	    	},
    	        response: function (response) {
    	            // do something on success
    	            if(response.data){
    	                 console.log("Sucess Interceptor");
    	            }
    	            return response;
    	        },
    	        responseError: function (response) {
    	        	
    	        	if((response.status)=="403"){ 
    	        		console.log("ehfhgdhgdhghg");
    	        		$rootScope.globals = {};
       			        $cookieStore.remove('globals');
       			        $cookieStore.remove('CSRFToken');
       			        $rootScope.loginmessage = "Session Expired/ Invalid Token";
    	        	}
    	        	$location.url('/'); 
    	            return $q.reject(response);
    	        }
    	    };
    	});
   
     myApp.factory('processContoller',function($rootScope, $document, $location){
	        var process = {};
	
	        process.InputController = function(scannedVariable) {
		       return true;//False -- Focus remains in the Input Box , True- Focus moved to next Controller
	        };
            return process;
       }); 

     myApp.directive('input',function($location, processContoller, loggerHirerachy) {
	        return {
		         restrict: 'E',
		         template: 'Testing',
		         link: function(scope, element, attrs) {
		        	 if((attrs.type=='checkbox') && (((attrs.class).indexOf('loggers')) != -1)){

		        		 if((loggerHirerachy.indexOf(attrs.id)) != -1)
			        		 element.attr('checked', 'true');
		        		 else
		        			 element.attr('checked', '');
		        		 
		        	 }

		         element.on('change', function(event){
		        	 if((attrs.type=='checkbox')){
		        		 console.log(attrs.id);
		        		 if((loggerHirerachy.indexOf(attrs.id)) == -1){
		        			 loggerHirerachy.push(attrs.id);
		        			 console.log("push");
		        		 }else{
		        			 loggerHirerachy.pop(attrs.id);
		        			 console.log("pop");
		        		 }
		        	 }
		        	      
		          });
			      element.on('keydown',function(event){
			    	//  console.log("KeyDown"+event.keyCode);
				     if((event.keyCode == 9) || (event.keyCode == 13)){
					     var inputvalue = element.val();
					     if((attrs.type=='text') && (attrs.validate=='true')){
					    	 console.log("Predodn");
					         console.log("done");
					        var focusStatus = processContoller.InputController(attrs.id);
					        if(!focusStatus)
					    	   event.preventDefault();
				
					         }
					         
					        
				       }
				
			        }); 
			
		         },
	
	
	       };

      });
      
      
     myApp.factory('Navigator', function($rootScope){
    		
	       var factory = {};
	
	       var nav = {"Navigator":[
	                            {"state":"POs", "status":""},
	                            {"state":"Pallets", "status":""},
	                            {"state":"Case/Item", "status":""}
	                        ],
	            "Receive":[
	                            {"state":"POs", "status":""},
	                            {"state":"Pallets", "status":""},
	                            {"state":"Case/Item", "status":""}
	                       ]};
	       var Active = "";
	
	
	       factory.setNavigation = function(temp){
		      Active = temp;
	       };
	
	       factory.getNavigation = function() {
		     return nav[$rootScope.module];
	       };
          return factory;
	
      });
     
     myApp.factory('BridgeUtils', function($cookieStore, $injector){
    		
 	    var utils = {};
 	    
 	    utils.getAppName = function(){
 		    return "Gazelle" ;
 	      };
 	    
 	    utils.getBaseState = function(StateName){
 	    	var temp = StateName;
 	    	if((temp.indexOf('.')) != -1)
 	    		temp = StateName.split(".")[0];
 		    return temp;
 	      };
 	      
 	    utils.getCommonAccessState = function(){
            var temp = ['/','MobileHome','menuopt','ConsoleHome','login'];
  		    return temp;
  	      };  
 	    
  	    utils.getAuthorizedState = function(){
  	    	var temp = $cookieStore.get('NonRestricted');
  	    	if(! angular.isUndefined(temp))
  	    	    return temp;
  	    	return [];
  	      };
  	     
  	    utils.getCookieGlobal = function(){
  	    	var temp = $cookieStore.get('globals');
  	    	if(! angular.isUndefined(temp))
  	    	    return temp;
  	    	return {};
  	       };
  	       
  	    utils.isLogContextEnabled = function(cntxt){
  	    	
  	    	var loggerHirerachy = $injector.get("loggerHirerachy");
      	    if(((loggerHirerachy.indexOf('All')) == -1) && (((loggerHirerachy.indexOf(cntxt)) == -1)))
      		    return 0;
      	   
      	     return 1;
          };
         
  	    utils.callBridgeLogger = function(exception, StartPage, winError){
  	    	
  	    	var $log = $injector.get("$log");
  	    	var loggerService =  $injector.get("loggerService");
   	    	var postLogger =  $log.getloggedEntries();
   	    	
   	    	if(exception)
   	    		postLogger.push(winError);
   	  	   
  		    if(postLogger != null && ((Object.keys(postLogger).length)>0)){
  			   if(!(exception &&  (angular.equals(StartPage,'login')))) {
  				    loggerService.async(postLogger).then(function(response){
  	    			    $log.ClearloggedEntries();
  		             });   
  			     }
  	           }  
   	    	
   	    	
   	       };
  	       
  	       
  	       
  	     
  	       
        return utils;
         
      });
   
     return myApp;

});