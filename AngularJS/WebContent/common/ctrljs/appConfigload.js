require.config({
    baseUrl: '.',
});

require(
	    [
	        'common/ctrljs/app',
	        'common/ctrljs/routeStateLoader',
			'common/ctrljs/sterlingConfigReader',
	        'Receive/Config/ReceiveApp',
	        'Count/CountApp',
	        'OutboundPick/Config/PickApp',
	        'Putaway/Config/PutawayApp',
	        'BlindReceive/blindApp',
	        'Settings/Config/SettingsApp',
	        'Move/MoveApp',
			'Configuration/Config/ConfigureApp',
			'CloseReceipt/Config/CloseReceiptApp',
			'Manifest/Config/ManifestApp'
	     ],
	 function () {
	        angular.bootstrap(document, ['BridgeApp']);
	    } );