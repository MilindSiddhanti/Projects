    var routeStateLoader = function () {

        this.$get = function () {
            return this;
        };

        this.routeConfig = function () {
            var controllersDirectory = '',

            setBaseDirectories = function (controllersDir) {
                controllersDirectory = controllersDir;
            },

            getControllersDirectory = function () {
                return controllersDirectory;
            };

            return {
                setBaseDirectories: setBaseDirectories,
                getControllersDirectory: getControllersDirectory,
            };
        }();

        this.route = function (routeConfig) {

            var resolve = function (baseName, configState) {
            
            	var routeDef = {};
            	var Statejs = '';
            	
            	if(configState)
            	    Statejs = routeConfig.getControllersDirectory() + configState + '.js';
            		
            	routeDef.resolve = {
            			load: ["$q", function($q) { 
                            var dependencies = [routeConfig.getControllersDirectory() + baseName + '.js' , Statejs];
                            return resolveDependencies($q, dependencies);
                        }]           			
            	};
                
              
            	
            	/*
            	if (!path) path = '';

                var routeDef = {};
                var baseFileName = baseName.charAt(0).toLowerCase() + baseName.substr(1);
                routeDef.templateUrl = routeConfig.getViewsDirectory() + path + baseFileName + '.html';
                routeDef.controller = baseName + 'Controller';
                if (controllerAs) routeDef.controllerAs = controllerAs;
                routeDef.secure = (secure) ? secure : false;
                console.log("Testing Sync"+ routeDef.templateUrl + "::" + routeDef.controller + "::" +routeConfig.getControllersDirectory() + path + baseFileName + '.js');
                routeDef.resolve = {
                    load: ['$q', '$rootScope', function ($q, $rootScope) {
                        var dependencies = [routeConfig.getControllersDirectory() + path + baseFileName + '.js'];
                        return resolveDependencies($q, $rootScope, dependencies);
                    }]
                };
                
                */

                return routeDef.resolve;
            },

            resolveDependencies = function ($q, dependencies) {
                var defer = $q.defer();
                require(dependencies, function () {
                    defer.resolve();
                });

                return defer.promise;
            };

            return {
                resolve: resolve
            }
        }(this.routeConfig);

    };

    var servicesApp = angular.module('routeStateLoaderServices', []);

    //Must be a provider since it will be injected into module.config()    
    servicesApp.provider('routeStateLoader', routeStateLoader);
