'use strict';

/*
 * An AngularJS HotKey Overrider Service
 *
 * 
 *
 */

angular.module('HotKeyOverriding', [])
    // Hot_Key_Overriding service responsible for overriding 
    // default Hot keys behavior with user injected logics
    .factory('HKOverride', ['$http', '$rootScope', '$window', function ($http, $rootScope, $window) {
        var HKOverride = {
            

        	status:'',
            

            setOverride: function(value) {
            	HKOverride.status = value;
            },
            
            getOverride: function() {
            	return HKOverride.status;
            },
            
            OverriseF1HotKey: function(){
            	return true;
            },
            
            OverriseF2HotKey: function(){
            	return true;
            },
            
            OverriseF5HotKey: function(){
            	return true;
            },
            
            ControlBehavior: function(e){
            	
            	if(HKOverride.status){
            		e.preventDefault();
            	}
            	
            }
            

        };


        return HKOverride;
        
    } ])
    .directive('body', ['HKOverride','$location','$state', function(HKOverride,$location,$state){
    	
        return {
            restrict:"E",
            link: function(scope, element, attrs){
  			  
  			  element.on('keydown',function(event){
  				  
  				  var SrtCtPg = document.getElementsByTagName("menuShortcutKey").length; 
  				  
  				  
  				  if(SrtCtPg>0){
	
  				  	    var shrtKey = document.getElementById(event.keyCode);
        			    if(angular.isObject(shrtKey)){
        			       var KeystateArray = (shrtKey.getAttribute('ui-sref')).split('(');
        			       var ostate = $state.get(KeystateArray[0]);
        				      
        			       if(ostate){
        				    	  var stateUrl = ostate.url;
            				      var csrf = $location.search().CSRFToken;
            				      scope.$apply(function() {
            						  $location.path(stateUrl).search({"CSRFToken": csrf});
            					  });
            		        }
        				      
        				      
        				      
        				//      console.log(event.keyCode);
        				//	  console.log(document.getElementById(event.keyCode));
        				//	  console.log(KeystateArray[0]);
        				//	  console.log($state.get(KeystateArray[0]).url);
        				//	  console.log($location.search().CSRFToken);//$location.path('/menuopt').search({"CSRFToken": csrf});
        			      }
  					
  				   }
  				  
  				  if((angular.equals(event.keyCode,112))){
  					  
  					  //F1 Override
  					  var retControl = HKOverride.OverriseF1HotKey();
  					  HKOverride.ControlBehavior(event);
  					  console.log("F1 Called");
  					  
  					  
  				  }else if((angular.equals(event.keyCode,113))){
  					  
  					  //F2 Override
  					  var retControl = HKOverride.OverriseF2HotKey();
  					  HKOverride.ControlBehavior(event);
  					  console.log("F2 Called");
  					  
  				  }else if((angular.equals(event.keyCode,116))){
  					  
  					  //F5 Override
  					  var retControl = HKOverride.OverriseF5HotKey();
  					  HKOverride.ControlBehavior(event);
  					  console.log("F5 Called");
  					  
  				  }
  				  
  				  
  				  
  			  });
  			  
  			  
  		     },
  		     
  		     
        };

       
    }]);    

/*

HKOverride.setOverride(false);

HKOverride.OverriseF1HotKey = function(){

	  console.log("F1 Hot Key Overrider");
	  
};*/