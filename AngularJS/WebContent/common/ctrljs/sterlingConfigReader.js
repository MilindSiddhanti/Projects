/*******************************************************************************
 * Name: sterlingConfigService Usage: Generic Service Purpose: This service has
 * multiple APIs which are used repetitively across the modules and the APIs to
 * read the different configurations present in the Sterling.
 ******************************************************************************/

function getExecutionExceptionListApi(exeObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getExecutionExceptionList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "ExecutionException";
	postObject.InputXml.ExceptionCode = exeObj.ExceptionCode;
	postObject.InputXml.Node = exeObj.Node;
	postObject.Template = new Object();
	postObject.Template.tagName = "ExecutionExceptionList";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "ExecutionException";
	postObject.Template.childNodes[0].AllowPartialDeposit = "";
	postObject.Template.childNodes[0].ExceptionType = "";
	postObject.Template.childNodes[0].ExceptionCode = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
}


function registerTaskInProgressApiObj(regTaskObj, authSer) {

	var postObject = new Object();
	postObject.CommandName = "registerTaskInProgress";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Task";
	postObject.InputXml.TaskId = regTaskObj.TaskId;
	postObject.InputXml.EquipmentId = regTaskObj.EquipmentId;
	postObject.InputXml.OrganizationCode = regTaskObj.OrganizationCode;
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "Inventory";
	postObject.InputXml.childNodes[0].SourceCaseId = regTaskObj.SourceCaseId;
	postObject.InputXml.childNodes[0].SourcePalletId = regTaskObj.SourcePalletId;
	postObject.InputXml.childNodes[0].Quantity = regTaskObj.Quantity;
	postObject.Template = new Object();
	postObject.Template.tagName = "Task";
	postObject.Template.TaskId = "";
	postObject.Template.TargetLocationId = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Inventory";
	postObject.Template.childNodes[0].SourceCaseId = "";
	postObject.Template.childNodes[0].SourcePalletId = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;

};

function getNextTaskApi(ntObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getNextTask";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "GetNextTask";
	postObject.InputXml.ActivityGroupId = ntObj.ActivityGroupId;
	postObject.InputXml.EquipmentId = ntObj.EquipmentId;
	postObject.InputXml.CurrentLocationId = ntObj.CurrentLocationId;
	postObject.InputXml.OrganizationCode = ntObj.OrganizationCode;
	postObject.InputXml.UserId = ntObj.UserId;
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "Task";

	postObject.Template = new Object();
	postObject.Template.tagName = "TaskList";
    postObject.Template.TotalNumberOfRecords = "";
    postObject.Template.childNodes = [];
    postObject.Template.childNodes[0] = new Object();
    postObject.Template.childNodes[0].tagName = "Task";
    postObject.Template.childNodes[0].TaskId = "";
    postObject.Template.childNodes[0].SuggestedLPNNo = "";
    postObject.Template.childNodes[0].SourceLocationId = "";
    postObject.Template.childNodes[0].childNodes = [];
    postObject.Template.childNodes[0].childNodes[0] = new Object();
    postObject.Template.childNodes[0].childNodes[0].tagName = "Inventory";
    postObject.Template.childNodes[0].childNodes[0].SourceCaseId = "";
    postObject.Template.childNodes[0].childNodes[0].SourcePalletId = "";
    postObject.Template.childNodes[0].childNodes[0].ItemId = "";
    postObject.Template.childNodes[0].childNodes[0].childNodes = [];
    postObject.Template.childNodes[0].childNodes[0].childNodes[0] = new Object();
    postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName = "Item";
    postObject.Template.childNodes[0].childNodes[0].childNodes[0].UnitOfMeasure = "";
    postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes = [];
    postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0] = new Object();
    postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].tagName = "PrimaryInformation";
    postObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].Description = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function getShipmentListApi(shipObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getShipmentList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Shipment";
	postObject.InputXml.DocumentType = shipObj.DocumentType;
	postObject.InputXml.ReceivingNode = shipObj.ReceivingNode;
	postObject.InputXml.ShipmentKey = shipObj.ShipmentKey;
	postObject.InputXml.ShipmentNo = shipObj.ShipmentNo;
	postObject.InputXml.OrderNo = shipObj.OrderNo;
	postObject.Template = new Object();
	postObject.Template.tagName = "Shipments";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Shipment";
	postObject.Template.childNodes[0].DoNotVerifyCaseContent = "";
	postObject.Template.childNodes[0].DoNotVerifyPalletContent = "";
	postObject.Template.childNodes[0].DocumentType = "";
	postObject.Template.childNodes[0].EnterpriseCode = "";
	postObject.Template.childNodes[0].ReceivingNode = "";
	postObject.Template.childNodes[0].SellerOrganizationCode = "";
	postObject.Template.childNodes[0].ShipmentNo = "";
	postObject.Template.childNodes[0].Status = "";
	postObject.Template.childNodes[0].ShipmentKey = "";

	postObject.Template.childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0] = new Object();
	postObject.Template.childNodes[0].childNodes[0].tagName = "Containers";

	postObject.Template.childNodes[0].childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0].childNodes[0] = new Object();
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName = "Container";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerScm = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerNo = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerType = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].ShipmentContainerKey = "";

	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function getReceiptListApi(recObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getReceiptList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Receipt";
	postObject.InputXml.DocumentType = recObj.DocumentType;
	postObject.InputXml.EnterpriseCode = recObj.EnterpriseCode;
	postObject.InputXml.ReceivingNode = recObj.ReceivingNode;
	postObject.InputXml.ShipmentKey = recObj.ShipmentKey;
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "Shipment";
	postObject.InputXml.childNodes[0].OrderNo = recObj.OrderNo;
	postObject.Template = new Object();
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
}

function getNodeItemDetailsApi(niObj, authSer) {

	var postObject = new Object();
	postObject.CommandName = "getNodeItemDetails";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Item";

	postObject.InputXml.ItemID = niObj.ItemID;
	postObject.InputXml.Node = niObj.Node;
	postObject.InputXml.OrganizationCode = niObj.OrganizationCode;
	postObject.InputXml.UnitOfMeasure = niObj.UnitOfMeasure;
	postObject.Template = new Object();
	postObject.Template.tagName = "Item";
	postObject.Template.ItemKey = "";
	postObject.Template.UnitOfMeasure = "";
	postObject.Template.OrganizationCode = "";
	postObject.Template.ItemID = "";

	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "PrimaryInformation";
	postObject.Template.childNodes[0].UnitHeight = "";
	postObject.Template.childNodes[0].UnitHeightUOM = "";
	postObject.Template.childNodes[0].UnitLength = "";
	postObject.Template.childNodes[0].UnitLengthUOM = "";
	postObject.Template.childNodes[0].UnitWeight = "";
	postObject.Template.childNodes[0].UnitWeightUOM = "";
	postObject.Template.childNodes[0].UnitWidth = "";
	postObject.Template.childNodes[0].UnitWidthUOM = "";
	postObject.Template.childNodes[0].DisplayItemDescription = "";
	postObject.Template.childNodes[0].NumSecondarySerials = "";

	postObject.Template.childNodes[1] = new Object();
	postObject.Template.childNodes[1].tagName = "InventoryParameters";
	postObject.Template.childNodes[1].IsSerialTracked = "";
	postObject.Template.childNodes[1].TagControlFlag = "";
	postObject.Template.childNodes[1].TimeSensitive = "";

	postObject.Template.childNodes[2] = new Object();
	postObject.Template.childNodes[2].tagName = "InventoryTagAttributes";
	postObject.Template.childNodes[2].LotNumber = "";
	postObject.Template.childNodes[2].BatchNo = "";
	postObject.Template.childNodes[2].LotAttribute1 = "";
	postObject.Template.childNodes[2].LotAttribute2 = "";
	postObject.Template.childNodes[2].LotAttribute3 = "";
	postObject.Template.childNodes[2].LotKeyReference = "";
	postObject.Template.childNodes[2].ManufacturingDate = "";
	postObject.Template.childNodes[2].RevisionNo = "";

	postObject.Template.childNodes[3] = new Object();
	postObject.Template.childNodes[3].tagName = "AlternateUOMList";

	postObject.Template.childNodes[3].childNodes = [];
	postObject.Template.childNodes[3].childNodes[0] = new Object();
	postObject.Template.childNodes[3].childNodes[0].tagName = "AlternateUOM";
	postObject.Template.childNodes[3].childNodes[0].Height = "";
	postObject.Template.childNodes[3].childNodes[0].HeightUOM = "";
	postObject.Template.childNodes[3].childNodes[0].Length = "";
	postObject.Template.childNodes[3].childNodes[0].LengthUOM = "";
	postObject.Template.childNodes[3].childNodes[0].Quantity = "";
	postObject.Template.childNodes[3].childNodes[0].UnitOfMeasure = "";
	postObject.Template.childNodes[3].childNodes[0].Weight = "";
	postObject.Template.childNodes[3].childNodes[0].WeightUOM = "";
	postObject.Template.childNodes[3].childNodes[0].Width = "";
	postObject.Template.childNodes[3].childNodes[0].WidthUOM = "";

	postObject.IsService = "N";
	postObject.Authenticate = authSer;

	return postObject;
};

function getTaskListApi(taskObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getTaskList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Task";
	postObject.InputXml.Node = taskObj.Node;
	postObject.InputXml.TaskStatus = taskObj.TaskStatus;
	if (taskObj.TaskStatus == 1300 || taskObj.TaskStatus == 1200) {
		postObject.InputXml.AssignedToUserId = taskObj.AssignedToUserId;
		postObject.InputXml.EquipmentId = taskObj.EquipmentId;
	}
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "TaskType";
	postObject.InputXml.childNodes[0].ActivityGroupId = taskObj.ActivityGroupId;
	postObject.InputXml.childNodes[1] = new Object();
	postObject.InputXml.childNodes[1].tagName = "Inventory";
	postObject.InputXml.childNodes[1].SourceCaseId = taskObj.SourceCaseId;
	postObject.InputXml.childNodes[1].SourcePalletId = taskObj.SourcePalletId;
	postObject.InputXml.childNodes[1].ItemId = taskObj.ItemId;
	postObject.InputXml.childNodes[2] = new Object();
	postObject.InputXml.childNodes[2].tagName = "TaskReferences";
	postObject.InputXml.childNodes[2].ShipmentNo = taskObj.ShipmentNo;
	;
	postObject.Template = new Object();
	postObject.Template.tagName = "TaskList";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Task";
	postObject.Template.childNodes[0].TaskId = "";
	postObject.Template.childNodes[0].TaskType = "";
	postObject.Template.childNodes[0].SuggestedLPNNo = "";
	postObject.Template.childNodes[0].SourceLocationId = "";
	postObject.Template.childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0] = new Object();
	postObject.Template.childNodes[0].childNodes[0].tagName = "Inventory";
	postObject.Template.childNodes[0].childNodes[0].SourceCaseId = "";
	postObject.Template.childNodes[0].childNodes[0].SourcePalletId = "";
	postObject.Template.childNodes[0].childNodes[0].ItemId = "";
	postObject.Template.childNodes[0].childNodes[0].ProductClass = "";
	postObject.Template.childNodes[0].childNodes[0].UnitOfMeasure = "";
	postObject.Template.childNodes[0].childNodes[0].Quantity = "";
	postObject.Template.childNodes[0].childNodes[0].CaptureTagInExecution = ""
	postObject.Template.childNodes[0].childNodes[1] = new Object();
	postObject.Template.childNodes[0].childNodes[1].tagName = "TaskReferences";
	postObject.Template.childNodes[0].childNodes[1].ShipmentNo = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function getEquipmentListApi(equipmentId, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getEquipmentList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Equipment";
	postObject.InputXml.EquipmentId = equipmentId;
	postObject.InputXml.Node = node;
	postObject.Template = new Object();
	postObject.Template.tagName = "Equipments";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Equipment";
	postObject.Template.childNodes[0].EquipmentType = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
}

function getLocationList(locationId, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getLocationList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Location";
	postObject.InputXml.LocationId = locationId;
	postObject.InputXml.Node = node;
	postObject.Template = new Object();
	postObject.Template.tagName = "Locations";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Location";
	postObject.Template.childNodes[0].ZoneId = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
}

function getZoneList(zoneId, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getZoneList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Zone";
	postObject.InputXml.ZoneId = zoneId;
	postObject.InputXml.Node = node;
	postObject.Template = new Object();
	postObject.Template.tagName = "Zones";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Zone";
	postObject.Template.childNodes[0].ForceLocnScanOnVisit = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
}

function getTaskTypeListApi(taskType, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getTaskTypeList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "TaskType";
	postObject.InputXml.TaskType = taskType;
	postObject.InputXml.Node = node;
	postObject.Template = new Object();
	postObject.Template.tagName = "TaskTypeList";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "TaskType";
	postObject.Template.childNodes[0].SuppressWarnOnLocnOverride = "";
	postObject.Template.childNodes[0].PerfTaskUsingEquipType = "";
	postObject.Template.childNodes[0].PrimaryEquipmentType = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
}

function translateBarCodeApi(barcodeData, barcodeType, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "translateBarCode";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "BarCode";
	postObject.InputXml.BarCodeData = barcodeData;
	postObject.InputXml.BarCodeType = barcodeType;
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "ContextualInfo";
	postObject.InputXml.childNodes[0].OrganizationCode = node;
	postObject.Template = new Object();
	postObject.Template.tagName = "BarCode";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Translations";
	postObject.Template.childNodes[0].BarCodeTranslationSource = "";
	postObject.Template.childNodes[0].TotalNumberOfRecords = "";
	postObject.Template.childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0] = new Object();
	postObject.Template.childNodes[0].childNodes[0].tagName = "Translation";
	postObject.Template.childNodes[0].childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0].childNodes[0] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName = "ContainerContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].CaseId = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].PalletId = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[1] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[1].tagName = "EquipmentContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[1].EquipmentId = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[1].IsMultiLocation = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[2] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[2].tagName = "ShipmentContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[2].ShipmentNo = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].tagName = "ItemContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].ItemID = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function translateBarCodeDetsApi(transObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "translateBarCode";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "BarCode";
	postObject.InputXml.BarCodeData = transObj.BarCodeData;
	postObject.InputXml.BarCodeType = transObj.BarCodeType;
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "ContextualInfo";
	postObject.InputXml.childNodes[0].OrganizationCode = transObj.OrganizationCode;
	postObject.InputXml.childNodes[0].EnterpriseCode = transObj.EnterpriseCode;
	postObject.InputXml.childNodes[1] = new Object();
	postObject.InputXml.childNodes[1].tagName = "ItemContextualInfo";
	postObject.InputXml.childNodes[1].ItemID = transObj.ItemID;
	postObject.InputXml.childNodes[1].InventoryUOM = transObj.InventoryUOM;
	postObject.InputXml.childNodes[2] = new Object();
	postObject.InputXml.childNodes[2].tagName = "ShipmentContextualInfo";
	postObject.InputXml.childNodes[2].SellerOrganizationCode = transObj.SellerOrganizationCode;
	postObject.InputXml.childNodes[2].ShipNode = transObj.ShipNode;
	postObject.InputXml.childNodes[2].ShipmentKey = transObj.ShipmentKey;
	postObject.InputXml.childNodes[2].ShipmentNo = transObj.ShipmentNo;
	postObject.InputXml.childNodes[3] = new Object();
	postObject.InputXml.childNodes[3].tagName = "ContainerContextualInfo";
	postObject.InputXml.childNodes[3].CaseId = transObj.CaseId;
	postObject.InputXml.childNodes[3].PalletId = transObj.PalletId;
	postObject.InputXml.childNodes[4] = new Object();
	postObject.InputXml.childNodes[4].tagName = "LocationContextualInfo";
	postObject.InputXml.childNodes[4].LocationId = transObj.LocationId;
	postObject.Template = new Object();
	postObject.Template.tagName = "BarCode";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Translations";
	postObject.Template.childNodes[0].BarCodeTranslationSource = "";
	postObject.Template.childNodes[0].TotalNumberOfRecords = "";
	postObject.Template.childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0] = new Object();
	postObject.Template.childNodes[0].childNodes[0].tagName = "Translation";
	postObject.Template.childNodes[0].childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0].childNodes[0] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName = "ContainerContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].CaseId = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[0].PalletId = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[1] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[1].tagName = "EquipmentContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[1].EquipmentId = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[1].IsMultiLocation = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[2] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[2].tagName = "ShipmentContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[2].ShipmentNo = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].tagName = "ItemContextualInfo";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].ItemID = "";
    postObject.Template.childNodes[0].childNodes[0].childNodes[3].ProductClass = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].InventoryUOM = "";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0]= new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].tagName="Inventory";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].InventoryStatus="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].ShipByDate="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[0] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[0].tagName="Receipt";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[0].ReceiptNo="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1] = new Object;
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1].tagName="TagDetail";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1].BatchNo="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1].LotNumber="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1].RevisionNo="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1].TagNumber="";
	postObject.Template.childNodes[0].childNodes[0].childNodes[3].childNodes[0].childNodes[1].ReceiptNo="";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function getItemListApi(itemId, enterpriseCode, uom, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getItemList";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Item";
	postObject.InputXml.ItemID = itemId;
	postObject.InputXml.EnterpriseCode = enterpriseCode;
	postObject.InputXml.UOM = uom;
	postObject.Template = new Object();
	postObject.Template.tagName = "ItemList";
	postObject.Template.TotalNumberOfRecords = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Item";
	postObject.Template.childNodes[0].ItemID = "";
	postObject.Template.childNodes[0].childNodes = [];
	postObject.Template.childNodes[0].childNodes[0] = new Object();
	postObject.Template.childNodes[0].childNodes[0].tagName = "PrimaryInformation";
	postObject.Template.childNodes[0].childNodes[0].SerializedFlag = "";
	postObject.Template.childNodes[0].childNodes[0].NumSecondarySerials = "";
	postObject.Template.childNodes[0].childNodes[1] = new Object;
	postObject.Template.childNodes[0].childNodes[1].tagName = "InventoryParameters";
	postObject.Template.childNodes[0].childNodes[1].IsSerialTracked = "";
	postObject.Template.childNodes[0].childNodes[1].IsFifoTracked = "";
	postObject.Template.childNodes[0].childNodes[1].TimeSensitive = "";
	postObject.Template.childNodes[0].childNodes[1].TagControlFlag = "";
	postObject.Template.childNodes[0].childNodes[2] = new Object;
	postObject.Template.childNodes[0].childNodes[2].tagName = "InventoryTagAttributes";
	postObject.Template.childNodes[0].childNodes[2].BatchNo = "";
	postObject.Template.childNodes[0].childNodes[2].ItemKey = "";
	postObject.Template.childNodes[0].childNodes[2].ItemTagKey = "";
	postObject.Template.childNodes[0].childNodes[2].LotAttribute1 = "";
	postObject.Template.childNodes[0].childNodes[2].LotAttribute2 = "";
	postObject.Template.childNodes[0].childNodes[2].LotAttribute3 = "";
	postObject.Template.childNodes[0].childNodes[2].LotKeyReference = "";
	postObject.Template.childNodes[0].childNodes[2].LotNumber = "";
	postObject.Template.childNodes[0].childNodes[2].ManufacturingDate = "";
	postObject.Template.childNodes[0].childNodes[2].RevisionNo = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function registerTaskCompletionApi(taskId, targetLoc, node, authSer) {
	var postObject = new Object();
	postObject.CommandName = "registerTaskCompletion";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Task";
	postObject.InputXml.OrganizationCode = node;
	postObject.InputXml.TaskId = taskId;
	if (angular.isDefined(targetLoc) && targetLoc.length > 0) {
		postObject.InputXml.TargetLocationId = targetLoc;
	}
	postObject.Template = new Object();
	postObject.Template.tagName = "Task";
	postObject.Template.TaskId = "";
	postObject.Template.TaskStatus = "";
	postObject.Template.TargetLocationId = "";
	postObject.Template.SourceLocationId = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Inventory";
	postObject.Template.childNodes[0].SourceCaseId = "";
	postObject.Template.childNodes[0].SourcePalletId = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function registerTaskCompletionApiObj(regTaskObj, authSer) {
	var postObject = new Object();
	postObject.CommandName = "registerTaskCompletion";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Task";
	postObject.InputXml.OrganizationCode = regTaskObj.OrganizationCode;
	postObject.InputXml.TaskId = regTaskObj.TaskId;
	postObject.InputXml.TargetLocationId = regTaskObj.TargetLocationId;
	postObject.InputXml.HoldReasonCode= regTaskObj.HoldReasonCode;
	postObject.Template = new Object();
	postObject.Template.tagName = "Task";
	postObject.Template.TaskId = "";
	postObject.Template.TaskStatus = "";
	postObject.Template.TargetLocationId = "";
	postObject.Template.SourceLocationId = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Inventory";
	postObject.Template.childNodes[0].SourceCaseId = "";
	postObject.Template.childNodes[0].SourcePalletId = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function getOrganizationHierarchyApi(orgCode, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getOrganizationHierarchy";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Organization";
	postObject.InputXml.OrganizationCode = orgCode;
	postObject.Template = new Object();
	postObject.Template.tagName = "Organization";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Node";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;
};

function registerTaskInProgressApi(lpn, lpnType, taskId, quantity, equipmentId,
		node, authSer) {

	var postObject = new Object();
	postObject.CommandName = "registerTaskInProgress";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Task";
	postObject.InputXml.TaskId = taskId;

	if (angular.isDefined(equipmentId) && equipmentId.length > 0) {
		postObject.InputXml.EquipmentId = equipmentId;
	}
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "Inventory";
	if (angular.isDefined(lpnType) && lpnType.length > 0 && lpnType == "Case") {
		postObject.InputXml.childNodes[0].SourceCaseId = lpn;
	} else if (angular.isDefined(lpnType) && lpnType.length > 0
			&& lpnType == "Pallet") {
		postObject.InputXml.childNodes[0].SourcePalletId = lpn;
	}
	if (angular.isDefined(quantity) && quantity.length > 0) {
		postObject.InputXml.childNodes[0].Quantity = quantity;
	}

	postObject.InputXml.OrganizationCode = node;
	postObject.Template = new Object();
	postObject.Template.tagName = "Task";
	postObject.Template.TaskId = "";
	postObject.Template.TargetLocationId = "";
	postObject.Template.childNodes = [];
	postObject.Template.childNodes[0] = new Object();
	postObject.Template.childNodes[0].tagName = "Inventory";
	postObject.Template.childNodes[0].SourceCaseId = "";
	postObject.Template.childNodes[0].SourcePalletId = "";
	postObject.IsService = "N";
	postObject.Authenticate = authSer;
	return postObject;

};

function getConfigProperties(propertyId, authSer) {
	var postObject = new Object();
	postObject.CommandName = "getConfigProperties";
	postObject.InputXml = new Object();
	postObject.InputXml.tagName = "Config";
	postObject.InputXml.childNodes = [];
	postObject.InputXml.childNodes[0] = new Object();
	postObject.InputXml.childNodes[0].tagName = "Property";
	postObject.InputXml.childNodes[0].Name = propertyId;
	postObject.Template = new Object();
	postObject.IsService = "N";
	postObject.LocalApi = "Y";
	postObject.Authenticate = authSer;
	return postObject;
};

function fnGetScanConfigurationForLocation(locationId, node, authSer,
		invokeApi, $q) {
	var postLocationList = getLocationList(locationId, node, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi
				.async(postLocationList)
				.then(
						function(response) {
							if (response.Locations.TotalNumberOfRecords > 0) {
								var zoneId = response.Locations.Location.ZoneId;
								var postZoneList = getZoneList(zoneId, node,
										authSer);
								return $q(function(resolve, reject) {
									resolve(invokeApi
											.async(postZoneList)
											.then(
													function(response) {
														if (response.Zones.TotalNumberOfRecords > 0) {
															var flag = response.Zones.Zone.ForceLocnScanOnVisit;
															if (flag != "Y") {
																return "N";
															}
															return flag;
														} else {
															response.ErrorDescription = "Invalid Zone Id";
															return response;
														}
													}));
								});
							} else {
								response.ErrorDescription = "Invalid Location Id";
								return response;
							}
						}));
	});
}




function fnGetTranslateBarCode(barcodeData, barcodeType, node, authSer,
		invokeApi, $q) {
	var postTranslateBCList = translateBarCodeApi(barcodeData, barcodeType,
			node, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTranslateBCList).then(function(response) {
			return response;
		}));
	});
}

function fnTranslateBarCode(transObj, authSer, invokeApi, $q) {
	var postTranslateBCList = translateBarCodeDetsApi(transObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTranslateBCList).then(function(response) {
			return response;
		}));
	});
}

function fnGetLocnOverrideWarningFlag(taskType, node, authSer, invokeApi, $q) {
	var postTaskType = getTaskTypeListApi(taskType, node, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi
				.async(postTaskType)
				.then(
						function(response) {

							if (response.TaskTypeList.TotalNumberOfRecords > 0) {
								var flag = response.TaskTypeList.TaskType.SuppressWarnOnLocnOverride;
								if (flag != "Y") {
									flag = "N";
								}
								return flag;
							} else {
								response.ErrorDescription = "Invalid Task Type";
								return response;
							}
						}));
	});
}

function fnCheckEquipmentConfig(taskType, equipmentId, node, authSer,
		invokeApi, $q) {
	var postTaskType = getTaskTypeListApi(taskType, node, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi
				.async(postTaskType)
				.then(
						function(response) {
							if (response.ErrorDescription) {
								return response;
							} else {

								if (response.TaskTypeList.TotalNumberOfRecords > 0) {
									var flag = response.TaskTypeList.TaskType.PerfTaskUsingEquipType;
									var primaryEqType = response.TaskTypeList.TaskType.PrimaryEquipmentType;
									if (flag == "Y") {
										var postEq = getEquipmentListApi(
												equipmentId, node, authSer);
										return $q(function(resolve, reject) {
											resolve(invokeApi
													.async(postEq)
													.then(
															function(response) {
																if (response.Equipments.TotalNumberOfRecords > 0) {
																	var eqType = response.Equipments.Equipment.EquipmentType;
																	if (eqType == primaryEqType) {
																		return "Y";
																	} else {
																		response.ErrorDescription = "Scanned Equipment Id has to be of type "
																				+ primaryEqType;
																		return response;

																	}
																} else {
																	response.ErrorDescription = "Invalid Equipment Id";
																	return response;
																}
															}));
										});
									} else {
										return flag;
									}
								} else {
									response.ErrorDescription = "Invalid Task Type";
									return response;
								}
							}
						}));
	});
}

function fnDetermineLPNType(lpn, node, authSer, invokeApi, $q) {
	var postTranslateBCList = translateBarCodeApi(lpn,
			"LocationOrInventoryContainer", node, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi
				.async(postTranslateBCList)
				.then(
						function(response) {

							if (response.BarCode.Translations.TotalNumberOfRecords > 0) {

								var case_value;
								var pallet_value;
								barCodeContext = response.BarCode.Translations;
								barCodeTranslationContext = barCodeContext.Translation.ContainerContextualInfo;
								if (angular
										.isDefined(barCodeTranslationContext)) {
									if (!angular
											.isDefined(barCodeTranslationContext.CaseId)
											&& !angular
													.isDefined(barCodeTranslationContext.PalletId)) {
										response.ErrorDescription = "Invalid LPN";
										return response;

									} else {
										if (barCodeTranslationContext.CaseId) {

											return "Case";
										} else {

											return "Pallet";

										}

									}
								} else {
									response.ErrorDescription = "Invalid LPN";
									return response;

								}
							} else {
								response.ErrorDescription = "Invalid LPN";
								return response;
							}
						}));
	});
}

function fnGetItemInventoryInfo(itemId, enterpriseCode, uom, node, authSer,
		invokeApi, $q) {
	var postItemInfo = getItemListApi(itemId, enterpriseCode, uom, node,
			authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postItemInfo).then(function(response) {
			if (response.ItemList.TotalNumberOfRecords > 0) {
				return response;
			} else {
				response.ErrorDescription = "Invalid Item Id";
				return response;
			}
		}));
	});
}

function fnRegisterTaskCompletion(taskId, targetLoc, node, authSer, invokeApi,
		$q) {
	var postTaskInfo = registerTaskCompletionApi(taskId, targetLoc, node,
			authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTaskInfo).then(function(response) {
			return response;
		}));
	});
}

function fnRegisterTaskCompletionObj(regTaskObj, authSer, invokeApi, $q) {
	var postTaskInfo = registerTaskCompletionApiObj(regTaskObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTaskInfo).then(function(response) {
			return response;
		}));
	});
}

function fnGetOrganizationHierarchy(orgCode, authSer, invokeApi, $q) {
	var postOrgCode = getOrganizationHierarchyApi(orgCode, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postOrgCode).then(function(response) {
			return response;
		}));
	});
}

function fnRegisterTaskInProgress(lpn, lpnType, taskId, quantity, equipmentId,
		node, authSer, invokeApi, $q) {
	var postTaskProg = registerTaskInProgressApi(lpn, lpnType, taskId,
			quantity, equipmentId, node, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTaskProg).then(function(response) {
			return response;
		}));
	});
}

function fnRegisterTaskInProgressObj(regTaskObj, authSer, invokeApi, $q) {
	var postTaskProg = registerTaskInProgressApiObj(regTaskObj, authSer);

	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTaskProg).then(function(response) {

			return response;
		}));
	});
}

function fnMultiApiRegisterTaskCompletion(taskListResponse, targetLocationId,
		node, userId, authSer, invokeApi, $q) {
	var multiApiObject = new Object();
	multiApiObject.CommandName = "multiApi";
	multiApiObject.InputXml = new Object();
	multiApiObject.InputXml.tagName = "MultiApi";
	multiApiObject.InputXml.childNodes = [];
	multiApiObject.Template = new Object();
	multiApiObject.IsService = "N";
	multiApiObject.Authenticate = authSer;

	for (var k = 0; k < taskListResponse.TaskList.Task.length; k++) {
		var TaskId = taskListResponse.TaskList.Task[k].TaskId;
		var SourceLocationId = taskListResponse.TaskList.Task[k].SourceLocationId;
		var TargetCaseId = taskListResponse.TaskList.Task[k].TargetCaseId;
		var TargetPalletId = taskListResponse.TaskList.Task[k].TargetPalletId;
		multiApiObject.InputXml.childNodes[k] = new Object();
		multiApiObject.InputXml.childNodes[k].tagName = "API";
		multiApiObject.InputXml.childNodes[k].Name = "registerTaskCompletion";
		multiApiObject.InputXml.childNodes[k].childNodes = [];
		multiApiObject.InputXml.childNodes[k].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[k].childNodes[0].tagName = "Input";
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes = [];
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].tagName = "Task";
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].AssignedToUserId = userId;
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].OrganizationCode = node;
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].TargetCaseId = TargetCaseId;
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].TargetLocationId = targetLocationId;
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].SourceLocationId = SourceLocationId;
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].TargetPalletId = TargetPalletId;
		multiApiObject.InputXml.childNodes[k].childNodes[0].childNodes[0].TaskId = ""
				+ TaskId;
	}
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(multiApiObject).then(function(response) {
			return response;
		}));
	});

};

function fnGetConfigProperty(propertyId, authSer, invokeApi, $q, $rootScope) {

	var value;
	if (angular.isDefined($rootScope.ConfigProperties.Config.Property.length)) {
		for (i = 0; i < $rootScope.ConfigProperties.Config.Property.length; i++) {
			if ($rootScope.ConfigProperties.Config.Property[i].Name == propertyId) {
				value = $rootScope.ConfigProperties.Config.Property[i].Value;
			}
		}
	} else {
		if ($rootScope.ConfigProperties.Config.Property.Name == propertyId) {
			value = $rootScope.ConfigProperties.Config.Property.Value;
		}

	}

	if (angular.isDefined(value) && value.length > 0) {
		return $q(function(resolve, reject) {
			resolve(value);
		});
	} else {
		var postCfgProp = getConfigProperties(propertyId, authSer);
		return $q(function(resolve, reject) {
			resolve(invokeApi.async(postCfgProp).then(function(cfgPropRes) {
				return cfgPropRes.Config.Property.Value;
			}));
		});
	}
}

function fnGetTaskList(taskObj, authSer, invokeApi, $q) {
	var postTaskInfo = getTaskListApi(taskObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postTaskInfo).then(function(response) {
			return response;
		}));
	});
}

function fnGetShipmentList(shpObj, authSer, invokeApi, $q) {
	var postShpInfo = getShipmentListApi(shpObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postShpInfo).then(function(response) {
			return response;
		}));
	});
}

function fnGetReceiptList(recObj, authSer, invokeApi, $q) {
	var postRecInfo = getReceiptListApi(recObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postRecInfo).then(function(response) {
			return response;
		}));
	});
}

function fnGetNodeItemDetails(niObj, authSer, invokeApi, $q) {
	var postRecInfo = getNodeItemDetailsApi(niObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postRecInfo).then(function(response) {
			return response;
		}));
	});
}


function fnGetExecutionExceptionList(exeObj, authSer, invokeApi, $q) {
	var postExeInfo = getExecutionExceptionListApi(exeObj, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi.async(postExeInfo).then(function(response) {
			return response;
		}));
	});
}

function fnGetNextTask(ntObj, authSer, invokeApi, $q, $cookieStore) {

	var taskObj1 = {
		TaskStatus : "1100",
		ActivityGroupId : ntObj.ActivityGroupId,
		Node : ntObj.OrganizationCode,

	};
	var postTaskInfo = getTaskListApi(taskObj1, authSer);
	return $q(function(resolve, reject) {
		resolve(invokeApi
				.async(postTaskInfo)
				.then(
						function(response) {
							if (response.TaskList.TotalNumberOfRecords > 0) {
								
								var postTaskInfo = getNextTaskApi(ntObj,
										authSer);
								return $q(function(resolve, reject) {
									resolve(invokeApi.async(postTaskInfo).then(
											function(taskresponse) {

												return taskresponse;

											}));
								});
							} else {
								var taskObj = {
									TaskStatus : "1200",
									ActivityGroupId : ntObj.ActivityGroupId,
									Node : ntObj.OrganizationCode,
									AssignedToUserId : ntObj.UserId
								};
								var postTaskInfo1 = getTaskListApi(taskObj,
										authSer);
								return $q(function(resolve, reject) {
									resolve(invokeApi
											.async(postTaskInfo1)
											.then(
													function(response) {
														if (response.TaskList.TotalNumberOfRecords > 0) {
															
															var postTaskInfo = getNextTaskApi(
																	ntObj,
																	authSer);
															return $q(function(
																	resolve,
																	reject) {
																resolve(invokeApi
																		.async(
																				postTaskInfo)
																		.then(
																				function(
																						taskresponse) {

																					return taskresponse;

																				}));
															});
														} else {
															return response;
														}

													}));
								});
							}

						}));
	});

}

var configReader = angular.module('sterlingConfigReaderProvider', []);

configReader.service('sterlingConfigService', function($rootScope,
		$cookieStore, AuthenticationService, invokeApi, $q) {

	this.getTranslateBarCodeData = function(barcodeData, barcodeType) {
		var translateData = fnGetTranslateBarCode(barcodeData, barcodeType,
				$cookieStore.get('globals').OrganizationCode,
				AuthenticationService.getAuth(), invokeApi, $q);
		return translateData;

	};

	this.getScanConfigurationForLocation = function(locationId) {
		var flag = fnGetScanConfigurationForLocation(locationId, $cookieStore
				.get('globals').OrganizationCode, AuthenticationService
				.getAuth(), invokeApi, $q);
		return flag;

	};

	this.getLocnOverrideWarningFlag = function(taskType) {
		var warnFlag = fnGetLocnOverrideWarningFlag(taskType, $cookieStore
				.get('globals').OrganizationCode, AuthenticationService
				.getAuth(), invokeApi, $q);
		return warnFlag;

	};

	this.checkEquipmentConfig = function(taskType, equipmentId) {
		var configEq = fnCheckEquipmentConfig(taskType, equipmentId,
				$cookieStore.get('globals').OrganizationCode,
				AuthenticationService.getAuth(), invokeApi, $q);
		return configEq;

	};

	this.determineLPNType = function(lpn) {
		var lpnType = fnDetermineLPNType(lpn,
				$cookieStore.get('globals').OrganizationCode,
				AuthenticationService.getAuth(), invokeApi, $q);
		return lpnType;

	};

	this.getItemInventoryInfo = function(itemId, enterpriseCode, uom) {
		var invInfo = fnGetItemInventoryInfo(itemId, enterpriseCode, uom,
				$cookieStore.get('globals').OrganizationCode,
				AuthenticationService.getAuth(), invokeApi, $q);
		return invInfo;

	};

	this.registerTaskCompletion = function(taskId, targetLoc) {
		var taskRes = fnRegisterTaskCompletion(taskId, targetLoc, $cookieStore
				.get('globals').OrganizationCode, AuthenticationService
				.getAuth(), invokeApi, $q);
		return taskRes;

	};

	this.getOrganizationHierarchy = function(orgCode) {
		var orgDets = fnGetOrganizationHierarchy(orgCode, AuthenticationService
				.getAuth(), invokeApi, $q);
		return orgDets;

	};

	this.multiApiRegisterTaskCompletion = function(taskListResponse,
			targetLocationId) {
		var regTaskComp = fnMultiApiRegisterTaskCompletion(taskListResponse,
				targetLocationId, $cookieStore.get('globals').OrganizationCode,
				$cookieStore.get('globals').currentUserId,
				AuthenticationService.getAuth(), invokeApi, $q);
		return regTaskComp;

	};

	this.registerTaskInProgress = function(lpn, lpnType, taskId, quantity,
			equipmentId) {
		var regTaskComp = fnRegisterTaskInProgress(lpn, lpnType, taskId,
				quantity, equipmentId,
				$cookieStore.get('globals').OrganizationCode,
				AuthenticationService.getAuth(), invokeApi, $q);
		return regTaskComp;

	};

	this.getConfigProperty = function(propertyId) {
		var config = fnGetConfigProperty(propertyId, AuthenticationService
				.getAuth(), invokeApi, $q, $rootScope);
		return config;

	};

	this.translateBarCode = function(transObj) {

		var transRes = fnTranslateBarCode(transObj, AuthenticationService
				.getAuth(), invokeApi, $q);
		return transRes;

	};

	this.getTaskList = function(taskObj) {

		var taskRes = fnGetTaskList(taskObj, AuthenticationService.getAuth(),
				invokeApi, $q);
		return taskRes;

	};

	this.getShipmentList = function(shpObj) {

		var shpRes = fnGetShipmentList(shpObj, AuthenticationService.getAuth(),
				invokeApi, $q);
		return shpRes;

	};

	this.getReceiptList = function(recObj) {

		var recRes = fnGetReceiptList(recObj, AuthenticationService.getAuth(),
				invokeApi, $q);
		return recRes;

	};

	this.getNodeItemDetails = function(niObj) {

		var niRes = fnGetNodeItemDetails(niObj,
				AuthenticationService.getAuth(), invokeApi, $q);
		return niRes;

	};

	this.getNextTask = function(ntObj) {

		var ntRes = fnGetNextTask(ntObj, AuthenticationService.getAuth(),
				invokeApi, $q, $cookieStore);
		return ntRes;

	};

	this.registerTaskInProgressObj = function(regTaskObj) {
		var regTaskComp = fnRegisterTaskInProgressObj(regTaskObj,
				AuthenticationService.getAuth(), invokeApi, $q);
		return regTaskComp;

	};

	this.registerTaskCompletionObj = function(regTaskObj) {
		var taskRes = fnRegisterTaskCompletionObj(regTaskObj,
				AuthenticationService.getAuth(), invokeApi, $q);
		return taskRes;

	};
	
	this.getExecutionExceptionList = function(exeObj) {
		var exeRes = fnGetExecutionExceptionList(exeObj,
				AuthenticationService.getAuth(), invokeApi, $q);
		return exeRes;

	};

});
