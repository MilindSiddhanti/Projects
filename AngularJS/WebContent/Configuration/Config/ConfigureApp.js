define(['common/ctrljs/app'], function (myApp) {

myApp.config(function($stateProvider, $urlRouterProvider, routeStateLoaderProvider) {
    
	var route = routeStateLoaderProvider.route;
	
    $stateProvider
     
       .state('Configuration.Location',{
    	    url: "/Location",
    	    views: {
    	    	'': {
    	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
    	    		controller: 'ConfigController',
    	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
    	    	},
    	    	'criteria@Configuration': {
        	    		templateUrl: 'Configuration/View/Location/Configmain.html',
        	    		controller: 'configControllerMain',
        	    		resolve   :  route.resolve('Configuration/Controller/Location/configControllerMain')
    	    	},
    	    },
         })
         
         .state('Configuration.LocationList',{
    	    url: "/LocationList",
    	    views: {
    	    	'': {
    	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
    	    		controller: 'ConfigController',	
    	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
    	    	},
    	    	'criteria@Configuration': {
        	    		templateUrl: 'Configuration/View/Location/LocationList.html',
        	    		controller: 'LocationListController',
        	    		resolve   :  route.resolve('Configuration/Controller/Location/LocationListController')
    	    },
    	    },
         })
         
    .state('Configuration.AddLocation',{
	    url: "/AddLocation",
	    views: {
	    	'': {
	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
	    		controller: 'ConfigController',	
	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
	    	},
	    	'criteria@Configuration': {
    	    		templateUrl: 'Configuration/View/Location/AddLocation.html',
    	    		controller: 'AddLocationController',
    	    		resolve   :  route.resolve('Configuration/Controller/Location/AddLocationController')
	    },
	    },
     })
    
    .state('Configuration.LocationDetails',{
	    url: "/LocationDetails",
	    views: {
	    	'': {
	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
	    		controller: 'ConfigController',	
	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
	    	},
	    	'criteria@Configuration': {
    	    		templateUrl: 'Configuration/View/Location/LocationDetails.html',
    	    		controller: 'LocationDetailsController',
    	    		resolve   :  route.resolve('Configuration/Controller/Location/LocationDetailsController')
	    },
	    },
     })
    
    .state('Configuration.ModifyLocation',{
	    url: "/ModifyLocation",
	    views: {
	    	'': {
	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
	    		controller: 'ConfigController',	
	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
	    	},
	    	'criteria@Configuration': {
    	    		templateUrl: 'Configuration/View/Location/ModifyLocation.html',
    	    		controller: 'ModifyLocationController',
    	    		resolve   :  route.resolve('Configuration/Controller/Location/ModifyLocationController')
	    },
	    },
     })
      .state('Configuration.LocationSize',{
    	    url: "/LocationSize",
    	    views: {
    	    	'': {
    	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
    	    		controller: 'ConfigController',
    	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
    	    	},
    	    	'criteria@Configuration': {
        	    		templateUrl: 'Configuration/View/LocationSize/LocationSizeHome.html',
        	    		controller: 'LocationSizeControllerHome',
        	    		resolve   :  route.resolve('Configuration/Controller/LocationSize/LocationSizeControllerHome')
    	    	},
    	    },
         })
      .state('Configuration.LocationSizeList',{
    	    url: "/LocationSizeList",
    	    views: {
    	    	'': {
    	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
    	    		controller: 'ConfigController',	
    	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
    	    	},
    	    	'criteria@Configuration': {
        	    		templateUrl: 'Configuration/View/LocationSize/LocationSizeList.html',
        	    		controller: 'LocationSizeListController',
        	    		resolve   :  route.resolve('Configuration/Controller/LocationSize/LocationSizeListController')
    	    },
    	    },
         })
         .state('Configuration.AddLocationSize',{
	    url: "/AddLocationSize",
	    views: {
	    	'': {
	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
	    		controller: 'ConfigController',	
	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
	    	},
	    	'criteria@Configuration': {
    	    		templateUrl: 'Configuration/View/LocationSize/AddLocationSize.html',
    	    		controller: 'AddLocationSizeController',
    	    		resolve   :  route.resolve('Configuration/Controller/LocationSize/AddLocationSizeController')
	    },
	    },
     })
     .state('Configuration.LocationSizeDetails',{
	    url: "/LocationSizeDetails",
	    views: {
	    	'': {
	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
	    		controller: 'ConfigController',	
	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
	    	},
	    	'criteria@Configuration': {
    	    		templateUrl: 'Configuration/View/LocationSize/LocationSizeDetails.html',
    	    		controller: 'LocationSizeDetailsController',
    	    		resolve   :  route.resolve('Configuration/Controller/LocationSize/LocationSizeDetailsController')
	    },
	    },
     })
      .state('Configuration.ModifyLocationSize',{
	    url: "/ModifyLocationSize",
	    views: {
	    	'': {
	    		templateUrl: 'Configuration/Config/ConfigFrame.html',
	    		controller: 'ConfigController',	
	    		resolve   :  route.resolve('Configuration/Controller/ConfigController')
	    	},
	    	'criteria@Configuration': {
    	    		templateUrl: 'Configuration/View/LocationSize/ModifyLocationSize.html',
    	    		controller: 'ModifyLocationSizeController',
    	    		resolve   :  route.resolve('Configuration/Controller/LocationSize/ModifyLocationSizeController')
	    },
	    },
     })
     ; 
       
});

});