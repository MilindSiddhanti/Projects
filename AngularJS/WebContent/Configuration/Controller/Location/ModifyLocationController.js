define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('ModifyLocationController', function($timeout,$scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){

		var orgCode = $cookieStore.get('globals').OrganizationCode;
		var selectedContext = $rootScope.selected;
		$scope.Location = selectedContext.LocationId;
		$scope.Loc_Type = selectedContext.LocationType;
		$scope.Zone = selectedContext.ZoneId;
		$scope.AisleNumber = selectedContext.AisleNumber;
		$scope.BayNumber = selectedContext.BayNumber;
		$scope.LevelNumber = selectedContext.LevelNumber;
		$scope.LocationSizeCode = selectedContext.LocationSizeCode;
		$scope.FreezeMoveIn = selectedContext.FreezeMoveIn;
		$scope.FreezeMoveOut = selectedContext.FreezeMoveOut;
		$scope.MoveInSeqNo = selectedContext.MoveInSeqNo;
		$scope.MoveOutSeqNo = selectedContext.MoveOutSeqNo;
		$scope.VelocityCode = selectedContext.VelocityCode;
		
				console.log("===>"+$rootScope.ZoneListContext);
			
		
		/************ modifyLocation API***********/
		function modifyLocation(LocationId,LocationType,ZoneId,AisleNumber,BayNumber,LevelNumber,LocationSizeCode,FreezeMoveIn,FreezeMoveOut,MoveInSeqNo,MoveOutSeqNo,VelocityCode) {
			var postObject = new Object();
			postObject.CommandName = "modifyLocation";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Location";
			postObject.InputXml.LocationId = LocationId;
			postObject.InputXml.LocationType = LocationType;
			postObject.InputXml.Node = orgCode;
			postObject.InputXml.ZoneId = ZoneId;
			postObject.InputXml.AisleNumber = AisleNumber;
			postObject.InputXml.BayNumber = BayNumber;
			postObject.InputXml.LevelNumber = LevelNumber;
			postObject.InputXml.LocationSizeCode = LocationSizeCode;
			postObject.InputXml.FreezeMoveIn = FreezeMoveIn;
			postObject.InputXml.FreezeMoveOut = FreezeMoveOut;
			postObject.InputXml.MoveInSeqNo = MoveInSeqNo;
			postObject.InputXml.MoveOutSeqNo = MoveOutSeqNo;
			postObject.InputXml.VelocityCode = VelocityCode;
			postObject.Template = new Object();
			postObject.IsService = "N";
			postObject.Authenticate = AuthenticationService
			.getAuth();
			return postObject;
		}
		
		$scope.Cancel = function()
		{
			$location.path('/Configuration/LocationDetails');
		}
		$scope.Modify = function()
		{
			var Location = $scope.Location;
			var Loc_Type = $scope.locationType;
			var Zone = $scope.Zone;
			var AisleNumber = $scope.AisleNumber;
			var BayNumber = $scope.BayNumber;
			var LevelNumber = $scope.LevelNumber;
			var LocationSizeCode = $scope.LocationSizeCode;
			var FreezeMoveIn = $scope.FreezeMoveIn;
			var FreezeMoveOut =  $scope.FreezeMoveOut;
			var MoveInSeqNo = $scope.MoveInSeqNo;
			var MoveOutSeqNo = $scope.MoveOutSeqNo;
			var VelocityCode = $scope.VelocityCode;
			var EditLocation = modifyLocation(Location, Loc_Type, Zone,AisleNumber,BayNumber,LevelNumber,LocationSizeCode,FreezeMoveIn,FreezeMoveOut,MoveInSeqNo,MoveOutSeqNo,VelocityCode);
			invokeApi.async(EditLocation).then(
					function(response) {
						$scope.postResponse = response;
						$rootScope.selected = $scope.postResponse.Location;
						if ($scope.postResponse.ErrorDescription) {
							$scope.ErrorMessage = $scope.postResponse.ErrorDescription;
						} else {
							$scope.ErrorMessage = "";
							$scope.InfoMessage = "Location Modified";
						
						$timeout(function() {
							$location.path('/Configuration/LocationDetails').search(
													{
														key : value
													});
								},
								5000);		
						}
					});
		}

	});
});