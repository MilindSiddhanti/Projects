define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('LocationListController', function($timeout,$scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	$scope.noOfRecordsReceived = $rootScope.noOfRecordsReceived;
	var count = $scope.noOfRecordsReceived;
	$scope.LocationListContext = $rootScope.LocationListContext;
	$scope.LocationList = $scope.LocationListContext;
	 $scope.setSelected = function() {
	        $scope.selected = this.list;
	        console.log($scope.selected);
	        $rootScope.selected = $scope.selected;
	        $location.path('/Configuration/LocationDetails');
	    };
	    
	    $scope.itemsPerPage = 10;
	    $scope.currentPage = 0;
	   console.log($scope.LocationListContext);
	   console.log($scope.LocationListContext.length);
	   if(count == 1){
	
		   $scope.LocationList = [];
		   
	   $scope.LocationList.push({
		   AisleNumber: $scope.LocationListContext.AisleNumber,
			BayNumber: $scope.LocationListContext.BayNumber,
		   FreezeMoveIn: $scope.LocationListContext.FreezeMoveIn,
		   FreezeMoveOut: $scope.LocationListContext.FreezeMoveOut,
		   LevelNumber: $scope.LocationListContext.LevelNumber,
		   LocationId: $scope.LocationListContext.LocationId,
		   LocationSizeCode: $scope.LocationListContext.LocationSizeCode,
		   LocationType: $scope.LocationListContext.LocationType,
		   MoveInSeqNo: $scope.LocationListContext.MoveInSeqNo,
		   MoveOutSeqNo: $scope.LocationListContext.MoveOutSeqNo,
		   Node: $scope.LocationListContext.Node,
		   OutStagingLocationId: $scope.LocationListContext.OutStagingLocationId,
		   VelocityCode: $scope.LocationListContext.VelocityCode,
		   ZoneId: $scope.LocationListContext.ZoneId
	   });
	   console.log("LocationList.push====>"+$scope.LocationList);
	   }
	   else
		   {
		   $scope.LocationList = $scope.LocationListContext;
		   }
		   
	    $scope.range = function() {
	      var rangeSize = Math.ceil($scope.LocationList.length/$scope.itemsPerPage);
	      var ret = [];
	      var start;

	      start = $scope.currentPage;
	      if ( start > $scope.pageCount()-rangeSize ) {
	        start = $scope.pageCount()-rangeSize+1;
	      }

	      for (var i=start; i<start+rangeSize; i++) {
	        ret.push(i);
	      }
	      return ret;
	    };

	    $scope.prevPage = function() {
	      if ($scope.currentPage > 0) {
	        $scope.currentPage--;
	      }
	    };

	    $scope.prevPageDisabled = function() {
	      return $scope.currentPage === 0 ? "disabled" : "";
	    };

	    $scope.pageCount = function() {
	      return Math.ceil($scope.LocationList.length/$scope.itemsPerPage)-1;
	    };

	    $scope.nextPage = function() {
	      if ($scope.currentPage < $scope.pageCount()) {
	        $scope.currentPage++;
	      }
	    };

	    $scope.nextPageDisabled = function() {
	      return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	    };

	    $scope.setPage = function(n) {
	      $scope.currentPage = n;
	    };

	
	
});
});
