define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('configControllerMain', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	console.log("main my control");
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	
	/******* Api Input for getLocationList  ********/
	function getLocationList(LocationId,LocationType,ZoneId) {
		var postObject = new Object();
		postObject.CommandName = "getLocationList";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Location";
		postObject.InputXml.LocationId = LocationId;
		postObject.InputXml.LocationType = LocationType;
		postObject.InputXml.Node = orgCode;
		postObject.InputXml.ZoneId = ZoneId;
		postObject.Template = new Object();
		postObject.Template.tagName = "Locations";
		postObject.Template.TotalNumberOfRecords = "";
		postObject.Template.childNodes = [];
		postObject.Template.childNodes[0] = new Object();
		postObject.Template.childNodes[0].tagName = "Location";
		postObject.IsService = "N";
		postObject.Authenticate = AuthenticationService
				.getAuth();
		return postObject;
	}
	
	
	$scope.Add = function()
	{
		$location.path('/Configuration/AddLocation');
	};
	$scope.Search = function()
	{
		var Location = $scope.Configmain_locationID;
		var Loc_Type = $scope.Configmain_locationType;
		var Zone = $scope.Configmain_ZoneID;
		var postLocationList = getLocationList(Location, Loc_Type, Zone);
		invokeApi.async(postLocationList).then(
						function(response) {
							$scope.postResponse = response;
							var noOfRecordsReceived = $scope.postResponse.Locations.TotalNumberOfRecords;
							console.log(noOfRecordsReceived);
							var LocationListContext = $scope.postResponse.Locations.Location;
							$rootScope.LocationListContext = LocationListContext;
							$rootScope.noOfRecordsReceived = noOfRecordsReceived;
						//	while(noOfRecordsReceived>0)
							//	{
								
								//noOfRecordsReceived--;
								//}
							if (noOfRecordsReceived == 0) {
								$scope.ErrorMessage = "No Records Exist";
							} else {
								$scope.ErrorMessage = "";
								
								$location.path('/Configuration/LocationList');
							}
						});
		
	};
});
});