define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('LocationDetailsController', function($timeout,$scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	
	       var selectedContext = $rootScope.selected;
	       $scope.Location = selectedContext.LocationId;
	       $scope.Loc_Type = selectedContext.LocationType;
	       $scope.Zone = selectedContext.ZoneId;
	       $scope.AisleNumber = selectedContext.AisleNumber;
	       $scope.BayNumber = selectedContext.BayNumber;
	       $scope.LevelNumber = selectedContext.LevelNumber;
	       $scope.LocationSizeCode = selectedContext.LocationSizeCode;
	       $scope.FreezeMoveIn = selectedContext.FreezeMoveIn;
	       $scope.FreezeMoveOut = selectedContext.FreezeMoveOut;
	       $scope.MoveInSeqNo = selectedContext.MoveInSeqNo;
	       $scope.MoveOutSeqNo = selectedContext.MoveOutSeqNo;
	       $scope.VelocityCode = selectedContext.VelocityCode;
	       
	       /************ getZoneList API***********/
			function getZoneList() {
				var postObject = new Object();
				postObject.CommandName = "getZoneList";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "Zone";
				postObject.InputXml.Node = orgCode;
				postObject.Template = new Object();
				postObject.Template.tagName = "Zones";
				postObject.Template.childNodes = [];
				postObject.Template.childNodes[0] = new Object();
				postObject.Template.childNodes[0].tagName = "Zone";
				postObject.IsService = "N";
				postObject.Authenticate = AuthenticationService
				.getAuth();
				return postObject;
			}
	       
	       var getZoneList = getZoneList();
			invokeApi.async(getZoneList).then(
					function(response) {
						$scope.postResponse = response;
						var ZoneListContext = $scope.postResponse.Zones.Zone;
						$rootScope.ZoneListContext = ZoneListContext;
						console.log("===>"+ZoneListContext);
					});
			
			
	       /******* Api Input for deleteLocation  ********/
	   	function deleteLocation(LocationId) {
	   		var postObject = new Object();
	   		postObject.CommandName = "deleteLocation";
	   		postObject.InputXml = new Object();
	   		postObject.InputXml.tagName = "Location";
	   		postObject.InputXml.LocationId = LocationId;
	   		postObject.InputXml.Node = orgCode;
	   		postObject.Template = new Object();
	   		postObject.IsService = "N";
	   		postObject.Authenticate = AuthenticationService
	   				.getAuth();
	   		return postObject;
	   	}
	   		$scope.Delete = function()
	   		{	
	   			var Location = selectedContext.LocationId;
	   			var DeleteLocation = deleteLocation(Location);
	   			invokeApi.async(DeleteLocation).then(
	   							function(response) {
	   								$scope.postResponse = response;
	   								if ($scope.postResponse.ErrorDescription) {
	   									$scope.ErrorMessage = $scope.postResponse.ErrorDescription;
	   								} else {
	   									$scope.ErrorMessage = "";
	   									$scope.InfoMessage = "Location Deleted";
	   								
	   								$timeout(function() {
	   									$location.path('/Configuration/Location').search(
	   															{
	   																key : value
	   															});
	   										},
	   										5000);		
	   								}
	   								
	   								
	   								
	   							});
	   		}
	   		
	   		$scope.Modify = function(){
	   			$location.path('/Configuration/ModifyLocation');
	   		}
});
});