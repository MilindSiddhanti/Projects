define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('AddLocationController', function($scope,$timeout, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	/************ createLocation API***********/
	function createLocation(LocationId,LocationType,ZoneId,AisleNumber,BayNumber,LevelNumber,LocationSizeCode) {
		var postObject = new Object();
		postObject.CommandName = "createLocation";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Location";
		postObject.InputXml.LocationId = LocationId;
		postObject.InputXml.LocationType = LocationType;
		postObject.InputXml.Node = orgCode;
		postObject.InputXml.ZoneId = ZoneId;
		postObject.InputXml.AisleNumber = AisleNumber;
		postObject.InputXml.BayNumber = BayNumber;
		postObject.InputXml.LevelNumber = LevelNumber;
		postObject.InputXml.LocationSizeCode = LocationSizeCode;
		postObject.Template = new Object();
		postObject.IsService = "N";
		postObject.Authenticate = AuthenticationService
				.getAuth();
		return postObject;
	}
	$scope.selected = undefined;
	$scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut',
	                 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 
	                 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 
	                 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 
	                 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio',
	                 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota',
	                 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
	  // Any function returning a promise object can be used to load values asynchronously
	  $scope.getLocation = function(val) {
	    return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
	      params: {
	        address: val,
	        sensor: false
	      }
	    }).then(function(response){
	      return response.data.results.map(function(item){
	        return item.formatted_address;
	      });
	    });
	  };
	$scope.Cancel = function()
	{
		$location.path('/Configuration/Location')
	}
	$scope.Add = function()
	{
		var Location = $scope.AddLocation_locationID;
		var Loc_Type = $scope.AddLocation_locationType;
		var Zone = $scope.AddLocation_ZoneID;
		var AisleNumber = $scope.AddLocation_AisleNumber;
		var BayNumber = $scope.AddLocation_BayNumber;
		var LevelNumber = $scope.AddLocation_LevelNumber;
		var LocationSizeCode = $scope.AddLocation_LocationSizeCode;
		var AddLocation = createLocation(Location, Loc_Type, Zone,AisleNumber,BayNumber,LevelNumber,LocationSizeCode);
		invokeApi.async(AddLocation).then(
						function(response) {
							$scope.postResponse = response;
							if ($scope.postResponse.ErrorDescription) {
									$scope.ErrorMessage = $scope.postResponse.ErrorDescription;
								} else {
									$scope.ErrorMessage = "";
									$scope.InfoMessage = "Location Added";
								
								$timeout(function() {
									$location.path('/Configuration/Location').search(
															{
																key : value
															});
										},
										5000);		
								}						
						});
	}
	
});
});