define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('LocationSizeControllerHome', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	
	/******* Api Input for getLocationList  ********/
	function getLocationSizeList(locSizeCode) {
		var postObject = new Object();
		postObject.CommandName = "GenericEntityApiInvoker";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Root";
		postObject.InputXml.ApiName = "listLocationSize";
		postObject.InputXml.childNodes = [];
		postObject.InputXml.childNodes[0] = new Object();
		postObject.InputXml.childNodes[0].tagName = "Input";
		postObject.InputXml.childNodes[0].childNodes = [];
		postObject.InputXml.childNodes[0].childNodes[0] = new Object();
		postObject.InputXml.childNodes[0].childNodes[0].tagName = "LocationSize";
		postObject.InputXml.childNodes[0].childNodes[0].Node = $cookieStore.get('globals').OrganizationCode;
		if(angular.isDefined(locSizeCode)){
			postObject.InputXml.childNodes[0].childNodes[0].LocationSizeCode = locSizeCode;
		}
		postObject.InputXml.childNodes[1] = new Object();
		postObject.InputXml.childNodes[1].tagName = "Template";
		postObject.InputXml.childNodes[1].childNodes = [];
		postObject.InputXml.childNodes[1].childNodes[0] = new Object();
		postObject.InputXml.childNodes[1].childNodes[0].tagName = "LocationSizes";
		postObject.InputXml.childNodes[1].childNodes[0].TotalNumberOfRecords = "";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes = [];
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0] = new Object();
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].tagName = "LocationSize";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].LocationSizeCode="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].Description="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].IsInfiniteCapacity="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].DimensionUom="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].CapacityTracked="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].IsSingleDeep="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].Width="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].Length="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].Height="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].Volume="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].MaxWeight="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].VolumeUom="";
		postObject.InputXml.childNodes[1].childNodes[0].childNodes[0].WeightUom="";
		postObject.Template = new Object();
		postObject.IsService = "Y";
		postObject.Authenticate = AuthenticationService
				.getAuth();
		return postObject;
	}
	
	
	$scope.AddLocationSize = function()
	{
		$rootScope.ToAddlocSizeCode=$scope.frmLocationSizeHome.locSizeCode;
		$location.path('/Configuration/AddLocationSize');
	};
	$scope.SearchLocationSize = function()
	{
		var locSizeCode=$scope.frmLocationSizeHome.locSizeCode;
		var postLocationSizeList = getLocationSizeList(locSizeCode);
		invokeApi.async(postLocationSizeList).then(
						function(response) {
							$scope.postResponse = response;
							var noOfRecordsReceived = $scope.postResponse.LocationSizes.TotalNumberOfRecords;
							
							var LocationListContext = $scope.postResponse.LocationSizes.LocationSize;
							$rootScope.LocationSizeListContext = LocationListContext;
							$rootScope.noOfRecordsLocSCReceived = noOfRecordsReceived;
							if(noOfRecordsReceived==1){
								$scope.locationSize=$scope.postResponse.LocationSizes.LocationSize;
								if($scope.locationSize.IsInfiniteCapacity=="Y"){
									$scope.locationSize.IsInfiniteCapacity="N";
								}
								else{
									$scope.locationSize.IsInfiniteCapacity="Y";
								}
							}else{
								for(var j=0; j < noOfRecordsReceived; j++){
									$scope.locationSize=$scope.postResponse.LocationSizes.LocationSize[j];
									if($scope.locationSize.IsInfiniteCapacity=="Y"){
										$scope.locationSize.IsInfiniteCapacity="N";
									}
									else{
										$scope.locationSize.IsInfiniteCapacity="Y";
									}
								}	
							}
							
							if (noOfRecordsReceived == 0) {
								$scope.ErrorMessage = "No Records Exist";
							} else {
								$scope.ErrorMessage = "";
								$location.path('/Configuration/LocationSizeList');
							}
						});
		
	};
});
});