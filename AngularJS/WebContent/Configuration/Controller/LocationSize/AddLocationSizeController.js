define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('AddLocationSizeController', function($scope, $rootScope,$timeout, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	
	/************ createLocationSize API***********/
	function createLocationSize(locationSizeCode, locSizeCodeDesc, capacityTracked,singleDeep,locSizeCodeLength,locSizeCodeWidth,locSizeCodeHeight,locSizeCodeMaxWeight) {
		var postObject = new Object();
		postObject.CommandName = "GenericEntityApiInvoker";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Root";
		postObject.InputXml.ApiName = "createLocationSize";
		postObject.InputXml.childNodes = [];
		postObject.InputXml.childNodes[0] = new Object();
		postObject.InputXml.childNodes[0].tagName = "Input";
		postObject.InputXml.childNodes[0].childNodes = [];
		postObject.InputXml.childNodes[0].childNodes[0] = new Object();
		postObject.InputXml.childNodes[0].childNodes[0].tagName = "LocationSize";
		postObject.InputXml.childNodes[0].childNodes[0].Node = $cookieStore.get('globals').OrganizationCode;
		postObject.InputXml.childNodes[0].childNodes[0].LocationSizeCode = locationSizeCode;
		postObject.InputXml.childNodes[0].childNodes[0].Description = locSizeCodeDesc;
		postObject.InputXml.childNodes[0].childNodes[0].CapacityTracked=capacityTracked;
		postObject.InputXml.childNodes[0].childNodes[0].IsSingleDeep=singleDeep;
		var infiniteCap;
		if(capacityTracked=="Y"){
			infiniteCap="N";
		}else{
			infiniteCap="Y";
		}
		postObject.InputXml.childNodes[0].childNodes[0].IsInfiniteCapacity=infiniteCap;
		postObject.InputXml.childNodes[0].childNodes[0].Length=locSizeCodeLength;
		postObject.InputXml.childNodes[0].childNodes[0].Width=locSizeCodeWidth;
		postObject.InputXml.childNodes[0].childNodes[0].Height=locSizeCodeHeight;
		postObject.InputXml.childNodes[0].childNodes[0].MaxWeight=locSizeCodeMaxWeight;
		postObject.Template = new Object();
		postObject.IsService = "Y";
		postObject.Authenticate = AuthenticationService
				.getAuth();
		return postObject;
	}
	
	if(angular.isDefined($rootScope.ToAddlocSizeCode)){
		
		$scope.locationSizeCode=$rootScope.ToAddlocSizeCode;
	}
	
	processContoller.InputController = function(scannedVariable){
		console.log("scannedVariable "+scannedVariable);
		if(scannedVariable=="cls_locSizeCode"){
			if(!angular.isDefined($scope.frmLocationSizeAdd.locSizeCode)){
				$scope.ErrorMessage="Enter valid Location size code";
			}
		}
		if(scannedVariable=="cls_locSizeCodeDesc"){
			if(!angular.isDefined($scope.frmLocationSizeAdd.locSizeCodeDesc)){
				$scope.ErrorMessage="Enter valid Description";
			}
		}
		if(scannedVariable=="cls_length"){
			if(!angular.isDefined($scope.frmLocationSizeAdd.length)){
				$scope.ErrorMessage="Enter valid Length";
			}else{
				if($scope.frmLocationSizeAdd.length<=0){
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}
			}
		}
		if(scannedVariable=="cls_width"){
			if(!angular.isDefined($scope.frmLocationSizeAdd.width)){
				$scope.ErrorMessage="Enter valid Width";
			}else{
				if($scope.frmLocationSizeAdd.width<=0){
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}
			}
		}
		if(scannedVariable=="cls_height"){
			if(!angular.isDefined($scope.frmLocationSizeAdd.height)){
				$scope.ErrorMessage="Enter valid Height";
			}else{
				if($scope.frmLocationSizeAdd.height<=0){
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}else{
					$scope.frmLocationSizeAdd_volume=$scope.frmLocationSizeAdd.height*$scope.frmLocationSizeAdd.width*$scope.frmLocationSizeAdd.length;
					
				}
			}
		}
		if(scannedVariable=="cls_maxWeight"){
			if(!angular.isDefined($scope.frmLocationSizeAdd.maxWeight)){
				$scope.ErrorMessage="Enter valid Max Weight";
			}else{
				if($scope.frmLocationSizeAdd.maxWeight<=0){
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}
			}
		}
	}
	
	$scope.disableButtons = function()
	{
		if(!$scope.frmLocationSizeAdd.capacityTracked){
			$scope.frmLocationSizeAdd.length=0;
			$scope.frmLocationSizeAdd.width=0;
			$scope.frmLocationSizeAdd.height=0;
			$scope.frmLocationSizeAdd.maxWeight=0;
			$scope.frmLocationSizeAdd.singleDeep=false;
		}
		
	}

	$scope.AddLocationSize = function()
	{
		var iFlag=0;
		var locationSizeCode;
		var locSizeCodeDesc;
		var capacityTracked;
		var locSizeCodeWidth;
		var locSizeCodeLength;
		var locSizeCodeHeight;
		var locSizeCodeMaxWeight;
		if(!angular.isDefined($scope.frmLocationSizeAdd.locSizeCode)){
			iFlag=1;
			$scope.ErrorMessage="Enter valid Location size code";
		}else{
			locationSizeCode=$scope.frmLocationSizeAdd.locSizeCode;
		}
		
		if(!angular.isDefined($scope.frmLocationSizeAdd.locSizeCodeDesc)){
			iFlag=1;
			$scope.ErrorMessage="Enter valid Description";
		}else{
			locSizeCodeDesc=$scope.frmLocationSizeAdd.locSizeCodeDesc;
		}
		
		if($scope.frmLocationSizeAdd.capacityTracked){
			capacityTracked="Y";
			
			if(!angular.isDefined($scope.frmLocationSizeAdd.length)){
				iFlag=1;
				$scope.ErrorMessage="Enter valid Length";
			}else{
				if($scope.frmLocationSizeAdd.length<=0){
					iFlag=1;
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}else{
					locSizeCodeLength=$scope.frmLocationSizeAdd.length;
				}
				
			}
			
			
			if(!angular.isDefined($scope.frmLocationSizeAdd.width)){
				iFlag=1;
				$scope.ErrorMessage="Enter valid Width";
			}else{
				if($scope.frmLocationSizeAdd.width<=0){
					iFlag=1;
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}else{
					locSizeCodeWidth=$scope.frmLocationSizeAdd.width;
				}
				
			}
			
			
			if(!angular.isDefined($scope.frmLocationSizeAdd.height)){
				iFlag=1;
				$scope.ErrorMessage="Enter valid Height";
			}else{
				if($scope.frmLocationSizeAdd.height<=0){
					iFlag=1;
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}else{
					locSizeCodeHeight=$scope.frmLocationSizeAdd.height;
				}
				
			}
			
			
			if(!angular.isDefined($scope.frmLocationSizeAdd.maxWeight)){
				iFlag=1;
				$scope.ErrorMessage="Enter valid Maximum Weight";
			}else{
				if($scope.frmLocationSizeAdd.maxWeight<=0){
					iFlag=1;
					$scope.ErrorMessage="Enter a quantity greater than 0";
				}else{
					locSizeCodeMaxWeight=$scope.frmLocationSizeAdd.maxWeight;
				}
				
			}
			
		}else{
			capacityTracked="N";
		}
		
		var singleDeep;
		if($scope.frmLocationSizeAdd.singleDeep){
			singleDeep="Y";
		}else{
			singleDeep="N";
		}
		
		
		if(iFlag==0){
			
			var addLocSize = createLocationSize(locationSizeCode, locSizeCodeDesc, capacityTracked,singleDeep,
					locSizeCodeLength,locSizeCodeWidth,locSizeCodeHeight,locSizeCodeMaxWeight);
			
			invokeApi.async(addLocSize).then(
							function(response) {
								$scope.postResponse = response;
								
							if($scope.postResponse.ErrorDescription){
								 $scope.ErrorMessage=$scope.postResponse.ErrorDescription;
							}else{
								 $scope.ErrorMessage="";
								 $scope.InfoMessage="Location Size Code added Successfully";
									 $timeout(function() {
										 $location.path('/Configuration/LocationSize');
															},5000);
														 

							}
							
							});	
		}

	}
	
});
});