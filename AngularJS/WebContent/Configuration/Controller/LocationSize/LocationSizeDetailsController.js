define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('LocationSizeDetailsController', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	
	       var selectedContext = $rootScope.selectedLocationSize;
	       console.log($rootScope.selectedLocationSize.LocationSizeCode+" $rootScope.selectedLocationSize "+JSON.stringify($rootScope.selectedLocationSize));
	       $scope.LocationSizeCode= selectedContext.LocationSizeCode;
	       $scope.Description=selectedContext.Description;
	       $scope.IsInfiniteCapacity=selectedContext.IsInfiniteCapacity;
	       $scope.DimensionUOM=selectedContext.DimensionUom;
	       $scope.CapacityTracked=selectedContext.CapacityTracked;
	       $scope.IsSingleDeep=selectedContext.IsSingleDeep;
	       $scope.Width=selectedContext.Width;
	       $scope.Length=selectedContext.Length;
	       $scope.Height=selectedContext.Height;
	       $scope.Volume=selectedContext.Volume;
	       $scope.MaxWeight=selectedContext.MaxWeight;
	       $scope.VolumeUOM=selectedContext.VolumeUom;
	       $scope.WeightUOM=selectedContext.WeightUom;
	       
	  
	       /******* Api Input for deleteLocationSize  ********/
	   	function deleteLocationSize(locationSizeCode) {
	   		var postObject = new Object();
			postObject.CommandName = "GenericEntityApiInvoker";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Root";
			postObject.InputXml.ApiName = "deleteLocationSize";
			postObject.InputXml.childNodes = [];
			postObject.InputXml.childNodes[0] = new Object();
			postObject.InputXml.childNodes[0].tagName = "Input";
			postObject.InputXml.childNodes[0].childNodes = [];
			postObject.InputXml.childNodes[0].childNodes[0] = new Object();
			postObject.InputXml.childNodes[0].childNodes[0].tagName = "LocationSize";
			postObject.InputXml.childNodes[0].childNodes[0].Node = $cookieStore.get('globals').OrganizationCode;
			postObject.InputXml.childNodes[0].childNodes[0].LocationSizeCode = locationSizeCode;
			postObject.Template = new Object();
			postObject.IsService = "Y";
			postObject.Authenticate = AuthenticationService
					.getAuth();
			return postObject;
	   	}
	   		$scope.Delete = function()
	   		{	
	   			
	   			if(confirm("Are you sure?")){
	   				var locationSize = selectedContext.LocationSizeCode;
		   			var deleteLocSize = deleteLocationSize(locationSize);
		   			invokeApi.async(deleteLocSize).then(
		   							function(response) {
		   								$location.path('/Configuration/LocationSize');
		   								
		   								
		   							});
	   			}
	   			
	   			
	   		}
	   		
	   		$scope.Modify = function(){
	   			$location.path('/Configuration/ModifyLocationSize');
	   		}
});
});