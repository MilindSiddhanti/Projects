define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('LocationSizeListController', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){
	
	var orgCode = $cookieStore.get('globals').OrganizationCode;
	$scope.noOfRecordsLocSCReceived = $rootScope.noOfRecordsLocSCReceived;
	var count = $scope.noOfRecordsLocSCReceived;
	$scope.LocationSizeListContext = $rootScope.LocationSizeListContext;
	$scope.LocationSizeList = $scope.LocationSizeListContext;
	 $scope.setSelected = function() {
	        $scope.selected = this.locSize;
	      
	        $rootScope.selectedLocationSize = $scope.selected;
	        $location.path('/Configuration/LocationSizeDetails');
	    };
	   
	    $scope.itemsPerPage = 10;
	    $scope.currentPage = 0;
	   
	   if(count == 1){
	
		   $scope.LocationSizeList = [];
		   
	   $scope.LocationSizeList.push({
		   LocationSizeCode: $scope.LocationSizeListContext.LocationSizeCode,
		   Description: $scope.LocationSizeListContext.Description,
		   IsInfiniteCapacity: $scope.LocationSizeListContext.IsInfiniteCapacity,
		   DimensionUOM: $scope.LocationSizeListContext.DimensionUom,
		   CapacityTracked: $scope.LocationSizeListContext.CapacityTracked,
		   IsSingleDeep: $scope.LocationSizeListContext.IsSingleDeep,
		   Width: $scope.LocationSizeListContext.Width,
		   Length: $scope.LocationSizeListContext.Height,
		   Height: $scope.LocationSizeListContext.Height,
		   Volume: $scope.LocationSizeListContext.Volume,
		   MaxWeight: $scope.LocationSizeListContext.MaxWeight,
		   VolumeUOM: $scope.LocationSizeListContext.VolumeUom,
		   WeightUOM: $scope.LocationSizeListContext.WeightUom
	   });
	  
	   }
	   else
		   {
		   $scope.LocationSizeList = $scope.LocationSizeListContext;
		   }
		   
	    $scope.range = function() {
	      var rangeSize = Math.ceil($scope.LocationSizeList.length/$scope.itemsPerPage);
	      var ret = [];
	      var start;

	      start = $scope.currentPage;
	      if ( start > $scope.pageCount()-rangeSize ) {
	        start = $scope.pageCount()-rangeSize+1;
	      }

	      for (var i=start; i<start+rangeSize; i++) {
	        ret.push(i);
	      }
	      return ret;
	    };

	    $scope.prevPage = function() {
	      if ($scope.currentPage > 0) {
	        $scope.currentPage--;
	      }
	    };

	    $scope.prevPageDisabled = function() {
	      return $scope.currentPage === 0 ? "disabled" : "";
	    };

	    $scope.pageCount = function() {
	      return Math.ceil($scope.LocationSizeList.length/$scope.itemsPerPage)-1;
	    };

	    $scope.nextPage = function() {
	      if ($scope.currentPage < $scope.pageCount()) {
	        $scope.currentPage++;
	      }
	    };

	    $scope.nextPageDisabled = function() {
	      return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	    };

	    $scope.setPage = function(n) {
	      $scope.currentPage = n;
	    };

	
	
});
});
