define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller('ModifyLocationSizeController', function($scope, $rootScope,$timeout, $location, $log, $cookieStore, $state, processContoller, invokeApi,AuthenticationService){

		var orgCode = $cookieStore.get('globals').OrganizationCode;
		var selectedContext = $rootScope.selectedLocationSize;
		
		 $scope.frmLocationSizeModify_locSizeCode= selectedContext.LocationSizeCode;
	    $scope.frmLocationSizeModify_locSizeCodeDesc=selectedContext.Description;
	       $scope.IsInfiniteCapacity=selectedContext.IsInfiniteCapacity;
	       $scope.DimensionUOM=selectedContext.DimensionUOM;
	       if(selectedContext.CapacityTracked=="Y"){
	    	   $scope.frmLocationSizeModify_capacityTracked=true;
	       }else{
	    	   $scope.frmLocationSizeModify_capacityTracked=false;
	       }
	       if(selectedContext.IsSingleDeep=="Y"){
	    	   $scope.frmLocationSizeModify_singleDeep=true;
	       }else{
	    	   $scope.frmLocationSizeModify_singleDeep=false;
	       }
	      
	       $scope.frmLocationSizeModify_width=selectedContext.Width;
	       $scope.frmLocationSizeModify_length=selectedContext.Length;
	       $scope.frmLocationSizeModify_height=selectedContext.Height;
	       $scope.Volume=selectedContext.Volume;
	       $scope.frmLocationSizeModify_maxWeight=selectedContext.MaxWeight;
	       $scope.VolumeUOM=selectedContext.VolumeUOM;
	       $scope.WeightUOM=selectedContext.WeightUOM;
	       
	   	/************ modifyLocationSize API***********/
	   	function modifyLocationSize(locationSizeCode, locSizeCodeDesc, capacityTracked,singleDeep,locSizeCodeLength,
	   			locSizeCodeWidth,locSizeCodeHeight,locSizeCodeMaxWeight) {
	   		var postObject = new Object();
	   		postObject.CommandName = "GenericEntityApiInvoker";
	   		postObject.InputXml = new Object();
	   		postObject.InputXml.tagName = "Root";
	   		postObject.InputXml.ApiName = "modifyLocationSize";
	   		postObject.InputXml.childNodes = [];
	   		postObject.InputXml.childNodes[0] = new Object();
	   		postObject.InputXml.childNodes[0].tagName = "Input";
	   		postObject.InputXml.childNodes[0].childNodes = [];
	   		postObject.InputXml.childNodes[0].childNodes[0] = new Object();
	   		postObject.InputXml.childNodes[0].childNodes[0].tagName = "LocationSize";
	   		postObject.InputXml.childNodes[0].childNodes[0].Node = $cookieStore.get('globals').OrganizationCode;
	   		postObject.InputXml.childNodes[0].childNodes[0].LocationSizeCode = locationSizeCode;
	   		postObject.InputXml.childNodes[0].childNodes[0].Description = locSizeCodeDesc;
	   		postObject.InputXml.childNodes[0].childNodes[0].CapacityTracked=capacityTracked;
	   		postObject.InputXml.childNodes[0].childNodes[0].IsSingleDeep=singleDeep;
	   		var infiniteCap;
	   		if(capacityTracked=="Y"){
	   			infiniteCap="N";
	   		}else{
	   			infiniteCap="Y";
	   		}
	   		postObject.InputXml.childNodes[0].childNodes[0].IsInfiniteCapacity=infiniteCap;
	   		postObject.InputXml.childNodes[0].childNodes[0].Length=locSizeCodeLength;
	   		postObject.InputXml.childNodes[0].childNodes[0].Width=locSizeCodeWidth;
	   		postObject.InputXml.childNodes[0].childNodes[0].Height=locSizeCodeHeight;
	   		postObject.InputXml.childNodes[0].childNodes[0].MaxWeight=locSizeCodeMaxWeight;
	   		postObject.InputXml.childNodes[0].childNodes[0].DimensionUOM= $scope.DimensionUOM;
	   		postObject.InputXml.childNodes[0].childNodes[0].WeightUOM= $scope.WeightUOM;
	   		postObject.Template = new Object();
	   		postObject.IsService = "Y";
	   		postObject.Authenticate = AuthenticationService
	   				.getAuth();
	   		return postObject;
	   	}
	   	
	   	
	   	processContoller.InputController = function(scannedVariable){
			console.log("scannedVariable "+scannedVariable);
			
			
			if(scannedVariable=="lsm_height"){
				if(!angular.isDefined($scope.frmLocationSizeModify_height)){
					$scope.ErrorMessage="Enter valid Height";
				}else{
					if($scope.frmLocationSizeModify_height<=0){
						$scope.ErrorMessage="Enter a quantity greater than 0";
					}else{
						$scope.frmLocationSizeModify_volume=$scope.frmLocationSizeModify_length*$scope.frmLocationSizeModify_width*$scope.frmLocationSizeModify_height;
						
					}
				}
			}
			
		}
		
	   	
	   	$scope.disableButtons = function()
		{
			if(!$scope.frmLocationSizeModify_capacityTracked){
				$scope.frmLocationSizeModify_length=0;
				$scope.frmLocationSizeModify_width=0;
				$scope.frmLocationSizeModify_height=0;
				$scope.frmLocationSizeModify_maxWeight=0;
				$scope.frmLocationSizeModify_singleDeep=false;
			}
			
		}
	   	
		$scope.CancelLocationSize = function()
		{
			$location.path('/Configuration/LocationSizeDetails');
		}
		$scope.ModifyLocationSize = function()
		{
			var iFlag=0;
			var locationSizeCode;
			var locSizeCodeDesc;
			var capacityTracked;
			var singleDeep;
			var locSizeCodeLength=0;
			var locSizeCodeWidth=0;
			var locSizeCodeHeight=0;
			var locSizeCodeMaxWeight=0;
			
			if(!angular.isDefined($scope.frmLocationSizeModify_locSizeCode)){
				iFlag=1;
				$scope.ErrorMessage="Enter valid Location size code";
			}else{
				locationSizeCode=$scope.frmLocationSizeModify_locSizeCode;
			}
			
			if(!angular.isDefined($scope.frmLocationSizeModify_locSizeCodeDesc)){
				iFlag=1;
				$scope.ErrorMessage="Enter valid Description";
			}else{
				locSizeCodeDesc=$scope.frmLocationSizeModify_locSizeCodeDesc;
			}
			
			if($scope.frmLocationSizeModify_capacityTracked){
				capacityTracked="Y";
				if(!angular.isDefined($scope.frmLocationSizeModify_length)){
					iFlag=1;
					$scope.ErrorMessage="Enter valid Length";
				}else{
					if($scope.frmLocationSizeModify_length<=0){
						iFlag=1;
						$scope.ErrorMessage="Enter a quantity greater than 0";
					}else{
						locSizeCodeLength=$scope.frmLocationSizeModify_length;
					}
					
				}
				
				
				if(!angular.isDefined($scope.frmLocationSizeModify_width)){
					iFlag=1;
					$scope.ErrorMessage="Enter valid Width";
				}else{
					if($scope.frmLocationSizeModify_width<=0){
						iFlag=1;
						$scope.ErrorMessage="Enter a quantity greater than 0";
					}else{
						locSizeCodeWidth=$scope.frmLocationSizeModify_width;
					}
					
				}
				
				
				if(!angular.isDefined($scope.frmLocationSizeModify_height)){
					iFlag=1;
					$scope.ErrorMessage="Enter valid Height";
				}else{
					if($scope.frmLocationSizeModify_height<=0){
						iFlag=1;
						$scope.ErrorMessage="Enter a quantity greater than 0";
					}else{
						locSizeCodeHeight=$scope.frmLocationSizeModify_height;
					}
					
				}
				
				
				if(!angular.isDefined($scope.frmLocationSizeModify_maxWeight)){
					iFlag=1;
					$scope.ErrorMessage="Enter valid Maximum Weight";
				}else{
					if($scope.frmLocationSizeModif_maxWeight<=0){
						iFlag=1;
						$scope.ErrorMessage="Enter a quantity greater than 0";
					}else{
						locSizeCodeMaxWeight=$scope.frmLocationSizeModify_maxWeight;
					}
					
				}
				
			}else{
				capacityTracked="N";
			}
			
			
			if($scope.frmLocationSizeModify_singleDeep){
				singleDeep="Y";
			}else{
				singleDeep="N";
			}
			
			
			if(iFlag==0){
				
				var modLocSize = modifyLocationSize(locationSizeCode, locSizeCodeDesc, capacityTracked,singleDeep,
						locSizeCodeLength,locSizeCodeWidth,locSizeCodeHeight,locSizeCodeMaxWeight);
				
				invokeApi.async(modLocSize).then(
								function(response) {
									$scope.postResponse = response;
									
								if($scope.postResponse.ErrorDescription){
									 $scope.ErrorMessage=$scope.postResponse.ErrorDescription;
								}else{
									 $scope.ErrorMessage="";
									 $scope.InfoMessage="Location Size Code modified Successfully";
									 $rootScope.selectedLocationSize=$scope.postResponse.LocationSize;
									
									
		
										 $timeout(function() {
											 $location.path('/Configuration/LocationSizeDetails');
																},5000);
								}
								
								});	
			}

		}

	});
});