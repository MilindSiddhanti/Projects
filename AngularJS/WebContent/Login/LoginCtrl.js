
define(['common/ctrljs/app'], function (myApp) {

	myApp.register.controller("LoginCtrl", function($scope, $location, $window, $rootScope, $log,$cookieStore, $state, processContoller, invokeApi, AuthenticationService, loggerHirerachy) {


		  var Logger = $log.getInstance($state.$current);
          Logger.info("Logg From the LoginCtrl"); 
    	
          $scope.formInfo = {};
          
          var UserId = '';
          
          var OrganisationCode = '';
          
          var UserRestrict = [];
          
          function getMenuHierarchyForUserApi(Loginid){
				var postObject = new Object();
				postObject.CommandName = "getMenuHierarchyForUser";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "getMenuHierarchyForUser";
				postObject.InputXml.Loginid = Loginid;
				postObject.InputXml.ApplicationName = "YFSSYS00005";
				
				postObject.Template = new Object();
				postObject.Template.tagName = "Menu";
				postObject.Template.Active = "";
				postObject.Template.MenuDescription = "";
				
				postObject.IsService = "N";
				postObject.Authenticate=AuthenticationService.getAuth();
				return postObject;
			};
			
			
			function getConfigProperties(){
				var postObject = new Object();
				postObject.CommandName = "getConfigProperties";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "Config";
				postObject.InputXml.childNodes=[];
				postObject.InputXml.childNodes[0]=new Object();
				postObject.InputXml.childNodes[0].tagName = "Property";
				postObject.InputXml.childNodes[0].Name="NoOfSecondsFrMsgDisplay";
				postObject.InputXml.childNodes[1]=new Object();
				postObject.InputXml.childNodes[1].tagName = "Property";
				postObject.InputXml.childNodes[1].Name="NoOfRecordsToBeDisplayed";
				postObject.InputXml.childNodes[2]=new Object();
				postObject.InputXml.childNodes[2].tagName = "Property";
				postObject.InputXml.childNodes[2].Name="NavigationTimeoutInSecs";
				postObject.Template = new Object();
				postObject.IsService = "N";
				postObject.LocalApi = "Y";
				postObject.Authenticate=AuthenticationService.getAuth();
				return postObject;
			};
          
			
			
			
			
			
			/*****************getOrganisationList Api Xml formation*****************************************/
			function getOrganisationListApi(OrgCode) {
				console.log("getOrganizationList");
				var postObject = new Object();
				postObject.CommandName = "getOrganizationList";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "Organization";
				postObject.InputXml.OrganizationCode = OrgCode;
				postObject.Template = new Object();
				postObject.Template.tagName = "OrganizationList";
				postObject.Template.childNodes = [];
				postObject.Template.childNodes[0] = new Object();
				postObject.Template.childNodes[0].tagName = "Organization";
				postObject.Template.childNodes[0].OrganizationCode = "";
				postObject.Template.childNodes[0].OrganizationKey = "";
				postObject.Template.childNodes[0].IsEnterprise = "";
				postObject.IsService = "N";
				postObject.Authenticate = AuthenticationService.getAuth();
				return postObject;
			};
			
			
			
			/*****************modifyUserHierarchy Api Xml formation*****************************************/
			function getUserLogStatusApi(userid, OrgCode) {
				console.log("getUserLogStatus");
				var postObject = new Object();
				postObject.CommandName = "getUserLogStatus";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "Users";
				postObject.InputXml.childNodes= [];
				
				postObject.InputXml.childNodes[0]= new Object();
				postObject.InputXml.childNodes[0].tagName = "User";//userIds[i];
				postObject.InputXml.childNodes[0].UserId = userid;
				postObject.InputXml.childNodes[0].OrgCode = OrgCode;
				
				postObject.Template = new Object();
				postObject.IsService = "N";
				postObject.LocalApi = "Y";
				postObject.Authenticate = AuthenticationService.getAuth();
				return postObject;
			};
			

			/*****************modifyUserLogStatus Api Xml formation*****************************************/
			function modifyUserLogStatusApi(UserId, OrgCode, states) {
				console.log("modifyUserLogStatus");
				var postObject = new Object();
				postObject.CommandName = "modifyUserLogStatus";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "Users";
				postObject.InputXml.childNodes= [];
				
					postObject.InputXml.childNodes[0]= new Object();
					postObject.InputXml.childNodes[0].tagName = "User";
					postObject.InputXml.childNodes[0].UserId = UserId;
					postObject.InputXml.childNodes[0].OrgCode = OrgCode;
				
					for(var i = 0; i<states.length; i++){
						var temp = states[i];
					    postObject.InputXml.childNodes[0][temp] = "N";
						
					}
					
				
				postObject.Template = new Object();
				postObject.IsService = "N";
				postObject.LocalApi = "Y";
				postObject.Authenticate = AuthenticationService.getAuth();
				return postObject;
			};
	       
			
			
			function generateLoggerHirarchy(objArray, loggerHirerachy){
				
				for(var i = 0; i<objArray.length; i++){
					    
				   if((loggerHirerachy.indexOf(objArray[i])) == -1)
					    loggerHirerachy.push(objArray[i]);
					
				}
				
				
			}
          
          
          $scope.validateUserData = function() {
    	  
          $scope.userRequired = '';
          $scope.passRequired = '';
          $scope.loginmessage = '';

          if (!$scope.frmlogin_userid ) {
            $scope.userRequired = 'User Name Required';
          }
          if (!$scope.frmlogin_password) {
            $scope.passRequired = 'Password Required';
          }
          
          if( ($scope.frmlogin_userid) && ($scope.frmlogin_password) ){
        	  var postInputXml = new Object();
        	//  AuthenticationService.ClearCredentials();
          	  postInputXml.tagName = "Login";
          	  postInputXml.LoginID = $scope.frmlogin_userid;
              postInputXml.Password = $scope.frmlogin_password;

              var postObject = {"CommandName" : "login", "Template" : {}, "Authenticate" : {}, "IsService" : "N"};
              postObject.InputXml = postInputXml;
              console.log(postObject);
              $scope.postResponse = new Object();
              invokeApi.async(postObject).then(function(response) {
        		      $scope.postResponse = response;
        		      if(($scope.postResponse.ApiOutput == undefined) || ($scope.postResponse.ApiOutput.Login == undefined)) {
        		    	  $scope.loginmessage = "Server Not Connected";
        		      }else if($scope.postResponse.ApiOutput.Login.ActivateFlag == "Y"){
        		    	  var authJson = new Object();
        		    	  var csrf = $scope.postResponse.CSRFToken;
        		    	  authJson.tagName = "Bauth";
        		    	  authJson.currentUserId = $scope.postResponse.ApiOutput.Login.LoginID;
        		    	  authJson.OrganizationCode = $scope.postResponse.ApiOutput.Login.OrganizationCode;
        		    	  authJson.UserToken = $scope.postResponse.ApiOutput.Login.UserToken;
        		    	  console.log(authJson);
        		    	  UserId = authJson.currentUserId;
        		    	  OrganisationCode = authJson.OrganizationCode;
        		    	  AuthenticationService.SetCredentials(authJson, csrf);
        		       	  //$location.path('/menu');
        		    	  
        		    	  //*******************Menu Code Start***********************/
        		    	  $location.search('CSRFToken', csrf);
        	
        		    	  
        		    	            var MenuHierarchy = getMenuHierarchyForUserApi(UserId);
        							invokeApi.async(MenuHierarchy).then(function(MenuHierarchyResponse) {
        								var postMenuResponse = MenuHierarchyResponse;
        								console.log(postMenuResponse);
        								$rootScope.Menu = postMenuResponse;
        								angular.forEach(postMenuResponse, function(value, key) {

        									for(var i = 0; i<Object.keys(value.SubMenu.Menu).length; i++){

        										UserRestrict.push(value.SubMenu.Menu[i].ScreenName);
        										
        									}
        									$cookieStore.put('NonRestricted', UserRestrict);
        									
        									
        									
        								});
        								
                                        /***************************************User Level Acess Validation Start**********************************************/
        								
        								var postgetOrganizationListObj = getOrganisationListApi(OrganisationCode); 
        								invokeApi.async(postgetOrganizationListObj).then(function(responsegetOrganizationListObj) {
        									 var FirstOrgNode = responsegetOrganizationListObj.OrganizationList.Organization;
        									 SuperAcessMode = FirstOrgNode.IsEnterprise;
        									 
        									 if(angular.equals(SuperAcessMode,"Y")){
        										 
        										 $rootScope.SuperAcessMode = true;
        										//Super User
        										console.log("****************SuperUser********************");
        										
        										$location.path('/menuopt').search({"CSRFToken": csrf});
        										
        									}else{
        										//Node User
        										
        										console.log("*****************NODE USERS*******************");
        										$rootScope.SuperAcessMode = false;
        										
        										var postgetUserLogStatusObject = getUserLogStatusApi(UserId, OrganisationCode);
        										invokeApi.async(postgetUserLogStatusObject).then(function(responsegetUserLogStatusObject) {
        											
        											angular.forEach(responsegetUserLogStatusObject.Users, function(value, key) {
        												
        												angular.forEach(value, function(oValue, oKey) {
        													
        													
        													if(!angular.equals(oValue,"")){
        													
        														var objArray = Object.keys(oValue);
            													
            													generateLoggerHirarchy(objArray, loggerHirerachy);
            													
        													}
        													
        													 
        													
        													
        												});
        												
        											});
        											  
        											if((loggerHirerachy.length)>0){
        												
        												var logOption = confirm("Log has been Configured. Do you want to disable the log?");
        												
        												if(logOption){
        													
        													var postUserLogStatusObject = modifyUserLogStatusApi(UserId, OrganisationCode, loggerHirerachy);
        												
        													invokeApi.async(postUserLogStatusObject).then(function(responseUserLogStatusObject) {
        													   
        														console.log(responseUserLogStatusObject);
        														
        														loggerHirerachy.length = 0;
        														
        													});
        													
        													
        													console.log("Log SucessFully disabled");
        													
        												}else{
        													
        													console.log("Continue with configured log");
        													
        												}
        										          
        										          
        												
        											}
        										
												var postCfgProp = getConfigProperties();
            							invokeApi.async(postCfgProp).then(function(cfgPropRes) {
            								$rootScope.ConfigProperties = cfgPropRes;
            								
            							});
												
												
        											$location.path('/menuopt').search({"CSRFToken": csrf});
        											
        										});
        										
        										
        									}
        									
        									 
        									 
        									
        								});
        								
        								/***************************************User Level Validation   End**********************************************/
        								
        								//$location.path('/menuopt').search({"CSRFToken": csrf});
        							});
        						
        						
        						
        		
        		    	  
        		    	  //*******************Menu Code End*************************/
        		    	  
        		    	  
        		    	  
        		    	  
        		    	  
        		    	  
        		       	
        		      }
        		    }, function(response){
        	    	   console.log("sdfusfusfsfdysdfdhs"+response.status);
        	    	   $scope.loginmessage = "Invalid Login Credentials. Try again or call IT Helpdesk";
     		    	   $location.path('/'); 
        	       });
       	  
          }
          
        
        };
        
            
        
        }) ;
    	
});