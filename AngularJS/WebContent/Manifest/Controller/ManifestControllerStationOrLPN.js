define(['common/ctrljs/app'], function (myApp) {

myApp.register.controller('ManifestControllerStationOrLPN', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService, sterlingConfigService){
	
	var formName = 'frmManifestStationOrLPN';
	var recevingNode = $cookieStore.get('globals').OrganizationCode;
	$rootScope.recevingNode=recevingNode;
	$rootScope.Manifest_moduleFlow = 'Find';
	$rootScope.Manifest_NavActive = "Manifests";
	$scope.ErrorMessage = "";
	
	console.log("StationOrLPN controller");
	
	var Logger = $log.getInstance($state.$current);
	Logger.info("Log from ManifestController StationOrLPN"); 
	
	document.getElementById("mnf_station").focus();

	/*****************getShipmentList Api Xml formation*****************************************/
	function getShipmentListApi(recevingNode, scannedscm){
		var postObject = new Object();
		postObject.CommandName = "getShipmentList";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Shipment";
		postObject.InputXml.DocumentType = "0001";
		postObject.InputXml.ShipNode = recevingNode;
		
		postObject.InputXml.childNodes= [];
		postObject.InputXml.childNodes[0]=new Object();
		postObject.InputXml.childNodes[0].tagName="Containers";
		
		postObject.InputXml.childNodes[0].childNodes= [];
		postObject.InputXml.childNodes[0].childNodes[0]=new Object();
		postObject.InputXml.childNodes[0].childNodes[0].tagName="Container";
		postObject.InputXml.childNodes[0].childNodes[0].ContainerScm= scannedscm;
		
		postObject.Template = new Object();
		postObject.Template.tagName = "Shipments";
		postObject.Template.TotalNumberOfRecords="";
		
		postObject.Template.childNodes= [];
		postObject.Template.childNodes[0]=new Object();
		postObject.Template.childNodes[0].tagName="Shipment";
		postObject.Template.childNodes[0].ShipmentNo="";
		postObject.Template.childNodes[0].SCAC="";	
		postObject.Template.childNodes[0].ShipmentKey="";
		postObject.Template.childNodes[0].SellerOrganizationCode="";
		
		postObject.Template.childNodes[0].childNodes= [];
		postObject.Template.childNodes[0].childNodes[0]=new Object();
		postObject.Template.childNodes[0].childNodes[0].tagName="Containers";
		
		postObject.Template.childNodes[0].childNodes[0].childNodes= [];
		postObject.Template.childNodes[0].childNodes[0].childNodes[0]=new Object();
		postObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName="Container";
		postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerScm="";
		postObject.Template.childNodes[0].childNodes[0].childNodes[0].ContainerNo="";
		postObject.Template.childNodes[0].childNodes[0].childNodes[0].IsManifested="";
	
		
		postObject.IsService = "N";
		postObject.Authenticate=AuthenticationService.getAuth();
		console.log("postObject object shipment list "+JSON.stringify(postObject));
		return postObject;
	};
	
	/*****************getManifestList Api Xml formation*****************************************/
	function getManifestListApi(recevingNode){
		var postObject = new Object();
		postObject.CommandName = "getManifestList";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Manifest";
		postObject.InputXml.SCAC = $rootScope.scac;
		postObject.InputXml.ShipNode = recevingNode;
		
		postObject.Template = new Object();
		postObject.Template.tagName = "Manifests";
		postObject.Template.TotalNumberOfRecords = "";
		
		postObject.Template.childNodes= [];
		postObject.Template.childNodes[0]=new Object();
		postObject.Template.childNodes[0].tagName="Manifest";
		postObject.Template.childNodes[0].ManifestDate="";
		postObject.Template.childNodes[0].ManifestNo="";	
		postObject.Template.childNodes[0].Scac="";	
		postObject.Template.childNodes[0].TrailerNo="";
		
		postObject.IsService = "N";
		postObject.Authenticate=AuthenticationService.getAuth();
		console.log("postObject object manifest list "+JSON.stringify(postObject));
		return postObject;
	};
	
	processContoller.InputController = function(scannedVariable){
		
		if(scannedVariable == "mnf_station"){
			if(angular.isDefined($scope.frmManifest_station) && $scope.frmManifest_station.length>0){
				$rootScope.stationid= $scope.frmManifest_station;
			}
			else {
				$scope.ErrorMessage= "Enter Station ID";
				Logger.error= ("Enter Station ID");
			}
		}
		
		if(scannedVariable == "mnf_LPN"){
		$rootScope.scannedscm= $scope.frmManifest_LPN;
		scannedscm = $rootScope.scannedscm;
		if(scannedscm){
             
			var getShipmentListObject=getShipmentListApi(recevingNode, scannedscm);
			invokeApi.async(getShipmentListObject).then(function(getShipmentListResponse) {
				$scope.postResponse = getShipmentListResponse;
				Logger.info("Invoke getShipmentList"); 
				$rootScope.ShipmentContext = $scope.postResponse; 
				var shipmentContext = $rootScope.ShipmentContext; 
				
				if(shipmentContext.Shipments.TotalNumberOfRecords > 0){
					$rootScope.shipno = shipmentContext.Shipments.Shipment.ShipmentNo;
					$rootScope.shipkey = shipmentContext.Shipments.Shipment.ShipmentKey;
					$rootScope.scac = shipmentContext.Shipments.Shipment.SCAC;
					$rootScope.seller = shipmentContext.Shipments.Shipment.SellerOrganizationCode;
					$rootScope.containerscm = shipmentContext.Shipments.Shipment.Containers.Container.ContainerScm;
					$rootScope.containerno = shipmentContext.Shipments.Shipment.Containers.Container.ContainerNo;
					$rootScope.ismanifested = shipmentContext.Shipments.Shipment.Containers.Container.IsManifested;
					
					console.log("shipno:" + $rootScope.shipno);
					console.log("shipkey:" + $rootScope.shipkey);
					console.log("scac:" + $rootScope.scac);
					console.log("containerscm:" + $rootScope.containerscm);
					console.log("containerno:" + $rootScope.containerno);
					console.log("ismanifested:" + $rootScope.ismanifested);
				}
				else if(shipmentContext.Shipments.TotalNumberOfRecords = 0){
					$scope.ErrorMessage = "No Records Found";
					Logger.error= ("No Records Found");
				}
			});
		}
		}
					
	};
	
	$scope.next = function(){
		if(angular.isUndefined($scope.frmManifest_station) && angular.isUndefined($scope.frmManifest_LPN)){
			$scope.ErrorMessage= "Mandatory parameters to be passed";
			Logger.error= ("Mandatory parameters to be passed");
		}
		
		if($rootScope.ismanifested == "Y"){
			var manifestListObject=getManifestListApi(recevingNode);
			invokeApi.async(manifestListObject).then(function(getManifestListResponse) {
				$scope.postResponse = getManifestListResponse;
				Logger.info("Invoke getManifestist Api for Remove LPN"); 
				var manifestListContext = $scope.postResponse;
				
				$rootScope.manifestdate = manifestListContext.Manifests.Manifest.ManifestDate;
				$rootScope.manifestNo = manifestListContext.Manifests.Manifest.ManifestNo;
				$rootScope.Scac = manifestListContext.Manifests.Manifest.SCAC;
				$rootScope.trailerno =  manifestListContext.Manifests.Manifest.TrailerNo;
				
				if(manifestListContext.Manifests.TotalNumberOfRecords=="0"){
					$scope.ErrorMessage = "No Records Found";
				}
				
				else{
					Logger.info("Call the Remove LPN screen ");
					$location.path('/Manifest/RemoveLPN');
				}				
				
			});
			
		}
		else if ($rootScope.ismanifested!= "Y"){
			var manifestListObject=getManifestListApi(recevingNode);
			invokeApi.async(manifestListObject).then(function(getManifestListResponse) {
				$scope.postResponse = getManifestListResponse;
				console.log("Manifest response:"+JSON.stringify($scope.postResponse));
				Logger.info("Invoke getManifestList Api for Add LPN"); 
				var manifestListContext = $scope.postResponse;
				
				if(manifestListContext.Manifests.TotalNumberOfRecords=="0"){
					$scope.ErrorMessage = "No Records Found";
				}
				
				else{
					Logger.info("Call the Add LPN screen ");
					$rootScope.manifestListContext = manifestListContext;
					$location.path('/Manifest/AddLPN');
				}				
				
			});
			
		}
	}
});
});