define([ 'common/ctrljs/app' ],function(myApp) {

myApp.register.controller('ManifestControllerAddLPN', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService, sterlingConfigService) {
         $rootScope.CloseReceipt_moduleFlow = 'AddLPN';

		var formName = 'frmManifestAddLPN';
		var recevingNode = $cookieStore.get('globals').OrganizationCode;
		$rootScope.recevingNode = recevingNode;
		$rootScope.Manifest_moduleFlow = 'Add LPN';
		$rootScope.Manifest_NavActive = "Manifests";
		$scope.ErrorMessage = "";
		$rootScope.GivenLimit=10;
		
		console.log("AddLPN controller");

		var Logger = $log.getInstance($state.$current);
		Logger.info("Log from ManifestController AddLPN");

		var manifestListContext = $rootScope.manifestListContext;
		if (angular.isArray(manifestListContext.Manifests.Manifest)) {
		 $rootScope.manifestContext = manifestListContext.Manifests.Manifest;
		}
     	else {
		$rootScope.manifestContext = manifestListContext.Manifests;
		}
		
		/*****************addContainerToManifest Api Xml formation*****************************************/
		function addContainerToManifestApi(recevingNode){
			var postObject = new Object();
			postObject.CommandName = "addContainerToManifest";
			postObject.InputXml = new Object();
			postObject.InputXml.tagName = "Container";
			postObject.InputXml.ContainerNo= $rootScope.containerno;
			postObject.InputXml.ContainerScm= $rootScope.containerscm;
			postObject.InputXml.LocationId= $rootScope.stationid;
			postObject.InputXml.ManifestNo= "";	
			postObject.InputXml.SCAC = "";
			postObject.InputXml.ShipNode = recevingNode;
			postObject.InputXml.ShipmentKey = $rootScope.shipkey;
			
			postObject.InputXml.childNodes= [];
			postObject.InputXml.childNodes[0]=new Object();
			postObject.InputXml.childNodes[0].tagName="Shipment";
			postObject.InputXml.childNodes[0].SellerOrganization= $rootScope.seller;
			postObject.InputXml.childNodes[0].ShipNode= recevingNode;
			postObject.InputXml.childNodes[0].ShipmentNo= $rootScope.shipno;
			postObject.InputXml.childNodes[1]=new Object();
			postObject.InputXml.childNodes[1].tagName="Manifest";
			postObject.InputXml.childNodes[1].ManifestDate= "";
			
			postObject.Template = new Object();
			
			postObject.IsService = "N";
			postObject.Authenticate=AuthenticationService.getAuth();
			console.log("postObject object add container to manifest" + JSON.stringify(postObject));
			return postObject;
		};
		
		$scope.proceed = function(ManifestNo, SCAC, ManifestDate){
			
		}
});
});