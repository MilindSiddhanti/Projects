define(['common/ctrljs/app'], function (myApp) {


myApp.config(function($stateProvider, $urlRouterProvider, routeStateLoaderProvider) {
	
	var route = routeStateLoaderProvider.route;
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
        
         .state('Manifest',{
    	    url: "/Manifest:action?CSRFToken",
    	    views: {
    	    	'': {
    	    		templateUrl: 'Manifest/Config/ManifestFrame.html',
    	    		controller: 'ManifestController',
    	    		resolve   :  route.resolve('Manifest/Controller/ManifestController')	
    	    	},
    	    	'ManifestNavigator@Manifest': {
    	    		templateUrl: 'Manifest/View/ManifestNavigator.html',
    	    		controller: 'ManifestNavigatorController',
    	    		resolve   :  route.resolve('Manifest/Controller/ManifestNavigatorController')		
    	    	},
    	    	'criteria@Manifest': {
        	    		templateUrl: 'Manifest/View/frmManifestStationOrLPN.html',
        	    		controller: 'ManifestControllerStationOrLPN',
        	    		resolve   :  route.resolve('Manifest/Controller/ManifestControllerStationOrLPN')
    	    	},
    	    },
         })
        
        .state('Manifest.RemoveLPN',{
			url: "/RemoveLPN",
			views: {
				'': {
					templateUrl: 'Manifest/Config/ManifestFrame.html',
					controller: 'ManifestController',
					resolve: route.resolve('Manifest/Controller/ManifestController')},
					'criteria@Manifest': {
						templateUrl: 'Manifest/View/frmManifestRemoveLPN.html',
						controller: 'ManifestControllerRemoveLPN',
						resolve: route.resolve('Manifest/Controller/ManifestControllerRemoveLPN')},
			},
		})
		
        .state('Manifest.AddLPN',{
			url: "/AddLPN",
			views: {
				'': {
					templateUrl: 'Manifest/Config/ManifestFrame.html',
					controller: 'ManifestController',
					resolve: route.resolve('Manifest/Controller/ManifestController')},
					'criteria@Manifest': {
						templateUrl: 'Manifest/View/frmManifestAddLPN.html',
						controller: 'ManifestControllerAddLPN',
						resolve: route.resolve('Manifest/Controller/ManifestControllerAddLPN')},
			},
		});
     
    });
});   
