define(['common/ctrljs/app'], function (myApp) {


myApp.config(function($stateProvider, $urlRouterProvider, routeStateLoaderProvider) {
	
	var route = routeStateLoaderProvider.route;
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
        
         .state('CloseReceipt',{
    	    url: "/CloseReceipt:action?CSRFToken",
    	    views: {
    	    	'': {
    	    		templateUrl: 'CloseReceipt/Config/CloseReceiptFrame.html',
    	    		controller: 'CloseReceiptController',
    	    		resolve   :  route.resolve('CloseReceipt/Controller/CloseReceiptController')	
    	    	},
    	    	'CloseReceiptNavigator@CloseReceipt': {
    	    		templateUrl: 'CloseReceipt/View/CloseReceiptNavigator.html',
    	    		controller: 'CloseReceiptNavigatorController',
    	    		resolve   :  route.resolve('CloseReceipt/Controller/CloseReceiptNavigatorController')		
    	    	},
    	    	'criteria@CloseReceipt': {
        	    		templateUrl: 'CloseReceipt/View/frmCloseReceiptFind.html',
        	    		controller: 'CloseReceiptControllerFPO',
        	    		resolve   :  route.resolve('CloseReceipt/Controller/CloseReceiptControllerFPO')
    	    	},
    	    },
         })

         
        
        .state('CloseReceipt.Results',{
        	url: '/Results',
        	views: {
        		'criteria@CloseReceipt': {
        			templateUrl: 'CloseReceipt/View/frmCloseReceiptResults.html',
                	controller: 'CloseReceiptControllerResults',
                	resolve   :  route.resolve('CloseReceipt/Controller/CloseReceiptControllerResults')
        		},
        	},
        	
        })
                
        .state('CloseReceipt.Details',{
        	url: '/Details',
        	views: {
        		'criteria@CloseReceipt': {
        			templateUrl: 'CloseReceipt/View/frmCloseReceiptDetails.html',
                	controller: 'CloseReceiptControllerDetails',
                	resolve   :  route.resolve('CloseReceipt/Controller/CloseReceiptControllerDetails')
        		},
        		
        		},
        	
        	
        });
    });
});   
