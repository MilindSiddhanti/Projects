/***********************************************************************************
 * Name: CloseReceiptControllerResults
 * Usage: Controller for receive module to render frmCloseReceiptResults.html view 
 * Purpose: This Controller loads the Receipt List that which are in the open status.
 * User on click of any one of the receipt is directed to the next screen to close the
 * receipt.
 ***********************************************************************************/


define(['common/ctrljs/app'], function (myApp) {


myApp.register.controller('CloseReceiptControllerResults', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService, sterlingConfigService){
	
	var formName = 'frmCloseReceiptResults';
	var recevingNode = $cookieStore.get('globals').OrganizationCode;
	$rootScope.recevingNode=recevingNode;
	$rootScope.CloseReceipt_moduleFlow = 'Results';
	$rootScope.CloseReceipt_NavActive = "Results";
	var Logger = $log.getInstance($state.$current);
	Logger.info("Log from CloseReceiptController Results"); 
	
	$rootScope.GivenLimit=10;
		
	// No Of Records To Be Displayed on the results screen
	sterlingConfigService.getConfigProperty("NoOfRecordsToBeDisplayed").then(function(response) {

		   if (angular.isDefined(response)) {
		    $rootScope.numOfReceiptsToBeShown = response;
		   
		   } else {
		    $rootScope.numOfReceiptsToBeShown = 3;
		  
		   }
		  });
	
	// pushing forward the receipt list context to generate the receipt list	
	var receiptListContext = $rootScope.receiptListContext;
	// checking for array of receipts	
	if (angular.isArray(receiptListContext.ReceiptList.Receipt)) {
		$rootScope.receiptContext = receiptListContext.ReceiptList.Receipt;
	} 
	// checking for a single receipt
	else {
		$rootScope.receiptContext = receiptListContext.ReceiptList;
	}
	
	/* Proceed Button to proceed to the receipt Details of the selected receipt */
	$scope.proceed = function(ReceiptNo, ReceiptDate, DocumentType) {
		
		$rootScope.ReceiptNoSelected = ReceiptNo;
		$rootScope.ReceiptDateSelected = ReceiptDate;
		$rootScope.DocumentTypeSelected = DocumentType;
	
		$location.path('/CloseReceipt/Details');
	}
		
});
});
