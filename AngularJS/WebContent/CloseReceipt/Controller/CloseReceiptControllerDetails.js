/***********************************************************************************
 * Name: CloseReceiptControllerDetails
 * Usage: Controller for receive module to render frmCloseReceiptDetails.html view 
 * Purpose: This Controller loads the selected receipt from the receipt list page.
 * User can either click on the Cancel button to go back to the results screen or 
 * can click on the Close Receipt button to close the open receipt. On click of the 
 * close receipt button if there is any other open receipts the user is navigated back 
 * to the results screen to close the other receipts that are open.
 ***********************************************************************************/


define(['common/ctrljs/app'], function (myApp) {


myApp.register.controller('CloseReceiptControllerDetails', function($scope, $rootScope, $location, $log, $timeout, $cookieStore, $state, processContoller, invokeApi, AuthenticationService, sterlingConfigService){
	
	var formName = 'frmReceivepo';
	var recevingNode = $cookieStore.get('globals').OrganizationCode;
	$rootScope.recevingNode=recevingNode;
	$rootScope.CloseReceipt_moduleFlow = 'Details';
	$rootScope.CloseReceipt_NavActive = "Details";

	$scope.ReceiptNo=$rootScope.ReceiptNoSelected;
	$scope.ReceiptDate=$rootScope.ReceiptDateSelected;
	$scope.DocumentType=$rootScope.DocumentTypeSelected;
	
	
	var Logger = $log.getInstance($state.$current);
	Logger.info("Log from CloseReceiptController Details"); 
	
	/**************************closeReceipt Xml formation*****************************************/
	function closeReceiptApi(recevingNode){
		var postObject = new Object();
		postObject.CommandName = "closeReceipt";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Receipt";
		postObject.InputXml.DocumentType = $scope.DocumentType;
		postObject.InputXml.ReceivingNode = recevingNode;
		postObject.InputXml.ReceiptNo = $scope.ReceiptNo;
		
		postObject.Template = new Object();
		
		postObject.IsService = "N";
		postObject.Authenticate=AuthenticationService.getAuth();
		
		return postObject;
		};
		
			/**************************getReceipt List Xml formation*****************************************/
			
			function getReceiptListApi(recevingNode, principal){
				var postObject = new Object();
				postObject.CommandName = "getReceiptList";
				postObject.InputXml = new Object();
				postObject.InputXml.tagName = "Receipt";
				postObject.InputXml.DocumentType = "0005";
				postObject.InputXml.ReceivingNode = recevingNode;
				postObject.InputXml.OpenReceiptFlag = "Y";
				postObject.InputXml.EnterpriseCode = principal;
		
				postObject.InputXml.childNodes= [];
				postObject.InputXml.childNodes[0]=new Object();
			    postObject.InputXml.childNodes[0].tagName= "Shipment";
						
				if($rootScope.ship.length>0){
					postObject.InputXml.childNodes[0].ShipmentNo = $rootScope.ship;
				}
				else if($rootScope.sell.length>0){
					postObject.InputXml.childNodes[0].SellerOrganizationCode = $rootScope.sell;
				}
				else if($rootScope.order.length>0){
					postObject.InputXml.childNodes[0].OrderNo = $rootScope.order;
				}
				else{
					postObject.InputXml.childNodes[0].ShipmentNo = "";
					postObject.InputXml.childNodes[0].OrderNo = "";
				}			
				postObject.Template = new Object();
				postObject.Template.tagName = "ReceiptList";
				postObject.Template.TotalNumberOfRecords="";
				postObject.Template.childNodes= [];
				postObject.Template.childNodes[0]=new Object();
				postObject.Template.childNodes[0].tagName="Receipt";
				postObject.Template.childNodes[0].ReceiptNo= "";
				postObject.Template.childNodes[0].ReceiptDate= "";
				
				
			    postObject.IsService = "N";
				postObject.Authenticate=AuthenticationService.getAuth();
				
				return postObject;
			};
			
		/* Cancel button function to go back to the results screen */	
		$scope.cancelreceipt = function(){
			
			$location.path('/CloseReceipt/Results');
			
		};
		
		/* Close Receipt function to close the receipt */
		$scope.closereceipt = function(){
			
			// invoking the closeReceipt api to close the selected receipt
			var closeReceiptObject=closeReceiptApi(recevingNode);
			invokeApi.async(closeReceiptObject).then(function(closeReceiptResponse) {
				$scope.postResponse = closeReceiptResponse;
				
				Logger.info("Invoke closeReceipt Api"); 
				// check for error from closeReceipt api
				if($scope.postResponse.ErrorCode){
					$scope.ErrorMessage=" "+$scope.postResponse.ErrorCode+" "+$scope.postResponse.ErrorDescription;
					Logger.error= ("Error in closeReceipt Api");
					}
				// success on closeReceipt
				else{
					$scope.InfoMessage = " Receipt Closed " + $scope.ReceiptNo;
					
					// invoking the getReceiptList api to check for any other open receipts									
					var receiptListObject=getReceiptListApi(recevingNode, $rootScope.principal, $rootScope.scanned);
					invokeApi.async(receiptListObject).then(function(getReceiptListResponse) {
						$scope.postgetReceiptResponse = getReceiptListResponse;
						
						Logger.info("Invoke getReceiptList Api"); 						
						var receiptListContext = $scope.postgetReceiptResponse;
						// check for open receipts
						if (angular.isArray(receiptListContext.ReceiptList.Receipt) || receiptListContext.ReceiptList.TotalNumberOfRecords == 1) {
							$rootScope.receiptListContext = receiptListContext;
							
							Logger.info("Call the results screen ");
							
							sterlingConfigService.getConfigProperty("NoOfSecondsFrMsgDisplay").then(function(response) {

					             if (angular.isDefined(response)) {
					            	 $timeout(function() {
										 $location.path('/CloseReceipt/Results');
									      },response);
					             } 
					             else {
					            	 $timeout(function() {
										 $location.path('/CloseReceipt/Results');
									      },5000);
					             }
					           });
							
							}
						// if no open receipts found, user is directed to criteria page for closing other receipts
						else {
							$rootScope.receiptListContext = receiptListContext;
							
							Logger.info("Call the find page");
							
							sterlingConfigService.getConfigProperty("NoOfSecondsFrMsgDisplay").then(function(response) {

					             if (angular.isDefined(response)) {
					            	 $timeout(function() {
										 $location.path('/CloseReceipt');
									      },response);
					             } 
					             else {
					            	 $timeout(function() {
										 $location.path('/CloseReceipt');
									      },5000);
					             }
					           });
							
						}
						
					});
					
			}
			
		});
		};
});
});