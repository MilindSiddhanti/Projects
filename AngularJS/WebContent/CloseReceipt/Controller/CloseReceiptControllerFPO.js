/*********************************************************************************
 * Name: CloseReceiptControllerFPO
 * Usage: Controller for receive module to render frmCloseReceiptFind.html view 
 * Purpose: This Controller loads the Close Receipt Criteria Page. User can search
 * for the Open Receipts by either scanning in the Receipt No, Order No, Shipment No,
 * or the Seller Name in the Reference field. User can also search for Open Receipts
 * by keying in the Principal Name 
 *********************************************************************************/
 

define(['common/ctrljs/app'], function (myApp) {
	

myApp.register.controller('CloseReceiptControllerFPO', function($scope, $rootScope, $location, $log, $cookieStore, $state, processContoller, invokeApi, AuthenticationService){
	
	var formName = 'frmCloseReceiptpo';
	var recevingNode = $cookieStore.get('globals').OrganizationCode;
	$rootScope.recevingNode=recevingNode;
	$rootScope.CloseReceipt_moduleFlow = 'Find';
	$rootScope.CloseReceipt_NavActive = "Receipts";
	
	var Logger = $log.getInstance($state.$current);
	Logger.info("Log from CloseReceiptController FPO"); 
	
	var tnrr, tnrs, tnro, principal;
	var sellerflag = false;
	$rootScope.hideship = false;
	$rootScope.hideorder = false;
	$rootScope.hideseller = false;
	$rootScope
	$scope.ErrorMessage="";
	document.getElementById("clf_reference").focus();

	/**************************multiApi Xml formation*****************************************/
	
	function fnMultiApigetReceiptList(scanned) {
		var multiApiObject = new Object();
		multiApiObject.CommandName = "multiApi";
		multiApiObject.InputXml = new Object();
		multiApiObject.InputXml.tagName = "MultiApi";
		multiApiObject.InputXml.childNodes = [];
		
		multiApiObject.InputXml.childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[0].tagName = "API";
		multiApiObject.InputXml.childNodes[0].Name = "getShipmentList";
		multiApiObject.InputXml.childNodes[0].childNodes = [];
		multiApiObject.InputXml.childNodes[0].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[0].childNodes[0].tagName = "Input";
		multiApiObject.InputXml.childNodes[0].childNodes[0].childNodes = [];
		multiApiObject.InputXml.childNodes[0].childNodes[0].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[0].childNodes[0].childNodes[0].tagName = "Shipment";
		multiApiObject.InputXml.childNodes[0].childNodes[0].childNodes[0].ShipmentNo = scanned;
		multiApiObject.InputXml.childNodes[1] = new Object();
		multiApiObject.InputXml.childNodes[1].tagName = "API";
		multiApiObject.InputXml.childNodes[1].Name = "getShipmentList";
		multiApiObject.InputXml.childNodes[1].childNodes = [];
		multiApiObject.InputXml.childNodes[1].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[1].childNodes[0].tagName = "Input";
		multiApiObject.InputXml.childNodes[1].childNodes[0].childNodes = [];
		multiApiObject.InputXml.childNodes[1].childNodes[0].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[1].childNodes[0].childNodes[0].tagName = "Shipment";
		multiApiObject.InputXml.childNodes[1].childNodes[0].childNodes[0].OrderNo = scanned;
		multiApiObject.InputXml.childNodes[2] = new Object();
		multiApiObject.InputXml.childNodes[2].tagName = "API";
		multiApiObject.InputXml.childNodes[2].Name = "getReceiptList";
		multiApiObject.InputXml.childNodes[2].childNodes = [];
		multiApiObject.InputXml.childNodes[2].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[2].childNodes[0].tagName = "Input";
		multiApiObject.InputXml.childNodes[2].childNodes[0].childNodes = [];
		multiApiObject.InputXml.childNodes[2].childNodes[0].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[2].childNodes[0].childNodes[0].tagName = "Receipt";
		multiApiObject.InputXml.childNodes[2].childNodes[0].childNodes[0].ReceiptNo = scanned;
		multiApiObject.InputXml.childNodes[3] = new Object();
		multiApiObject.InputXml.childNodes[3].tagName = "API";
		multiApiObject.InputXml.childNodes[3].Name = "getOrganizationList";
		multiApiObject.InputXml.childNodes[3].childNodes = [];
		multiApiObject.InputXml.childNodes[3].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[3].childNodes[0].tagName = "Input";
		multiApiObject.InputXml.childNodes[3].childNodes[0].childNodes = [];
		multiApiObject.InputXml.childNodes[3].childNodes[0].childNodes[0] = new Object();
		multiApiObject.InputXml.childNodes[3].childNodes[0].childNodes[0].tagName = "Organization";
		multiApiObject.InputXml.childNodes[3].childNodes[0].childNodes[0].OrganizationCode = scanned;
		
		
		multiApiObject.Template = new Object();
		multiApiObject.Template.tagName = "MultiApi";
		multiApiObject.Template.childNodes = [];
	
		multiApiObject.Template.childNodes[0] = new Object();
		multiApiObject.Template.childNodes[0].tagName = "API";
		multiApiObject.Template.childNodes[0].Name = "getShipmentList";
		multiApiObject.Template.childNodes[0].childNodes = [];
		multiApiObject.Template.childNodes[0].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[0].childNodes[0].tagName = "Output";
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes = [];
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].tagName = "Shipments";
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].TotalNumberOfRecords = "";
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes = [];
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].tagName = "Shipment";
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].ShipmentNo = "";
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].SellerOrganizationCode = "";
		multiApiObject.Template.childNodes[0].childNodes[0].childNodes[0].childNodes[0].OrderNo = "";
		multiApiObject.Template.childNodes[1] = new Object();
		multiApiObject.Template.childNodes[1].tagName = "API";
		multiApiObject.Template.childNodes[1].Name = "getReceiptList";
		multiApiObject.Template.childNodes[1].childNodes = [];
		multiApiObject.Template.childNodes[1].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[1].childNodes[0].tagName = "Output";
		multiApiObject.Template.childNodes[1].childNodes[0].childNodes = [];
		multiApiObject.Template.childNodes[1].childNodes[0].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[1].childNodes[0].childNodes[0].tagName = "ReceiptList";
		multiApiObject.Template.childNodes[1].childNodes[0].childNodes[0].TotalNumberOfRecords = "";
		multiApiObject.Template.childNodes[2] = new Object();
		multiApiObject.Template.childNodes[2].tagName = "API";
		multiApiObject.Template.childNodes[2].Name = "getOrganizationList";
		multiApiObject.Template.childNodes[2].childNodes = [];
		multiApiObject.Template.childNodes[2].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[2].childNodes[0].tagName = "Output";
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes = [];
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0].tagName = "OrganizationList";
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0].childNodes = [];
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0].childNodes[0] = new Object();
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0].childNodes[0].tagName = "Organization";
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0].childNodes[0].IsSeller = "";
		multiApiObject.Template.childNodes[2].childNodes[0].childNodes[0].childNodes[0].OrganizationCode = "";

		multiApiObject.IsService = "N";
		multiApiObject.Authenticate = AuthenticationService.getAuth();		
		
		return multiApiObject;
	};	
	
	
	/**************************getReceipt List Xml formation*****************************************/
	
	function getReceiptListApi(recevingNode, principal, scanned){
		var postObject = new Object();
		postObject.CommandName = "getReceiptList";
		postObject.InputXml = new Object();
		postObject.InputXml.tagName = "Receipt";
		postObject.InputXml.ReceivingNode = recevingNode;
		postObject.InputXml.OpenReceiptFlag = "Y";
		postObject.InputXml.EnterpriseCode = principal;
		
	    if(tnrr>0){
			postObject.InputXml.ReceiptNo = scanned;	
	    }
		else{
			postObject.InputXml.ReceiptNo = "";
		}

		postObject.InputXml.childNodes= [];
		postObject.InputXml.childNodes[0]=new Object();
	    postObject.InputXml.childNodes[0].tagName= "Shipment";
				
		if(tnrs>0){
			postObject.InputXml.childNodes[0].ShipmentNo = scanned;
		}
		else{
			postObject.InputXml.childNodes[0].ShipmentNo = "";
		}
		
		if(sellerflag){
			postObject.InputXml.childNodes[0].SellerOrganizationCode = scanned;
	    }
		else{
			postObject.InputXml.childNodes[0].SellerOrganizationCode = "";
		}
			
		if(tnro>0){
			postObject.InputXml.childNodes[0].OrderNo = scanned;
		}
		else{
			postObject.InputXml.childNodes[0].OrderNo = "";
		}
		
		postObject.Template = new Object();
		postObject.Template.tagName = "ReceiptList";
		postObject.Template.TotalNumberOfRecords="";
		postObject.Template.childNodes= [];
		postObject.Template.childNodes[0]=new Object();
		postObject.Template.childNodes[0].tagName="Receipt";
		postObject.Template.childNodes[0].ReceiptNo= "";
		postObject.Template.childNodes[0].ReceiptDate= "";
		postObject.Template.childNodes[0].DocumentType= "";
		
		
	    postObject.IsService = "N";
		postObject.Authenticate=AuthenticationService.getAuth();
		
		return postObject;
	};
	
	/* Start of ProcessController to check for open receipts based on scanned variable */
	processContoller.InputController = function(scannedVariable){
				
			if(scannedVariable == "clf_reference"){
			$rootScope.scanned= $scope.frmCloseReceipt_reference;

			if($rootScope.scanned){
                // invoking multiApi to validate the scanned variable  
				var multiApiObject=fnMultiApigetReceiptList($rootScope.scanned);
				invokeApi.async(multiApiObject).then(function(multiApiResponse) {
					$scope.postResponse = multiApiResponse;
					Logger.info("Invoke MultiApi"); 
					$rootScope.multiApiContext = $scope.postResponse; 
					var multiApiContext = $rootScope.multiApiContext; 
				
					tnrs= multiApiContext.MultiApi.API[0].Output.Shipments.TotalNumberOfRecords;
					tnro= multiApiContext.MultiApi.API[1].Output.Shipments.TotalNumberOfRecords;
					tnrr= multiApiContext.MultiApi.API[2].Output.ReceiptList.TotalNumberOfRecords;
					if(angular.isDefined(multiApiContext.MultiApi.API[3].Output.OrganizationList.Organization)){
						isseller = multiApiContext.MultiApi.API[3].Output.OrganizationList.Organization.IsSeller;
					}else{
						isseller="";
					}
					// checking for shipment based receipt
					if(tnrs>0){
						$rootScope.hideorder = true;
						$rootScope.ship = multiApiContext.MultiApi.API[0].Output.Shipments.Shipment.ShipmentNo;
						$rootScope.sell = multiApiContext.MultiApi.API[0].Output.Shipments.Shipment.SellerOrganizationCode;
					}
					// checking for receipt 
					if(tnrr>0){
						$rootScope.hideship = true;
						$rootScope.hideorder = true;
						$rootScope.hideseller = true;
					}
					// checking for order based receipt					
					if(tnro>0){
						$rootScope.order = multiApiContext.MultiApi.API[1].Output.Shipments.Shipment.OrderNo; 
						$rootScope.ship = multiApiContext.MultiApi.API[1].Output.Shipments.Shipment.ShipmentNo;
						$rootScope.sell = multiApiContext.MultiApi.API[1].Output.Shipments.Shipment.SellerOrganizationCode;
					}
					// checking for receipts under the seller 
					if(isseller == "Y"){
						sellerflag = true;
						$rootScope.sell = multiApiContext.MultiApi.API[3].Output.OrganizationList.Organization.OrganizationCode;
						$rootScope.hideorder = true;
						$rootScope.hideship = true;
					}
					else{
						sellerflag = false;
					}
					
					// check for invalid scan
					if(tnrs == 0 && tnro == 0 && tnrr== 0 && isseller == "" ){
						$scope.ErrorMessage = "Invalid Scan";
						Logger.error= ("Invalid Scan");
					}
	
				});
			}

		}
			//angular.element(document.getElementById('clf_findreceipt'))[0].disabled = false;
		
	};// end of processController
	
	/* On Click of Find Receipt button, a list of open receipts show up */
	$scope.findreceipt = function(){
		
		// check for receipts under principal
		if(angular.isDefined($scope.frmCloseReceipt_principal) && $scope.frmCloseReceipt_principal.length>0){
			
			$rootScope.principal = $scope.frmCloseReceipt_principal;
			$rootScope.hideseller = true;
			$rootScope.hideorder = true;	
			$rootScope.hideship = true;
			// invoking getReceiptList api to get the list of open receipts under the principal
			var receiptListObject=getReceiptListApi(recevingNode, $rootScope.principal, $rootScope.scanned);
			invokeApi.async(receiptListObject).then(function(getReceiptListResponse) {
				$scope.postResponse = getReceiptListResponse;
				
				Logger.info("Invoke getReceiptList Api"); 
				var receiptListContext = $scope.postResponse;
				// if no open receipts found 
				if(receiptListContext.ReceiptList.TotalNumberOfRecords=="0"){
					$scope.ErrorMessage = "No Records Found";
					Logger.error= ("No Records Found");
				}
				// for open receipts
				else{
					Logger.info("Call the close receipt results screen ");
					$rootScope.receiptListContext = receiptListContext;
					$location.path('/CloseReceipt/Results');
				}				
				
			});
			
		}
		// for invalid entry of the fields
		else if(angular.isUndefined($scope.frmCloseReceipt_reference) && angular.isUndefined($scope.frmCloseReceipt_principal)){
			$scope.ErrorMessage= "Enter Principal for Search";
			Logger.error= ("Enter Principal for Search");
		}
		else{
			// invoking getReceiptList api to get the list of open receipts
			var receiptListObject=getReceiptListApi(recevingNode, $rootScope.principal, $rootScope.scanned);
			invokeApi.async(receiptListObject).then(function(getReceiptListResponse) {
				$scope.postResponse = getReceiptListResponse;
				
				Logger.info("Invoke getReceiptList Api"); 
				var receiptListContext = $scope.postResponse;
				// if no open receipts found 
				if(receiptListContext.ReceiptList.TotalNumberOfRecords=="0"){
					$scope.ErrorMessage = "No Records Found";
				}
				// for open receipts
				else{
					Logger.info("Call the close receipt results screen ");
					$rootScope.receiptListContext = receiptListContext;
					$location.path('/CloseReceipt/Results');
				}				
				
			});
		}
		
	};
	$rootScope.ship = "";
	$rootScope.sell = "";
	$rootScope.order = "";
});
});