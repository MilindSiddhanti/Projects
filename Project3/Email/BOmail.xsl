<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
				
<xsl:template match="/">
<HTML>
<BODY topmargin="0" leftmargin="0">
	<BR/><BR/><font> Order Number: <xsl:value-of select="/Order/@OrderNo"/><BR/><BR/>
	</font>
	
<p/>
		<table class="table" >
		<thead>
		<tr>
			<td class="tablecolumnheader">
				LineNumber
			</td>
			<td class="tablecolumnheader">
				Item#
			</td>			
			<td class="tablecolumnheader">
				TagNumber
			</td>
			<td class="tablecolumnheader">
				SerialNo
			</td>
			<td class="tablecolumnheader" style="text-align:right;">
				Quantity
			</td>
			<td  WIDTH="30%">
			</td>
		</tr>
	</thead>
	  <xsl:for-each select="/Order/OrderLines/OrderLine">
        <tr>
          
		   <td>
              <xsl:value-of select="@PrimeLineNo"/>
            </td>

            <td>
              <xsl:value-of select="Item/@ItemID"/>
            </td>
						
			 <td>
              <xsl:value-of select="@LotNumber"/>
            </td>
			
			 <td>
              <xsl:value-of select="@SerialNo"/>
            </td>
			
			 <td>
              <xsl:value-of select="@OrderedQty"/>
            </td>
          
        </tr>
		</xsl:for-each>
	</table>
	
	<BR/><font> Warehouse: <xsl:value-of select="/Order/@ReceivingNode"/><BR/><BR/>
	</font>
	
</BODY>
</HTML>
</xsl:template>		
</xsl:stylesheet>
