<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:lxslt="http://xml.apache.org/xslt"
                version="1.0">
<xsl:template match="/">
<BODY topmargin="0" leftmargin="0">
	<BR/><BR/><font> Order Number: <xsl:value-of select="/Shipments/Shipment/@OrderNo"/><BR/><BR/>	
	</font>

	<font> CustomerID: <xsl:value-of select="/Shipments/Shipment/ShipmentLines/ShipmentLine/Order/@ShipToID"/><BR/><BR/>
	</font>
	
	<xsl:variable name="DateTime" select="/Shipments/Shipment/ShipmentLines/ShipmentLine/Order/@OrderDate"/>
	
	<xsl:variable name="DD" select="substring($DateTime,9,2)"/>
	<xsl:variable name="MM" select="substring($DateTime,6,2)"/>
	<xsl:variable name="YYYY" select="substring($DateTime,1,4)"/>
	<xsl:variable name="Time" select="substring($DateTime,12,5)"/>	
	
	<font> Order Created Date/Time: <xsl:value-of select="$YYYY"/><xsl:value-of select="$MM"/><xsl:value-of select="$DD"/><xsl:text> </xsl:text><xsl:value-of select="$Time"/>hrs<BR/><BR/>
	</font>
	
	<xsl:variable name="StatusName" select="/Shipments/Shipment/Status/@StatusName"/>
	
	<xsl:for-each select="/Shipments/Shipment/ShipmentStatusAudits/ShipmentStatusAudit">
	
	<xsl:if test="/Shipments/Shipment/ShipmentStatusAudits/ShipmentStatusAudit/NewStatus/@Description = 'StatusName'"> 
	<xsl:variable name="StatusDateTime" select="/Shipments/Shipment/ShipmentStatusAudits/ShipmentStatusAudit/@NewStatusDate"/>
	<xsl:variable name="StatusDD" select="substring($StatusDateTime,9,2)"/>
	<xsl:variable name="StatusMM" select="substring($StatusDateTime,6,2)"/>
	<xsl:variable name="StatusYYYY" select="substring($StatusDateTime,1,4)"/>
	<xsl:variable name="StatusTime" select="substring($StatusDateTime,12,5)"/>
	</xsl:if>
	
	</xsl:for-each>
	
	
	<font> Status: <xsl:value-of select="/Shipments/Shipment/Status/@StatusName"/> at <xsl:value-of select="$StatusYYYY"/><xsl:value-of select="$StatusMM"/><xsl:value-of select="$StatusDD"/><xsl:text> </xsl:text><xsl:value-of select="$StatusTime"/>hrs<BR/><BR/>
	</font>
	
	<p/>		
		<table class="table" >
		<thead>
		<tr>
			<td class="tablecolumnheader">
				Item#
			</td>
			<td class="tablecolumnheader">
				SPR#
			</td>
			<td class="tablecolumnheader">
				Serial#
			</td>

			<td class="tablecolumnheader" style="text-align:right;">
				Quantity
			</td>
			<td  WIDTH="30%">
			</td>
		</tr>
	</thead>
	  <xsl:for-each select="/Shipments/Shipment/ShipmentLines/ShipmentLine">
        <tr>
          
            <td>
              <xsl:value-of select="@ItemID"/>
            </td>
			
			 <td>
              <xsl:value-of select="@SerialNo"/>
            </td>
			
			 <td>
              <xsl:value-of select="@LotNumber"/>
            </td>
			
			 <td>
              <xsl:value-of select="@Quantity"/>
            </td>
          
        </tr>
		</xsl:for-each>
	</table>
	
	<font> Carrier: <xsl:value-of select="/Shipments/Shipment/@ScacAndService"/><BR/><BR/>
	</font>

	<font> Payment: Non Commercial Shipment<BR/><BR/>
	</font>
	
	<font> Freight Terms: <xsl:value-of select="/Shipments/Shipment/@FreightTerms"/><BR/><BR/>
	</font>
	<!-->
	<BR/><font> Comments: <xsl:value-of select="/Shipments/Shipment/@Status"/><BR/>
	<BR/>
	</font>
	
	<BR/><font> Special Instructions: <xsl:value-of select="/Shipments/Shipment/@Status"/><BR/>
	<BR/>
	</font> <-->
	
	<font> Warehouse: <xsl:value-of select="/Shipments/Shipment/@ShipNode"/><BR/><BR/>
	</font>
	
</BODY>
</xsl:template>		
</xsl:stylesheet>