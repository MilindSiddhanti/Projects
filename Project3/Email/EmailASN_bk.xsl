<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:lxslt="http://xml.apache.org/xslt"
                version="1.0">
				
<xsl:template match="/">
<BODY topmargin="0" leftmargin="0">
	<BR/><BR/><font> ASN Number: <xsl:value-of select="/ReceiptList/Receipt/Shipment/@ShipmentNo"/><BR/><BR/>
	</font>
	
	<xsl:for-each select="/ReceiptList/Receipt/ReceiptStatusAudits/ReceiptStatusAudit">
	<xsl:if test="/NewStatus/@Description = 'Receipt In Progress'"> 
	<xsl:variable name="DateTime" select="@NewStatusDate"/>	
	<xsl:variable name="DD" select="substring($DateTime,9,2)"/>
	<xsl:variable name="MM" select="substring($DateTime,6,2)"/>
	<xsl:variable name="YYYY" select="substring($DateTime,1,4)"/>
	<xsl:variable name="Time" select="substring($DateTime,12,5)"/>	
	</xsl:if>
	<font> Status: Putaway at: <xsl:value-of select="$YYYY"/><xsl:value-of select="$MM"/><xsl:value-of select="$DD"/><xsl:text> </xsl:text><xsl:value-of select="$Time"/>hrs<BR/><BR/>
	</font>
	</xsl:for-each>	
	
<p/>
		<table class="table" >
		<thead>
		<tr>
			<td class="tablecolumnheader">
				Item#
			</td>
			<td class="tablecolumnheader">
				SPR#
			</td>
			<td class="tablecolumnheader">
				Serial#
			</td>

			<td class="tablecolumnheader" style="text-align:right;">
				Quantity
			</td>
			<td  WIDTH="30%">
			</td>
		</tr>
	</thead>
	  <xsl:for-each select="/ReceiptList/Receipt/ReceiptLines/ReceiptLine">
        <tr>
          
            <td>
              <xsl:value-of select="@ItemID"/>
            </td>
			
			 <td>
              <xsl:value-of select="@SerialNo"/>
            </td>
			
			 <td>
              <xsl:value-of select="@LotNumber"/>
            </td>
			
			 <td>
              <xsl:value-of select="@Quantity"/>
            </td>
          
        </tr>
		</xsl:for-each>
	</table>
	
	<font> Warehouse: <xsl:value-of select="/ReceiptList/Receipt/@ReceivingNode"/><BR/><BR/>
	</font>
	
</BODY>
</xsl:template>		
</xsl:stylesheet>
