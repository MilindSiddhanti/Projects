<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>

 <xsl:template name="Zone">
     <xsl:sequence select=
     "translate(
        string(
           adjust-dateTime-to-timezone(
              xs:dateTime('2011-11-29T04:15:22-08:00'),
              xs:dayTimeDuration('PT0H')
                                  )
             ),
         'TZ',
         ' '
                )
     "/>
 </xsl:template>
 
 <xsl:template name="TimeZone">
 <xsl:call-template name="Zone">
		<xsl:with-param name="FirstName" select="@FirstName"/>
		<xsl:with-param name="LastName" select="@LastName"/>
		<xsl:with-param name="AddressLine1" select="@AddressLine1"/>
		<xsl:with-param name="AddressLine2" select="@AddressLine2"/>
		<xsl:with-param name="AddressLine3" select="@AddressLine3"/>
		<xsl:with-param name="City" select="@City"/>
		<xsl:with-param name="State" select="@State"/>
		<xsl:with-param name="ZipCode" select="@ZipCode"/>
		<xsl:with-param name="Country" select="@Country"/>
	</xsl:call-template>
</xsl:template>
	
</xsl:stylesheet>