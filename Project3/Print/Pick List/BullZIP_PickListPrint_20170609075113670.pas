*JOBNAME,BullZIP_PickListPrint_20170609075113670
*QUANTITY,1
*PAGES,1
*PRINTERNAME,Foxit
*DUPLICATES,0
PageNo,1
TotalPages,2
*FORMAT,PickList.lwl
ShipmentNo,100001567
ConsolidatedOrder,MO09061
OrderNo,MO09061
Node,APIT01
EnterpriseKey,APACIT01
TaskType,UI_PACK
SNo_1,1
SNo_2,2
SNo_3,2
SNo_4,2
SNo_5,2
SNo_6,2
SNo_7,2
SNo_8,2
SNo_9,2
SNo_10,2
WaveNo,1000784
ScacKey,FEDX
ScacDesc,FEDEX GROUND
RMK1,Remarks for the order
RMK2,
RMK3,
ShipInst,Shipping Instructions
Name,Team Arya
Company,
AddressLine1,First Floor, AP building
AddressLine2,
AddressLine3, 
City,Bangalore
State,KA
ZipCode,560001
Country,IN
Department,
DayPhone,
SourceLocationId_1,B1-010101
SourceLocationId_2,B1-010101
SourceLocationId_3,B1-010101
SourceLocationId_4,B1-010101
SourceLocationId_5,B1-010101
SourceLocationId_6,B1-010101
SourceLocationId_7,B1-010101
SourceLocationId_8,B1-010101
SourceLocationId_9,B1-010101
SourceLocationId_10,B1-010101
ShipByDate_1,2500-01-01
ShipByDate_2,2500-01-01
ShipByDate_3,2500-01-01
ShipByDate_4,2500-01-01
ShipByDate_5,2500-01-01
ShipByDate_6,2500-01-01
ShipByDate_7,2500-01-01
ShipByDate_8,2500-01-01
ShipByDate_9,2500-01-01
ShipByDate_10,2500-01-01
ProductClass_1,4000
ProductClass_2,4000
ProductClass_3,4000
ProductClass_4,4000
ProductClass_5,4000
ProductClass_6,4000
ProductClass_7,4000
ProductClass_8,4000
ProductClass_9,4000
ProductClass_10,4000
Quantity_1,1.00
Quantity_2,1.00
Quantity_3,1.00
Quantity_4,1.00
Quantity_5,1.00
Quantity_6,1.00
Quantity_7,1.00
Quantity_8,1.00
Quantity_9,1.00
Quantity_10,1.00
ItemID_1,NOR-00002
ItemID_2,NOR-00002
ItemID_3,NOR-00002
ItemID_4,NOR-00002
ItemID_5,NOR-00002
ItemID_6,NOR-00002
ItemID_7,NOR00001
ItemID_8,NOR00001
ItemID_9,NOR00001
ItemID_10,NOR00001
GTIN_1,NOR-00002GTIN2
GTIN_2,NOR-00002GTIN2
GTIN_3,NOR-00002GTIN2
GTIN_4,NOR-00002GTIN2
GTIN_5,NOR-00002GTIN2
GTIN_6,NOR-00002GTIN2
GTIN_7,NOR00001GTINIT
GTIN_8,NOR00001GTINIT
GTIN_9,NOR00001GTINIT
GTIN_10,NOR00001GTINIT
SerialBatch_1,
SerialBatch_2,
SerialBatch_3,
SerialBatch_4,
SerialBatch_5,
SerialBatch_6,
SerialBatch_7,
SerialBatch_8,
SerialBatch_9,
SerialBatch_10,
UnitOfMeasure_1,EACH
UnitOfMeasure_2,EACH
UnitOfMeasure_3,EACH
UnitOfMeasure_4,EACH
UnitOfMeasure_5,EACH
UnitOfMeasure_6,EACH
UnitOfMeasure_7,EACH
UnitOfMeasure_8,EACH
UnitOfMeasure_9,EACH
UnitOfMeasure_10,EACH
ShortDescription_1,Normal Item 2
ShortDescription_2,Normal Item 2
ShortDescription_3,Normal Item 2
ShortDescription_4,Normal Item 2
ShortDescription_5,Normal Item 2
ShortDescription_6,Normal Item 2
ShortDescription_7,Normal Item
ShortDescription_8,Normal Item
ShortDescription_9,Normal Item
ShortDescription_10,Normal Item
LRMK_1,L remarks for the item NOR-00002 Test
LRMK_2,L remarks for the item NOR-00002 Test
LRMK_3,L remarks for the item NOR-00002 Test
LRMK_4,L remarks for the item NOR-00002 Test
LRMK_5,L remarks for the item NOR-00002 Test
LRMK_6,L remarks for the item NOR-00002 Test
LSHIP_1,L ship instruction type Test1
LSHIP_2,L ship instruction type Test1
LSHIP_3,L ship instruction type Test1
LSHIP_4,L ship instruction type Test1
LSHIP_5,L ship instruction type Test1
LSHIP_6,L ship instruction type Test1
TotalPageCount,12
TotalPageQty,12.00
PageCount_1,10
PageQty_1,10.00
ShipDate,
CreateDate,09 Jun 2017 07:43:31
RequestedShipmentDate,20170609
RequestedShipmentTime,
PrintDate,09 Jun 2017 07:51:14
*PRINTLABEL
*DUPLICATES,0
PageNo,2
TotalPages,2
*FORMAT,PickList_Last.lwl
ShipmentNo,100001567
ConsolidatedOrder,MO09061
OrderNo,MO09061
Node,APIT01
EnterpriseKey,APACIT01
TaskType,UI_PACK
SNo_1,2
SNo_2,2
WaveNo,1000784
ScacKey,FEDX
ScacDesc,FEDEX GROUND
RMK1,Remarks for the order
RMK2,
RMK3,
ShipInst,Shipping Instructions
Name,Team Arya
Company,
AddressLine1,First Floor, AP building
AddressLine2,
AddressLine3, 
City,Bangalore
State,KA
ZipCode,560001
Country,IN
Department,
DayPhone,
SourceLocationId_1,B1-010101
SourceLocationId_2,B1-010101
ShipByDate_1,2500-01-01
ShipByDate_2,2500-01-01
ProductClass_1,4000
ProductClass_2,4000
Quantity_1,1.00
Quantity_2,1.00
ItemID_1,NOR00001
ItemID_2,NOR00001
GTIN_1,NOR00001GTINIT
GTIN_2,NOR00001GTINIT
SerialBatch_1,
SerialBatch_2,
UnitOfMeasure_1,EACH
UnitOfMeasure_2,EACH
ShortDescription_1,Normal Item
ShortDescription_2,Normal Item
TotalPageCount,12
TotalPageQty,12.00
PageCount_1,2
PageQty_1,2.00
ShipDate,
CreateDate,09 Jun 2017 07:43:31
RequestedShipmentDate,20170609
RequestedShipmentTime,
PrintDate,09 Jun 2017 07:51:14
*PRINTLABEL
